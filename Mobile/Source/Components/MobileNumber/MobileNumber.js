import React, { Component } from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';

import PhoneInput from 'react-native-phone-input';
import { SCREEN_WIDTH } from '../../Helper/Constants';
import ModalPickerImage from './ModalPickerImage';
import { moderateScale} from '../../Helper/Scaling';

export default class MobileNumber extends Component {
    constructor() {
        super();

        this.onPressFlag = this.onPressFlag.bind(this);
        this.selectCountry = this.selectCountry.bind(this);
        this.state = {
            pickerData: null,
        };
    }

    componentDidMount() {
        this.setState({
            pickerData: this.refs.phoneInput.getPickerData(),
        });
    }

    onPressFlag() {
        this.myCountryPicker.open();
    }

    selectCountry(country) {
        this.refs.phoneInput.selectCountry(country.iso2);
    }

    render() {

        const {customStyle} = this.props

        return (
            <View style={[styles.container, customStyle.containerStyle]}>
                <PhoneInput
                    ref="phoneInput"
                    initialCountry="sg"
                    disableFullscreenUI={true}
                    style={phoneInputStyleObject}
                    textStyle={styles.textInput}
                    textProps={{ disableFullscreenUI: true, placeholder: 'Mobile Number', placeholderTextColor: 'gray' }}
                    onPressFlag={this.onPressFlag}
                />
                <View style={styles.separatorView} />
                <ModalPickerImage
                    ref={(ref) => {
                        this.myCountryPicker = ref;
                    }}
                    data={this.state.pickerData}
                    onChange={(country) => {
                        this.selectCountry(country);
                    }}
                    cancelText="Cancel"
                />
            </View>
        );
    }

    isValidPhoneNumber() {
        return this.refs.phoneInput.isValidNumber()
    }

    phoneNumber() {
        return this.refs.phoneInput.getValue().trim()
    }

    countryCode() {
        return this.refs.phoneInput.getCountryCode()        
    }

    clearPhoneNumber() {
        this.refs.phoneInput.value = ""
    }
}

MobileNumber.defaultProps = {
    customStyle: {
        containerStyle: {

        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 50,        
        marginHorizontal: SCREEN_WIDTH * 0.09,
    },
    phoneInput: {
        height: 40
    },
    separatorView: {
        height: 1,
        margin: 0,
        bottom:0,
        backgroundColor: '#C8C8C8'
    },
    textInput: { 
        color: 'white', 
        fontSize: moderateScale(14, 0.09),
        fontFamily:'OpenSans-Regular'
    }
});

const phoneInputStyleObject = StyleSheet.flatten(styles.phoneInput);
