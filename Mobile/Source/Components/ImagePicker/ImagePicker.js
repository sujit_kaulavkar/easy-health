
var ImagePicker = require('react-native-image-picker');

export default class CustomImagePicker {
    showImagePicker() {        
        return new Promise((resolve, reject) => {

            const options = {
                allowsEditing: true,
                mediaType: "photo",
                maxWidth:300,
                maxWiddth:300
            }

            ImagePicker.showImagePicker(options, (response) => {
                if (response.didCancel) {                    
                    reject("User cancelled image picker")
                }
                else if (response.error) {
                    reject(response.error)                    
                }
                else {
                    resolve(response)
                }
            });
        })
    }
}