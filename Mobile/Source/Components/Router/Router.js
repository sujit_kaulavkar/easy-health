import React, { Component } from 'react'

import {
    SCREEN_WIDTH,
    SCREEN_HEIGHT
} from '../../Helper/Constants.js'

import {
    KYC,
    DASHBOARD,
    DRAWER,
    LOGIN,
    TOP_UP,
    ACTIVATE_CARD,
    CLAIM_CANCEL,
    SEND,
    REQUEST,
    PAY,
    SETTINGS,
    HOME,
    Helper
} from '../../Helper/FilePath';

import { createDrawerNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation';

const LandingStackNavigation = createStackNavigator(
    {
        DashboardScreen: {
            screen: DASHBOARD.LANDING_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        PaymmentSuccessScreen: {
            screen: TOP_UP.PAYMENT_SUCCESS,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        TransactionHistoryScreen: {
            screen: DASHBOARD.TRANSACTION_HISTORY_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        TransactionDetailsScreen: {
            screen: DASHBOARD.TRANSACTION_DETAILS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        LoadCardScreen: {
            screen: DASHBOARD.LOAD_CARDS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        LoadCardSuccessScreen: {
            screen: DASHBOARD.LOAD_CARD_SUCCESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ATMPinScreen: {
            screen: DASHBOARD.ATM_PIN_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        NotificationScreen: {
            screen: DASHBOARD.NOTIFICATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        NotificationDetailScreen: {
            screen: DASHBOARD.NOTIFICATION_DETAILS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }, 
        EditEmailAddressScreen: {
            screen: SETTINGS.EDIT_EMAIL_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        EditPhoneNumberScreen: {
            screen: SETTINGS.EDIT_PHONE_NUMBER_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        OTPVerificationScreen: {
            screen: LOGIN.OTP_VERIFICATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        LoyaltyPointsScreen: {
            screen: DASHBOARD.LOYALTY_POINTS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }, 
        KYCPersonalDetailScreen: {
            screen: KYC.PERSONAL_DETAIL_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        KYCResidentilAddressScreen: {
            screen: KYC.RESIDENTIAL_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        KYCUploadIDProofScreen: {
            screen: KYC.UPLOAD_ID_PROOF_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        DocumentSelectionScreen: {
            screen: KYC.DOCUMENT_SELECTION,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'DashboardScreen'
    }
)

const TopUpStackNavigation = createStackNavigator (
    {
        TopUpScreen: {
            screen: TOP_UP.TOPUP,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ChoosePaymentScreen: {
            screen: TOP_UP.CHOOSE_PAYMENT,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        PaymmentSuccessScreen: {
            screen: TOP_UP.PAYMENT_SUCCESS,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        PaymentScreen: {
            screen: TOP_UP.PAYMENT_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        CardInputScreen: {
            screen: TOP_UP.CARD_INPUT_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        SingPostTopupScreen: {
            screen: TOP_UP.SING_POST_TOPUP_STEPS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        KYCPersonalDetailScreen: {
            screen: KYC.PERSONAL_DETAIL_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        KYCResidentilAddressScreen: {
            screen: KYC.RESIDENTIAL_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        KYCUploadIDProofScreen: {
            screen: KYC.UPLOAD_ID_PROOF_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        DocumentSelectionScreen: {
            screen: KYC.DOCUMENT_SELECTION,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'TopUpScreen'
    }
)

const ActivateCardStackNavigation = createStackNavigator(
    {
        ActivateCardScreen: {
            screen: ACTIVATE_CARD.ACTIVATE_CARD,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ActivateCardSuccessScreen: {
            screen: ACTIVATE_CARD.ACTIVATE_CARD_SUCCESS,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        OTPVerificationScreen: {
            screen: LOGIN.OTP_VERIFICATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        InputCardNumberScreen: {
            screen: ACTIVATE_CARD.INPUT_CARD_NUMBER_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        VenderScreen: {
            screen: ACTIVATE_CARD.VENDER_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'VenderScreen'
    }
)

const AppoimentStackNavigation = createStackNavigator(
    {
        AppoinmentTypeScreen: {
            screen: LOGIN.APPOINMENT_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        CheckListScreen: {
            screen: LOGIN.CEHCK_LIST,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        HospitalList: {
            screen: LOGIN.HOSPITAL_LIST,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ConfirmAppoinment: {
            screen: LOGIN.CONFIRM_APPOINMENT,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        MYPaymentScreen: {
            screen: LOGIN.MY_PAYMENT_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        SummaryAppoinment: {
            screen: LOGIN.SUMMARY,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        AppoinmentDateScreen: {
            screen: LOGIN.APPOINMENT_DATE,
            navigationOptions: {
                gesturesEnabled: false,
            },
        }          
    },
    {
        headerMode: 'none',
        initialRouteName: 'AppoinmentTypeScreen'
    }
)

const SendStackNavigation = createStackNavigator(
    {
        SendEntryScreen: {
            screen: SEND.SEND_ENTRY,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        SendConfirmScreen: {
            screen: SEND.SEND_CONFIRM,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        SendSuccessScreen: {
            screen: SEND.SEND_SUCCESS,
            navigationOptions: {
                gesturesEnabled: false,
            },
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'SendEntryScreen'
    }
)

const RequestStackNavigation = createStackNavigator(
    {
        RequestScreen: {
            screen: REQUEST.REQUEST,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        RequestQRScreen: {
            screen: REQUEST.REQUEST_QR,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }, 
        PaySuccessScreen: {
            screen: PAY.PAY_SUCCESS, 
            navigationOptions: {
                gesturesEnabled: false,
            }
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'RequestScreen'
    }
)

const PayStackNavigation = createStackNavigator(
    {
        PayScreen: {
            screen: PAY.PAY,
            navigationOptions: {
                gesturesEnabled: false,
            },
            PaySuccessScreen: {
                screen: PAY.PAY_SUCCESS, 
                navigationOptions: {
                    gesturesEnabled: false,
                }
            }
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'PayScreen'
    }
)

const SettingsStackNavigation = createStackNavigator(
    {
        SettingsScreen: {
            screen: SETTINGS.SETTINGS,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ManageProfileScreen: {
            screen: SETTINGS.MANAGE_PROFILE_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        MyAddressScreen: {
            screen: SETTINGS.MY_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        AddAddressScreen: {
            screen: SETTINGS.ADD_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        EditEmailAddressScreen: {
            screen: SETTINGS.EDIT_EMAIL_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        EditPhoneNumberScreen: {
            screen: SETTINGS.EDIT_PHONE_NUMBER_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ChangePasswordScreen: {
            screen: SETTINGS.CHANGE_PASSWORD_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        HelpFeedbackScreen: {
            screen: SETTINGS.HELP_FEEDBACK_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        NotificationScreen: {
            screen: DASHBOARD.NOTIFICATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        NotificationDetailScreen: {
            screen: DASHBOARD.NOTIFICATION_DETAILS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        OTPVerificationScreen: {
            screen: LOGIN.OTP_VERIFICATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        EditProfileScreen: {
            screen: SETTINGS.EDIT_PROFILE_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        FAQScreen: {
            screen: SETTINGS.FAQ_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        FAQDetailScreen: {
            screen: SETTINGS.FAQ_DETAIL_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'SettingsScreen'
    }
)

class customStacknavigation extends Component {
    render() {
        const { navigation } = this.props;
        return <LandingStackNavigation
            screenProps={{
                params: navigation.state.params,
                rootNavigation: navigation
            }} />
    }
}

const HomeStackNavigation = createStackNavigator(
    {
        HomeScreen: {
            screen: HOME.HOME_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        VouchersMainScreen: {
            screen: HOME.VOUCHERS_MAIN_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        MyVouchersScreen: {
            screen: HOME.MY_VOUCHER_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        VoucherDetailScreen: {
            screen: HOME.VOUCHER_DETAIL_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        MyAddressScreen: {
            screen: SETTINGS.MY_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        AddAddressScreen: {
            screen: SETTINGS.ADD_ADDRESS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        ActivateCardSuccessScreen: {
            screen: ACTIVATE_CARD.ACTIVATE_CARD_SUCCESS,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        OTPVerificationScreen: {
            screen: LOGIN.OTP_VERIFICATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        InputCardNumberScreen: {
            screen: ACTIVATE_CARD.INPUT_CARD_NUMBER_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        MyRewardsScreen: {
            screen: HOME.MY_REWARDS_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        PointsToCashScreen: {
            screen: HOME.POINTS_TO_CASH_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'HomeScreen'
    }
)

const appoinmentStack = createStackNavigator(
    {
        MyAppoints: {
            screen: LOGIN.MY_APPOINMENTS,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }
    },
     {
        headerMode: 'none',
         initialRouteName: 'MyAppoints'
    }
)

const switchNavigator = createSwitchNavigator(
    {
        LandingStackNavigation: {
            screen: LandingStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        appoinmentStack: {
            screen: appoinmentStack,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        HomeStackNavigation: {
            screen: HomeStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        TopUpStackNavigation: {
            screen: TopUpStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ActivateCardStackNavigation: {
            screen: ActivateCardStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        AppoimentStackNavigation: {
            screen: AppoimentStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        SendStackNavigation: {
            screen: SendStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        RequestStackNavigation: {
            screen: RequestStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        PayStackNavigation: {
            screen: PayStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        SettingsStackNavigation: {
            screen: SettingsStackNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
    },
    {
        initialRouteName: 'appoinmentStack'
    }
)

const DrawerNavigation = createDrawerNavigator(
    {
        switchNavigator: {
            screen: switchNavigator,
            navigationOptions: {
                gesturesEnabled: false,
            },
        }
    },
    {
        drawerWidth: (Helper.DEVICE_DETECTOR.getInstance().isTablet == true ? (SCREEN_WIDTH < SCREEN_HEIGHT ? SCREEN_WIDTH * 0.4 : SCREEN_HEIGHT * 0.3) : (SCREEN_WIDTH < SCREEN_HEIGHT ? SCREEN_WIDTH * 0.8 : SCREEN_HEIGHT * 0.8)),
        initialRouteName: 'switchNavigator',
        drawerBackgroundColor: 'transparent',
        contentComponent: DRAWER.DRAWER
    }
)

const RootStacknavigation = createStackNavigator(
    {
        SplashScreen: {
            screen: LOGIN.SPLASH_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        LoginScreen: {
            screen: LOGIN.LOGIN_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        RegistrationScreen: {
            screen: LOGIN.REGISTRATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        AppoinmentTypeScreen: {
            screen: LOGIN.APPOINMENT_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ForgotPasswordScreen: {
            screen: LOGIN.FORGOT_PASSWORD_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        OTPVerificationScreen: {
            screen: LOGIN.OTP_VERIFICATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        ResetPasswordScreen: {
            screen: LOGIN.RESET_PASSWORD_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        DrawerNavigation: {
            screen: DrawerNavigation,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        TNCScreen: {
            screen: LOGIN.TNC_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        KYCScreen: {
            screen: LOGIN.KYC_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        },
        TutorialScreen: {
            screen: LOGIN.TUTORIAL_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            }
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'SplashScreen'
    }
)

export default RootStacknavigation
