import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,  
  TextInput,
  TouchableOpacity
} from 'react-native';

import {
  IS_IOS,
  TOP_BAR_HEIGHT,
  isIphoneX,
  SCREEN_WIDTH,
  BLUE_COLOR,
} from '../../Helper/Constants.js';

import { moderateScale } from '../../Helper/Scaling';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default class TopBar extends Component {

  backButton = null;
  constructor(props) {
    super(props);

    this.state = {
      textInputStatus: 'untouched'
    }
  }

  render() {
    backButton = this.showLeftButton()
    menuButton = this.showMenuButton()
    contentView = this.showContentView()
    separatorView = this.showSeparatorView()
    crossView = this.showRightCrossView()
    bellView = this.showBellView()
    skipView = this.showSkipButton()
    return (
      <View style={[styles.maincontainer, {
        height: TOP_BAR_HEIGHT,
        paddingTop: isIphoneX() == true ? 20 : 0,
      }]}>
        {backButton}
        {menuButton}
        {contentView}
        {crossView}
        {bellView}
        {skipView}
      </View>
    );
  }

  showLeftButton() {
    
    if (this.props.isBackButtonNeeded == true) {

      return (
        <View style={[styles.leftViewStyle, { top: IS_IOS ? (isIphoneX() == true ? 30 : 15) : 7 }]}>
          <Ionicons.Button
            name="ios-arrow-round-back"
            color={BLUE_COLOR}
            size={45}
            style={{ paddingTop: IS_IOS ? -5 : 0, left: 0 }}
            backgroundColor={"transparent"}
            onPress={this.props.backButtonAction}
          />
        </View>
      )
    }
  }

  showMenuButton() {
    if (this.props.isMenuButtonNeeded == true) {

      return (
        <View style={[styles.leftViewStyle, {top: IS_IOS ? (isIphoneX() == true ? 30 : 12) : 0}]}>
          <Ionicons.Button
            name="md-menu"
            color="#4698CB"
            size={45}            
            opacity={1}
            backgroundColor={"transparent"}
            onPress={this.props.menuButtonAction}
          />

        </View>
      )
    }
  }

  showContentView() {

    if (this.props.isShowSearch) {
      return (
        <View style={styles.textInputContainterStyle}>
          <TextInput
            style={styles.textInputStyle}
            placeholderStyle={[styles.placeholderStyle]}
            ref="searchTextInput"
            returnKeyType='search'
            placeholder='Search'
            placeholderTextColor='gray'
            autoCorrect={false}
            disableFullscreenUI={true}
            clearButtonMode='never'
            autoCorrect={false}
            underlineColorAndroid='transparent'
            onKeyPress={(event) => this.onKeyPress(event)}
            onChangeText={(text) => this.onTextChange(text)}
            onSubmitEditing={(event) => this.onEndEditing(event)}
          />
          <View style={styles.underlineStyle} />
        </View>
      )
    }
    else {
      return (
        <Text style={styles.textStyle}>{this.props.title}</Text>
      )
    }
  }

  showRightCrossView() {
    if (this.props.isRightCrossNeeded == true) {

      return (
        <View style={[styles.rightViewStyle, {top: IS_IOS ? (isIphoneX() == true ? 30 : 15) : 5 }]}>
          <MaterialIcons.Button
            name="close"
            color="white"
            size={30}
            opacity={1}
            backgroundColor={"transparent"}
            onPress={this.props.crossButtonAction}
          />

        </View>
      )
    }
  }

  showBellView() {
    if (this.props.isBellNeeded == true) {

      return (
        <View style={[styles.rightViewStyle, {top: IS_IOS ? (isIphoneX() == true ? 35 : 15) : 5 }]}>
          <FontAwesome.Button
            name="bell-o"
            color="white"
            size={25}
            style={{marginRight:-10}}
            opacity={1}
            backgroundColor={"transparent"}
            onPress={this.props.bellButtonAction}
          />

        </View>
      )
    }
  }

  showSkipButton() {
    if (this.props.isSkipNeeded == true) {

      return (
        <View style={[styles.rightViewStyle, { top: IS_IOS ? (isIphoneX() == true ? 35 : 15) : 5 }]}>
          <TouchableOpacity 
          style={styles.skipButtonStyle}
          onPress={this.props.skipButtonAction}>
            <Text style={styles.skipTextStyle}>
              SKIP
            </Text>
          </TouchableOpacity>        
        </View>
      )
    }
  }

  showSeparatorView() {
    return (
      <View style={{position:'absolute', height: 1, bottom: 1, width: SCREEN_WIDTH, backgroundColor: '#C0C0C0'}}/>
    )
  }
}

const styles = StyleSheet.create({
  maincontainer: {
    flex: 1,
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    right: 0
  },
  textStyle: {
    color: '#4698CB',
    top: IS_IOS == true ? 4 : -5,    
    fontSize: moderateScale(25, 0.09),
    fontFamily: "BarlowCondensed-Medium"
  },
  skipTextStyle:{
    color: '#4698CB',
    fontSize: moderateScale(20, 0.09),
    fontFamily: "BarlowCondensed-Regular"
  },
  leftViewStyle: {
    width: 50,
    height: 50,
    position: 'absolute',
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: 'transparent',
    left: 0
  },
  rightViewStyle: {
    width: 50,
    height: 50,
    position: 'absolute',
    backgroundColor: 'transparent',
    right: 0
  },
  textInputContainterStyle: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'stretch',
    top: IS_IOS ? 5 : 0,
    bottom: 0,
    height: 45,
    backgroundColor: 'transparent',
    marginLeft: 50,
    marginRight: 55 //85
  },
  textInputStyle: {
    fontSize: moderateScale(17, 0.09),
    color: 'black',
    backgroundColor: 'transparent',
    top: IS_IOS ? 20 : 0,
    paddingRight: 25
  },
  placeholderStyle: {
  },
  underlineStyle: {
    height: 1,
    backgroundColor: '#353839',
    top: IS_IOS ? 25 : -10
  },
  skipButtonStyle: { 
    justifyContent: 'center', 
    alignItems: 'center', 
    height: 50, 
    width: 50 
  }
});