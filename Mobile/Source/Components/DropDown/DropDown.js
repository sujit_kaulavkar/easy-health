import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Animated,
    Platform
} from 'react-native';

import {
    DropDownOptions, APP_THEME,
} from '../../Helper/Constants';

import FontAwesome from "react-native-vector-icons/FontAwesome";
import AccountIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
    Helper
} from '../../Helper/FilePath';
import { moderateScale } from '../../Helper/Scaling';

const OPTION_HEIGHT = 50
const animationDuration = 400

export class DropDown extends Component {

    constructor(props) {
        super(props)
        this.state = {
            animation: new Animated.Value(-OPTION_HEIGHT / 2),
            opacity: new Animated.Value(0),
        };
    }

    componentDidMount() {
        this.expand(true, DropDownOptions.noAction)
    }

    expand(status, index) {

        let finalHeight = status ? 0 : -OPTION_HEIGHT / 2;
        let finalOpacity = status ? 1 : 0;

        Animated.parallel([

            Animated.timing(
                this.state.animation,
                {
                    toValue: finalHeight,
                    duration: animationDuration,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                this.state.opacity,
                {
                    toValue: finalOpacity,
                    duration: animationDuration,
                    useNativeDriver: true
                }
            )
        ]).start(() => {
            if (status == false) {
                this.props.dropDownButtonAction(index)
            }
        })
    }

    render() {

        let animationStyle = {
            transform: [{ translateY: this.state.animation }]
        }

        return (
            <TouchableOpacity activeOpacity={1.0} style={styles.container} onPress={() => { this.dropDownAction(DropDownOptions.noAction) }}>
                <Animated.View style={[styles.subContainer, { top: (Platform.OS == 'ios' ? 75 : 40), opacity: this.state.opacity, alignItems: 'flex-end' }]}>

                    {/* Home option */}
                    <TouchableOpacity activeOpacity={0.6} style={[styles.buttonStyle, animationStyle]} onPress={() => this.dropDownAction(DropDownOptions.home)}>
                        <Text style={styles.textStyle}>Home</Text>
                        <View style={[styles.iconViewStyle, { backgroundColor: '#1abc9c' }]}>
                            <FontAwesome name="home" style={{ fontSize: 20, color: 'white', }} />
                        </View>
                    </TouchableOpacity>

                    {/* Favorites option */}
                    <TouchableOpacity activeOpacity={0.6} style={[styles.buttonStyle, animationStyle]} onPress={() => this.dropDownAction(DropDownOptions.favorites)}>
                        <Text style={styles.textStyle}>Favorites</Text>                        
                    </TouchableOpacity>

                    {/* Account option */}
                    <TouchableOpacity activeOpacity={0.6} style={[styles.buttonStyle, animationStyle]} onPress={() => this.dropDownAction(DropDownOptions.account)}>
                        <Text style={styles.textStyle}>Account</Text>
                        <View style={styles.iconViewStyle}>
                            <AccountIcon name="account-circle" style={{fontSize: 20, color: 'white', }} />
                        </View>
                    </TouchableOpacity>

                </Animated.View>
            </TouchableOpacity>
        );
    }

    dropDownAction(index) {
        this.props.willHideDropDown()
        setTimeout(() => {
            this.expand(false, index)
        }, 500);   // this delay is added to sync the lottie animation and dropdown animation        
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.4)',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    subContainer: {
        backgroundColor: 'transparent'
    },
    actionButtonIcon: {
        fontSize: 20,
        color: 'white',
    },
    buttonStyle: {
        width: 150,
        height: OPTION_HEIGHT,
        paddingRight: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        right: 0
    },
    iconViewStyle: {
        right: 0,
        shadowOpacity: 0.75,
        shadowRadius: 2,
        shadowColor: 'black',
        shadowOffset: { height: 0, width: 0 },
        backgroundColor: APP_THEME,
        height: 30,
        width: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        right: 20,
        fontSize: moderateScale(17, 0.09),
        paddingBottom: 2,
        alignSelf: 'center',
        color: 'white',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 2
    }
});
