import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { moderateScale } from "../../Helper/Scaling";

let timer = () => {};

export default class Timer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            remainingTime: props.time
        };  
        
        isTimerRunning = false
    }

    componentDidMount() {
        this.startTimer()    
    }

    startTimer() {
        if (isTimerRunning == false) {
            isTimerRunning = true

            this.state = {
                remainingTime: this.props.time
            };

            this.countdownTimer()
        }
    }

    stopTimer() {
        isTimerRunning = false
        clearInterval(timer);
    }

    countdownTimer() {
        this.setState({ remainingTime: this.props.time });
        this.stopTimer()
        timer = setInterval(() => {
            if (!this.state.remainingTime) {
                this.stopTimer()
                return false;
            }
            this.setState(prevState => {
                return { remainingTime: prevState.remainingTime - 1 }
            });
        }, 1000);
    }

    getTime() {        
        if (this.state.remainingTime == 0) {
            this.props.timerStop()
            return;
        }

        let minutes = Math.floor(this.state.remainingTime / 60);
        let seconds = this.state.remainingTime - minutes * 60;

        if (minutes > 0) {
            minutes = minutes + "m"
        }

        if (seconds > 0) {
            seconds = seconds + "s"
        }

        return minutes.toString() + " : " + seconds.toString()
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={{
                    color:"white",
                    fontSize: moderateScale(14, 0.09),
                    fontFamily: "BarlowCondensed-Regular"}}>{this.getTime()}</Text>                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    }
});
