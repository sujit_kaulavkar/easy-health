import React, { Component } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
} from 'react-native';
import { moderateScale } from '../../Helper/Scaling';
import { SCREEN_WIDTH } from '../../Helper/Constants';
import { Validator } from '../../Helper/Validator';
import { Helper } from '../../Helper/FilePath';

export default class CardNumberInput extends Component {
    constructor() {
        super();

        this.state = {
            cardNumber: "",
            isValidCardNumber: false
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    ref="textInput"
                    maxLength={19}
                    value={this.state.cardNumber}
                    disableFullscreenUI={true}
                    style={[styles.input, { color: this.state.isValidCardNumber ? 'white' : 'white' }]}
                    placeholder="XXXX XXXX XXXX XXXX"
                    placeholderTextColor="white"
                    autoCorrect={false}
                    autoCapitalize="none"
                    returnKeyType="done"
                    keyboardType="numeric"
                    onChangeText={(text) => this.handleCardNumber(text)}
                    underlineColorAndroid="transparent"
                />
                <View style={{ backgroundColor: this.state.isValidCardNumber ? 'gray' : 'red', height: 1, bottom: 0 }} />
            </View>
        )
    }

    handleCardNumber = (text) => {
        let formattedText = text.split(' ').join('');
        if (formattedText.length > 0) {
            formattedText = formattedText.match(new RegExp('.{1,4}', 'g')).join(' ');
        }

        let validator = new Helper.VALIDATOR(text.replace(/\s+/g, ''))
        let result = validator.validateCardNumber()

        if (result.length == 0) {
            this.setState({
                cardNumber: formattedText,
                isValidCardNumber: true
            });

            this.props.validCardNumber(true)
        }
        else {
            this.setState({
                cardNumber: formattedText,
                isValidCardNumber: false
            });
            this.props.validCardNumber(false)
        }
    }

    cardNumber() {
        return this.refs.textInput._lastNativeText.replace(/\s+/g, '')
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 40,
        marginHorizontal: SCREEN_WIDTH * 0.09,
    },
    input: {
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Medium',
        height: 40,
        minWidth: SCREEN_WIDTH * 0.75,
        maxWidth: SCREEN_WIDTH * 0.75,
    }
})