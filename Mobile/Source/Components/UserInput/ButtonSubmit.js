import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Animated,
  Easing,
  View,
  ActivityIndicator
} from 'react-native';

import { SCREEN_WIDTH, DARK_BLUE_COLOR } from '../../Helper/Constants';
import { moderateScale } from "../../Helper/Scaling";
const MARGIN = 40;

export default class ButtonSubmit extends Component {
  constructor() {
    super();

    this.state = {
      isLoading: false,
      isDisable: false      
    };

    this.buttonAnimated = new Animated.Value(0);
    this._onPress = this._onPress.bind(this);
  }

  _onPress() {
    this.props.onPress()
  }

  startAnimation() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });
    Animated.timing(this.buttonAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();
  }

  stopAnimation() {
    if (this.state.isLoading) {
      this.setState({ isLoading: false });
      this.buttonAnimated.setValue(0);
    }
  }

  render() {    
    const changeWidth = this.buttonAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [SCREEN_WIDTH * 0.82, MARGIN],
    });

    const {customStyles} = this.props
    return (
      <View style={[styles.container, customStyles.containerStyle]}>
        <Animated.View style={{ width: changeWidth}}>
          <TouchableOpacity
            refs="button"
            style={[styles.button, { backgroundColor: this.state.isDisable ? '#A8A8A8' : DARK_BLUE_COLOR, borderRadius: this.state.isLoading ? MARGIN /2 : 0}]}
            onPress={this._onPress}
            activeOpacity={1}
            disabled={this.state.isDisable ? true : false}>            
            {this.state.isLoading ? (
              <ActivityIndicator size="large" color="white" style={styles.image} />
            ) : (
                <Text style={styles.text}>{this.props.title}</Text>
              )}
          </TouchableOpacity>
        </Animated.View>
      </View>
    );
  }

  disableSubmitButton() {
      this.setState({
        isDisable: true
      })    
  }

  enableSubmitButton() {
    this.setState({
      isDisable: false
    })    
  }
}

ButtonSubmit.defaultProps = {
  customStyles: {
    containerStyle: {

    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',    
    height: MARGIN * 0.9,    
    zIndex: 100,
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
    fontSize: moderateScale(18, 0.09),
    fontFamily: 'BarlowCondensed-Medium'
  },
  image: {
    width: 24,
    height: 24,
  },
});