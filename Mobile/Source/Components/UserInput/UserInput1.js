
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';

import {
    Helper
} from '../../Helper/FilePath';

import { SCREEN_WIDTH, BLUE_COLOR } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default class UserInput1 extends Component {

    isBackPressed = false
    constructor(props) {
        super(props)
        this.state = {
            textInputStatus: 'untouched',
            isSecureText: this.props.secureTextEntry
        }
    }

    render() {
        return (
            <View style={styles.container}>

                {this.props.source ?
                    <MaterialIcons
                        name={this.props.source}
                        color="#2e2e1f"
                        size={20}
                        style={styles.inlineImg}
                        backgroundColor={"transparent"}
                    /> : null}

                <TextInput
                    ref="textInput"
                    disableFullscreenUI={true}
                    style={styles.input}
                    placeholder={this.props.placeholder}
                    placeholderTextColor="#A9A9A9"
                    secureTextEntry={this.state.isSecureText}
                    autoCorrect={this.props.autoCorrect}
                    autoCapitalize={this.props.autoCapitalize}
                    returnKeyType={this.props.returnKeyType}
                    onKeyPress={(event) => this.onKeyPress(event)}
                    onChangeText={(text) => this.onTextChange(text)}
                    onSubmitEditing={(event) => this.onEndEditing(event)}
                    placeholderTextColor="darkgray"
                    underlineColorAndroid="transparent"
                />

                {this.props.rightImage ?

                    <Entypo
                        name={this.state.isSecureText ? "eye-with-line" : "eye"}
                        color="white"
                        size={20}
                        style={styles.rightImageStyle}
                        backgroundColor={"transparent"}
                        onPress={() => this.showPassword()}
                    /> : null}

            </View>
        );
    }

    onKeyPress = (event) => {

        if (event.nativeEvent.key == "Backspace") {
            this.isBackPressed = true
        }
    }

    onTextChange = (text) => {

        if (this.isBackPressed == false) {
            if (this.props.onTextChange != null) {
                this.props.onTextChange(text)
            }
        }
        this.isBackPressed = false
    }

    onEndEditing = (event) => {

        if (event) {
            if (this.props.onEditingEnd != null) {
                this.props.onEditingEnd(event)
            }
        }
    }

    showPassword() {
        this.setState({
            isSecureText: !this.state.isSecureText
        })
    }

    getText() {
        return this.refs.textInput._lastNativeText
    }

    clearTextInput() {
        this.refs.textInput.value = ""
    }
}

UserInput1.propTypes = {
    source: PropTypes.string,
    placeholder: PropTypes.string.isRequired,
    secureTextEntry: PropTypes.bool,
    autoCorrect: PropTypes.bool,
    autoCapitalize: PropTypes.string,
    returnKeyType: PropTypes.string,
};

const styles = StyleSheet.create({
    input: {
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        height: 35,
        marginLeft:10,
        minWidth: SCREEN_WIDTH * 0.75,
        maxWidth: SCREEN_WIDTH * 0.75,
        color: BLUE_COLOR,         
    },
    container: {
        height: 37,    
        borderRadius: 10,
        justifyContent:'center',
        borderColor: 'gray',
        borderWidth: 1,           
        marginHorizontal: SCREEN_WIDTH * 0.09,
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 15,
        top: 9,
    },
    rightImageStyle: {
        position: 'absolute',
        zIndex: 100,
        width: 22,
        height: 22,
        right: 10,
    },
    separatorView: {
        height: 1,
        margin: 0,
        backgroundColor: '#C8C8C8'
    }
});