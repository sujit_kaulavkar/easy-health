'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import createReactClass from 'create-react-class';

import {
    StyleSheet,
    TextInput,
    LayoutAnimation,
    Animated,
    Easing,
    Text,
    View,
    Platform,
    ViewPropTypes
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';

import { SCREEN_WIDTH } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

var textPropTypes = Text.propTypes || ViewPropTypes
var textInputPropTypes = TextInput.propTypes || textPropTypes
var propTypes = {
    ...textInputPropTypes,
    inputStyle: textInputPropTypes.style,
    labelStyle: textPropTypes.style,
    disabled: PropTypes.bool,
    style: ViewPropTypes.style,
}

var UserInput = createReactClass({
    propTypes: propTypes,
    isBackPressed: false,    
    getInitialState() {        
        var state = {
            text: this.props.defaultText,
            dirty: (this.props.defaultText || this.props.placeholder),
            isSecureText: this.props.secureTextEntry
        };

        var style = state.dirty ? dirtyStyle : cleanStyle
        state.labelStyle = {
            fontSize: new Animated.Value(style.fontSize),
            top: new Animated.Value(style.top)
        }

        return state
    },

    componentWillReceiveProps(props) {
        if (typeof props.value !== 'undefined' && props.value !== this.state.text) {
            this.setState({ text: props.value, dirty: !!props.value })
            this._animate(!!props.value)
        }
    },

    _animate(dirty) {
        var nextStyle = dirty ? dirtyStyle : cleanStyle
        var labelStyle = this.state.labelStyle
        var anims = Object.keys(nextStyle).map(prop => {
            return Animated.timing(
                labelStyle[prop],
                {
                    toValue: nextStyle[prop],
                    duration: 200
                },
                Easing.ease
            )
        })

        Animated.parallel(anims).start()
    },

    _onFocus() {
        this._animate(true)
        this.setState({ dirty: true })
        if (this.props.onFocus) {
            this.props.onFocus(arguments);
        }
    },

    _onBlur() {
        if (!this.state.text) {
            this._animate(false)
            this.setState({ dirty: false });
        }

        if (this.props.onBlur) {
            this.props.onBlur(arguments);
        }
    },

    onChangeText(text) {        
        this.setState({ text: text })
        if (this.isBackPressed == false) {
            if (this.props.onTextChange != null) {
                this.props.onTextChange(text)
            }
        }
        this.isBackPressed = false
    },

    updateText(event) {
        var text = event.nativeEvent.text
        this.setState({ text: text })

        if (this.props.onEndEditing) {
            this.props.onEndEditing(event)
        }
    },

    _renderLabel() {
        return (
            <Animated.Text
                ref='label'
                style={[this.state.labelStyle, styles.label, this.props.labelStyle]}
            >
                {this.props.children}
            </Animated.Text>
        )
    },

    onKeyPress(event) {

        if (event.nativeEvent.key == "Backspace") {
            this.isBackPressed = true
        }
    },

    onSubmitEditing(event) {

        if (event) {
            if (this.props.onEditingEnd != null) {
                this.props.onEditingEnd(event)
            }
        }
    },

    showPassword() {
        this.setState({
            isSecureText: !this.state.isSecureText
        })
    },

    getText() {
        return this.refs.textInput._lastNativeText
    },

    clearTextInput() {
        this.refs.textInput.value = ""
    },

    render() {
        var props = {
            autoCapitalize: this.props.autoCapitalize,
            autoCorrect: this.props.autoCorrect || false,
            autoFocus: this.props.autoFocus,
            bufferDelay: this.props.bufferDelay,
            clearButtonMode: this.props.clearButtonMode,
            clearTextOnFocus: this.props.clearTextOnFocus,
            controlled: this.props.controlled,
            editable: this.props.editable,
            enablesReturnKeyAutomatically: this.props.enablesReturnKeyAutomatically,
            keyboardType: this.props.keyboardType,
            multiline: this.props.multiline,
            numberOfLines: this.props.numberOfLines,
            onBlur: this._onBlur,
            onChange: this.props.onChange,
            onChangeText: this.onChangeText,
            onEndEditing: this.updateText,
            onFocus: this._onFocus,
            password: this.props.secureTextEntry || this.props.password, // Compatibility
            secureTextEntry: this.state.isSecureText,
            returnKeyType: this.props.returnKeyType,
            selectTextOnFocus: this.props.selectTextOnFocus,
            selectionState: this.props.selectionState,
            selectionColor: this.props.selectionColor,
             style: [styles.input],
             testID: this.props.testID,
            value: this.state.text,
            underlineColorAndroid: this.props.underlineColorAndroid, // android TextInput will show the default bottom border            
        },
            elementStyles = [styles.element];

        if (this.props.inputStyle) {
            props.style.push(this.props.inputStyle);
        }

        if (this.props.style) {
            elementStyles.push(this.props.style);
        }

        const { customStyles } = this.props           
        return (
            <View style={[elementStyles, customStyles.containerStyle]}>
                {this._renderLabel()}
                <TextInput
                    ref="textInput"
                    {...props}
                    disableFullscreenUI={true}
                    value  = {this.state.text}
                    onKeyPress={(event) => this.onKeyPress(event)}
                    onSubmitEditing={(event) => this.onSubmitEditing(event)}
                    underlineColorAndroid="transparent"
                >
                </TextInput>

                {this.props.source ?
                    <MaterialIcons
                        name={this.props.source}
                        color="#2e2e1f"
                        size={20}
                        style={styles.inlineImg}
                        backgroundColor={"transparent"}
                    /> : null}

                {this.props.rightImage ?

                    <Entypo
                        name={this.state.isSecureText ? "eye-with-line" : "eye"}
                        color="rgba(0,0,0,0.5)"
                        size={20}
                        style={styles.rightImageStyle}
                        backgroundColor={"transparent"}
                        onPress={() => this.showPassword()}
                    /> : null}

                <View style={styles.separatorView} />

            </View>
        );
    },
});

UserInput.propTypes = {
    defaultText: PropTypes.string,
    customStyles: PropTypes.object
};

UserInput.defaultProps = {
    defaultText: "",
    customStyles: {
        containerStyle: {
        }
    }
}

var labelStyleObj = {
    marginTop: 21,
    paddingLeft: 0,
    fontSize: moderateScale(17, 0.09),
    fontFamily: 'BarlowCondensed-Regular',  
    color: 'rgba(30, 34, 170, 0.4)',
    position: 'absolute'
}

if (Platform.OS === 'web') {
    labelStyleObj.pointerEvents = 'none'
}

var styles = StyleSheet.create({
    element: {        
        flex:1,
        marginHorizontal: SCREEN_WIDTH * 0.09,
    },
    input: {
        height: 40,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        color: 'rgba(30, 34, 170, 1)',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular',        
        paddingLeft: 0,
        marginTop: 20,
        minWidth: SCREEN_WIDTH * 0.75,
        maxWidth: SCREEN_WIDTH * 0.75,
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 15,
        top: 9,
    },
    rightImageStyle: {
        position: 'absolute',
        zIndex: 100,
        width: 22,
        height: 22,
        right: 0,
        bottom: 10,
    },
    separatorView: {
        height: 1,
        margin: 0,
        backgroundColor: 'rgba(0,0,0,0.2)'
    },
    label: labelStyleObj
})

var cleanStyle = {
    fontSize: moderateScale(15, 0.09),
    top: 7,
}

var dirtyStyle = {
    fontSize: moderateScale(12, 0.09),
    top: -17,
}

module.exports = UserInput;