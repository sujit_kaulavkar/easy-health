import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    PixelRatio
} from 'react-native'

import CountryPicker from 'react-native-country-picker-modal'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { SCREEN_WIDTH, BLUE_COLOR } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default class CountrySelection extends Component {

    constructor(props) {
        super(props)
        this.state = {
            value: {
                cca2: "SGP",
                name: "Singapore"
            }
        }
    }

    render() {            
        return (
            <View style={styles.container}>
                <CountryPicker
                    cca2={this.state.value.cca2}
                    filterable={true}
                    disabled={this.props.isDisableCountrySelection}
                    closeButtonImage={require('../../Resources/Images/close.png')}
                    closeable={true}
                    filterPlaceholder="Search country"
                    filterPlaceholderTextColor="red"
                    countryName={this.state.value.name}                    
                    children={<Text style={styles.countryNameTextStyle}>
                        {this.state.value.name}
                    </Text>}
                    filterPlaceholderTextColor="white"
                    onChange={value => {
                        this.setState({
                            value: value
                        })
                    }}                    
                    styles={darkTheme}
                />

                <View style={styles.separatorStyle} />

                <SimpleLineIcons
                    name="arrow-down"
                    color="white"
                    size={18}
                    style={styles.downArrowStyle}
                />
            </View>
        )
    }

    getCountry() {
        return this.state.value.name
    }

    getCountryCode() {
        return this.state.value.cca2
    }
}

CountrySelection.defaultProps = {
    isDisableCountrySelection: true
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 60,
        top:15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    data: {
        padding: 15,
        marginTop: 10,
        backgroundColor: '#ddd',
        borderColor: '#888',
        borderWidth: 1 / PixelRatio.get(),
        color: '#777'
    },
    countryNameTextStyle: {
        color: 'rgba(30, 34, 170, 1)',
        flex: 1,
        width: SCREEN_WIDTH,
        marginTop: 7,
        paddingHorizontal: moderateScale(SCREEN_WIDTH * 0.08, 0.09),
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    },
    separatorStyle: {
        height: 1,
        width: moderateScale(SCREEN_WIDTH - SCREEN_WIDTH * 0.15, 0.09),
        bottom: 20,
        backgroundColor: '#C8C8C8'
    },
    downArrowStyle: {
        position: 'absolute',
        right: 60,
        top: 15
    }
})

const darkTheme = StyleSheet.create({
    modalContainer: {
        backgroundColor: BLUE_COLOR
    },
    contentContainer: {
        backgroundColor: BLUE_COLOR
    },
    header: {
        backgroundColor: BLUE_COLOR
    },
    itemCountryName: {
        borderBottomWidth: 0
    },
    countryName: {
        color: "white"
    },
    letterText: {
        color: "white"
    },
    input: {
        color: "white",
        borderBottomWidth: 1,
        borderColor: "white"
    },
    // closeButtonImage: {
    //     height: 15,
    //     width: 15
    // }
});