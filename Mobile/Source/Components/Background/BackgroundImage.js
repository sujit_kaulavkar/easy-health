
import React, { Component } from 'react';
import {View, Image } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../Helper/Constants';

export default class BackgroundImage extends Component {

    render() {
        const { children, isShowMMLogo, isTouchEnabled } = this.props        
        return (
            <View style={{ flex: 1}} pointerEvents={isTouchEnabled}>                

                <Image
                    resizeMode="cover"
                    style={{ width: SCREEN_WIDTH, height: SCREEN_HEIGHT, position: 'absolute' }}
                    source={require("../../Resources/Images/generic_bg_image.jpg")} />

                {children}     

                {isShowMMLogo == true ? 
                <View style={{ justifyContent: 'flex-end', marginTop: 10, marginBottom: 10 }}>
                    <Image
                        style={{ alignSelf: 'center', height: 30, width: 300 }}
                        source={require('../../Resources/Images/mm_logo.png')} />
                </View>
                : null }                                
            </View>
        )
    }
}

BackgroundImage.defaultProps = {
    isTouchEnabled: "auto"
}