import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
} from "react-native";

import {
    SCREEN_WIDTH,
    APP_THEME,
    APP_NAME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION
} from "../../Helper/Constants";

import {
    Helper
} from '../../Helper/FilePath';

import Dialog from "react-native-dialog";
import { moderateScale } from "../../Helper/Scaling";
import DropdownAlert from 'react-native-dropdownalert';

export default class CardName extends Component {

    cardNameText = ""

    constructor(props) {
        super(props);

        this.state = {
            isShowSpinner: false,
            dialogVisible: false
        }

        handleDone = this.handleDone.bind(this)
    }

    remoteEditCardNameCall() {
        if (this.cardNameText.trim().length == 0) {
            return;
        }

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const card = this.props.selectedCard
        apiHelper.editCardName(user.user_hash_id, card.card_hash_id, this.cardNameText)
            .then((responseJson) => {
                if (responseJson) {
                    this.handleEditCardNameResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }

                this.hideSpinner()
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleEditCardNameResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            this.props.selectedCard.name = this.cardNameText
            this.cardNameText = ""
        }
        else {
            this.props.showServerErrorMessage()
        }
    }

    render() {

        const cardName = this.props.selectedCard.name
        return (
            <View style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <TouchableOpacity
                    activeOpacity={1}
                    style={styles.button}
                    onPress={() => this.showDialog()}>
                    <Text style={styles.cardNameStyle}>{cardName}</Text>
                    <Image
                        style={styles.editStyle}
                        source={require('../../Resources/Images/edit.png')} />
                </TouchableOpacity>

                {this.dialog()}

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </View>
        );
    }

    dialog() {
        return (
            <View>
                <Dialog.Container visible={this.state.dialogVisible}>
                    <Dialog.Title>{APP_NAME}</Dialog.Title>
                    <Dialog.Input
                        ref="input"
                        placeholder="Enter card name"
                        onChangeText={(text) => this.onDoneCardEntry(text)} />
                    <Dialog.Button label="Cancel" onPress={this.handleCancel} />
                    <Dialog.Button label="Done" onPress={() => this.handleDone()} />
                </Dialog.Container>
            </View>
        )
    }

    showDialog() {
        this.setState({ dialogVisible: true });
    };

    handleCancel = () => {
        this.setState({ dialogVisible: false });
    };

    handleDone() {
        this.setState({ dialogVisible: false });
        this.remoteEditCardNameCall()
    };

    onDoneCardEntry(text) {
        this.cardNameText = text
    }

    changeCardName() {
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        height: 30,
        width: SCREEN_WIDTH,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    cardNameStyle: {
        color: 'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    editStyle: {
        width: 15,
        height: 15,
        margin: 10
    }
});
