import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { moderateScale } from '../Helper/Scaling';

export default class RadioButton extends Component {

    constructor(props) {
        super(props);
        radioButtonAction = this.radioButtonAction.bind(this)
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity 
                activeOpacity={1}
                    style={{flex:1, flexDirection: 'row', alignItems: 'center', marginRight: 10}}
                onPress={() => this.radioButtonAction()} >
                    <MaterialCommunityIcons
                        name={this.props.isSelected ? "radiobox-marked" : "radiobox-blank"}
                        size={20}
                        color="rgba(0,0,0,0.5)"/>                    
                    <Text style={styles.titleStyle}>{this.props.title}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    radioButtonAction() {
        if (typeof this.props.radioButtonAction == 'function') {
            this.props.radioButtonAction(this.props.title)
        }        
    }
}

const styles = StyleSheet.create({
    container: {    
        width:100,
        height:40,
        marginRight:5        
    },
    titleStyle:{
        fontSize: moderateScale(19, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: "rgba(30, 34, 170, 1.0)",
        bottom:2,
        left:5
    }
});
