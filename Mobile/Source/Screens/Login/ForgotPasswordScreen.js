import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Alert,
  Keyboard
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';

import {
  Components,
  Helper
} from '../../Helper/FilePath';

import {
  APP_THEME,
  APP_NAME,
  OK_TEXT,
  OTP_SEND_MESSAGE,
  TOP_BAR_HEIGHT,
  SCREEN_WIDTH,
  OTP_OPTIONS,
  SOMETHING_WRONG_MESSAGE,
  NO_INTERNET_CONNECTION,
  BLUE_COLOR
} from '../../Helper/Constants.js';

import { moderateScale } from "../../Helper/Scaling";

export default class ForgotPasswordScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isTouchEnabled: true,
    }
  }

  submitButtonAction() {

    const email = this.refs.email.getText()

    let validator = new Helper.VALIDATOR(email)
    let errorMessage = validator.validateEmailAddress()
    if (errorMessage.length != 0) {
      this.dropdown.alertWithType('error', "", errorMessage);
      return;
    }

    Keyboard.dismiss()
    this.remoteForgotPasswordCall(email);
  }

  remoteForgotPasswordCall(email) {

    let apiHelper = Helper.API_HELPER.getInstance();
    if (apiHelper.isInternetConnected() == false) {
      this.showNoInternetAlert()
      return;
    }

    this.showLoader()
    this.refs.submitButton.startAnimation()
    apiHelper.forgotPassword(email).then((responseJson) => {
      this.hideLoader()
      if (responseJson) {
        this.handleForgotPassordResponse(email, responseJson)
      }
      else {
        this.showServerErrorMessage()
      }
    })
      .catch(() => {
        this.hideLoader()
        this.showServerErrorMessage()
      })
  }

  handleForgotPassordResponse(email, response) {
    const status = response.success;
    if (status == true) {
      this.showMobileNumberVerificationScreen(email)
    }
    else {
      const errorMessage = response["errorMessage"];
      if (errorMessage) {
        this.dropdown.alertWithType('error', "", errorMessage);
      }
      else {
        this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
      }
    }
  }

  showMobileNumberVerificationScreen = (email) => {

    setTimeout(() => {
      Alert.alert(APP_NAME, OTP_SEND_MESSAGE, [
        {
          text: OK_TEXT, onPress: () => this.props.navigation.navigate('OTPVerificationScreen', {
            email: email
          })
        }
      ], { cancelable: false })
    }, 500);
  }

  render() {

    return (
      <Components.BACKGROUND_IMAGE style={styles.container} isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>
        <Components.TOP_BAR
          topbarTranslate={0}
          isPortraitMode={true}
          ref="topbarRef"
          title='FORGOT PASSWORD'
          isBackButtonNeeded={true}
          backButtonAction={() => this.props.navigation.goBack()}/>

        <View style={styles.subContainer}>

          <Components.USER_INPUT
            ref="email"
            returnKeyType={'done'}
            autoCapitalize={'none'}
            autoCorrect={false}
          >EMAIL</Components.USER_INPUT>

          {/* <Components.MOBILE_NUMBER ref="mobilenumber" /> */}
          <Text style={styles.messageStyle}>Verification OTP will be send to this entered Email Address</Text>
        </View>

        <View style={styles.submitButtonBaseStyle}>
          <Components.BUTTON_SUBMIT
            ref="submitButton"
            activeOpacity={0.7}
            title="Submit"
            onPress={() => this.submitButtonAction()}
          />
        </View>

        <DropdownAlert
          inactiveStatusBarBackgroundColor={APP_THEME}
          ref={ref => this.dropdown = ref}
          onClose={data => this.onCloseDropdown(data)} />

      </Components.BACKGROUND_IMAGE>
    );
  }

  showLoader() {
    if (this.state.isTouchEnabled == true) {
      this.setState({
        isTouchEnabled: false
      })
    }
  }

  hideLoader() {
    if (this.state.isTouchEnabled == false) {
      this.refs.submitButton.stopAnimation()
      this.setState({
        isTouchEnabled: true
      })
    }
  }

  showServerErrorMessage() {
    setTimeout(() => {
      this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
    }, 500);
  }

  showNoInternetAlert() {
    this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
  }

  onCloseDropdown() { }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    top: TOP_BAR_HEIGHT + 10,
    flexDirection: 'column',
    height: 80
  },
  messageStyle: {
    fontSize: moderateScale(13, 0.09),
    fontFamily: 'BarlowCondensed-Regular',
    color: BLUE_COLOR,
    marginHorizontal: SCREEN_WIDTH * 0.09,
  },
  submitButtonBaseStyle: {
    position: 'absolute',
    top: 180,
    alignSelf: 'center'
  }
});