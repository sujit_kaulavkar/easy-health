
import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

import {
    SCREEN_WIDTH, SCREEN_HEIGHT
} from '../../Helper/Constants';

const styles = StyleSheet.create({
    image: {
        flex:1,
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
    }
});

const slides = [
    {
        key: 'first',
        image: require('../../Resources/Images/Tutorial/first.jpg'),
        imageStyle: styles.image,
    },
    {
        key: 'second',
        image: require('../../Resources/Images/Tutorial/second.jpg'),
        imageStyle: styles.image,
    },
    {
        key: 'third',
        image: require('../../Resources/Images/Tutorial/third.jpg'),
        imageStyle: styles.image
    },
    {
        key: 'forth',
        image: require('../../Resources/Images/Tutorial/forth.jpg'),
        imageStyle: styles.image
    },
    {
        key: 'fifth',
        image: require('../../Resources/Images/Tutorial/fifth.jpg'),
        imageStyle: styles.image
    }
];

export default class TutorialScreen extends Component {

    constructor() {
        super()
        this.state = {
            showRealApp: false
        }
    }

    _onDone = () => {
        console.log("done clicked");        
        this.props.navigation.navigate("LoginScreen")
        //this.props.navigation.navigate("KYCScreen")
    }

    _renderItem = props => (
        <View style={{flex:1}}>
        <Image
            style={{width: SCREEN_WIDTH, height: SCREEN_HEIGHT}}
            resizeMode= "cover"
            source={props.image}/>
        </View>
    );

    render() {
        return (
            <View style={{ flex: 1 }}>
                <AppIntroSlider
                    slides={slides}
                    renderItem={this._renderItem}
                    bottomButton
                    hideNextButton={true}
                    onDone={this._onDone} />
            </View>
        )
    }
}
