import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
    Keyboard,
    BackHandler,
    Linking
} from 'react-native';

import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';
import SplashScreen from 'react-native-splash-screen';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    APP_THEME,
    socialType,
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    TOP_BAR_HEIGHT,
    NO_INTERNET_CONNECTION,
    SCREEN_HEIGHT,
    PINK_COLOR,
    DARK_BLUE_COLOR
} from '../../Helper/Constants.js';

var currentSocialType = socialType["noOptionSelected"]

export default class LoginScreen extends Component {

    constructor(props) {
        super(props);

        deeplink = null

        this.state = {
            user: undefined,
            isShowSpinner: false,
            isTouchEnabled: true
        }

        loginToFacebook = this.loginToFacebook.bind(this)
        loginToGoogle = this.loginToGoogle.bind(this)
        showHomeScreen = this.showHomeScreen.bind(this)
        loginButtonAction = this.loginButtonAction.bind(this)
        socialLoginSuccessful = this.socialLoginSuccessful.bind(this)
        viewDidAppear = this.viewDidAppear.bind(this)
        remoteSocialLoginCall = this.remoteSocialLoginCall.bind(this)
    }

    componentDidMount() {
        Helper.PUSH_HELPER.getInstance()
        Helper.API_HELPER.getInstance().addInternetListener();
        SplashScreen.hide();
        Events.trigger('lockDrawer')
        // Helper.DATABASE_HANDLER.getInstance().getUserDetails().then((userInfo) => {
        //     if (userInfo) {
        //         this.setState({
        //             user: userInfo
        //         })

        //         Helper.USER_MANAGER.getInstance().setUser(userInfo)

        //         setTimeout(() => {
        //             showHomeScreen()
        //         }, 1000)
        //     }
        // })

        this.handleDeepLink()
        this.addObservers()
        console.disableYellowBox = true;
    }

    componentWillUnmount() {
        didBlurObserver.remove();
        didFocusObserver.remove();
    }

    handleDeepLink() {

        Linking.getInitialURL().then((url) => {
            if (url) {
                deeplink = url
            }
        }).catch(err => console.error('An error occurred', err));
        Linking.addEventListener('url', this._handleOpenURL);
    }

    _handleOpenURL = (event) => {
        if (event.url != null) {
            deeplink = event.url
        }
    }

    addObservers() {
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {
                this.viewDidAppear()
            }
        );

        // listener called when viewWillDisappear
        didBlurObserver = this.props.navigation.addListener(
            'didBlur',
            payload => {
                BackHandler.removeEventListener('hardwareBackPress', this.handleBackFromLogin);
            }
        );
    }

    viewDidAppear() {
        // const userInfo = Helper.USER_MANAGER.getInstance().getUser()
        // if (userInfo) {
        //     this.setState({
        //         user: userInfo
        //     })

        //     setTimeout(() => {
        //         showHomeScreen()
        //     }, 2000)
        // }

        this.refs.email.clearTextInput()
        this.refs.password.clearTextInput()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
    }

    handleBackFromLogin = () => {
        return true;
    }

    skipLoginAction = () => {
        this.showHomeScreen()
    }

    socialLoginSuccessful = (userInfo) => {

        if (userInfo != undefined && userInfo != "") {

            this.setState({
                user: userInfo
            })

            setTimeout(() => {
                showHomeScreen()
            }, 1000)
        }

        currentSocialType = socialType["noOptionSelected"]
    }

    remoteLoginCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()        

        const emailAddress = this.refs.email.getText()
        const password = this.refs.password.getText().trim()
        apiHelper.login(emailAddress, password).then((responseJson) => {
            this.hideSpinner()
            if (responseJson) {
                this.handleLoginResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        }).catch((error) => {
            this.hideSpinner()
            this.showServerErrorMessage()
        })
    }

    handleLoginResponse(response) {
        const status = response.success;
        if (status == true) {
            let userData = response.data.user_details;
            const accessToken = response.data.access_token;
            userData.accessToken = accessToken            
                        
            Helper.USER_MANAGER.getInstance().setUser(userData)
            Helper.DATABASE_HANDLER.getInstance().saveUserDetails(userData)
            
            setTimeout(() => {
                this.showHomeScreen()
            }, 500);
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    remoteSocialLoginCall(firstName, lastName, socialId) {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        apiHelper.socialLogin(firstName, lastName, socialId, currentSocialType).then((responseJson) => {
            currentSocialType = socialType["noOptionSelected"]
            this.hideSpinner()
            if (responseJson) {
                this.handleSocialLoginResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch(() => {
                currentSocialType = socialType["noOptionSelected"]
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleSocialLoginResponse(response) {
        const status = response["status"];
        if (status == true) {
            const userData = response["data"];
            Helper.USER_MANAGER.getInstance().setUser(userData)
            Helper.DATABASE_HANDLER.getInstance().saveUserDetails(userData)
            this.socialLoginSuccessful(userData)
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE
                style={styles.container}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <ScrollView
                    style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ paddingTop: 20, flexGrow: 1 }}>

                    <View style={{ alignItems: 'center' }}>
                        <Image
                            resizeMode="cover"
                            style={{ width: 150, height: 150 }}
                            source={require("../../Resources/Images/splash.png")} />
                    </View>

                    <View style={styles.userInput}>

                        <Components.USER_INPUT1
                            ref="email"
                            placeholder="EMAIL ADDRESS"
                            secureTextEntry={false}
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}/>

                        <Components.USER_INPUT1
                            ref="password"
                            placeholder="PASSWORD"
                            secureTextEntry={true}
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            rightImage={true}/>
                    </View>

                    <View style={styles.submitButtonBase}>
                        <TouchableOpacity 
                        activeOpacity={1}
                        style={{
                            width: 130,
                            height: 35,
                            borderRadius: 10,
                            alignItems:'center',
                            justifyContent:'center',                        
                            backgroundColor: PINK_COLOR
                        }} onPress={() => this.loginButtonAction()}>
                            <Text style={{
                                color: 'white',
                                textAlign:'center',
                                fontSize: moderateScale(17.5, 0.09),
                                fontFamily: 'BarlowCondensed-Medium'
                            }}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: 10 }}>
                        <TouchableOpacity style={styles.forgotPasswordStyle} onPress={() => this.forgotPasswordAction()}>
                            <Text style={[styles.signUpText]}>FORGOT PASSWORD?</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.signUpBase}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{ flexDirection: 'row', alignSelf: 'flex-end', paddingBottom: 50 }}
                            onPress={() => this.signUpAction()}>
                            <Text style={styles.dontHaveAccountText}>DON'T HAVE AN ACCOUNT?</Text>
                            <Text style={styles.signUpText}>SIGN UP</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        );
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    loginButtonAction() {    
        
        const emailAddress = this.refs.email.getText()
        let validator = new Helper.VALIDATOR(emailAddress)
        errorMessage = validator.validateEmailAddress()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        const password = this.refs.password.getText()
        validator = new Helper.VALIDATOR(password)
        errorMessage = validator.validatePassword()
        if (errorMessage.length != 0) {

            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        Keyboard.dismiss()
        this.remoteLoginCall()
    }

    signUpAction() {
        this.props.navigation.navigate("RegistrationScreen")
    }

    forgotPasswordAction() {                
        this.props.navigation.navigate("ForgotPasswordScreen")
    }

    loginToFacebook() {

        if (currentSocialType == socialType["noOptionSelected"]) {
            currentSocialType = socialType["Facebook"]
            Helper.FACEBOOK_HANDLER.getInstance().loginToFacebook().then((result, reject) => {
                if (result) {
                    const firstName = result["last_name"]
                    const lastName = result["last_name"]
                    const socialId = result["id"]
                    setTimeout(() => {
                        this.remoteSocialLoginCall(firstName, lastName, socialId)
                    }, 500);
                }
                else if (reject) {
                    currentSocialType = socialType["noOptionSelected"]
                    this.dropdown.alertWithType('error', "", "Problem in login to Facebook. Try again");
                }
            }).catch((error) => {
                console.log("error:", error);
                this.dropdown.alertWithType('error', "", "Problem in login to Facebook. Try again");
            })
        }
    }

    loginToGoogle() {

        if (currentSocialType == socialType["noOptionSelected"]) {
            currentSocialType = socialType["Google"]
            Helper.GOOGLE_HANDLER.getInstance().loginToGoogle().then((result, reject) => {
                if (result) {
                    const firstName = result.user.givenName
                    const lastName = result.user.familyName
                    const socialId = result.user.id

                    setTimeout(() => {
                        this.remoteSocialLoginCall(firstName, lastName, socialId)
                    }, 500);
                }
                else if (reject) {
                    currentSocialType = socialType["noOptionSelected"]
                    this.dropdown.alertWithType('error', "", "Problem in login to Google. Try again");
                }
            }).catch((error) => {
                console.log("error:", error);
                this.dropdown.alertWithType('error', "", "Problem in login to Google. Try again");
            })
        }
    }

    showHomeScreen = () => {
        if (this.state.user == undefined) {
            this.props.navigation.replace('DrawerNavigation', { "deepLink": deeplink });
        }
        else {
            this.props.navigation.replace('DrawerNavigation', { "deepLink": deeplink });
        }
    }

    onCloseDropdown() { }
}

const iconStyles = {
    borderRadius: 10,
    iconStyle: { paddingVertical: 2 },
    fontSize: moderateScale(18, 0.09),
    fontFamily: 'Ubuntu-Medium'
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    userInput: {
        marginTop: 50,
        height: 90,
        justifyContent:"space-between",
        flexDirection: 'column'
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
        color: '#333',
        fontSize: moderateScale(17, 0.09),
        marginBottom: 5,
        paddingHorizontal: 10,
    },
    buttons: {
        justifyContent: 'space-between',
        flexDirection: 'column',
        top: 100,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.2, 0.09),
        height: 90,
        backgroundColor: 'transparent'
    },
    skipButton: {
        paddingVertical: 20,
        backgroundColor: 'transparent',
    },
    skipButtonText: {
        color: "#4c4cff",
        fontSize: moderateScale(18, 0.09),
        backgroundColor: 'transparent',
    },
    btnSubmit: {
        top: 100,
        right: 28,
    },
    signUpBase: {
        top: 10,
        height: 50,
        flex: 1,
        width: SCREEN_WIDTH,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    dontHaveAccountText: {
        fontSize: moderateScale(15, 0.09),
        textAlign: 'center',
        color: 'rgba(255,255,255,0.5)',
        fontFamily: 'BarlowCondensed-Regular'
    },
    forgotPasswordStyle: {
        marginTop: 0
    },
    signUpText: {
        color:  DARK_BLUE_COLOR,
        paddingLeft: 5,
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    },
    submitButtonBase: {
        marginTop: 30,
        alignItems:'center'       
    }
});