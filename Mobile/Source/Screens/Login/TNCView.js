import React, { PureComponent } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity
} from 'react-native';

import {
    SCREEN_WIDTH, DARK_BLUE_COLOR
} from '../../Helper/Constants';

import { moderateScale } from "../../Helper/Scaling";

import Feather from 'react-native-vector-icons/Feather';

export default class TNCView extends PureComponent {

    constructor() {
        super()
        this.state = {
            isSelected: false
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Feather
                    name={this.state.isSelected ? "check-square" : "square"}
                    color={DARK_BLUE_COLOR}
                    size={20}
                    style={styles.rightImageStyle}
                    backgroundColor={"transparent"}
                    onPress={() => this.changeCheckboxStatus()}
                />
                <Text style={styles.agreeTextStyle}>
                    I agree to the
                </Text>

                <TouchableOpacity 
                activeOpacity={1}
                onPress={() => {this.showTNCPage()}}>
                    <Text style={styles.tncTextStyle}>
                        Terms & Conditions
                        </Text>
                </TouchableOpacity>

            </View>
        )
    }

    changeCheckboxStatus() {
        this.setState({
            isSelected: !this.state.isSelected
        })
    }

    isTNCSelected() {
        return this.state.isSelected
    }

    showTNCPage() {
        if (this.props.showTNCPage) {
            this.props.showTNCPage()
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 40,
        marginTop:0,
        marginHorizontal: SCREEN_WIDTH * 0.09,
        flexDirection: 'row',
        alignItems: 'center'
    },
    rightImageStyle: {
        zIndex: 100,
        width: 20,
        height: 20
    },
    agreeTextStyle: {
        left: 10,
        color: 'rgba(0,0,0,0.5)',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    },
    tncTextStyle: {
        color: DARK_BLUE_COLOR,
        paddingLeft:16,
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    }
})