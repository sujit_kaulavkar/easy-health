import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Alert,
    Keyboard,
    Text,
    TouchableOpacity,
    StatusBar
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';
import OtpInputs from 'react-native-otp-inputs'
import Feather from 'react-native-vector-icons/Feather';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    APP_NAME,
    APP_THEME,
    OK_TEXT,
    SOMETHING_WRONG_MESSAGE,
    TOP_BAR_HEIGHT,
    NO_INTERNET_CONNECTION,
    REGISTRATION_SUCCESSFUL_MESSAGE,
    DARK_BLUE_COLOR,
    OTP_OPTIONS,
    CHANGE_EMAIL_SUCCESSFUL,
    CHANGE_MOBILE_NUMBER_SUCCESSFUL,
    CARD_ALREADY_USED_MESSAGE,
    WRONG_CARD_ENTRY_MESSAG,
    BLUE_COLOR
} from '../../Helper/Constants.js';

import {
    moderateScale
} from '../../Helper/Scaling';

export default class OTPVerificationScreen extends Component {

    constructor(props) {
        super(props);

        otpCode = ""
        this.state = {
            isTouchEnabled: true,
            isResendOTP: false,
            isShowSpinner: false
        }

        timerStop = this.timerStop.bind(this)
        submitButtonAction = this.submitButtonAction.bind(this)
    }

    remoteVerifyCodeCall() {
        const { mobileNumber } = this.props.navigation.state.params;
        
        this.showSpinner()
        let apiHelper = Helper.API_HELPER.getInstance();
        apiHelper.verifyOTP(otpCode, mobileNumber)
            .then((responseJson) => {

                this.hideSpinner()
                if (responseJson) {
                    this.handleVerifyCodeResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleVerifyCodeResponse(responseJson) {
        const status = responseJson.success;

        const { mobileNumber } = this.props.navigation.state.params;
        if (status == true) {
            this.props.navigation.navigate('ResetPasswordScreen', {
                email: mobileNumber                
            })
        }
        else {
            this.hideLoader()
            const errorMessage = responseJson["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
        }
    }    

    render() {
        return (
            <Components.BACKGROUND_IMAGE
                style={styles.container}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    isPortraitMode={true}
                    ref="topbarRef"
                    title='OTP VERIFICATION'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <View style={styles.subContainerStyle}>

                    <View style={styles.otpView}>
                        <OtpInputs                            
                            inputContainerStyles={{ backgroundColor: "rgba(64,126,201, 0.5)" }}
                            handleChange={code => { otpCode = code }}
                            numberOfInputs={5} />
                    </View>

                    <Text style={styles.taglineMessage}>
                        ENTER OTP THAT HAS SENT TO YOUR EMAIL ADDRESS
                    </Text>
            
                    <View style={styles.submitButtonBaseStyle}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            style={styles.btnSubmit}
                            title="SUBMIT"
                            onPress={() => this.submitButtonAction()}
                        />
                    </View>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        );
    }

    showLoader() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideLoader() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    submitButtonAction() {
        otpCode = otpCode == undefined ? "" : otpCode
        if (otpCode.trim().length == 0) {
            this.dropdown.alertWithType('error', "", "You have not entered Verification code");
            return;
        }

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }
        else {
            this.remoteVerifyCodeCall()
            Keyboard.dismiss()
        }
    }

    timerStop() {
        this.setState({
            isResendOTP: false
        })
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainerStyle: {
        top: TOP_BAR_HEIGHT
    },
    otpView: {
        paddingHorizontal: "5%",
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnSubmit: {
        top: 300,
        right: 28,
    },
    taglineMessage: {
        top: 75,
        fontSize: moderateScale(16, 0.09),
        margin: 20,
        textAlign: 'center',
        fontFamily:"BarlowCondensed-Regular",
        color:  DARK_BLUE_COLOR
    },
    submitButtonBaseStyle: {
        position: 'absolute',
        top: 160,
        alignSelf: 'center'
    },
    resendOTPView: {
        top: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    resendOTPButton: {
        height: 50,
        width: 80,
        alignItems: 'center',
        justifyContent: 'center'
    },
    resendOTPText: {
        textAlign: 'center',
        fontSize: moderateScale(14, 0.09),
        fontFamily: "BarlowCondensed-Regular",
    }
});