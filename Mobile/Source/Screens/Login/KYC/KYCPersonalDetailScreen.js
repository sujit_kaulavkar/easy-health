import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Keyboard,
    TouchableOpacity
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    SCREEN_WIDTH,
    TOP_BAR_HEIGHT,
    APP_THEME,
    NO_INTERNET_CONNECTION,
    SOMETHING_WRONG_MESSAGE
} from '../../../Helper/Constants';

import UserForm from '../../Settings/Manage Profile/UserForm';
import ProfilePicView from '../../Settings/Manage Profile/ProfilePicView';
import GenderView from '../../Settings/Manage Profile/GenderView';
import { moderateScale } from '../../../Helper/Scaling';

export default class KYCPersonalDetailScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isMale: true,
            imageObject: null,
            isShowSpinner: false,
            selectedDocument: null
        }

        showDocumentSelectionScreen = this.showDocumentSelectionScreen.bind(this)
    }

    remoteKYCPersonalDetails() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()

        const imageSting = this.refs.profilePic.getImageString()
        const title = this.refs.userForm.title()
        const firstname = this.refs.userForm.firstName()
        const lastname = this.refs.userForm.lastName()
        const gender = this.refs.gender.gender()
        const idNumber = this.refs.idNumber.getText()


        const user = Helper.USER_MANAGER.getInstance().getUser()
        const userObject = {}
        userObject["title"] = title
        userObject["first_name"] = firstname
        userObject["last_name"] = lastname
        userObject["gender"] = gender
        const params = JSON.stringify(userObject)

        const idType = this.state.selectedDocument.code
        const country = this.refs.country.getCountry()

        apiHelper.updateKYCUserDetails(user.user_hash_id, params, imageSting, idType, idNumber, country)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleKYCPersonalResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleKYCPersonalResponse(response) {
        const status = response.status;
        if (status == 200) {

            const user = Helper.USER_MANAGER.getInstance().getUser()
            user.firstName = this.refs.userForm.firstName()
            user.lastName = this.refs.userForm.lastName()
            user.title = this.refs.userForm.title()
            user.gender = this.refs.gender.gender()

            // const data = response.data.data
            // if (data) {
            //     const imageUrl = data.profile_pic_url
            //     if (imageUrl && imageUrl != null) {
            //         user.profile_pic_url = imageUrl
            //     }
            // } 

            Helper.USER_MANAGER.getInstance().setUser(user)
            Helper.DATABASE_HANDLER.getInstance().saveUserDetails(user)
            this.props.navigation.navigate("KYCResidentilAddressScreen")
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    render() {

        const { isFromRegistration } = this.props.navigation.state.params
        let isShowBack = true
        if (isFromRegistration == true) {
            isShowBack = false
        }

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <ScrollView
                    style={{ flex: 1 }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ paddingTop: TOP_BAR_HEIGHT + 20, flexGrow: 1 }}>

                    <View>
                        <ProfilePicView ref="profilePic" />
                    </View>

                    <View style={{ height: 180 }}>
                        <UserForm ref="userForm" />
                    </View>

                    <View style={{ marginTop: 80, marginHorizontal: SCREEN_WIDTH * 0.09 }}>
                        <GenderView ref="gender" />
                    </View>

                    <View>
                        <View style={{ height: 50, marginTop: 20, marginHorizontal: SCREEN_WIDTH * 0.09 }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={{ justifyContent: 'center', height: 49 }}
                                onPress={() => this.showDocumentSelectionScreen()}>
                                <Text style={{
                                    top: 0,
                                    color: 'rgba(30, 34, 170, 1.0)',
                                    fontSize: moderateScale(17, 0.09),
                                    fontFamily: 'BarlowCondensed-Regular'
                                }}>
                                    {this.state.selectedDocument == null ? "Identity Information" : this.state.selectedDocument.description}
                                </Text>
                            </TouchableOpacity>
                            <View style={{ top: 20, right: 20, position: 'absolute' }}>
                                <SimpleLineIcons
                                    name="arrow-down"
                                    size={18}
                                    color='white'
                                />
                            </View>

                            <View style={styles.separatorView} />
                        </View>

                        <View style={{ marginTop: 5 }}>
                            <Components.USER_INPUT
                                ref="idNumber"
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                customStyles={{ containerStyle: { marginHorizontal: SCREEN_WIDTH * 0.09 } }}
                            >ID number</Components.USER_INPUT>
                        </View>

                        <View style={{ marginTop: 15 }}>
                        <Text style={{                                                         
                                position:'absolute',
                                marginHorizontal: SCREEN_WIDTH * 0.09,
                                color: 'rgba(30, 34, 170, 0.5)',
                                fontSize: moderateScale(16, 0.09),
                                fontFamily: 'BarlowCondensed-Regular'
                        }}>
                            Country of issue
                        </Text>
                            <Components.COUNTRY_SELECTION ref="country" isDisableCountrySelection={false} />
                        </View>
                    </View>

                    <View style={styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title="Next"
                            onPress={() => this.nextButtonAction()}
                        />
                    </View>
                </ScrollView>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    isPortraitMode={true}
                    ref="topbarRef"
                    title='KYC - Personal Details'
                    isBackButtonNeeded={isShowBack}
                    isSkipNeeded={true}
                    backButtonAction={() => this.props.navigation.popToTop()}
                    skipButtonAction={() => this.props.navigation.popToTop()} />

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        )
    }

    profilePicButtonAction() {
        const imagePicker = new Components.IMAGE_PICKER()
        imagePicker.showImagePicker().then((imageData) => {
            this.setState({
                imageObject: imageData
            })
        })
            .catch(() => {

            })
    }

    radioButtonAction(gender) {
        this.setState({
            isMale: gender == "Male" ? true : false
        })
    }

    showDocumentSelectionScreen() {
        this.props.navigation.navigate("DocumentSelectionScreen", {
            handleSelectedDocument: (document) => this.handleSelectedDocument(document)
        })
    }

    handleSelectedDocument(document) {
        this.setState({
            selectedDocument: document
        })
    }

    nextButtonAction() {
        const firstname = this.refs.userForm.firstName()
        const lastname = this.refs.userForm.lastName()
        const idNumber = this.refs.idNumber.getText()
        const imageSting = this.refs.profilePic.getImageString()

        if (imageSting == undefined) {
            this.dropdown.alertWithType('error', "", "Upload your photo");
            return;
        }

        let validator = new Helper.VALIDATOR(firstname)
        let errorMessage = validator.validateFirstName()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(lastname)
        errorMessage = validator.validateLastName()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        if (this.state.selectedDocument == null) {
            this.dropdown.alertWithType('error', "", "Select Identity Information type");
            return;
        }
        
        validator = new Helper.VALIDATOR(idNumber)
        errorMessage = validator.validateIDNumber()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        Keyboard.dismiss()
        this.remoteKYCPersonalDetails()
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    profilePicViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 130
    },
    profilePicStyle: {
        height: 80,
        width: 80,
        borderRadius: 80,
        borderWidth: 1,
        borderColor: 'white',
    },
    submitButtonBase: {
        flex: 0.5,
        bottom: 20,
        position: 'absolute',
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    separatorView: {
        height: 1.5,
        marginBottom: 10,
        backgroundColor: 'white'
    }
})