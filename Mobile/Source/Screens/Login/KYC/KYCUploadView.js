import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import { moderateScale } from '../../../Helper/Scaling';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { BLUE_COLOR, SCREEN_WIDTH } from '../../../Helper/Constants';
import { Components } from '../../../Helper/FilePath';

const imageHeight = 170
const FRONT_SIDE_MESSAGE = "from your device of the front side of your NRIC ID"
const BACK_SIDE_MESSAGE = "from your device of the back side of your NRIC ID"

export default class KYCUploadView extends Component {

    constructor(props) {
        super(props)

        imageData = undefined
        this.state = {
            imageUrl: null
        }
    }
    render() {

        return (
            <View>
                {this.state.imageUrl != null ? this.changeDoc() : this.uploadNewDoc()}                
            </View>
        )
    }

    uploadNewDoc() {

        const { title, type } = this.props
        return (
            <View style={styles.subContainer}>
                <Text style={styles.titleStyle}>
                    {title}
                </Text>

                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.profilePicButtonAction()}
                    style={styles.idView}>                
                    <View style={styles.imageViewStyle}>
                        <FontAwesome
                            size={35}
                            name="image"
                            color={BLUE_COLOR}
                            backgroundColor="#3b5998"
                            {...iconStyles}
                        />

                        <FontAwesome
                            size={35}
                            color={BLUE_COLOR}
                            name="image"
                            backgroundColor="#3b5998"
                            {...iconStyles}
                        />
                    </View>

                    <View style={styles.messageViewStyle}>
                        <Text style={[styles.textStyle, { fontFamily: 'BarlowCondensed-Medium' }]}>
                            Take Picture<Text> </Text>
                            <Text style={[styles.textStyle]}>
                                or<Text> </Text>
                                <Text style={[styles.textStyle, { fontFamily: 'BarlowCondensed-Medium' }]}>
                                    Select File<Text> </Text>
                                    <Text style={[styles.textStyle]}>
                                        {type == 1 ? FRONT_SIDE_MESSAGE : BACK_SIDE_MESSAGE}    
                                    </Text>
                                </Text>
                            </Text>
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    changeDoc() {

        const { title, type } = this.props
        return (
            <ScrollView style={{
                marginHorizontal: 25,
                flexDirection: 'column'
            }}>

                <Text style={[styles.titleStyle, { marginTop: type == 2 ? 20 : 0}]}>
                    {title}
                </Text>

                <View style={styles.profilePicStyle}>
                    
                    <Image
                        defaultSource={require('../../Drawer/placeholder_image.png')}
                        style={{ height: "100%", width: "70%" }}
                        source={{ uri: this.state.imageUrl }}/>

                    <View style={{flex:1, justifyContent:'space-around'}}>

                        <View style={{ flex: 1, justifyContent:'center', alignItems:'center' }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => this.profilePicButtonAction()}
                                style={styles.reloadBtnStyle}>
                                <Text style={[styles.textStyle, {color:'white', fontSize: moderateScale(19, 0.09)}]}>Reupload</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => this.deleteButtonAction()}
                                style={styles.reloadBtnStyle}>
                                <Text style={[styles.textStyle, { color: 'white', fontSize: moderateScale(19, 0.09)}]}>Delete</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>           
            </ScrollView>
        )
    }

    profilePicButtonAction() {
        const imagePicker = new Components.IMAGE_PICKER()
        imagePicker.showImagePicker().then((imageData) => {                        
            this.setState({
                imageUrl: imageData.uri
            })

            this.imageData = imageData.data
        })
        .catch(() => {

        })
    }

    deleteButtonAction() {
        this.setState({
            imageUrl: null
        })

        this.imageData = undefined
    }

    getImageString() {
        return this.imageData
    }
}

const iconStyles = {
    borderRadius: 10,
    iconStyle: { paddingVertical: 2 },
    fontSize: moderateScale(18, 0.09),
};

const styles = StyleSheet.create({    
    subContainer: {
        height:120,
        marginHorizontal: 25,
        flexDirection: 'column'
    },
    idView: {
        flex: 1,
        top:10,
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 1,
        borderColor: BLUE_COLOR,
        borderStyle: "dashed"
    },
    titleStyle: {                
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: BLUE_COLOR,
    },
    imageViewStyle: {
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexDirection: 'row',
        width: 100
    },
    messageViewStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal:5
    },
    textStyle: {
        color: BLUE_COLOR,
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    },
    profilePicStyle: {
        height: imageHeight,        
        flexDirection:'row',
        width: SCREEN_WIDTH - 50,
    },
    reloadBtnStyle: {
        height: 50,        
        width:100,
        alignItems:'center',
        justifyContent:'center',
    }
})