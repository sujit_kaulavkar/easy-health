import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {    
    TOP_BAR_HEIGHT, 
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION
} from '../../../Helper/Constants';

import DropdownAlert from 'react-native-dropdownalert';

export default class KYCResidentilAddressScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isMale: true,
            imageObject: null,
            isShowSpinner: false
        }
    }

    remoteChangeAddressCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()

        const user = Helper.USER_MANAGER.getInstance().getUser()        
        const addressType = "residential"
        const address1 = this.refs.addressLin1.getText()
        const address2 = this.refs.addressLin2.getText()
        const city = this.refs.city.getText()
        const state = this.refs.state.getText()
        const countryCode = this.refs.country.getCountryCode()
        const country = this.refs.country.getCountry()
        const zipCode = this.refs.zipCode.getText()

        apiHelper.addUpdateAddress(user.user_hash_id, addressType, address1, address2, city, state, country, countryCode, zipCode)
            .then((responseJson) => {                                
                this.hideSpinner()
                if (responseJson) {

                    this.updateAddress()
                    this.handleChangeAddressResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleChangeAddressResponse(response) {
        const status = response.status;
        if (status == 1) {
            this.props.navigation.navigate("KYCUploadIDProofScreen")
        }
        else {
            const errorMessage = response.errorMessage;
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    updateAddress() {
        var address = {}
        address["address_line_1"] = this.refs.addressLin1.getText()
        address["address_line_2"] = this.refs.addressLin2.getText()
        address["city"] = this.refs.city.getText()
        address["state"] = this.refs.state.getText()
        address["country"] = this.refs.country.getCountry()
        address["country_code"] = this.refs.country.getCountryCode()
        address["zip_code"] = this.refs.zipCode.getText()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        user.address.residential = address

        Helper.USER_MANAGER.getInstance().setUser(user)
        Helper.DATABASE_HANDLER.getInstance().saveUserDetails(user)
    }

    render() {

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const address = user.address.residential

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <ScrollView
                    style={{ flex: 1 }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ paddingTop: TOP_BAR_HEIGHT, flexGrow: 1 }}>

                    <View style={{ height: 360 }}>

                        <Components.USER_INPUT
                            ref="addressLin1"                            
                            returnKeyType={'next'}
                            defaultText={address == undefined ? "" : address.address_line_1}
                        >Address Line 1</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="addressLin2"                            
                            returnKeyType={'next'}
                            defaultText={address == undefined ? "" : address.address_line_2}
                        >Address Line 2</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="city"                            
                            returnKeyType={'next'}
                            defaultText={address == undefined ? "" : address.city}
                        >City</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="state"                            
                            returnKeyType={'next'}
                            defaultText={address == undefined ? "" : address.state}
                        >State</Components.USER_INPUT>

                        <Components.COUNTRY_SELECTION ref="country" />

                        <Components.USER_INPUT
                            ref="zipCode"
                            returnKeyType={'done'}
                            defaultText={address == undefined ? "" : address.zip_code}
                        >Zip Code</Components.USER_INPUT>

                    </View>

                    <View style={styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title="Next"
                            onPress={() => this.nextButtonAction()}
                        />
                    </View>
                </ScrollView>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    isPortraitMode={true}
                    ref="topbarRef"
                    title='KYC - Residential Address'
                    isBackButtonNeeded={true}
                    isSkipNeeded={true}
                    backButtonAction={() => this.props.navigation.goBack()}
                    skipButtonAction={() => this.props.navigation.popToTop()} />

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        )
    }

    nextButtonAction() {

        const addressLin1 = this.refs.addressLin1.getText()
        const addressLin2 = this.refs.addressLin2.getText()
        const city = this.refs.city.getText()
        const state = this.refs.state.getText()
        const zip = this.refs.zipCode.getText()

        let validator = new Helper.VALIDATOR(addressLin1)
        let errorMessage = validator.validateAddressLine1()
        if (errorMessage.length != 0) {
            console.log("addressLin1");            
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(addressLin2)
        errorMessage = validator.validateAddressLine2()
        if (errorMessage.length != 0) {
            console.log("addressLin2");
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(city)
        errorMessage = validator.validateCity()
        if (errorMessage.length != 0) {
            console.log("city");
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(state)
        errorMessage = validator.validateState()
        if (errorMessage.length != 0) {
            console.log("state");
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(zip)
        errorMessage = validator.validateZIPCode()
        if (errorMessage.length != 0) {
            console.log("zip");
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        this.remoteChangeAddressCall()        
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() {}
}

// Mr Mrs Miss Dr Madam 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    profilePicViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 130
    },
    profilePicStyle: {
        height: 80,
        width: 80,
        borderRadius: 80,
        borderWidth: 1,
        borderColor: 'gray',
    },
    submitButtonBase: {
        flex: 0.5,
        bottom: 20,
        position:'absolute',
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    separatorView: {
        height: 1.5,
        marginBottom: 10,
        backgroundColor: '#A0A0A0'
    }
})