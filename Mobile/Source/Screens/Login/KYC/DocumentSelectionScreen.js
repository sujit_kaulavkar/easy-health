import React, { Component } from 'react';
import {
    View,
    Text,
    Keyboard,
    FlatList,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    moderateScale
} from '../../../Helper/Scaling';

import {
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    BLUE_COLOR
} from '../../../Helper/Constants';

import DropdownAlert from 'react-native-dropdownalert';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default class DocumentSelectionScreen extends Component {

    constructor() {
        super()

        this.state = {
            documents: [],
            selectedDocument: undefined,
            isShowSpinner: false
        }

        didSelectItemFromCell = this.didSelectItemFromCell.bind(this)
    }

    componentDidMount() {
        this.remoteFetchDocuments()
        this.refs.submitButton.disableSubmitButton()
    }

    remoteFetchDocuments() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        apiHelper.documentIds().then((responseJson) => {
            this.hideSpinner()
            if (responseJson) {
                this.handleDocumentResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
        .catch((error) => {
            this.hideSpinner()
            this.showServerErrorMessage()
        })
    }

    handleDocumentResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            let documents = data.id_types
            this.setState({
                documents: documents
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Select Document Type'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        Keyboard.dismiss()
                        this.props.navigation.goBack()
                    }} />

                <View style={styles.flatListContainerStyle}>
                    <FlatList
                        style={styles.flatListStyle}
                        data={this.state.documents}
                        key="documentsFlatList"
                        extraData={this.state}
                        keyExtractor={(item) => item.code}
                        renderItem={({ item, index }) => this.renderCellItem(item, index)}
                    />
                </View>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title="Done"
                        onPress={() => this.proceedButtonAction()}
                    />
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        )
    }

    renderCellItem(item) {
        const selectedDocument = this.state.selectedDocument
        let code = -1
        if (selectedDocument != undefined) {
            code = selectedDocument.code
        }

        return (
            <View style={[styles.cellContainerStyle, { marginTop: 6, width: SCREEN_WIDTH - 70, height: 60}]}>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                    <View style={{ flex: 1 }}>
                        <Text style={styles.postTitleStyle}>
                            {item.description}
                        </Text>
                    </View>

                    <View style={{ alignSelf: 'flex-end', alignItems:'center', justifyContent: 'center', flexDirection: 'row', height: 50, width: 50 }}>
                        <MaterialIcons
                            name={code == item.code ? "radio-button-checked" : "radio-button-unchecked"}
                            color="white"
                            size={25}
                        />
                    </View>
                </View>

                <View style={{ position: 'absolute', bottom: 0, height: 1, width: SCREEN_WIDTH, backgroundColor: 'rgba(255,255,255,0.5)' }} />
                <TouchableOpacity style={{ flex: 1, zIndex: 0, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} onPress={() => this.didSelectItemFromCell(item)} />
            </View>
        )
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    proceedButtonAction() {        
        const { handleSelectedDocument } = this.props.navigation.state.params
        handleSelectedDocument(this.state.selectedDocument)

        this.props.navigation.goBack()
    }

    didSelectItemFromCell = (item) => {
        this.setState({
            selectedDocument: item
        })

        this.refs.submitButton.enableSubmitButton()
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    handleBackFromLogin = () => {
        return true;
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    flatListContainerStyle: {
        flex: 1,
        marginTop: 60,
        marginBottom: 100
    },
    flatListStyle: {
        flex: 1,
        marginHorizontal: 35
    },
    cellContainerStyle: {
        left: 0,
        right: 0,
        justifyContent: 'center'
    },
    postTitleStyle: {
        left: 15,
        fontSize: moderateScale(18, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: BLUE_COLOR,
        textAlignVertical: 'top',
    }
})