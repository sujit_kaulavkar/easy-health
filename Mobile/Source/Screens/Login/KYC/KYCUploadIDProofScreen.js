import React, { Component } from 'react';
import {
    View,
    Alert,
    StyleSheet,
} from 'react-native';

import { 
    TOP_BAR_HEIGHT, 
    NO_INTERNET_CONNECTION, 
    SOMETHING_WRONG_MESSAGE, 
    APP_THEME,
    APP_NAME,
    OK_TEXT
} from "../../../Helper/Constants";

import {
    Components,
    KYC,
    Helper
} from '../../../Helper/FilePath';

import DropdownAlert from 'react-native-dropdownalert';

export default class KYCUploadIDProofScreen extends Component {

    constructor() {
        super() 

        this.state = {
            isShowSpinner: false
        }
    }

    remoteKYCUploadDocument() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()

        const frontImageString = this.refs.frontImageView.getImageString()
        const backImageString = this.refs.backImageView.getImageString()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.uploadKYCDocument(user.user_hash_id, frontImageString, backImageString)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleKYCPersonalResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleKYCPersonalResponse(response) {
        const status = response.status;
        if (status == 200) {
            
            Alert.alert(APP_NAME, "Documents uploaded successfully. You will get approval witin 24 hrs. If not get approval then contact customer care", [
                {
                    text: OK_TEXT, onPress: () => this.props.navigation.popToTop()
                }
            ], { cancelable: false })
        }
        else {
            const errorMessage = response.errorMessage;
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    render() {        
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <View style={{ flex: 0.5, top: TOP_BAR_HEIGHT + 20, justifyContent:'space-between'}}>
                    <KYC.UPLOAD_VIEW ref="frontImageView" title="Front Side" type={1}/>
                    <KYC.UPLOAD_VIEW ref="backImageView" title="Back Side" type={2}/>
                </View>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    isPortraitMode={true}
                    ref="topbarRef"
                    title='KYC - Upload ID Proofs'
                    isBackButtonNeeded={true}
                    isSkipNeeded={true}
                    backButtonAction={() => this.props.navigation.goBack()}
                    skipButtonAction={() => this.props.navigation.popToTop()} />                

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title="Submit"
                        onPress={() => this.nextButtonAction()}
                    />
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        )
    }

    nextButtonAction() {
        const frontImageString = this.refs.frontImageView.getImageString()
        const backImageString = this.refs.backImageView.getImageString()

        if (frontImageString == undefined) {
            this.dropdown.alertWithType("error", "", "Upload front side photo")
            return;
        }

        if (backImageString == undefined) {
            this.dropdown.alertWithType("error", "", "Upload back side photo")
            return;
        }

        this.remoteKYCUploadDocument()
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    submitButtonBase: {
        flex:1,
        bottom:20,
        position:'absolute',
        flexDirection: 'row',
        alignItems: 'flex-end'
    }
})