import { requireNativeComponent } from 'react-native';

// requireNativeComponent automatically resolves 'RNTMap' to 'RNTMapManager'
module.exports = requireNativeComponent('RNTKYC', null);


// import React, { Component } from 'react';
// import {
//     View,
//     NativeModules
// } from 'react-native';

// import {
//     Components,
//     Helper
// } from '../../../Helper/FilePath';

// var KYC = NativeModules.KYCController;

// export default class KYCScreen extends Component {
//     render() {
//         // const userInfo = Helper.USER_MANAGER.getInstance().getUser()        
//         return <KYC userHashId={"sdh8489237823748327489"} />
//     }
// }
