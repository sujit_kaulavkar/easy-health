import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Keyboard,
    Alert,
    Text,
    TouchableOpacity,
    TextInput
} from 'react-native';

import {
    Components,
    Helper,
    LOGIN
} from '../../Helper/FilePath';

import GenderView from '../Settings/Manage Profile/GenderView';
import Icon from 'react-native-vector-icons/FontAwesome';
import DropdownAlert from 'react-native-dropdownalert';

const viewHeight = 750;

import {
    APP_NAME,
    APP_THEME,
    OK_TEXT,
    SOMETHING_WRONG_MESSAGE,
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    socialType,
    NO_INTERNET_CONNECTION,
    OTP_SEND_MESSAGE,
    OTP_OPTIONS,
    BLUE_COLOR,
    DARK_BLUE_COLOR
} from '../../Helper/Constants.js';

import {
    moderateScale
} from '../../Helper/Scaling';

var currentSocialType = socialType["noOptionSelected"]

export default class RegistrationScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isTouchEnabled: true,
            isShowSpinner: false
        }

        loginToFacebook = this.loginToFacebook.bind(this)
        loginToGoogle = this.loginToGoogle.bind(this)
        registrationButtonAction = this.registrationButtonAction.bind(this)
        socialLoginSuccessful = this.socialLoginSuccessful.bind(this)
        showTNCPage = this.showTNCPage.bind(this)
    }

    remoteRegistrationCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showLoader()

        const mobileNumber = this.refs.mobilenumber.getText()
        const firstname = this.refs.firstname.getText()
        const emailAddress = this.refs.emailAddress.getText()
        const age = this.refs.age.getText()
        const gender = this.refs.gender.gender()
        const address = this.refs.address._lastNativeText
        const password = this.refs.password.getText()

        apiHelper.registration(firstname, emailAddress, mobileNumber, age, gender, address, password)
            .then((responseJson) => {
                this.hideLoader()

                if (responseJson) {
                    this.handleRegistrationResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleRegistrationResponse(response) {        
        const status = response.success;
        if (status == true) {
            this.showLoginScreen()
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
        }
    }

    remoteSocialLoginCall(firstName, lastName, socialId) {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }
        this.showLoader()

        apiHelper.socialLogin(firstName, lastName, socialId, currentSocialType)
            .then((responseJson) => {

                currentSocialType = socialType["noOptionSelected"]
                this.hideLoader()
                if (responseJson) {
                    this.handleSocialLoginResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch(() => {

                currentSocialType = socialType["noOptionSelected"]
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleSocialLoginResponse(response) {
        const status = response["status"];
        if (status == true) {
            const userData = response["data"];
            Helper.USER_MANAGER.getInstance().setUser(userData)
            const userDataString = JSON.stringify(userData)
            Helper.DATABASE_HANDLER.getInstance().saveUserDetails(userDataString)
            this.socialLoginSuccessful(userData)
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
        }
    }

    socialLoginSuccessful() {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE
                isShowMMLogo={false}
                style={styles.container}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <View style={styles.subContainerStyle}>

                    <ScrollView
                        style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                        keyboardShouldPersistTaps="always"
                        contentContainerStyle={{ flexGrow: 1, paddingBottom: 10, paddingTop: 10 }}>

                        <View style={styles.userInput}>

                            <Components.USER_INPUT
                                ref="firstname"
                                autoCapitalize={'none'}
                                returnKeyType={'done'}
                                autoCorrect={true}
                            >FULL Name</Components.USER_INPUT>

                            {/* <Components.USER_INPUT
                                ref="lastname"
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={true}
                            >Last Name</Components.USER_INPUT> */}

                            <Components.USER_INPUT
                                ref="emailAddress"
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                            >Email Address</Components.USER_INPUT>

                            <Components.USER_INPUT
                                ref="mobilenumber"
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                            >MOBILE NUMBER</Components.USER_INPUT>

                            <Components.USER_INPUT
                                ref="age"
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                            >AGE (IN YEARS)</Components.USER_INPUT>

                            <View style={{ marginTop: 20, marginHorizontal: SCREEN_WIDTH * 0.09 }}>
                                <GenderView ref="gender" />
                            </View>

                            <View style={{
                                marginTop: 20,
                                marginHorizontal: SCREEN_WIDTH * 0.09,
                            }}>
                                <Text style={{
                                    color: "rgba(30, 34, 170, 0.7)",
                                    fontSize: moderateScale(18, 0.09),
                                    fontFamily: 'BarlowCondensed-Medium',
                                }}>ADDRESS</Text>
                                <TextInput
                                    multiline={true}
                                    numberOfLines={100}
                                    ref="address"
                                    style={{
                                        color:DARK_BLUE_COLOR,
                                        paddingHorizontal: 5,
                                        textAlignVertical: 'top',
                                        height: 60,
                                        marginTop: 10,
                                        borderColor: 'rgba(0,0,0,0.3)',
                                        borderWidth: 1,
                                        borderRadius: 10
                                    }} />
                            </View>

                            <Components.USER_INPUT
                                ref="password"
                                secureTextEntry={true}
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                rightImage={true}
                            >Password</Components.USER_INPUT>

                            <Components.USER_INPUT
                                ref="confirmpassword"
                                secureTextEntry={true}
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                rightImage={true}
                            >Confirm Password</Components.USER_INPUT>

                            <LOGIN.TNC_VIEW ref="tncView" showTNCPage={() => this.showTNCPage()} />

                            <Components.BUTTON_SUBMIT
                                ref="submitButton"
                                activeOpacity={0.7}
                                style={styles.btnSubmit}
                                title="SIGN UP"
                                onPress={() => this.registrationButtonAction()} />
                        </View>

                        <View style={styles.signUpBase}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={{ flexDirection: 'row', alignSelf: 'flex-end', paddingBottom: 10 }}
                                onPress={() => this.goBack()}>
                                <Text style={styles.dontHaveAccountText}>Already have an Account ?</Text>
                                <Text style={styles.signUpText}>Sign In Now</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>

                    <Components.TOP_BAR
                        topbarTranslate={0}
                        isPortraitMode={true}
                        ref="topbarRef"
                        title='SIGN UP'
                        isBackButtonNeeded={true}
                        backButtonAction={() => this.props.navigation.goBack()} />

                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        );
    }

    showTNCPage() {
        this.props.navigation.navigate("TNCScreen")
    }

    showSpinner() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideSpinner() {
        if (this.state.isTouchEnabled == false) {
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    showLoader() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideLoader() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    goBack() {
        this.props.navigation.goBack()
    }

    registrationButtonAction() {
        const firstname = this.refs.firstname.getText()
        const mbNumber = this.refs.mobilenumber.getText()
        const emailAddress = this.refs.emailAddress.getText()
        const age = this.refs.age.getText()
        const address = this.refs.address._lastNativeText == undefined ? "" : this.refs.address._lastNativeText.trim()
        const password = this.refs.password.getText()
        const confirmPassword = this.refs.confirmpassword.getText()

        let validator = new Helper.VALIDATOR(firstname)
        let errorMessage = validator.validateFirstName()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(emailAddress)
        errorMessage = validator.validateEmailAddress()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(mbNumber)
        errorMessage = validator.validateMobileNumber()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(age)
        errorMessage = validator.validateAge()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(address)
        errorMessage = validator.validateAddress()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(password)
        errorMessage = validator.validatePassword()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(confirmPassword)
        errorMessage = validator.validateConfirmPassword(password)
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        const tncStatus = this.refs.tncView.isTNCSelected()
        if (tncStatus == false) {
            this.dropdown.alertWithType('error', "", "You need to accept the Terms and Conditions");
            return;
        }

        Keyboard.dismiss()
        this.remoteRegistrationCall()
    }

    showLoginScreen() {
        setTimeout(() => {
            Alert.alert(APP_NAME, "Your registration is successful", [
                {
                    text: OK_TEXT, onPress: () => {
                        this.props.navigation.goBack()
                    }
                }
            ], { cancelable: false })
        }, 500);
    }

    showMobileNumberVerificationScreen() {
        
        const emailAddress = this.refs.emailAddress.getText()
        setTimeout(() => {
            Alert.alert(APP_NAME, OTP_SEND_MESSAGE, [
                {
                    text: OK_TEXT, onPress: () => {
                        this.props.navigation.navigate('OTPVerificationScreen', {
                            emailAddress: emailAddress
                        })
                    }
                }
            ], { cancelable: false })
        }, 500);
    }

    // Social Login
    loginToFacebook() {

        if (currentSocialType == socialType["noOptionSelected"]) {
            currentSocialType = socialType["Facebook"]
            Helper.FACEBOOK_HANDLER.getInstance().loginToFacebook().then((result, reject) => {
                if (result) {
                    const firstName = result["last_name"]
                    const lastName = result["last_name"]
                    const socialId = result["id"]

                    setTimeout(() => {
                        this.remoteSocialLoginCall(firstName, lastName, socialId)
                    }, 500);
                }
                else if (reject) {
                    currentSocialType = socialType["noOptionSelected"]
                    this.dropdown.alertWithType('error', "", "Problem in login to Facebook. Try again");
                }
            })
        }
    }

    loginToGoogle() {

        if (currentSocialType == socialType["noOptionSelected"]) {
            currentSocialType = socialType["Google"]
            Helper.GOOGLE_HANDLER.getInstance().loginToGoogle().then((result, reject) => {
                if (result) {
                    const firstName = result.user.givenName
                    const lastName = result.user.familyName
                    const socialId = result.user.id

                    setTimeout(() => {
                        this.remoteSocialLoginCall(firstName, lastName, socialId)
                    }, 500);
                }
                else if (reject) {
                    currentSocialType = socialType["noOptionSelected"]
                    this.dropdown.alertWithType('error', "", "Problem in login to Google. Try again");
                }
            })
        }
    }

    onCloseDropdown() { }
}

const iconStyles = {
    borderRadius: 10,
    iconStyle: { paddingVertical: 2 },
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    subContainerStyle: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent'
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },
    signupButton: {
        backgroundColor: "#FF4DFF",
    },
    leftViewStyle: {
        width: 45,
        height: 50,
        position: 'absolute',
        backgroundColor: 'transparent',
        left: 10,
        top: 10
    },
    menuImgStyle: {
        width: 25,
        height: 20,
        resizeMode: 'contain',
    },
    userInput: {
        height: viewHeight,
        flexDirection: 'column',
        backgroundColor: 'transparent'
    },
    btnSubmit: {
        bottom: 0,
        right: 28,
    },
    signUpBase: {  
        top:-30,      
        height: 50,
        width: SCREEN_WIDTH,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    dontHaveAccountText: {
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        textAlign: 'center',
        color: 'rgba(0,0,0,0.5)'
    },
    signUpText: {
        color: 'rgba(0,0,0,0.5)',
        paddingLeft: 5,
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'BarlowCondensed-SemiBold'
    },
    buttons: {
        justifyContent: 'space-between',
        flexDirection: 'column',
        top: 10,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.2, 0.09),
        height: 90,
        backgroundColor: 'transparent'
    }
});