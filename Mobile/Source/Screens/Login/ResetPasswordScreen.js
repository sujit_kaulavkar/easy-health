import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Keyboard,
    Alert,
    StatusBar
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

const viewHeight = 120;

import {
    APP_THEME,
    APP_NAME,
    OK_TEXT,
    NO_INTERNET_CONNECTION,
    PASSWORD_RESET_SUCCESSFUL_MESSAGE
} from '../../Helper/Constants.js';

export default class ResetPasswordScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isTouchEnabled: true
        }

        resetPasswordButtonAction = this.resetPasswordButtonAction.bind(this)
    }

    remoteResetPasswordCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        const password = this.refs.password.getText().trim()

        const params = this.props.navigation.state.params;
        console.log("params", params);
        
        const mobileNumber = params.email;
        apiHelper.resetPassword(mobileNumber, password)
            .then((responseJson) => {

                this.hideSpinner()

                if (responseJson) {
                    this.handleNavigation(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch(() => {

                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleNavigation(response) {
        const status = response["status"];
        if (status == 1) {
            this.showLoginScreen()
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE
                style={styles.container}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <View style={styles.subContainerStyle}>

                    <Components.TOP_BAR
                        topbarTranslate={0}
                        isPortraitMode={true}
                        ref="topbarRef"
                        title='Reset Password'
                        isBackButtonNeeded={true}
                        backButtonAction={() => {
                            this.props.navigation.goBack()
                        }}
                    />

                    <View style={styles.userInput}>

                        <Components.USER_INPUT
                            ref="password"
                            secureTextEntry={true}
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            rightImage={true}
                        >Enter New Password</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="confirmpassword"
                            secureTextEntry={true}
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            rightImage={true}
                        >Reenter New Password</Components.USER_INPUT>
                    </View>

                    <View style={{
                        top: 130
                    }}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            style={styles.btnSubmit}
                            title="Reset"
                            onPress={() => this.resetPasswordButtonAction()}
                        />
                    </View>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        );
    }

    showSpinner() {
        if (this.state.isTouchEnabled == true) {
            this.refs.submitButton.startAnimation()
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideSpinner() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    resetPasswordButtonAction() {

        const password = this.refs.password.getText()
        const confirmPassword = this.refs.confirmpassword.getText()

        let validator = new Helper.VALIDATOR(password)
        errorMessage = validator.validatePassword()
        if (errorMessage.length != 0) {

            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(confirmPassword)
        errorMessage = validator.validateConfirmPassword(password)
        if (errorMessage.length != 0) {

            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        Keyboard.dismiss()
        this.remoteResetPasswordCall()
    }

    showLoginScreen = () => {

        Alert.alert(APP_NAME, PASSWORD_RESET_SUCCESSFUL_MESSAGE, [
            { text: OK_TEXT, onPress: () => this.props.navigation.pop(3) }
        ], { cancelable: false })
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    subContainerStyle: {
        flex: 1
    },
    userInput: {
        top: 100,
        height: viewHeight,
        flexDirection: 'column'
    },
    btnSubmit: {
        bottom: 0,
        right: 28,
    },
});