import React, { Component } from 'react';
import {
    View,
    Image,
} from 'react-native';

import {
    Helper
} from '../../Helper/FilePath';

import SplashScreen from 'react-native-splash-screen';
import Events from 'react-native-simple-events';
export default class MySplashScreen extends Component {

    constructor() {
        super()

        this.showHomeScreen = this.showHomeScreen.bind(this)
        this.showLoginScreen = this.showLoginScreen.bind(this)
    }

    componentDidMount() {
        SplashScreen.hide();
        Events.trigger('lockDrawer')
        Helper.DATABASE_HANDLER.getInstance().getUserDetails().then((userInfo) => {            
            if (userInfo) {
                Helper.USER_MANAGER.getInstance().setUser(userInfo)
                setTimeout(() => {
                    this.showHomeScreen()
                }, 1000)
            }
            else {
                this.showLoginScreen()
            }
        }).catch((error) => {
            this.showLoginScreen()
        })

        console.disableYellowBox = true;
    }

    render() {
        return (
            <View style={{flex:1, backgroundColor:'white', justifyContent:'center', alignItems:'center'}}>
                <Image 
                style={{width:250, height:250}}
                    source={require('../../Resources/Images/splash.png')} />
            </View>
        )
    }

    showHomeScreen = () => {
        this.props.navigation.replace('DrawerNavigation', { "deepLink": null });
    }

    showLoginScreen = () => {
        this.props.navigation.replace('TutorialScreen');
    }
}