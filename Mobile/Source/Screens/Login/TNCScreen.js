
import React, { Component } from 'react';
import {
    View,
    StyleSheet
} from 'react-native';

import {
    Helper,
    Components
} from '../../Helper/FilePath';
import { WebView } from "react-native-webview";

import {
    APP_THEME, 
    TOP_BAR_HEIGHT
} from '../../Helper/Constants';

export default class TNCScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isShowSpinner: false
        }

        payload = {
            uri: "https://www.google.com"
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Terms and Conditions'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}/>

                <View style={styles.subContainer}>
                    <WebView
                        style={{ flex: 1 }}
                        allowUniversalAccessFromFileURLs={true}
                        allowFileAccess={true}
                        useWebKit={true}
                        onLoadStart={() => this.showSpinner()}
                        onLoad={() => this.hideSpinner()}
                        source={payload} />
                </View>
            </Components.BACKGROUND_IMAGE>
        );
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }
}

const styles = StyleSheet.create({
    subContainer: {
        flex: 1,
        marginTop: TOP_BAR_HEIGHT,
        backgroundColor: 'red'
    }
})