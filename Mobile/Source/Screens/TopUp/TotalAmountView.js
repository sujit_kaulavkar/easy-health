
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import { moderateScale } from "../../Helper/Scaling";

export default class TotalAmountView extends Component {
    render() {

        const {topupAmount, transferFee} = this.props;
        const totalAmount = (topupAmount + transferFee).toFixed(2)

        return (
            <View style={styles.container}>
                <View style={styles.subContainer}>

                    <View style={styles.amountViewStyle}>
                        <Text style={styles.textStyle}>
                            Amount
                        </Text>

                        <Text style={styles.textStyle}>
                            {topupAmount} SGD
                        </Text>
                    </View>

                    <View style={styles.transferFeeStyle}>
                        <Text style={styles.textStyle}>
                            Transfer Fees
                        </Text>

                        <Text style={styles.textStyle}>
                            {transferFee} SGD
                        </Text>
                    </View>

                    <View style={[styles.totalAmountMainViewStyle, { borderTopColor: (this.props.separatorLineColor ? this.props.separatorLineColor : "#F0F0F0")}]}>
                        <View style={styles.totalAmountViewStyle}>
                            <Text style={[styles.textStyle, { color: 'white'}]}>
                                Total Amount
                        </Text>

                            <Text style={[styles.textStyle, { color: 'white' }]}>
                                {totalAmount} SGD
                        </Text>
                        </View>
                    </View>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {        
        height: 120,
        flexDirection: 'column',
        marginHorizontal: 5,
        justifyContent: 'space-between'
    },
    amountViewStyle: {
        flexDirection: 'row',
        marginHorizontal: 15,
        justifyContent: 'space-between'
    },
    transferFeeStyle: {
        top: -5,
        flexDirection: 'row',
        marginHorizontal: 15,
        justifyContent: 'space-between'
    },
    textStyle: {
        fontSize: moderateScale(14, 0.09),
        color:'white',
        fontFamily: 'OpenSans-Regular'
    },
    totalAmountMainViewStyle: {
        height: 45,
        borderBottomColor: '#F0F0F0',
        borderBottomWidth: 1,        
        borderTopWidth: 1,
    },
    totalAmountViewStyle: {
        flexDirection: 'row',
        marginHorizontal: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '100%'
    }
})