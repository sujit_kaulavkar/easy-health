
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';
import Events from 'react-native-simple-events';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { format } from 'date-fns'
import { Components, TOP_UP, Helper } from "../../Helper/FilePath";
import { moderateScale } from '../../Helper/Scaling';
import {
    TOP_BAR_HEIGHT,
    DrawerItems,
    BLUE_COLOR,
    APP_THEME,
    PAYMENT_STATUS,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION
} from '../../Helper/Constants';

const SUCCESS_STATUS = "Success"
const SUCCESS_MESSAGE = "Payment Successful"
const TOPUP_SUCCESS = "successful"

const PENDING_STATUS = "Pending"
const PENDING_MESSAGE = "Payment Pending"
const PENDING_SUCCESS = "Pending"

const FAILURE_STATUS = "Failure"
const FAILURE_MESSAGE = "Payment Failed"
const TOPUP_FAILURE = "failed"

export default class PaymmentSuccessScreen extends Component {

    constructor(props) {
        super(props)

        const { paymentStatus } = props.navigation.state.params

        let status = ""
        let sucessMessage = ""
        let topupMessage = ""

        switch (paymentStatus) {
            case PAYMENT_STATUS.SUCCESSFUL: {
                status = SUCCESS_STATUS
                sucessMessage = SUCCESS_MESSAGE
                topupMessage = TOPUP_SUCCESS
            }
                break;
            case PAYMENT_STATUS.PENDING: {
                status = PENDING_STATUS
                sucessMessage = PENDING_MESSAGE
                topupMessage = PENDING_SUCCESS
            }
                break;
            case PAYMENT_STATUS.FAILURE: {
                status = FAILURE_STATUS
                sucessMessage = FAILURE_MESSAGE
                topupMessage = TOPUP_FAILURE
            }
                break;
            default:
                break;
        }

        this.state = {
            paymentStatus: status,
            sucessMessage: sucessMessage,
            topupMessage: topupMessage
        }
    }

    componentDidMount() {
        const { paymentStatus } = this.props.navigation.state.params
        if (paymentStatus == PAYMENT_STATUS.PENDING) {
            this.remotePaymentStatusCall()
        }
    }

    remotePaymentStatusCall() {
        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const { transactionId } = this.props.navigation.state.params
        const user = Helper.USER_MANAGER.getInstance().getUser()

        apiHelper.checkPaymentStatus(user.user_hash_id, transactionId)
            .then((responseJson) => {
                if (responseJson) {
                    this.handlePaymentStatusResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.showServerErrorMessage()
            })
    }

    handlePaymentStatusResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const paymentStatus = responseJson.data.status
            switch (paymentStatus) {
                case PAYMENT_STATUS.SUCCESSFUL:
                    this.setState({
                        paymentStatus: SUCCESS_STATUS,
                        sucessMessage: SUCCESS_MESSAGE,
                        topupMessage: TOPUP_SUCCESS
                    })
                    break;

                case PAYMENT_STATUS.PENDING:
                    this.remotePaymentStatusCall()
                    break;

                case PAYMENT_STATUS.FAILURE:
                    this.setState({
                        paymentStatus: FAILURE_STATUS,
                        sucessMessage: FAILURE_MESSAGE,
                        topupMessage: TOPUP_FAILURE
                    })
                    break;
            }
        }
        else {
            const errorMessage = responseJson["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    render() {

        const { topupAmount, transferFee, transactionId } = this.props.navigation.state.params
        const { paymentStatus, sucessMessage, topupMessage } = this.state
        const amount = topupAmount + transferFee
        const date = new Date()
        const dateString = format(date, 'DD MMM YYYY HH:MM')

        return (
            <Components.BACKGROUND_IMAGE
                isShowMMLogo={true}
                style={styles.container}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title={paymentStatus.toUpperCase()}
                    isBackButtonNeeded={true}
                    isRightCrossNeeded={true}
                    backButtonAction={() => {
                        this.goToDashboard()
                    }}
                    crossButtonAction={() => {
                        this.goToDashboard()
                    }}
                />

                <ScrollView
                    style={{ flex: 1, top: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1, paddingBottom: TOP_BAR_HEIGHT }}>

                    <View style={styles.subContainer}>

                        <MaterialCommunityIcons
                            name="check-circle-outline"
                            color="white"
                            size={130}
                            style={{ alignSelf: 'center' }}
                        />

                        <Text style={styles.cardActivatedText}>{sucessMessage}</Text>
                        <Text style={styles.cardVisitText}>Your topup order for SGD {amount} has {topupMessage}</Text>
                    </View>

                    <View style={styles.amountBaseStyle}>
                        <Text style={styles.receiptStyle}>
                            Transaction Receipt
                        </Text>

                        <View style={{
                            marginHorizontal: 40,
                            top: -60,
                            paddingTop: 20,
                            flex: 1,
                            paddingHorizontal: -10,
                        }}>

                            <TOP_UP.TOTAL_AMOUNT_VIEW
                                separatorLineColor="#D3D3D3"
                                topupAmount={parseFloat(topupAmount)}
                                transferFee={parseFloat(transferFee)} />
                        </View>
                    </View>

                    <View style={styles.paymentReferenceViewStyle}>

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>Payment Reference ID</Text>
                            <Text style={styles.referenceDetailStyle}>{transactionId}</Text>
                        </View>

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>TimeStamp</Text>
                            <Text style={styles.referenceDetailStyle}>{dateString}</Text>
                        </View>

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>Status</Text>
                            <Text style={styles.referenceDetailStyle}>{paymentStatus}</Text>
                        </View>

                    </View>
                </ScrollView>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    goToDashboard() {
        Events.trigger('ChangeDrawerMenu', { id: DrawerItems.DASHBOARD })
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardActivatedText: {
        color: 'white',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'Ubuntu-Medium'
    },
    cardVisitText: {
        paddingTop: 5,
        marginHorizontal: 30,
        textAlign: 'center',
        color: 'white',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    amountBaseStyle: {
        height: 270,
        top: 5,
        justifyContent: 'center'
    },
    paymentReferenceViewStyle: {
        top: -30,
        height: 160,
        marginHorizontal: 40,
        paddingHorizontal: 20,
        backgroundColor: 'white',
        justifyContent: 'space-evenly'
    },
    referenceViewStyle: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    referenceTitleStyle: {
        color: BLUE_COLOR,
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    referenceDetailStyle: {
        color: BLUE_COLOR,
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    receiptStyle: {
        alignSelf: 'center',
        top: 30,
        flex: 1,
        color: "white",
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'OpenSans-Regular'
    }
})