
import React, { Component } from 'react';
import {
    View,
    StatusBar,
    StyleSheet
} from 'react-native';

import { WebView } from "react-native-webview";

import {
    Components,
    Helper
} from '../../Helper/FilePath';
import {
    TOP_BAR_HEIGHT,
    APP_THEME,
    PAYMENT_STATUS
} from '../../Helper/Constants';

export default class PaymentScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isShowSpinner: false
        }

        handleState = this.handleState.bind(this)

        const { paymentUrl, html_content } = props.navigation.state.params

        if (paymentUrl) {
            payload = {
                uri: paymentUrl
            }
        }
        else {
            payload = {
                html: html_content
            }
        }
    }

    componentDidMount() {
        didBlurObserver = this.props.navigation.addListener(
            'didBlur',
            payload => {
                this.hideSpinner()
            }
        );
    }

    componentWillUnmount() {
        didBlurObserver.remove();
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Payment'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={styles.subContainer}>
                    <WebView
                        style={{ flex: 1 }}
                        allowUniversalAccessFromFileURLs={true}
                        allowFileAccess={true}
                        useWebKit={true}
                        onLoadStart={() => this.showSpinner()}
                        onLoad={() => this.hideSpinner()}
                        onNavigationStateChange={(state) => this.handleState(state)}
                        source={payload} />
                </View>
            </Components.BACKGROUND_IMAGE>
        );
    }

    handleState(state) {
        const url = state.url
        const isValidUrl = this.isValidURL(url)
        if (isValidUrl) {
            const isEnets = url.includes("enets")
            if (isEnets) {
                //console.log("url", url);                
            }
            else {
                const status = url.includes("callback")
                if (status == true) {
                    const refId = url.substring(url.lastIndexOf('/') + 1)
                    if (refId != undefined && refId.length > 0) {
                        this.paymentSuccessAction(refId)
                    }
                }
            }
        }
    }

    isValidURL(string) {
        return string.startsWith("https") || string.startsWith("http")
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    paymentSuccessAction(transactionId) {
        const { paymentMode, amount } = this.props.navigation.state.params
        const charges = (amount * (paymentMode.fee_details.fee_percentage / 100)).toFixed(2)
        this.props.navigation.navigate("PaymmentSuccessScreen", {
            topupAmount: amount,
            transferFee: charges,
            transactionId: transactionId,
            paymentStatus: PAYMENT_STATUS.PENDING
        })
    }
}

const styles = StyleSheet.create({
    subContainer: {
        flex: 1,
        marginTop: TOP_BAR_HEIGHT,
        backgroundColor: 'red'
    }
})