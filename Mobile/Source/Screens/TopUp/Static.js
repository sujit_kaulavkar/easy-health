export const funds = {
    "items": [
        {
            "id": "065ef4010e0db529044b6dcf560c1228",
            "name": "Enets",
            "product_code": "sgasiatp",
            "provider_hash": "69148e31855f8204468c48a96efab164",
            "provider_type": "online",
            "provider_currency": "SGD",
            "provider_mode": "Direct Debit",
            "date_created": "2018-10-25 13:19:34",
            "topup_min_amount": "1.000",
            "fee_details": {
                "fee_flat": "0.000",
                "fee_percentage": "0.00",
                "tax_flat": "0.000",
                "tax_percentage": "0.00"
            },
            "description": "ENets Payment Gateway",
            "image": {
                "source": "http://vcard-assets.s3.amazonaws.com/payments/Enets.png"
            }
        },
        {
            "id": "296d0d659062387889f8d6790f433181",
            "name": "MasterCardDirectPayment",
            "product_code": "sgasiatp",
            "provider_hash": "316643082e790943fe77e1eacea40cff",
            "provider_type": "online",
            "provider_currency": "SGD",
            "provider_mode": "Direct Payment",
            "date_created": "2018-10-25 13:19:36",
            "topup_min_amount": "1.000",
            "fee_details": {
                "fee_flat": "0.000",
                "fee_percentage": "0.00",
                "tax_flat": "0.000",
                "tax_percentage": "0.00"
            },
            "description": "MasterCard Direct Payment Gateway Service",
            "image": {
                "source": "N/A"
            }
        },
        {
            "id": "d246028b79b9b41ffb35cd55d5c88cce",
            "name": "SingPost",
            "product_code": "sgasiatp",
            "provider_hash": "b7eb16965b5e1a1d1e385f23c6022cdb",
            "provider_type": "online",
            "provider_currency": "SGD",
            "provider_mode": "Cash In Channels",
            "date_created": "2018-10-25 13:19:36",
            "topup_min_amount": "1.000",
            "fee_details": {
                "fee_flat": null,
                "fee_percentage": null,
                "tax_flat": null,
                "tax_percentage": null
            },
            "description": "SingPost Cash In Channel",
            "image": {
                "source": "N/A"
            }
        }
    ]
}