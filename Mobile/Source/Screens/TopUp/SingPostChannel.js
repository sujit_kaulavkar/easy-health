
import React from 'react';
import { StyleSheet, View } from 'react-native';
import SimplePicker from 'react-native-simple-picker';

import { SCREEN_WIDTH } from '../../Helper/Constants';

export const POST_OFICE = 'Post Office'
export const SAM_KISOK = 'SAM Kisok'
export const SAM_ONLINE = 'SAM Online'

export default class SingPostChannel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [POST_OFICE, SAM_KISOK, SAM_ONLINE]
        };

        selectedChannel = this.selectedChannel.bind(this)
    }

    render() {
        return (
            <View style={styles.container}>

                <SimplePicker
                    ref= "channelPicker"
                    options={this.state.items}
                    onSubmit={(option) => this.selectedChannel(option)}
                />
            </View>
        );
    }

    selectedChannel(option) {
        if (this.props.selectedChannel) {
            this.props.selectedChannel(option)
        }        
    }

    show() {
        this.refs.channelPicker.show()
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        top: 1000,        
        position: 'absolute',
        justifyContent: 'center'
    },
    separatorView: {
        height: 1.5,
        alignSelf: 'center',
        marginBottom: 10,
        width: SCREEN_WIDTH - SCREEN_WIDTH * 0.17,        
        backgroundColor: '#C8C8C8'
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        marginHorizontal: SCREEN_WIDTH * 0.09,
        paddingBottom: 12,
        color: 'white'        
    }    
});
