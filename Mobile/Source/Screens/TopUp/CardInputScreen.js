
import React, { Component } from 'react';
import {
    View,
    Alert,
    ScrollView,
    StyleSheet
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import { CreditCardInput } from "react-native-credit-card-input";
import {
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    PAYMENT_STATUS,
    POST_KYC,
    PRE_KYC,
    APP_NAME,
    CANCEL_TEXT,
    YES_TEXT,
    OTP_OPTIONS
} from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';
import DropdownAlert from 'react-native-dropdownalert';

const MAX_TOPUP_LIMIT = "maxTopupLifetimeCountLimitExceeded"
const MAX_AMOUNT_LIMIT = "resource_user_wallet_fund_amount_max_amount_limit"

export default class CardInputScreen extends Component {

    constructor() {
        super()
        cardform = undefined
        this.state = {
            isShowSpinner: false
        }
    }

    remoteMasterCardpaymentCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const { paymentMode, amount } = this.props.navigation.state.params
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { number, expiry, cvc } = this.cardform.values

        const cardNumber = number.replace(/ /g, '')
        const expiryArray = expiry.split('/')
        const expiryMonth = expiryArray[0]
        const expiryYear = expiryArray[1]

        this.showSpinner()
        apiHelper.initiateMastercardPayment(paymentMode.provider_hash,
            amount,
            user.kyc_status == "approved" ? POST_KYC : PRE_KYC,
            user.user_hash_id,
            user.email,
            cardNumber,
            expiryMonth,
            expiryYear,
            cvc).then((responseJson) => {
                this.hideSpinner()

                if (responseJson) {
                    this.handleMasterCardPaymentResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleMasterCardPaymentResponse(responseJson) {        
        const status = responseJson.status
        if (status == 200) {            
            const data = responseJson.data
            const html_content = data.html_content
            if (html_content != null && html_content.length > 0) {
                this.showPaymentScreen(html_content)
            }
            else {
                this.paymentSuccessAction(data.ref)
            }
        }
        else {            
            const message = responseJson.message;                        
            if (this.checkForMaxTopupLimitReached(message) == true) {                
                Alert.alert(
                    APP_NAME,
                    "You have reached your max pre KYC topup count limit. You need to complete the KYC now. Do you want to proceed?",
                    [
                        {
                            text: CANCEL_TEXT, onPress: () => { }
                        },
                        {
                            text: YES_TEXT, onPress: () => { this.props.navigation.navigate("KYCPersonalDetailScreen", {
                                isFromRegistration: OTP_OPTIONS.IS_FROM_REGISTRATION
                            }) }
                        }
                    ],
                    { cancelable: false }
                )
            }
            else if (this.checkForMaxAmountLimitReached(message) == true) {
                Alert.alert(
                    APP_NAME,
                    "You have reached your max pre KYC topup amount limit. You need to complete the KYC now. Do you want to proceed?",
                    [
                        {
                            text: CANCEL_TEXT, onPress: () => { }
                        },
                        {
                            text: YES_TEXT, onPress: () => { this.props.navigation.navigate("KYCPersonalDetailScreen", {
                                isFromRegistration: OTP_OPTIONS.IS_FROM_REGISTRATION
                            }) }
                        }
                    ],
                    { cancelable: false }
                )
            }
            else {
                const errorMessage = responseJson.errorMessage;
                if (errorMessage) {
                    this.dropdown.alertWithType('error', "", errorMessage);
                }
                else {
                    this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
                }
            }            
        }
    }

    checkForMaxTopupLimitReached(message) {        
        return message.includes(MAX_TOPUP_LIMIT)
    }

    checkForMaxAmountLimitReached(message) {
        return message.includes(MAX_AMOUNT_LIMIT)
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.subContainer}>

                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Card Payment'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        console.log("go back");                        
                        this.props.navigation.goBack()
                    }}
                />

                <ScrollView
                    style={{ flex: 1, top: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1 }}>
                    <View style={styles.subContainer}>
                        <CreditCardInput
                            allowScroll={true}
                            onChange={(form) => this._onChange(form)}
                            inputStyle={styles.inputStyle}
                            placeholderColor="rgba(255, 255, 255, 0.5)"
                            labelStyle={styles.labelStyle}
                            inputContainerStyle={styles.inputContainerStyle}
                        />
                    </View>
                </ScrollView>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title="Proceed"
                        onPress={() => this.proceedButtonAction()}
                    />
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    _onChange(form) {
        this.cardform = form
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    proceedButtonAction() {
        if (this.cardform == undefined) {
            this.dropdown.alertWithType("error", "", "Enter card details")
            return;
        }

        const { valid } = this.cardform
        if (valid == false) {
            this.dropdown.alertWithType("error", "", "Card details are incomplete")
            return;
        }

        this.remoteMasterCardpaymentCall()
    }

    showPaymentScreen(html_content) {
        const { paymentMode, amount } = this.props.navigation.state.params        
        this.props.navigation.navigate("PaymentScreen", {
            html_content: html_content,
            paymentMode: paymentMode,
            amount: amount
        })
    }

    paymentSuccessAction(transactionId) {
        const { paymentMode, amount } = this.props.navigation.state.params
        const charges = (amount * (paymentMode.fee_details.fee_percentage / 100)).toFixed(2)

        this.props.navigation.navigate("PaymmentSuccessScreen", {
            topupAmount: amount,
            transferFee: charges,
            transactionId: transactionId,
            paymentStatus: PAYMENT_STATUS.PENDING
        })
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {
        marginTop: 20,
        marginHorizontal: 5
    },
    inputStyle: {
        color: 'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    },
    labelStyle: {
        color: "white"
    },
    inputContainerStyle: {
        borderBottomWidth: 1,
        borderBottomColor: "white"
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    }
})
