
import React, { Component } from 'react';
import {
    View,
    Alert,
    StatusBar,
    StyleSheet,
    ScrollView
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';
import { Components, TOP_UP, Helper } from "../../Helper/FilePath";
import { moderateScale } from '../../Helper/Scaling';
import {
    APP_NAME,
    YES_TEXT,
    CANCEL_TEXT,
    SCREEN_WIDTH,
    TOP_BAR_HEIGHT,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    SELECT_PAYMENT_MODE_MESSAGE,
    POST_KYC,
    PRE_KYC,
    OTP_OPTIONS
} from '../../Helper/Constants';
import SingPostChannel, { SAM_KISOK, SAM_ONLINE } from './SingPostChannel'

const MAX_TOPUP_LIMIT = "maxTopupLifetimeCountLimitExceeded"
const MAX_AMOUNT_LIMIT = "resource_user_wallet_fund_amount_max_amount_limit"

export default class ChoosePaymentScreen extends Component {

    constructor(props) {
        super(props)

        amount = parseFloat(props.navigation.state.params.topupAmount)
        channelName = ""
        this.state = {
            allPaymentModes: [],
            paymentMode: null,
            isShowSpinner: false,
            transferFeeAmount: 0.00
        }

        selectedChannel = this.selectedChannel.bind(this)
        selectedPaymentMode = this.selectedPaymentMode.bind(this)
    }

    componentDidMount() {
        this.remotePaymentModeCall()
    }

    remotePaymentModeCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        apiHelper.fundProviders().then((responseJson) => {
            this.hideSpinner()
            if (responseJson) {
                this.handlePaymentModeResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handlePaymentModeResponse(responseJson) {

        const status = responseJson.status;
        if (status == 200) {
            const items = responseJson.data.items
            this.setState({
                allPaymentModes: items
            })
        }
        else {
            const errorMessage = responseJson["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    remoteSingPostPaymentCall() {

        let channel = "post_office"
        if (this.channelName == SAM_KISOK) {
            channel = "sam_kiosk"
        }
        else if (this.channelName == SAM_ONLINE) {
            channel = "sam_online_and_mobile"
        }

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        this.showSpinner()
        apiHelper.initiateSingPostPayment(
            this.state.paymentMode.provider_hash,
            amount + this.state.transferFeeAmount,
            channel,
            user.isKYCDone == true ? POST_KYC : PRE_KYC,
            user.user_hash_id,
            user.email
        ).then((responseJson) => {
            this.hideSpinner()
            if (responseJson) {
                this.handleSingpostPaymentResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleSingpostPaymentResponse(responseJson) {

        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            const refId = data.payment_ref_id
            const totalAmount = data.fees.amount
            const fee = parseFloat(data.fees.total_fees).toFixed(2)
            const creditAmount = data.fees.credit_amount

            if (refId != undefined && refId.length > 0) {
                this.props.navigation.navigate("SingPostTopupScreen", {
                    refId: refId,
                    totalAmount: totalAmount,
                    fee: fee,
                    channel: this.channelName,
                    creditAmount: creditAmount,
                    expireTime: 10
                })
            }
        }
        else {
            const message = responseJson.message;
            if (this.checkForMaxTopupLimitReached(message) == true) {
                Alert.alert(
                    APP_NAME,
                    "You have reached your max pre KYC topup count limit. You need to complete the KYC now. Do you want to proceed?",
                    [
                        {
                            text: CANCEL_TEXT, onPress: () => { }
                        },
                        {
                            text: YES_TEXT, onPress: () => {
                                this.props.navigation.navigate("KYCPersonalDetailScreen", {
                                    isFromRegistration: OTP_OPTIONS.IS_FROM_REGISTRATION
                                })
                            }
                        }
                    ],
                    { cancelable: false }
                )
            }
            else if (this.checkForMaxAmountLimitReached(message) == true) {
                Alert.alert(
                    APP_NAME,
                    "You have reached your max pre KYC topup amount limit. You need to complete the KYC now. Do you want to proceed?",
                    [
                        {
                            text: CANCEL_TEXT, onPress: () => { }
                        },
                        {
                            text: YES_TEXT, onPress: () => {
                                this.props.navigation.navigate("KYCPersonalDetailScreen", {
                                    isFromRegistration: OTP_OPTIONS.IS_FROM_REGISTRATION
                                })
                            }
                        }
                    ],
                    { cancelable: false }
                )
            }
            else {
                const errorMessage = responseJson.errorMessage;
                if (errorMessage) {
                    this.dropdown.alertWithType('error', "", errorMessage);
                }
                else {
                    this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
                }
            }
        }
    }

    checkForMaxTopupLimitReached(message) {
        return message.includes(MAX_TOPUP_LIMIT)
    }

    checkForMaxAmountLimitReached(message) {
        return message.includes(MAX_AMOUNT_LIMIT)
    }

    render() {
        const { transferFeeAmount, allPaymentModes } = this.state
        const totalPayable = (amount + transferFeeAmount).toFixed(2)

        return (
            <Components.BACKGROUND_IMAGE
                isShowMMLogo={true}
                style={styles.container}>

                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <ScrollView
                    style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1 }}>

                    <View style={{ flex: 1 }}>
                        <View style={styles.cardBaseStyle}>
                            <TOP_UP.PAYMENT_MODE_VIEW
                                ref="paymentModeViewRef"
                                allPaymentModes={allPaymentModes}
                                selectedPaymentMode={(paymentMode) => this.selectedPaymentMode(paymentMode)} />
                        </View>

                        <View style={styles.amountBaseStyle}>
                            <TOP_UP.TOTAL_AMOUNT_VIEW topupAmount={amount} transferFee={transferFeeAmount} />
                        </View>

                        <View style={styles.submitButtonBase}>
                            <Components.BUTTON_SUBMIT
                                ref="pay"
                                activeOpacity={0.7}
                                title={"Pay " + totalPayable + " SGD"}
                                onPress={() => this.payButtonAction()}
                            />
                        </View>
                    </View>

                    <SingPostChannel ref="singPostChannel" selectedChannel={(channel) => this.selectedChannel(channel)} />
                </ScrollView>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Choose Payment'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        );
    }

    selectedChannel(channel) {
        this.channelName = channel
        setTimeout(() => {
            this.showWarningMessage()
        }, 1000);
    }

    showWarningMessage() {

        const totalPayable = (amount + this.state.transferFeeAmount).toFixed(2)
        Alert.alert(
            APP_NAME,
            "You will pay SGD " + totalPayable + " using " + this.state.paymentMode.name + ". Are you sure you want to continue?",
            [
                {
                    text: CANCEL_TEXT, onPress: () => { }
                },
                {
                    text: YES_TEXT, onPress: () => {

                        const { paymentMode } = this.state
                        if (paymentMode.name == "Enets") {
                            this.enetsPayment(paymentMode)
                        }
                        else if (paymentMode.name == "MasterCardDirectPayment") {
                            this.masterCardPayment(paymentMode)
                        }
                        else if (paymentMode.name == "SingPost") {

                            this.singPostPayment(paymentMode)
                        }
                    }
                }
            ],
            { cancelable: false }
        )
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    selectedPaymentMode(item) {        
        let charges = item.fee_details.fee_percentage
        if (charges == null || charges == undefined) {
            charges = 0.0
        }

        charges = (amount * (charges / 100)).toFixed(2)
        
        this.setState({
            paymentMode: item,
            transferFeeAmount: parseFloat(charges)
        })
    }

    payButtonAction() {
        const { paymentMode } = this.state
        if (paymentMode == null) {
            this.dropdown.alertWithType('error', "", SELECT_PAYMENT_MODE_MESSAGE);
            return;
        }

        const minAmount = parseFloat(paymentMode.topup_min_amount)
        if (amount < minAmount) {
            const message = paymentMode.name + " need minimum topup amount of $" + minAmount
            this.dropdown.alertWithType('error', "", message);
            return;
        }

        this.goToPaymentPage()
    }

    goToPaymentPage() {
        const { paymentMode } = this.state
        if (paymentMode.name == "Enets") {
            this.showWarningMessage()
        }
        else if (paymentMode.name == "MasterCardDirectPayment") {
            this.showWarningMessage()
        }
        else if (paymentMode.name == "SingPost") {
            this.refs.singPostChannel.show()
        }
    }

    enetsPayment() {

    }

    masterCardPayment() {
        this.props.navigation.navigate("CardInputScreen", {
            paymentMode: this.state.paymentMode,
            amount: amount            
        })
    }

    singPostPayment() {
        this.remoteSingPostPaymentCall()
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    amountBaseStyle: {
        marginTop: 40,
        marginHorizontal: 10
    },
    cardBaseStyle: {
        marginTop: 10,
    },
    submitButtonBase: {
        marginTop: 30,
        marginBottom: 20,
        height: 50,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    }
})