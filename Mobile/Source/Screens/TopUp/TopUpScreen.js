
import React, { Component } from 'react';
import {
  View,
  Text,
  Keyboard,
  TextInput,
  StyleSheet,
  BackHandler
} from 'react-native';
import Events from 'react-native-simple-events';
import { Components, Helper } from "../../Helper/FilePath";
import DropdownAlert from 'react-native-dropdownalert';
import {
  moderateScale
} from '../../Helper/Scaling';
import { TOP_BAR_HEIGHT, SCREEN_WIDTH, APP_THEME } from '../../Helper/Constants';

export default class TopUpScreen extends Component {

  constructor() {
    super()

    this.state = {
      walletInfo: {
        "walletBalance": "0.0",
        "walletCurrency": "SGD"
      }
    }

    proceedButtonAction = this.proceedButtonAction.bind(this)
  }

  componentDidMount() {
    this.addObservers()
    const walletInfo = Helper.USER_MANAGER.getInstance().getWalletBalance()
    if (walletInfo != undefined) {
      this.setState({
        walletInfo: walletInfo
      })  
    }
  }

  componentWillUnmount() {
    didBlurObserver.remove();
    didFocusObserver.remove();
  }

  addObservers() {

    BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
    // listener called when viewWillAppear
    didFocusObserver = this.props.navigation.addListener(
      'didFocus',
      payload => {
        Events.trigger('unlockDrawer')
      }
    );

    // listener called when viewWillDisappear
    didBlurObserver = this.props.navigation.addListener(
      'didBlur',
      payload => {
        Events.trigger('lockDrawer')
      }
    );
  }

  render() {
    console.log("this.syte", this.state);
    
    return (
      <Components.BACKGROUND_IMAGE style={styles.container}>

        <Components.TOP_BAR
          topbarTranslate={0}
          ref="topbarRef"
          title='Top Up'
          isMenuButtonNeeded={true}
          menuButtonAction={() => {
            Keyboard.dismiss()
            this.props.navigation.openDrawer()
          }}/>

        <View style={styles.balanceContainerStyle}>
          <Text style={styles.walletBalanceStyle}>
            Wallet Balance
            </Text>

          <Text style={styles.amountStyle}>
            $ {this.state.walletInfo.walletBalance} {this.state.walletInfo.walletCurrency}
          </Text>
        </View>

        <View style={styles.amountInputContaierStyle}>
          <Text style={styles.currencyTextStyle}>
            Enter Amount
          </Text>

          <View style={{ height: 60 }}>
            <View style={styles.currencyContainerStyle}>
              <Text style={styles.currencyTextStyle}>
                SGD
              </Text>
              <View style={styles.verticalSeparatorStyle} />
              <TextInput ref="textInput" style={styles.textInputStyle} />
            </View>
            <View style={styles.underlineStyle} />
          </View>
        </View>

        <View style={styles.submitButtonBase}>
          <Components.BUTTON_SUBMIT
            ref="submitButton"
            activeOpacity={0.7}
            title="Proceed"
            onPress={() => this.proceedButtonAction()}
          />
        </View>

        <DropdownAlert
          inactiveStatusBarBackgroundColor={APP_THEME}
          ref={ref => this.dropdown = ref}
          onClose={data => this.onCloseDropdown(data)} />
      </Components.BACKGROUND_IMAGE>
    );
  }

  proceedButtonAction() {
    const value = this.refs.textInput._lastNativeText
    let validator = new Helper.VALIDATOR(value)
    let errorMessage = validator.validateAmount()
    if (errorMessage.length != 0) {
      this.dropdown.alertWithType('error', "", errorMessage);
      return;
    }

    //if (this.isTopupLimitReached() == false) {
      this.props.navigation.navigate("ChoosePaymentScreen", {
        topupAmount: value
      })
    //}    
  }

  isTopupLimitReached() {
    const value = this.refs.textInput._lastNativeText
    const amount = parseFloat(value).toFixed(2)
    const message = new Helper.KYC_LIMIT().checkForTopUpLimit(amount)
    if (message.length > 0) {
      this.dropdown.alertWithType('error', "", message);
      return true
    }

    return false
  }

  handleBackFromLogin = () => {
    return true;
  }

  onCloseDropdown() { }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  submitButtonBase: {
    position: 'absolute',
    bottom: 40,
    marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
  },
  walletBalanceStyle: {
    color: 'white',
    fontSize: moderateScale(20, 0.09),
    fontFamily: 'BarlowCondensed-Regular'
  },
  balanceContainerStyle: {
    flexDirection: 'column',
    top: TOP_BAR_HEIGHT,
    height: 80,
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  amountStyle: {
    color: 'white',
    fontSize: moderateScale(30, 0.09),
    fontFamily: 'BarlowCondensed-Medium'
  },
  amountInputContaierStyle: {
    marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09),
    flexDirection: 'column',
    top: 120,
    height: 80
  },
  currencyContainerStyle: {
    top: 5,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },
  currencyTextStyle: {
    color: "white",
    fontSize: moderateScale(17, 0.09),
    fontFamily: 'BarlowCondensed-Regular'
  },
  verticalSeparatorStyle: {
    left: 5,
    height: "50%",
    width: 1,
    backgroundColor: 'rgba(255,255,255,0.7)'
  },
  textInputStyle: {
    flex: 1,
    left: 10,
    color: "white",
    fontSize: moderateScale(20, 0.09),
    fontFamily: 'BarlowCondensed-Regular'
  },
  underlineStyle: {
    height: 1,
    bottom: -3,
    width: "100%",
    backgroundColor: 'rgba(255,255,255,0.7)'
  }
})