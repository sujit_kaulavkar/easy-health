
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { moderateScale } from '../../Helper/Scaling';

export default class PaymentModeView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedPaymentModeIndex: -1,
        }
    }

    render() {

        const { allPaymentModes } = this.props

        return (
            <View style={styles.container}>                
                {this.paymentModeMessage()}
                {this.card()}
                {this.paymentSelectMessage()}
            </View >
        );
    }

    paymentModeMessage() {

        const { allPaymentModes } = this.props
        if (allPaymentModes.length <= 0) return null

        return (
            <View style={styles.choosePaymentModeViewStyle}>
                <Text style={styles.choosePaymentModeStyle}>
                    Choose Payment Mode
                    </Text>
            </View>
        )
    }

    card(item) {

        const { selectedPaymentModeIndex } = this.state
        const { allPaymentModes } = this.props
        if (allPaymentModes.length <= 0) return null

        return (
            <View>
                {allPaymentModes.map((item, i) => (
                    <TouchableOpacity
                        key={item.name}
                        activeOpacity={1}
                        onPress={() => this.paymentModeButtonAction(i)}>
                        <View style={styles.paymentModeBaseViewStyle}>

                            <View style={styles.cardIconBaseViewStyle}>
                                <Ionicons
                                    name="md-card"
                                    color="#696969"
                                    size={40}
                                    backgroundColor={"transparent"}
                                />
                            </View>

                            <View style={styles.cardMessageViewStyle}>
                                <Text style={styles.paymentModeTextStyle}>
                                    {item.name}
                                </Text>

                                {this.isChargesAdded(item.fee_details.fee_percentage)}

                            </View>

                            <View style={styles.radioBtnViewStyle}>
                                <Ionicons
                                    name={selectedPaymentModeIndex == i ? "md-radio-button-on" : "md-radio-button-off"}
                                    color="#404040"
                                    size={20}
                                    backgroundColor={"transparent"}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        )
    }

    isChargesAdded(charges) {
        if (charges != null || charges != undefined) {
            return <Text style={styles.conveninceChangesStyles}>
                {charges}% Convenience Charges
            </Text>
        }

        return null
    }

    paymentSelectMessage() {

        const { allPaymentModes } = this.props
        if (allPaymentModes.length <= 0) return null

        return (
            <View style={styles.paymentMessageBaseViewStyle}>
                <FontAwesome
                    name="info-circle"
                    color="white"
                    size={15}
                    backgroundColor={"transparent"}
                />

                <Text
                    numberOfLines={2}
                    style={styles.paymentMessageTextStyle}>
                    You will be directed to the selected payment gateway
                        </Text>
            </View>
        )
    }

    paymentModeButtonAction(paymentModeIndex) {
        const { allPaymentModes } = this.props
        this.setState({
            selectedPaymentModeIndex: paymentModeIndex
        })

        this.props.selectedPaymentMode(allPaymentModes[paymentModeIndex])
    }

    selectedPaymentMode() {
        return this.state.selectedPaymentModeIndex;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 0,
        marginHorizontal: 20
    },
    choosePaymentModeViewStyle: {
        height: 40
    },
    choosePaymentModeStyle: {
        fontSize: moderateScale(15, 0.09),
        color: "white",
        fontFamily: 'OpenSans-Regular',
        left: 10
    },
    paymentModeBaseViewStyle: {
        height: 70,
        backgroundColor: '#E8E8E8',
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardIconBaseViewStyle: {
        width: 60,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardMessageViewStyle: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'column',
    },
    paymentModeTextStyle: {
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: "#404040"
    },
    conveninceChangesStyles: {
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: "#808080"
    },
    radioBtnViewStyle: {
        alignSelf: 'flex-end',
        width: 50,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    radioBtnStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    paymentMessageBaseViewStyle: {
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paymentMessageTextStyle: {
        left: 7,
        fontSize: moderateScale(13, 0.09),
        color: "white",
        alignSelf: 'center',
        fontFamily: 'OpenSans-Regular'
    }
})