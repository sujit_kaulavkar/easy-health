
import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet
} from 'react-native';

import {
    Components
} from '../../Helper/FilePath';
import { moderateScale } from '../../Helper/Scaling';
import { TOP_BAR_HEIGHT, SCREEN_WIDTH, DARK_BLUE_COLOR } from '../../Helper/Constants';
import { POST_OFICE, SAM_KISOK, SAM_ONLINE } from './SingPostChannel'

const postOfficeSteps = "1. Search for MatchMove\
                         \n2. Enter the Token Number and Amount\
                         \n3. Enter F10 Settle"

const samKisokSteps = "1. Proceed to SAMKisok and Select \"Top Up\"\
                         \n2. Click on \"MatchMove\"\
                         \n3. Enter the above Token Number\
                         \n4. Enter the Top Up amount\
                         \n5. Click done and proceed with payment"

const samOnlineSteps = "1.  Sign up and login into to SAM account at https://www.mysam.sg/continue/dmbox/reg\
                         \n2. Go to Payments and search for MatchMove\
                         \n3. Enter the above Token Number\
                         \n4. Enter the Top Up amount\
                         \n5. Select your Payment Mode and proceed with payments"

export default class SingPostTopupStepsScreen extends Component {

    getSteps() {

        const { channel } = this.props.navigation.state.params
        if (channel == POST_OFICE) {
            return postOfficeSteps
        }
        else if (channel == SAM_KISOK) {
            return samKisokSteps
        }
        else if (channel == SAM_ONLINE) {
            return samOnlineSteps
        }

        return ""
    }

    render() {
        const { refId, totalAmount, fee, creditAmount, expireTime } = this.props.navigation.state.params
        const token = refId
        const message = "Please note token expires in " + expireTime + " minutes"

        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='SingPost'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={{ marginTop: TOP_BAR_HEIGHT, flex: 1 }}>
                    <ScrollView
                        style={{ flex: 1 }}
                        contentContainerStyle={{flexGrow: 1}}>

                        <View>
                            <View style={styles.tokenBaseView}>
                                <Text style={styles.yourTokenText}>
                                    Your Token
                                </Text>
                                <Text style={styles.tokenText}>
                                    {token}
                                </Text>
                                <Text style={styles.tokenMessageText}>
                                    {message}
                                </Text>
                            </View>

                            <View style={{ marginTop: 20 }}>

                                <View style={{ height: 1, top: 1, width: SCREEN_WIDTH, backgroundColor: 'white' }} />

                                <View style={styles.amountBaseView}>

                                    <View style={[styles.amountLeftView, { marginRight: 15 }]}>
                                        <Text style={styles.yourTokenText}>
                                            Amount
                                </Text>

                                        <Text style={styles.yourTokenText}>
                                            Fee
                                </Text>

                                        <Text style={styles.yourTokenText}>
                                            Total Payable
                                </Text>
                                    </View>

                                    <View style={[styles.amountLeftView, { marginLeft: 15 }]}>
                                        <Text style={styles.amountText}>
                                            SGD {totalAmount}
                                        </Text>

                                        <Text style={styles.amountText}>
                                            SGD {fee}
                                        </Text>

                                        <Text style={styles.amountText}>
                                            SGD {creditAmount}
                                        </Text>
                                    </View>

                                </View>

                                <View style={{ height: 1, bottom: 1, width: SCREEN_WIDTH, backgroundColor: 'white' }} />
                            </View>

                            <View style={{ flex: 1, marginHorizontal: 20, marginTop: 30 }}>

                                <Text style={styles.amountText}>
                                    Steps to Topup:
                        </Text>
                                <Text style={[styles.amountText, { marginTop: 15 }]}>
                                    {this.getSteps()}
                                </Text>
                            </View>

                            <View style={styles.submitButtonBase}>
                                <Components.BUTTON_SUBMIT
                                    ref="submitButton"
                                    activeOpacity={0.7}
                                    title="Done"
                                    onPress={() => this.doneButtonAction()}
                                />
                            </View>

                        </View>                        
                    </ScrollView>
                </View>

            </Components.BACKGROUND_IMAGE>
        );
    }

    doneButtonAction() {
        this.props.navigation.popToTop()
    }
}

const styles = StyleSheet.create({
    tokenBaseView: {
        marginTop: 20,
        height: 100,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    yourTokenText: {
        color: 'white',
        textAlign: 'right',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    tokenText: {
        color: DARK_BLUE_COLOR,
        fontSize: moderateScale(25, 0.09),
        fontFamily: 'OpenSans-Bold'
    },
    tokenMessageText: {
        color: 'white',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    separatorView: {
        position: 'relative',
        width: SCREEN_WIDTH - SCREEN_WIDTH * 0.09,
        height: 1,
        margin: 0,
        backgroundColor: '#C8C8C8'
    },
    amountBaseView: {
        height: 100,
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 10
    },
    amountLeftView: {
        height: 100,
        flex: 1,
        justifyContent: 'space-around'
    },
    amountText: {
        color: 'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    submitButtonBase: {
        marginTop: 20,
        marginBottom: 20,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
})