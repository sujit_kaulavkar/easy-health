import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Image
} from 'react-native';

import {
    Components,
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    SCREEN_WIDTH,
    DARK_BLUE_COLOR
} from '../../Helper/Constants';

export default class AppoinmentTypeScreen extends Component {

    constructor() {
        super()
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='APPOINTMENT'
                    isMenuButtonNeeded={true}
                    menuButtonAction={() => {
                        this.props.navigation.openDrawer()
                    }} />

                <View style={{
                    marginTop: 200
                }}>
                    <Text style={{
                        fontSize: moderateScale(17, 0.09),
                        fontFamily: 'OpenSans-SemiBold',
                        marginTop: 5,
                        color: DARK_BLUE_COLOR,
                        alignSelf: 'center',
                        textAlign:'center',
                        marginHorizontal:50
                    }}>
                        SELECT ANY ONE OPTION FOR WHICH YOU WANT BOOK AN APPOINTMENT 
                    </Text>
                </View>

                <View style={{
                    alignSelf:'center',
                    height: 200,
                    top:50,
                    width: SCREEN_WIDTH,
                    flexDirection: 'row',
                    alignItems:'center',
                    justifyContent:'space-around',
                }}>
                    <TouchableOpacity 
                    activeOpacity={0.7}
                        style={{alignItems:'center', borderRadius:10, justifyContent:'center', width: 130, height: 130, backgroundColor: 'rgba(236,65,46,1)'}}
                    onPress={() => this.handleSelfType()}>
                        <Image style={{width:100, height:100}} source={require("../../Resources/Images/self.png")} />

                        <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            marginTop: 5,
                            color: "white",
                            alignSelf:'center'
                        }}>SELF</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ alignItems: 'center', borderRadius: 10, justifyContent: 'center', width: 130, height: 130, backgroundColor: 'rgba(236,65,46,1)' }} activeOpacity={0.7} onPress={() => this.handleRelativeType()}>
                        <Image style={{ width: 100, height: 100, width: 100, height: 100 }} source={require("../../Resources/Images/relative.png")} />

                        <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            marginTop: 5,
                            color: "white",
                            alignSelf: 'center'
                        }}>RELATIVES</Text>
                    </TouchableOpacity>
                </View>

            </Components.BACKGROUND_IMAGE>
        )
    }

    handleSelfType() {
        this.props.navigation.navigate("CheckListScreen", {
            appoinmentType: 1
        })
    }

    handleRelativeType() {
        this.props.navigation.navigate("CheckListScreen")
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    flatListContainerStyle: {
        flex: 1,
        marginTop: 100,
        marginBottom: 10
    },
    flatListStyle: {
        flex: 1,
        marginHorizontal: 35
    },
    cellContainerStyle: {
        left: 0,
        justifyContent: 'center'
    },
    postTitleStyle: {
        left: 15,
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: 'white',
        textAlignVertical: 'top',
    }
})