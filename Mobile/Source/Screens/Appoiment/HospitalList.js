import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Keyboard,
    FlatList,
    StyleSheet,
    BackHandler,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    DARK_BLUE_COLOR,
    BLUE_COLOR
} from '../../Helper/Constants';

import Dialog, {
    DialogTitle,
    DialogContent,
    DialogFooter,
    DialogButton,
    SlideAnimation,
    ScaleAnimation,
} from 'react-native-popup-dialog';

import { Rating, AirbnbRating } from 'react-native-ratings';
import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default class HospitalList extends Component {

    constructor() {
        super()

        this.state = {
            defaultAnimationDialog: false,
            loading: false,
            venders: [],
            selectedVendor: undefined
        }

        onSearchTextChange = this.onSearchTextChange.bind(this)
        onSearchEnd = this.onSearchEnd.bind(this)
        handleVendorResponse = this.handleVendorResponse.bind(this)
        didSelectItemFromCell = this.didSelectItemFromCell.bind(this)
    }

    componentDidMount() {
        this.refs.submitButton.disableSubmitButton()
        this.addObservers()
        this.remoteFetchVendor()
    }

    componentWillUnmount() {
        didBlurObserver.remove();
        didFocusObserver.remove();
    }

    addObservers() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {
                Events.trigger('unlockDrawer')
            }
        );

        // listener called when viewWillDisappear
        didBlurObserver = this.props.navigation.addListener(
            'didBlur',
            payload => {
                Events.trigger('lockDrawer')
            }
        );
    }

    remoteFetchVendor() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const { facility } = this.props.navigation.state.params

        apiHelper.hospitals(facility.id).then((responseJson) => {
            if (responseJson) {
                this.handleVendorResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.showServerErrorMessage()
            })
    }

    handleVendorResponse(responseJson) {
        const status = responseJson.success
        if (status == true) {
            const data = responseJson.data
            let vendors = data.facility.hospitals

            this.setState({
                venders: vendors
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    onSearchTextChange(text) {
        //this.remoteFetchVendor(text)
    }

    onSearchEnd(text) {
        // this.remoteFetchVendor(text)
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='SERVICE PROVIDER'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                <View style={styles.flatListContainerStyle}>
                    <FlatList
                        style={styles.flatListStyle}
                        data={this.state.venders}
                        extraData={this.state}
                        key="vendorFlatList"
                        keyboardShouldPersistTaps="always"
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({ item, index }) => this.renderCellItem(item, index)}
                    />
                </View>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title="NEXT"
                        onPress={() => this.showDialog()}
                    />
                </View>

                <Dialog
                    onDismiss={() => {
                        this.setState({ defaultAnimationDialog: false });
                    }}
                    width={0.9}
                    visible={this.state.defaultAnimationDialog}
                    rounded
                    actionsBordered
                    dialogTitle={
                        <DialogTitle
                            title="Your Service Provider"
                            style={{
                                backgroundColor: '#F7F7F8',
                                fontSize: moderateScale(19, 0.09),
                                fontFamily: 'OpenSans-Regular'
                            }}
                            hasTitleBar={false}
                            align="left"
                        />
                    }
                    footer={
                        <DialogFooter>
                            <DialogButton
                                text="CANCEL"
                                bordered
                                onPress={() => {
                                    this.setState({ defaultAnimationDialog: false });
                                }}
                                key="button-1"
                            />
                            <DialogButton
                                text="PROCEED"
                                bordered
                                onPress={() => {
                                    this.proceedButtonAction()
                                    this.setState({ defaultAnimationDialog: false });
                                }}
                                key="button-2"
                            />
                        </DialogFooter>
                    }>
                    
                    <DialogContent
                        style={{                            
                            backgroundColor: 'white',
                        }}>
                        <Image
                            style={{marginTop:10,height:80, width:80, alignSelf:'center'}}
                            source={{ uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTExMVFhUVFyAYFRcXFxsgIBggICggICAoHx8gJTAqICYxJR8fKjsrMTU3NTU1ICs7QDo1PzA1NjUBCgoKDg0OFxAQFjgdFhorNCsvKys3Ky0rNyszKzctLTMyLS03NzcrLS0rNzc3Kys4KzgrODgrKzg4NjctNy81MP/AABEIAMgAyAMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUCAwYBB//EADwQAAEDAgQDBQUGBQQDAAAAAAEAAhEDIQQSMUEFUWEGEyJxgTKRobHBB0JSYtHwIzNy4fEUgpLSY6Ky/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAECAwQF/8QAJBEBAQACAgICAgIDAAAAAAAAAAECAxEhEjEEUUFxMoETImH/2gAMAwEAAhEDEQA/APuKIiAiIgIiICIqHtjxr/S0C4HxOs3npJj9evoQce7U0cPLZDnjVo281wvFO11d4zF2UH2GNnTqOfn8FzArPdme+/i8UyZJHXXn7lDe9zjlFOTzMlZ3Jrjhz6WNLiFR7iS4Hq9x+WZTBxFtNwOhB2cSD6mY8tPgoVPhVVwAawNG8mPcBorXC9nrS8/5WN24xvj8fK/h0vZ/7QKfsVZtuTJHruPjzXc4TF06rc1NwcOYXxPF9nHgksI9LLoewfFXMf3TgQZ066e/4LTDZKy2abj7fU0WLTIWS2YCIiAiIgIiICIiAiIgIiICIiAiIgLgPtJYX1KbPuimXeuYAfvou/XJds8MCc3OnlHvn9+aipnt8yNIlvdxADpHMg/sKxwGGAiBCVmeMxoP8LdRqMbAe4BcW3m16ejxkW+F0hS6bFWYDjeFLsodJV931MAui0rLw+2/+T6QarVUjC5cQyqBv4h5f2Ut3HWPJa2k8xqRp7zC9w+Ka59MibuLSDsYJHy+K01yzJjvymWF+30ukBAjRZrRgv5bJ/CFvXe8oREQEREBERAREQEREBERAREQEREHi5ntriWtFNn335i0dBE/Me9dOvn/ANqtOo0UazdKbXx0dLHD4NcfRVzvE5aasZllxXPYOTTe8jxd44HpEED4qBU4Y9z5zZRF3QJnbXb9+XTcBoCpw9tWQX1JqujSdHRyjL6LDCsBsVzbOZeY7dMl5l/Cqq8OHcBpOarlu8WB5EDYzyMHkLET+HVHGm6k4yR8Vv42WMa24uRA5k6SvOD0gXulzQZus7zXTJMUVnAKXeGo7NpAAIjQib3BGY6HWDsIlPwIa3M2ZaWkE62I+MSrLvmNMG7ZjMDoeq21HAaXTm8/pTLCWft1HB8SKlFhGwykciLFTlX8Bo5aDBzGb33+qnrtx9R5eySZXj09REVlBERAREQEREBERAREQEREBERAVZ2h4YMRQfT31aeThcfp5FWa8UWcxMtl5jguDYOrSZUp1GZblzbROaQ6B5gn16hVmH1HX6aru+0bgKYdydr6FcM18ExqDmaubdOJI7dGfNtRMfWa8hj8skTlJHpqtnDeG0WkvDsrjrB15zzWeMp06wBLQSDIkLfw7ChvsU2+4LPGx1+/aQHtNhLgdQB+qnYPBkuawHXQnb9wVrZbXXdW3ZuiXPLzo0QPM/2+aYzyykZbc/HGuiY2AByWSIu55YiIgIiICIiAiIgIiICIiAiIgIiICIoON4hTY5tIvAqVAcjdzGp6Dqg5/tTx2kWuw4Ds5GbQAQNbzuJHquGp4wggG/J3Pz6ru+KYJrv4gbLmgtNtWm5/fUrmsbwKJfTBcw+0zdvVvMdNRG+0btVuPli1+Puxl8cukKhiBOsK6wlcNGy55+AIIg2OhU7B8OcTBdHouLiO7udLt1cHl1XWcCbFBltRPv8A7LiHUAwRMndd3wh4NGnH4B8ltpndc/yL1ExERdDkEREBERAREQEREBERAREQEREBFpq4lrd78goNTEOceQ5fqpk5RakV8eADl8RC+a9nca6vxitUqWc2lLW/lIaLeVveV3RbldMWOq+dds6D8BjaOPY0ljXRUA3Y6zh8THUjkttcnc+2Wdvt9KiHWUbE4fKczdD8FJpVW1Gtew5mPaHNI3BuCt7W2M6clEvCbOXPY3hjaot4X7H9R9VUmjUpuyuBB2PPyK6erSLD0OhVD2u7S4fCsDKnjqvE06QieWZx+42dzrBibxnt+Pjn3j1W+n5WWHWXcVmPc8iGCXu8LBzcfoNSdgCu64O3u2NpgzlAE84sfJfLT23bSqMqtw0iCH53+Mt5siWnrzj7ouvo/AuIU8Q1lai7NTewkH1AgjYgggjYgpr0XXjZfdRu+RNuU8fUX7HgrNQXslGYgtsbqPFXlORRaeNabEER+9lvZUB0MqODlmiIiRERAREQEREBERAVZjsUS7K0kRrG6sXGASufp1Lw+06FWxiuVSabAdLHqtmmu6Nat0AiDoVZVrqNkc1yn2kcSazCCj3fe1MSe6os5kwJ9JGm5C62mDodRvzGx/XqqDthw3PSZVa3M/CVW4hgGrgwy9o82gwOYCvhZMornzx05rs/wjjOGw9MU6tKo1rfDRc5ttyJNOdZtmEaSuh7NdrW4kvo1GGliKft0nCPMgHTyvrYm5VzhnBzRUaczXjN4Ta+jhGsiCfeud7admnVCzGYYkYqjp/5W/hPMwYB6wbGRbymV76UkuM6dVnbl8XsjVfBPtD4LXw1c4jvn1qFd5IqPAOV34HtiAQBaBECwEQPtnC8YK9FlTK5veMnK4EFp3EEA2IN99VWHh1Ou2rg64mnWG2oIuC07HcdQonS17fFsCw1xTY3wl9VrJF2y4gAx912+3KANfrPYzhRwOKfQYS6hXa6owHWm9uUO8wQW3/KvnI4M/BYynSqtbmpV6RzhsNNIPaWu5XIvOhtMm32Km2MXQ/pqD4NP0V7f9apJ2vSFhUpyPktjmAi4B81wOG4c/D8Yc0vf3OJpOfRaKrvC5mUuGWbCJ6EHoQMcZz+W2V4dpSGYToQtrRBkarxogu63/VZt0UUS6bwRKzUKnUynpupbXAiRoVSxeVkiIoSIiICIiAiIg04swx/9J+S5x9Q3EZgukxIljvIrmqcHQq+KmSbgqx0MmNDuPMfVT2qCadg4axdSaFabbqaiN68IusoRQlEwOGbTaGMs0eyPw3MAdACAOgC3NEHoduX9lEwlf8AiuYdcrXjqCA0+4tH/JTomymoVL6PcVLfy6jpH5XbjyIE+Y5lb+I4PMA9ntC4UrEUBUpuYbTvyIuCOoMFaOCYkvpw72mEtcOo+nLop5OHLfaFwkY3AuqsbNWgC4tH3gJzNI3tJG+oGpVb9n/HjiDgw4zUYa1Nx5w2Wn1bF9yCu/q0cju8aLffbzH6hfK+y/C/9Jx44dv8pzXVaMaZCx2WP+Tm+bSrY3qq2dx9gC4PtMMvG+HP/FTew+5//Zd4uA7aOA4twv8AqI95hNXv+qbPTuoXrAvQvKZVF3kLdhTt6rSfmsw6CCopEtF4CvVRcREQEREBERB4QubfhWk8j810Nd8NceQVKXgaiQrYq5MaBeyx8Q2UoMDrixWoaS0yNYW8AajfRXUbKbzodfmtoCwCzaoWVOOYWinXaCTS9oDVzD7Q68wOYCs2PBggghwkEbg6QvGMgR6Ku4ee6eaJ9gkmkeW7memo6GPupULM6+aqwe7xf5a7Z/3Nt8R8lauCquPSBSqD7lQf+3h+qQq4VCeBs/11LEaGnTqMbbVr8pifyuBI/rPJXrTIB5rWR/EB/KfmP0URLzGYgU2Oebx8dh8SvnnFRUr4+hiXmnkw0FrWzmOpgzO9wbeVpX0PG4YVGOYdHCFxGJ4BVEnI41ZAL2zD2gmBIPWSCBcC+ymWzuIynPVdW7HF+HdVoNzuDSWsJDSSNjNgfNQOBVn1Gs7zNLGiS4e0SBf3tJ9eqYLAPoYSsHGHva4wNiRA9ZVxSAl2wmB0DQB8wVEGxjpk8rL0tso9fFsFi65vC1VeJMByjxAe0RED9VFyi8wyv4TsO7LA2JUxQGEEzy0U1jgUpGSIiqkREQEREGL2yCOdlz1cGCDqLLo1ScSZD3dYPwj6KYrkg4OqQYVsxypmKzw1Sy0UTGLJalmCqrPKhhQ+I4eRInnbUEaEdQplUSCsWPBtuFKFPxHtG2hTa59KrUJMHuWZo/MZIgHQbzbVc9j+P4rE4lmHoYc9ycrnOc0zBuXFwOVkXtcmORC66vw+k54zsB3EjQ7+8KZTYG2AAHIIGEPgb5LJvtnoAPfKxwwiR1KzYLu8/oFFTGblivXLxAcVW8YxQpsERmdZjeZ105c/NWL1xvF657+pWmRT/hsHM6f/AESs9mfji104eeXbzK9zyzMS4/zHTz28/kFi5gDxTaJDYLz8hPx9ywog0WA5i57zAm9zqT5XJVng8HUFPwMJcTq6B6mdb3XHJbXfcpjFtgA4C4gTYD92+an0ngGR6qq4bhajPatzvqeZInqrMdV34/xjztn8r2mgr1acOdluVQREQEREBVnF2XaeYI+o+qs1D4oyaZO7TP6/CVM9oqgbqpVEdVEr6yplFgIkLRmmUnbLcorQVKpukKKmMgqvi2JFItdeDZWQChcZwhq0srfaBBF4/diq5WyWxfCS5SX0rcd2ip5LB2cXFt1p4Z2lo1XF5qEZAJbld4DuHCJJ9I5Ktq8IqscA9sTcEHX15rGrwMB3ftnvGtvoQ9tiWuBBBFrbgrmx35c8ZR1Z/Hw8ecatsRxRlNgfS11IbMEgG0Hc8tYgqJV7WV/Flp0wSbF2YgabAj5qyxWAaKbopZZgguFJsbw3u/aMayYtYqnxeFbroU3bMpej4+rCy+TpeB8YbiWFwGV7bVGTOUm4g7g7HzFiCFYFcBh8WMJVZXJimT3db+h2h/2ug+RcN19DIWurZ5Y/9Y7tfhl16YQoxwFK38NljI8I156a3N1KRa8SsubGLRGiZVkiIeJC8leyg8ZUgqaoTmqWw2HkoyTGSIiqsIiICxc2QQd7LJEHJ1W+006tK1YauWnop3FWZap5Ov8Aqq1wvC0jKugbMAjdZgkbKs4dii3wm42Vm2t0RLY2qD0PIrMDda5WUqEvK9IOEH06KIcCdnX6hTWr2VW4S+4vM7PVcLS4BWa5kurPNPxAOqNy5jNmkgFwkmATpHJZVXOmHNLTyc0ifKdfRdoCAtdVwcC1zQWnUESD5hVz1TJfXuuD55xwB1JzTur/AOzTi/e4c0XumpQOW+pYfZ91x6Dms+J9mqTmnI57Z2zTHlmm3T3RoeMoYbF8NxTK0CpSnK8ssXNOog2nQi8SAq69OWN67jTZuwzx+q+uPCxLllTqBzQ5plrgC0jcG4QhbOZrL16Hr0tWJpogcjXSvMpWD6Z1BhShk52ysAFW4cEuAN4uVZquS2IiIqrCIiAiIgpOPs8TT0+X+VS1TdEWk9M8vbbh3K3olEUoSA4IXrxFCXmYr3MiKRm0BZQERQPHAQudpPaS6g8jUinO8/dP093JEVsUVa8EYKdPu5s1xyg7A3j3ypzqo5hEVb3Vp6a3Ylo3Wt2NaNwiJwjlpfxRnO/S/wAkompVNrDclES9QndWuGw4YIHqea3IizaCIiD/2Q==" }}
                        />
                        <Text style={{
                            marginTop: 10,
                            fontSize: moderateScale(15, 0.09),
                            fontFamily: 'OpenSans-Regular',}}>Dr. Sanjay Rawt </Text>
                        <Text style={{
                            marginTop: 5,
                            fontSize: moderateScale(15, 0.09),
                            fontFamily: 'OpenSans-Regular',
                        }}>Cardiologists </Text>

                        <View style={{
                            marginTop: 20
                        }}>
                            <Rating
                                style={{ backgroundColor: 'transparent' }}
                                imageSize={30}
                                onFinishRating={this.ratingCompleted}
                            />
                        </View>

                    </DialogContent>
                </Dialog>

                <DropdownAlert                    
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        )
    }

    renderCellItem(item) {
        const imageWidth = 80;
        const imageHeight = 80;

        const selectedVendor = this.state.selectedVendor
        let id = -1
        if (selectedVendor != undefined) {
            id = selectedVendor.id
        }

        return (
            <View style={[styles.cellContainerStyle, { top: 6, width: SCREEN_WIDTH - 70, height: imageHeight + 20 }]}>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View>
                        <Image
                            source={{ uri: "http://cleverhippo.org/wp-content/uploads/2018/08/hospital-logo-s-l300.jpg" }}
                            style={{ left: 5, backgroundColor: 'gray', width: imageWidth - 10, height: imageHeight - 10 }}
                            onLoadStart={() => this.setState({ loading: true })}
                            onLoadEnd={() => {
                                this.setState({ loading: false })
                            }} />
                        {this.state.loading && <Components.LOADING_VIEW />}
                    </View>

                    <View style={{ flex: 1 }}>
                        <Text numberOfLines={2} style={styles.postTitleStyle}>
                            {item.name}
                        </Text>
                    </View>

                    <View style={{ alignSelf: 'flex-end', justifyContent: 'center', flexDirection: 'row', height: 50, width: 50 }}>
                        <MaterialIcons
                            name={id == item.id ? "radio-button-checked" : "radio-button-unchecked"}
                            color={BLUE_COLOR}
                            size={25}
                        />
                    </View>
                </View>

                <View style={{ position: 'absolute', bottom: 1, height: 1, width: SCREEN_WIDTH, backgroundColor: 'rgba(0,0,0,0.5)' }} />
                <TouchableOpacity style={{ flex: 1, zIndex: 0, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} onPress={() => this.didSelectItemFromCell(item)} />
            </View>
        )
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    showDialog() {
        this.setState({ defaultAnimationDialog: true });
    }

    proceedButtonAction() {        
        
        const { appoinmentType, facility } = this.props.navigation.state.params
        this.props.navigation.navigate("AppoinmentDateScreen", {
            appoinmentType: appoinmentType,
            facility: facility,
            hospital: this.state.selectedVendor
        })
    }

    didSelectItemFromCell = (item) => {
        this.setState({
            selectedVendor: item
        })

        this.refs.submitButton.enableSubmitButton()
        // this.props.navigation.navigate("InputCardNumberScreen", {
        //     vendor: item
        // })
    }

    handleBackFromLogin = () => {
        return true;
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    flatListContainerStyle: {
        flex: 1,
        marginTop: 80,
        marginBottom: 100
    },
    flatListStyle: {
        flex: 1,
        marginHorizontal: 35
    },
    cellContainerStyle: {
        left: 0,
        right: 0,
        justifyContent: 'center'
    },
    postTitleStyle: {
        left: 15,
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: DARK_BLUE_COLOR,
        textAlignVertical: 'top',
    }
})