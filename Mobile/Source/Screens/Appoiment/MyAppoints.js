import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Alert,
    Keyboard,
    FlatList,
    StyleSheet,
    BackHandler,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    APP_NAME,
    CANCEL_TEXT,
    OK_TEXT,
    DrawerItems,
    BLUE_COLOR,
    DARK_BLUE_COLOR
} from '../../Helper/Constants';

import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';

export default class MyAppoints extends Component {

    constructor() {
        super()

        this.state = {
            loading: false,
            venders: [],
            selectedVendor: undefined,
            isShowSpinner: false
        }

        handleVendorResponse = this.handleVendorResponse.bind(this)
        didSelectItemFromCell = this.didSelectItemFromCell.bind(this)
    }

    componentDidMount() {
        this.remoteFetchVendor()
    }

    remoteFetchVendor() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        this.showSpinner()
        apiHelper.myAppoinments().then((responseJson) => {

            this.hideSpinner()
            if (responseJson) {
                this.handleVendorResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleVendorResponse(responseJson) {
        const status = responseJson.success
        if (status == true) {
            const data = responseJson.data
            let vendors = data.appointments
            this.setState({
                venders: vendors
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>

                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='APPOINTMENTS'
                    isMenuButtonNeeded={true}
                    menuButtonAction={() => {
                        this.props.navigation.openDrawer()
                    }} />

                <View style={styles.flatListContainerStyle}>
                    <FlatList
                        style={styles.flatListStyle}
                        data={this.state.venders}
                        extraData={this.state}
                        key="vendorFlatList"
                        keyExtractor={(item, index) => item.name}
                        keyboardShouldPersistTaps="always"
                        renderItem={({ item, index }) => this.renderCellItem(item)} />
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        )
    }

    renderCellItem(item) {

        return (
            <View style={[styles.cellContainerStyle, { top: 6, width: SCREEN_WIDTH - 70, height: 200 }]}>

                <View style={{
                    height: 180,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 0 },
                    shadowOpacity: 0.8,
                    shadowRadius: 2,
                    borderRadius: 10,
                    borderColor: 'rgba(0,0,0,0.5)',
                    backgroundColor: 'white',
                    marginVertical: 15,
                    justifyContent: 'space-around'
                }}>

                    <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        PATIENT NAME:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.name.toUpperCase()}
                        </Text>
                    </Text>

                    <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        PATIENT GENDER:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.gender.toUpperCase()}
                        </Text>
                    </Text>

                    <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        PATIENT AGE:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.age}
                        </Text>
                        <Text> </Text>
                        YEARS
                    </Text>

                    <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        APPOINMENT DATE-TIME:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.appointment_date.toUpperCase()}
                        </Text>
                    </Text>

                    {/* <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        HOSPITAL:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.hospitalName.toUpperCase()}
                        </Text>
                    </Text> */}

                    <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        ADDRESS:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.location.toUpperCase()}
                        </Text>
                    </Text>

                    <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        PAYMENT MODE:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.payment_mode.toUpperCase()}
                        </Text>
                    </Text>

                    <Text style={{
                        fontSize: moderateScale(12, 0.09),
                        fontFamily: 'OpenSans-Regular',
                        marginLeft: 5,
                        color: BLUE_COLOR
                    }}>
                        APPOINMENT STATUS:  <Text style={{
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular',
                            marginLeft: 5,
                            color: DARK_BLUE_COLOR
                        }}>
                            {item.status.toUpperCase()}
                        </Text>
                    </Text>
                </View>

                <TouchableOpacity style={{ flex: 1, zIndex: 0, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} onPress={() => this.didSelectItemFromCell(item)} />
            </View>
        )
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    didSelectItemFromCell = (item) => {
        this.setState({
            selectedVendor: item
        })

        // this.props.navigation.navigate("InputCardNumberScreen", {
        //     vendor: item
        // })
    }

    handleBackFromLogin = () => {
        return true;
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    flatListContainerStyle: {
        flex: 1,
        marginTop: 100,
        marginBottom: 10
    },
    flatListStyle: {
        flex: 1,
        marginHorizontal: 35
    },
    cellContainerStyle: {
        left: 0,
        justifyContent: 'center'
    },
    postTitleStyle: {
        left: 15,
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: 'white',
        textAlignVertical: 'top',
    }
})