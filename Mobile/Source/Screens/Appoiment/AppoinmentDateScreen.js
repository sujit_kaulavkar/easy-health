
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Image,
    ScrollView,
    TextInput,
    Alert
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    APP_NAME,
    CANCEL_TEXT,
    YES_TEXT,
    SCREEN_WIDTH,
    DARK_BLUE_COLOR,
    BLUE_COLOR,
    TOP_BAR_HEIGHT,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION
} from '../../Helper/Constants';

import DropdownAlert from 'react-native-dropdownalert';

export default class AppoinmentDateScreen extends Component {

    constructor() {
        super()

        this.state = {
            selectedIndex: -1
        }
    }


    remoteProceedCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showLoader()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { facility, hospital } = this.props.navigation.state.params

        const mbNumber = this.refs.contactNumber.getText()
        const location = this.refs.location._lastNativeText == undefined ? "" : this.refs.location._lastNativeText.trim()
        const dateTime = this.refs.dateTime.getText()
        const paymentMode = this.state.selectedIndex == 1 ? "cash" : "card"
        apiHelper.placeOrder(user.name, user.gender, user.age, dateTime, hospital.id, facility.id, mbNumber, location, paymentMode)
            .then((responseJson) => {
                this.hideLoader()

                if (responseJson) {
                    this.handleRegistrationResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleRegistrationResponse(response) {
        const status = response.success;
        if (status == true) {
            this.props.navigation.navigate("SummaryAppoinment", {
                key: response.data.key
            })
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
        }
    }

    render() {

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { facility, hospital } = this.props.navigation.state.params

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='CONFIRM APPOINTMENT'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                <ScrollView
                    style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1, paddingBottom: 10, paddingTop: 10 }}>
                    <View>
                        <View style={{
                            marginHorizontal: 20,
                            justifyContent: 'space-around'
                        }}>
                            <Text style={{
                                fontSize: moderateScale(14, 0.09),
                                fontFamily: 'OpenSans-Regular',
                                marginTop: 5,
                                height: 40,
                                color: BLUE_COLOR,
                                backgroundColor: 'white',
                                paddingLeft: 15,
                                borderWidth: 1,
                                borderColor: 'rgba(0,0,0,0.2)',
                                paddingTop: 10
                            }}>
                                PATIENT NAME:  <Text style={{
                                    fontSize: moderateScale(15, 0.09),
                                    fontFamily: 'OpenSans-Regular',
                                    marginLeft: 5,
                                    color: DARK_BLUE_COLOR
                                }}>
                                    {user.name.toUpperCase()}
                                </Text>
                            </Text>

                            <Text style={{
                                fontSize: moderateScale(14, 0.09),
                                fontFamily: 'OpenSans-Regular',
                                paddingLeft: 15,
                                marginTop: 5,
                                height: 40,
                                backgroundColor: 'white',
                                color: BLUE_COLOR,
                                borderWidth: 1,
                                borderColor: 'rgba(0,0,0,0.2)',
                                paddingTop: 10
                            }}>
                                PATIENT GENDER:  <Text style={{
                                    fontSize: moderateScale(15, 0.09),
                                    fontFamily: 'OpenSans-Regular',
                                    marginLeft: 5,
                                    color: DARK_BLUE_COLOR
                                }}>
                                    {user.gender.toUpperCase()}
                                </Text>
                            </Text>

                            <Text style={{
                                fontSize: moderateScale(14, 0.09),
                                fontFamily: 'OpenSans-Regular',
                                paddingLeft: 15,
                                marginTop: 5,
                                height: 40,
                                backgroundColor: 'white',
                                color: BLUE_COLOR,
                                borderWidth: 1,
                                borderColor: 'rgba(0,0,0,0.2)',
                                paddingTop: 10
                            }}>
                                PATIENT AGE:  <Text style={{
                                    fontSize: moderateScale(16, 0.09),
                                    fontFamily: 'OpenSans-Regular',
                                    marginLeft: 5,
                                    color: DARK_BLUE_COLOR
                                }}>
                                    {user.age.toUpperCase()} YEARS
                        </Text>
                            </Text>

                            <Text style={{
                                fontSize: moderateScale(14, 0.09),
                                fontFamily: 'OpenSans-Regular',
                                paddingLeft: 15,
                                marginTop: 5,
                                height: 40,
                                backgroundColor: 'white',
                                color: BLUE_COLOR,
                                borderWidth: 1,
                                borderColor: 'rgba(0,0,0,0.2)',
                                paddingTop: 10
                            }}>
                                FACILITY:  <Text style={{
                                    fontSize: moderateScale(16, 0.09),
                                    fontFamily: 'OpenSans-Regular',
                                    marginLeft: 5,
                                    color: DARK_BLUE_COLOR
                                }}>
                                    {facility.name}
                                </Text>
                            </Text>

                            <Text style={{
                                fontSize: moderateScale(14, 0.09),
                                fontFamily: 'OpenSans-Regular',
                                paddingLeft: 15,
                                marginTop: 5,
                                height: 40,
                                backgroundColor: 'white',
                                color: BLUE_COLOR,
                                paddingTop: 10,
                                borderWidth: 1,
                                borderColor: 'rgba(0,0,0,0.2)',
                            }}>
                                PROVIDER:  <Text style={{
                                    fontSize: moderateScale(16, 0.09),
                                    fontFamily: 'OpenSans-Regular',
                                    marginLeft: 5,
                                    color: DARK_BLUE_COLOR
                                }}>
                                    {hospital.name}
                                </Text>
                            </Text>
                        </View>

                        <View style={{ height: 200 }}>
                            <Components.USER_INPUT
                                defaultText={user.contact_no}
                                ref="contactNumber"
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                            >CONTACT NUMBER</Components.USER_INPUT>

                            <View style={{
                                marginTop: 30,
                                marginHorizontal: SCREEN_WIDTH * 0.09,
                            }}>
                                <Text style={{
                                    color: "rgba(30, 34, 170, 0.7)",
                                    fontSize: moderateScale(18, 0.09),
                                    fontFamily: 'BarlowCondensed-Medium',
                                }}>ADDRESS</Text>
                                <TextInput
                                    defaultValue={user.address}
                                    multiline={true}
                                    numberOfLines={100}
                                    ref="location"
                                    style={{
                                        color: DARK_BLUE_COLOR,
                                        paddingHorizontal: 5,
                                        textAlignVertical: 'top',
                                        height: 60,
                                        marginTop: 10,
                                        borderColor: 'rgba(0,0,0,0.3)',
                                        borderWidth: 1,
                                        borderRadius: 10
                                    }} />
                            </View>

                            <Components.USER_INPUT
                                ref="dateTime"
                                returnKeyType={'done'}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                            >APPOINTMENT DATE & TIME</Components.USER_INPUT>
                        </View>

                        <View style={{
                            alignSelf: 'center',
                            height: 100,
                            width: 200,
                            top: 40,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-around',
                        }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={{
                                    alignItems: 'center', borderRadius: 10, justifyContent: 'center', width: 80, height: 80,
                                }}
                                onPress={() => this.codButtonAction()}>
                                <Image style={{ width: 70, height: 70 }} source={require("../../Resources/Images/cod.jpg")} />

                                <Text style={{
                                    fontSize: this.state.selectedIndex == 1 ? moderateScale(15, 0.09) : moderateScale(13, 0.09),
                                    fontFamily: this.state.selectedIndex == 1 ? 'OpenSans-Bold' : 'OpenSans-Regular',
                                    marginLeft: 5,
                                    marginTop: 5,
                                    color: DARK_BLUE_COLOR,
                                    alignSelf: 'center'
                                }}>COD</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={{ alignItems: 'center', borderRadius: 10, justifyContent: 'center', width: 80, height: 80 }} activeOpacity={0.7}
                                onPress={() => this.onlineButtonAction()}>
                                <Image style={{ width: 70, height: 70 }} source={require("../../Resources/Images/online.png")} />

                                <Text style={{
                                    fontSize: this.state.selectedIndex == 2 ? moderateScale(15, 0.09) : moderateScale(13, 0.09),
                                    fontFamily: this.state.selectedIndex == 2 ? 'OpenSans-Bold' : 'OpenSans-Regular',
                                    marginLeft: 5,
                                    marginTop: 5,
                                    color: DARK_BLUE_COLOR,
                                    alignSelf: 'center'
                                }}>ONLINE</Text>

                            </TouchableOpacity>
                        </View>

                        <View style={{ marginTop: 50 }}>
                            <Components.BUTTON_SUBMIT
                                ref="submitButton"
                                activeOpacity={0.7}
                                style={styles.btnSubmit}
                                title="PROCEED"
                                onPress={() => this.proceedButtonAction()} />
                        </View>

                    </View>
                </ScrollView>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        )
    }

    codButtonAction() {
        this.setState({
            selectedIndex: 1
        })
    }

    onlineButtonAction() {
        this.setState({
            selectedIndex: 2
        })
    }

    showLoader() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideLoader() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }

    proceedButtonAction() {

        const mbNumber = this.refs.contactNumber.getText()
        const location = this.refs.location._lastNativeText == undefined ? "" : this.refs.location._lastNativeText.trim()
        const dateTime = this.refs.dateTime.getText()

        validator = new Helper.VALIDATOR(mbNumber)
        errorMessage = validator.validateMobileNumber()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(location)
        errorMessage = validator.validateAddress()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(dateTime)
        errorMessage = validator.validateDate()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        if (this.state.selectedIndex == -1) {
            this.dropdown.alertWithType('error', "", "Select Payment mode");
            return;
        }

        Alert.alert(
            APP_NAME,
            "Are you sure that you want to book the appointment?",
            [
                {
                    text: CANCEL_TEXT, onPress: () => { }
                },
                {
                    text: YES_TEXT, onPress: () => {
                        setTimeout(() => {
                            this.remoteProceedCall()
                        }, 500);
                    }
                }
            ],
            { cancelable: false }
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    flatListContainerStyle: {
        flex: 1,
        marginTop: 100,
        marginBottom: 10
    },
    flatListStyle: {
        flex: 1,
        marginHorizontal: 35
    },
    cellContainerStyle: {
        left: 0,
        justifyContent: 'center'
    },
    postTitleStyle: {
        left: 15,
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: 'white',
        textAlignVertical: 'top',
    }
})