import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View
} from 'react-native';

import {
    Components,
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    SCREEN_WIDTH, TOP_BAR_HEIGHT, DARK_BLUE_COLOR,
} from '../../Helper/Constants';
import LottieView from 'lottie-react-native';
export default class SummaryAppoinment extends Component {

    constructor() {
        super()
    }

    componentDidMount() {
        setTimeout(() => {
            this.lottie.play()
        }, 500);
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='SUCCESS'
                    isMenuButtonNeeded={true}
                    menuButtonAction={() => {
                        this.props.navigation.openDrawer()
                    }} />

                {this._renderDefaultContent()}

            </Components.BACKGROUND_IMAGE>
        )
    }

    _renderDefaultContent() {

        const {key} = this.props.navigation.state.params
        return (
            <View style={styles.background}>
                <View style={{
                    width: 300,
                    height: 300,
                    justifyContent: 'center',
                    alignItems: 'center'                    
                }}>
                    <LottieView
                        ref={lottie => this.lottie = lottie}
                        style={{
                            width: 300,
                            height: 300,
                        }}
                        loop={false}
                        source={require("../../Resources/Lottie/request_loader.json")}
                    />
                </View>

                <View>
                    <Text style={{
                        textAlign:'center',
                        color: DARK_BLUE_COLOR,
                        fontSize: moderateScale(18, 0.09),
                        fontFamily: 'OpenSans-Regular',
                    }}>YOUR ORDER REFERENCE ID: {'\n'} {key} </Text>
                </View>
            </View>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,        
    },
    background: {
        position: 'absolute',
        top: TOP_BAR_HEIGHT + 100,
        bottom: 0,
        left: 0,
        right: 0, 
        alignItems: 'center'
    }
})