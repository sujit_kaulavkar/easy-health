
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import { Components, Helper } from "../../Helper/FilePath";
import NotificationCell from './NotificationCell';
import { TOP_BAR_HEIGHT } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default class NotificationDetailScreen extends Component {

    componentDidMount() {

        const { item } = this.props.navigation.state.params;

        if (item.is_read == 0) {
            this.remoteReadAllNotificationsRequest()
        }
    }

    remoteReadAllNotificationsRequest() {
        let apiHelper = Helper.API_HELPER.getInstance();
        const user = Helper.USER_MANAGER.getInstance().getUser()

        const { item } = this.props.navigation.state.params;
        apiHelper.notificationRead(user.user_hash_id, item.id)
            .then((responseJson) => {
                if (responseJson) {
                    this.handleReadAllNotificationsResponse(responseJson)
                }
            })
    }

    handleReadAllNotificationsResponse(responseJson) {
        const status = responseJson["status"]
        if (status == 200) {
            const { item, reloadData } = this.props.navigation.state.params;
            item.is_read = 1
            reloadData()
        }
    }

    render() {

        const params = this.props.navigation.state.params;
        const item = params.item
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Notification Details'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={styles.cellStyle}>
                    <NotificationCell item={item} />
                </View>

                <View style={styles.detailStyle}>
                    <Text style={[styles.textStyle, { color: "white", left: 0 }]}>
                        Details
                    </Text>
                    <View style={[styles.detailViewStyle, { color: '#A8A8A8' }]}>
                        <Text style={styles.textStyle}>
                            Platform
                    </Text>
                        <Text style={styles.textStyle}>
                            {item.platform}
                        </Text>
                    </View>
                </View>

            </Components.BACKGROUND_IMAGE>
        );
    }
}

const styles = StyleSheet.create({
    cellStyle: {
        top: TOP_BAR_HEIGHT + 30
    },
    detailStyle: {
        top: TOP_BAR_HEIGHT * 2 + 50,
        marginHorizontal: 15,
    },
    detailViewStyle: {
        height: 60,
        top: 5,
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    textStyle: {
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: "#707070",
        left: 10
    }
})