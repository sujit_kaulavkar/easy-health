
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import Octicons from 'react-native-vector-icons/Octicons';
import { BLUE_COLOR, DARK_BLUE_COLOR } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default class NotificationCell extends Component {
    render() {

        const { item } = this.props
        return (
            <View style={styles.container}>
                <View style={[styles.subContainer,{borderColor:item.is_read ? "transparent" : DARK_BLUE_COLOR}]}>
                    <View style={styles.readViewStyle}>
                        <Octicons
                            name="primitive-dot"
                            color={item.is_read ? "#C0C0C0" : BLUE_COLOR}
                            size={60}
                            style={{ alignSelf: 'center'}} />
                    </View>

                    <View style={styles.detailView}>
                        <Text style={styles.textStyle}>{item.message}</Text>
                        <Text style={styles.textStyle}>{item.activity_time}</Text>
                    </View>
                </View>

                <TouchableOpacity style={{ flex: 1, zIndex: 0, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} onPress={() => this.notificationButtonAction(item)} />                
            </View>
        );
    }

    notificationButtonAction(item) {
        if (typeof this.props.showNotificationDetails == 'function') {
            this.props.showNotificationDetails(item)
        }        
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: 70
    },
    subContainer: {
        flex: 1,
        height: 60,
        marginHorizontal: 15,
        borderRadius: 5,
        borderWidth:1,                
        backgroundColor:'white',
        flexDirection: 'row'
    },
    readViewStyle: {
        height: 60,
        width: 60,
        justifyContent:'center',        
        alignItems:'center'
    },
    detailView: {
        flex: 1,        
        marginLeft:5,
        justifyContent:'center'
    },
    textStyle:{
        fontSize: moderateScale(13.5, 0.09),
        fontFamily: 'OpenSans-Regular'
    }
})