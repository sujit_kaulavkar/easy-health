
import React, { Component } from 'react';
import {
    View,
    Text,
    Animated,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import DropdownAlert from 'react-native-dropdownalert';

import {
    Components,
    Helper
} from "../../Helper/FilePath";

import {
    IS_IOS,
    TOP_BAR_HEIGHT,
    STATUS_BAR_HEIGHT_IOS,
    SCREEN_WIDTH,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION
} from '../../Helper/Constants';

import NotificationCell from './NotificationCell';
import { moderateScale } from '../../Helper/Scaling';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
//Offset value for which we need to load more the content
export const CONTENT_LOAD_OFFSET_POSITION = 0.3

export default class NotificationScreen extends Component {

    constructor() {
        super()

        totalPages = 1;
        pageNumber = 1;
        isLoadingMore = false

        overScrollModeStatus = 'never'
        visibleItemIndex = 0;

        const scrollAnim = new Animated.Value(0);
        const offsetAnim = new Animated.Value(0);
        reloadData = this.reloadData.bind(this)
        onViewableItemsChanged = this.onViewableItemsChanged.bind(this)
        showNotificationDetails = this.showNotificationDetails.bind(this)

        this.state = {
            scrollAnim,
            offsetAnim,
            isAllmessagesRead: true,
            isShowSpinner: false,
            notifications: [],
            clampedScroll: Animated.diffClamp(
                Animated.add(
                    scrollAnim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 0.5],
                        extrapolateLeft: 'clamp',
                    }),
                    offsetAnim,
                ),
                0,
                TOP_BAR_HEIGHT - STATUS_BAR_HEIGHT_IOS,
            ),
        }
    }

    componentDidMount() {
        this.state.scrollAnim.addListener(({ value }) => {
            this.handleScroll()
        });

        this.remoteFetchNotificationsRequest()
    }

    componentWillUnmount() {
        this.state.scrollAnim.removeAllListeners();
    }

    handleScroll() {
        const { notifications } = this.state
        if (notifications && (notifications.length > 0)) {
            if (visibleItemIndex / notifications.length > CONTENT_LOAD_OFFSET_POSITION) {
                this.handleLoadMore()
            }
        }
    }

    clearPages() {
        totalPages = 1;
        pageNumber = 1;
        isLoadingMore = false;
    }

    remoteFetchNotificationsRequest() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        if (pageNumber == 1) {
            this.showSpinner()
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.notification(user.user_hash_id, pageNumber)
            .then((responseJson) => {
                this.isLoadingMore = false
                this.hideSpinner()
                if (responseJson) {
                    this.handleNotificationSuccess(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.isLoadingMore = false
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleNotificationSuccess(responseJson) {
        const status = responseJson["status"]
        if (status == 200) {
            const data = responseJson.data
            totalPages = data.total_pages

            const notifications = data.data
            const allNotifications = this.state.notifications.concat(notifications)
            const unreadNotifications = allNotifications.filter((notification) => {
                return notification.is_read == 0
            })

            let isReadAllMessages = true
            if (unreadNotifications.length > 0) {
                isReadAllMessages = false
            }

            this.setState({
                isAllmessagesRead: isReadAllMessages,
                notifications: allNotifications
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    remoteReadAllNotificationsRequest() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        this.showSpinner()
        apiHelper.notificationRead(user.user_hash_id)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleReadAllNotificationsResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleReadAllNotificationsResponse(responseJson) {
        const status = responseJson["status"]
        if (status == 200) {
            const notifications = this.state.notifications
            notifications.map((item) => {
                item.is_read = true
            })
            this.setState({
                isAllmessagesRead: true,
                notifications: notifications
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    handleLoadMore = () => {

        if ((totalPages != 1) && totalPages > pageNumber) {
            if (isLoadingMore == false) {
                isLoadingMore = true
                pageNumber = pageNumber + 1;
                this.remoteFetchNotificationsRequest()
            }
        }
    }

    onViewableItemsChanged({ viewableItems }) {

        let item = viewableItems[viewableItems.length - 1]
        if (item) {
            visibleItemIndex = item.index
        }
    }

    viewabilityConfig = {
        viewAreaCoveragePercentThreshold: 90 // how much an item need to be visible in precent
    }

    render() {

        const animOffset = Animated.event([{
            nativeEvent:
            {
                contentOffset:
                {
                    y: this.state.scrollAnim
                }
            }
        }],
            { useNativeDriver: true }
        )

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Notifications'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                {
                    this.state.isAllmessagesRead == false ? <View style={styles.readAllView}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={styles.readAllBtn}
                            onPress={() => this.readAllAction()}>
                            <Ionicons
                                name="md-checkmark-circle-outline"
                                color="white"
                                size={IS_IOS ? 25 : 20}
                                style={{ alignSelf: 'center', marginRight: 10 }} />

                            <Text style={styles.readAllText}>
                                Mark all as read
                    </Text>
                        </TouchableOpacity>
                    </View> : null
                }

                <AnimatedFlatList
                    onScroll={animOffset}
                    scrollEventThrottle={1}
                    removeClippedSubviews={true}
                    legacyImplementation={false}
                    overScrollMode={overScrollModeStatus}
                    key="notificationFlatList"
                    style={styles.flatListStyle}
                    extraData={this.state}
                    data={this.state.notifications}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => this.renderCellItem(item)}
                    onViewableItemsChanged={(this.onViewableItemsChanged)}
                    viewabilityConfig={this.viewabilityConfig}
                />

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    renderCellItem(item) {
        return <NotificationCell item={item} showNotificationDetails={(item) => this.showNotificationDetails(item)} />
    }

    readAllAction() {
        this.remoteReadAllNotificationsRequest()
    }

    showNotificationDetails(item) {
        this.props.navigation.navigate("NotificationDetailScreen", {
            item: item,
            reloadData: () => this.reloadData()
        })
    }

    reloadData() {
        const { notifications } = this.state
        this.setState({
            notifications: notifications
        })
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    flatListStyle: {
        flex: 1,
        marginTop: TOP_BAR_HEIGHT + 10
    },
    readAllView: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        top: TOP_BAR_HEIGHT,
        marginTop: 10,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.04, 0.09)
    },
    readAllText: {
        bottom: 2,
        color: 'white',
        textAlign: 'right',
        alignSelf: 'center',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    readAllBtn: {
        flexDirection: 'row'
    }
})