import React, { PureComponent } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList
} from 'react-native';
import Dash from 'react-native-dash';

import {
    Helper
} from '../../Helper/FilePath';

import { moderateScale } from '../../Helper/Scaling';
import { TopSpendings } from '../Dashboard/CardView/static/entries';
import { IS_IOS, topHeaderHeight, SCREEN_WIDTH } from '../../Helper/Constants';

const top3Speninds = "Top 3 Spendings"

export default class TopSpendingsView extends PureComponent {

    constructor() {
        super()
        this.state = {
            topSpendings: []
        }
    }

    loadTop3Spendings() {
        this.remoteTop3SpendingsCall()
    }

    remoteTop3SpendingsCall() {
        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            return;
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.top3Spendings(user.user_hash_id).then((responseJson) => {            
            if (responseJson) {
                this.handleTop3SpeningsResponse(responseJson)
            }
        })
        .catch((error) => {
        })
    }

    handleTop3SpeningsResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            this.setState({
                topSpendings: data
            })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Dash
                    dashGap={10}
                    dashLength={10}
                    dashColor='rgba(255,255,255,0.4)'
                    style={{
                        height: 1,
                        width: SCREEN_WIDTH,
                    }}
                />

                {this.state.topSpendings.length != 0 ? (
                    <View>
                        <Text style={styles.titleStyle}>
                            {top3Speninds}
                        </Text>
                        <FlatList
                            style={styles.flatListStyle}
                            data={this.state.topSpendings}
                            key="topSpendings"
                            contentContainerStyle={{ paddingTop: (IS_IOS ? 0 : topHeaderHeight) }}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => this.renderCellItem(item, index)}
                        />
                    </View>
                ) : null}

            </View>
        )
    }

    renderCellItem(item, index) {

        return (
            <View style={styles.cellStyle}>
                <View style={styles.cellSubContainer}>
                    <View style={styles.venderNameContainerStyle}>
                        <Text style={styles.numberStyle}>
                            {++index}
                        </Text>
                        <Text style={styles.venderNameStyle}>
                            {item.merchant_name}
                        </Text>
                    </View>

                    <View style={styles.amountContainerStyle}>
                        <Text style={styles.amountSpendStyle}>
                            {item.total_spending_amount}
                        </Text>

                        <Text style={styles.amountStyle}>
                            SGD
                    </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    titleStyle: {
        top: 15,
        left: 20,
        color: 'white',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular',
    },
    flatListStyle: {
        flex: 1,
        paddingTop: 30
    },
    amountStyle: {
        color: 'black',
        fontSize: moderateScale(23, 0.09),
    },
    cellStyle: {
        flex: 1,
        height: 80,
        flexDirection: 'row'
    },
    cellSubContainer: {
        flex: 1,
        height: 60,
        flexDirection: 'row',
        marginHorizontal: 15
    },
    venderNameContainerStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    numberStyle: {
        textAlign: 'center',
        marginLeft: 5,
        paddingTop: 2,
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular',
        width: 25,
        height: 25,
        borderRadius: 12.5,
        borderWidth: 1,
        borderColor: 'white',
        color: 'white'
    },
    venderNameStyle: {
        marginLeft: 20,
        marginRight: 45,
        color: 'white',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'OpenSans-Regular',
    },
    amountContainerStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "flex-end"
    },
    amountSpendStyle: {
        paddingRight: 20,
        color: 'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
    },
    amountStyle: {
        right: 15,
        color: 'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
    }
})