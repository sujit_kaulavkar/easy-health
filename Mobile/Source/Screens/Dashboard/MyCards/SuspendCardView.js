import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Dash from 'react-native-dash';
import { moderateScale } from '../../../Helper/Scaling';
import { SCREEN_WIDTH, APP_NAME, NO_TEXT, YES_TEXT, SUSPEND_CARD_MESSAGE, APP_THEME } from '../../../Helper/Constants';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    Helper
} from '../../../Helper/FilePath';

export default class SuspendCardView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isShowSpinner: false
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <Dash
                    dashGap={10}
                    dashLength={10}
                    dashColor='rgba(255,255,255,0.4)'
                    style={{
                        height: 1,
                        width: SCREEN_WIDTH,
                    }}
                />
                
                <TouchableOpacity
                    style={styles.subContainerStyle}
                    activeOpacity={1}
                    onPress={() => this.suspendButtonAction()}>
                    <Text style={{
                        color: "white",
                        fontSize: moderateScale(15, 0.09)
                    }}>
                        Suspend Card
                    </Text>

                    <Ionicons
                        name="ios-close-circle-outline"
                        color="white"
                        size={20}
                    />
                </TouchableOpacity>

                <Dash
                    dashGap={10}
                    dashLength={10}
                    dashColor='rgba(255,255,255,0.4)'
                    style={{
                        height: 1,
                        width: SCREEN_WIDTH,
                    }}
                />

                <View style={{ justifyContent: 'flex-end', marginTop: 10, marginBottom: 10 }}>
                    <Image
                        style={{ alignSelf: 'center', height: 30, width: 300 }}
                        source={require('../../../Resources/Images/mm_logo.png')} />
                </View>
               
            </View>
        )
    }

    suspendButtonAction() {
        Alert.alert(
            APP_NAME,
            SUSPEND_CARD_MESSAGE,
            [
                {
                    text: NO_TEXT, onPress: () => { }
                },
                {
                    text: YES_TEXT, onPress: () => { this.remoteSuspendCardCall() }
                }
            ],
            { cancelable: false }
        )
    }

    remoteSuspendCardCall() {

        const card = this.props.selectedCard
        const cardId = card.card_hash_id
        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.props.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.suspendCard(user.user_hash_id, cardId)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleSuspendCardResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.props.showServerErrorMessage()
            })
    }

    handleSuspendCardResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            this.props.suspendButtonAction()
        }
        else {
            this.props.showServerErrorMessage()
        }
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainerStyle: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 20,
        height: 45
    }
})