
import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';

import {
    DASHBOARD,
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT, 
    SOMETHING_WRONG_MESSAGE, 
    APP_THEME, 
    NO_INTERNET_CONNECTION, 
    BLUE_COLOR,
    SCREEN_WIDTH
} from '../../../Helper/Constants';
import TransactionJSON from '../../../Resources/JSON/Transaction.json';
import Events from 'react-native-simple-events';
import deepcopy from 'deepcopy';
import {
    itemHeight
} from '../CardView/styles/SliderEntry.style';
import { moderateScale } from '../../../Helper/Scaling';

export default class TransactionHistoryScreen extends Component {

    constructor(props) {
        super(props)

        const card = props.navigation.state.params.selectedCard;
        this.state = {
            selectedCard: card,
            isShowSpinner: false,
            isShowCVC: false
        }

        selectedTransactionAction = this.selectedTransactionAction.bind(this)
        loadCard = this.loadCard.bind(this)
        unloadCard = this.unloadCard.bind(this)
        handleCardScroll = this.handleCardScroll.bind(this)
        handleSelectedCard = this.handleSelectedCard.bind(this)
        generateCVC = this.generateCVC.bind(this)
        suspendButtonAction = this.suspendButtonAction.bind(this)
        removeCVCVIew = this.removeCVCVIew.bind(this)
        showNoInternetAlert = this.showNoInternetAlert.bind(this)
        showLoyaltyPointScreen = this.showLoyaltyPointScreen.bind(this)
        updateLoyaltyPoints = this.updateLoyaltyPoints.bind(this)
    }

    componentDidMount() {
        this.addObservers()
    }

    componentWillUnmount() {
        didFocusObserver.remove();
    }

    addObservers() {
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {
                Events.trigger('unlockDrawer')
            }
        );
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE                
                style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Transactions History'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />
                <View style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}>
                    <DASHBOARD.TRANSACTION_LIST_VIEW
                        selectedCard={this.state.selectedCard}
                        headerComponent={this.renderHeaderComponent()}
                        showSuspendCardOption={this.showSuspendCardOption()}
                        selectedIndex={(transaction) => this.selectedTransactionAction(transaction)} />
                </View>

                {this.state.isShowCVC ? this.showCVCOption() : null}

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        );
    }

    renderHeaderComponent() {
        const params = this.props.navigation.state.params;
        return (
            <View>
                <View style={{ height: itemHeight + 150 }}>
                    <DASHBOARD.CARDS_VIEW
                        classRef={this}
                        cards={params.cards}
                        showLoyaltyPointScreen={(card) => this.showLoyaltyPointScreen(card)}
                        selectedCard={this.state.selectedCard}
                        loadCard={() => this.loadCard()}
                        unloadCard={() => this.unloadCard()}
                        handleCardScroll={(card) => this.handleCardScroll(card)}
                        handleSelectedCard={(card) => { this.handleSelectedCard(card) }}
                    />

                    {this.showMMMessage()}                    
                </View>
                
                {this.showGeneratePinOption()}                
            </View>
        )
    }

    showMMMessage() {
        return (
            <Text style={{
                top: 5,
                textAlign: 'center',
                color: 'white',
                marginHorizontal: 20,
                fontFamily: 'OpenSans-Regular',
                fontSize: moderateScale(13, 0.09)
            }}>
                This card is issued by MatchMove Pay Pte Ltd pursuant to license by MasterCard Asia/Pacific Pte Ltd
                    </Text>
        )
    }

    showGeneratePinOption() {

        const { selectedCard } = this.state
        const isActive = selectedCard.status == "locked" ? false : true
        if (isActive == false) {
            return null
        }

        const isCardEmpty = this.state.selectedCard.card_balance >= 1 ? false : true
        return (
            <View style={styles.cardActionBaseViewStyle}>
                <TouchableOpacity
                    activeOpacity={1}
                    disabled={isCardEmpty}
                    style={{ width: 150, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: isCardEmpty ? '#E8E8E8' : BLUE_COLOR }} onPress={() => this.generateATMPin()}>
                    <Text style={{ fontSize: moderateScale(13, 0.09), color: 'white', fontFamily: 'OpenSans-Regular' }}>Generate ATM Pin</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    activeOpacity={1}
                    disabled={isCardEmpty}
                    style={{ width: 100, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: isCardEmpty ? '#E8E8E8' : BLUE_COLOR }} onPress={() => this.generateCVC()}>
                    <Text style={{ fontSize: moderateScale(13, 0.09), color: 'white', fontFamily: 'OpenSans-Regular' }}>Generate CVC</Text>
                </TouchableOpacity>
            </View>
        )
    }

    showCVCOption() {        
        const { selectedCard } = this.state
        const isActive = selectedCard.status == "locked" ? false : true
        if (isActive == false) {
            return null
        }        

        return (
            <DASHBOARD.CVC_VIEW
                removeCVCVIew={() => this.removeCVCVIew()}
                showNoInternetAlert={() => this.showNoInternetAlert()}
                showServerErrorMessage={() => this.showServerErrorMessage()}
                selectedCard={this.state.selectedCard} />
        )
    }

    removeCVCVIew() {
        this.setState({
            isShowCVC: false
        })
    }

    showSuspendCardOption() {

        const { selectedCard } = this.state
        const isActive = selectedCard.status == "locked" ? false : true
        if (isActive == false) {
            return null
        }

        return (
            <DASHBOARD.SUSPEND_CARD_VIEW
                showSpinner={() => this.showSpinner()}
                hideSpinner={() => this.hideSpinner()}
                showNoInternetAlert={() => this.showNoInternetAlert()}
                showServerErrorMessage={() => this.showServerErrorMessage()}
                selectedCard={this.state.selectedCard}
                suspendButtonAction={() => this.suspendButtonAction()} />
        )
    }

    showLoyaltyPointScreen(card) {
        this.props.navigation.navigate("LoyaltyPointsScreen", {
            card: card,
            updateLoyaltyPoints: () => this.updateLoyaltyPoints()
        })
    }

    updateLoyaltyPoints() {
        const params = this.props.navigation.state.params;
        const newItem = deepcopy(this.state.selectedCard)
        var index = params.cards.indexOf(this.state.selectedCard)
        params.cards.splice(index, 1, newItem);

        this.setState({
            selectedCard: newItem
        })
    }

    loadCard() {
        this.props.navigation.navigate("LoadCardScreen", {
            isLoadCard: true,
            card: this.state.selectedCard
        })
    }

    unloadCard() {
        this.props.navigation.navigate("LoadCardScreen", {
            isLoadCard: false,
            card: this.state.selectedCard
        })
    }

    generateATMPin() {
        this.props.navigation.navigate("ATMPinScreen", {
            isLoadCard: true,
            card: this.state.selectedCard
        })
    }

    generateCVC() {
        this.setState({
            isShowCVC: true
        })
    }

    suspendButtonAction() {

        const params = this.props.navigation.state.params;
        const newItem = deepcopy(this.state.selectedCard)
        var index = params.cards.indexOf(this.state.selectedCard)
        params.cards.splice(index, 1, newItem);
        newItem.status = "locked"
        this.setState({
            selectedCard: newItem
        })
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    handleCardScroll(card) {
        this.setState({
            selectedCard: card
        })
    }

    handleSelectedCard(card) {
    }

    selectedTransactionAction(transaction) {
        this.props.navigation.navigate("TransactionDetailsScreen", {
            transaction: transaction
        })
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    cardActionBaseViewStyle: {
        alignSelf: 'center',
        top: 10,
        height: 40,
        width: SCREEN_WIDTH,
        justifyContent: 'space-evenly',
        flexDirection: 'row',
        alignItems: 'center'
    }
})