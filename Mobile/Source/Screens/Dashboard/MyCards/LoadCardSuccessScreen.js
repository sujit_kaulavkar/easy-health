
import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    StyleSheet,
    ImageBackground
} from 'react-native';

import {
    Components    
} from '../../../Helper/FilePath';
import Events from 'react-native-simple-events';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { moderateScale } from '../../../Helper/Scaling';
import { SCREEN_WIDTH, IS_IOS} from '../../../Helper/Constants';

export default class LoadCardSuccessScreen extends Component {

    componentDidMount() {
        Events.trigger('updateCardDetails')
    }

    render() {
        const {card, amount, transactionId, isLoadCard } = this.props.navigation.state.params

        const cardName = card.name
        const currency = card.currency
        const cardAmount = card.card_balance
        const newAmount = isLoadCard ? parseFloat(cardAmount) + parseFloat(amount) : parseFloat(cardAmount) - parseFloat(amount)
        card.card_balance = newAmount

        const message = isLoadCard ? "You have successfully loaded " + amount + " " + currency + " in " + cardName + " card from your wallet"
            : "You have successfully unloaded " + amount + " " + currency + " in wallet from card " + cardName

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title={isLoadCard ? 'Load Card' : 'Unload Card'}
                    isRightCrossNeeded={true}
                    crossButtonAction={() => {
                        this.goToDashboard()
                    }}
                />

                <View style={styles.subContainer}>

                    <MaterialCommunityIcons
                        name="check-circle-outline"
                        color="white"
                        size={100}
                        style={{ alignSelf: 'center' }}
                    />

                    <Text style={styles.messageStyle}>{message}</Text>
                </View>

                <View style={{
                    height: 100,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Text style={{
                        textAlign: 'center',
                        color: "white",
                        fontSize: moderateScale(15, 0.09)
                    }}>
                        ID - {transactionId}
                    </Text>

                    <View style={{
                        top: 5,
                        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.14, 0.09),                        
                        height: 50,
                        flexDirection: 'row'
                    }}>
                        <ImageBackground
                            resizeMode="stretch"
                            style={{
                                width: null,
                                height: null,
                                flex: 1
                            }}
                            source={require('./coupon.png')}>
                            <View style={{
                                marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09),
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                top: IS_IOS ? 18 : 16
                            }}>
                                <Text style={{
                                    fontSize: moderateScale(12, 0.09),
                                    color: "gray"
                                }}>
                                    Updated Card Balance
                            </Text>

                                <Text style={{
                                    fontSize: moderateScale(13, 0.09)
                                }}>
                                    {currency} {newAmount}
                                </Text>
                            </View>
                        </ImageBackground>
                    </View>
                </View>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title={isLoadCard ? 'Load Again' : 'Unload Again'}
                        onPress={() => this.loadAgainButtonAction()} />
                </View>
            </Components.BACKGROUND_IMAGE>
        );
    }

    loadAgainButtonAction() {
        this.props.navigation.goBack()
    }

    goToDashboard() {
        this.props.navigation.popToTop()
        //Events.trigger('ChangeDrawerMenu', { id: DrawerItems.DASHBOARD })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {
        marginTop: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    messageStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: moderateScale(16, 0.09)
    },
    submitButtonBase: {
        top: 20,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
})