
import React, { Component } from 'react';
import {
    View,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropdownAlert from 'react-native-dropdownalert';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    DARK_BLUE_COLOR,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    BLUE_COLOR
} from '../../../Helper/Constants';

import {
    moderateScale
} from '../../../Helper/Scaling';

const PIN_SEND_OPTIONS = {
    EMAIL_OPTION: 1,
    SMS_OPTION: 2
}

const warningEmailAddressMessage = "You will receive your ATM Pin at your registered email address"
const warningMobileNumberMessage = "You will receive your ATM Pin at your registered mobile number"

const atmPinSendToEmailAddress = "Your ATM pin has been sent to your Registered email address"
const atmPinSendToMobileNumber = "Your ATM pin has been sent to your Registered mobile number"

const emailAddressMessage = "To edit your Email address go to"
const smsMessage = "To edit your Mobile number go to"
const settingEmailAddressMessage = "Settings > Edit email address"
const settingPhoneNumberMessage = "Settings > Edit mobile number"

export default class ATMPinScreen extends Component {

    constructor() {
        super()

        this.state = {
            pinSendOption: PIN_SEND_OPTIONS.EMAIL_OPTION,
            isPinSend: false,
            isTouchEnabled: true
        }
    }

    remoteGenerateATMPinCall() {
        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        this.refs.submitButton.startAnimation()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { card } = this.props.navigation.state.params

        let mode = "M"
        if (this.state.pinSendOption == PIN_SEND_OPTIONS.SMS_OPTION) {
            mode = "S"
        }

        apiHelper.generateATMPin(user.user_hash_id, card.card_hash_id, mode)
            .then((responseJson) => {
                if (responseJson) {
                    this.handleGenerateATMPinResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }

                this.hideSpinner()
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleGenerateATMPinResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            this.setState({
                isPinSend: true
            })
        }
        else {
            this.props.showServerErrorMessage()
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Generate ATM Pin'
                    isRightCrossNeeded={true}
                    crossButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={{
                    flex: 1,
                    marginTop: TOP_BAR_HEIGHT,
                    marginBottom: 0
                }}>
                    {this.showATMPinSendUI()}
                    {this.showSuccessUI()}
                    {this.showSettingUI()}
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    showATMPinSendUI() {
        return (
            <View style={[styles.pinSendView, { opacity: this.state.isPinSend == false ? 1 : 0 }]}>
                <Text style={styles.messageStyle}>Send ATM Pin On</Text>

                <View style={styles.cardActionBaseViewStyle}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={[styles.pinSendButton, { backgroundColor: this.state.pinSendOption == PIN_SEND_OPTIONS.EMAIL_OPTION ? DARK_BLUE_COLOR : BLUE_COLOR }]}
                        onPress={() => this.emailButtonAction()}>
                        <Text style={[styles.pinSenText, { color: "white" }]}>Email</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={1}
                        style={[styles.pinSendButton, { backgroundColor: this.state.pinSendOption == PIN_SEND_OPTIONS.SMS_OPTION ? DARK_BLUE_COLOR : BLUE_COLOR }]}
                        onPress={() => this.smsButtonAction()}>
                        <Text style={[styles.pinSenText, { color: "white" }]}>SMS</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    emailButtonAction() {
        this.setState({
            pinSendOption: PIN_SEND_OPTIONS.EMAIL_OPTION
        })
    }

    smsButtonAction() {
        this.setState({
            pinSendOption: PIN_SEND_OPTIONS.SMS_OPTION
        })
    }

    showSuccessUI() {

        const message = this.state.pinSendOption == PIN_SEND_OPTIONS.EMAIL_OPTION ? atmPinSendToEmailAddress : atmPinSendToMobileNumber
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const userDetails = this.state.pinSendOption == PIN_SEND_OPTIONS.EMAIL_OPTION ? user.email : user.mobileNumber
        const warningMessage = this.state.pinSendOption == PIN_SEND_OPTIONS.EMAIL_OPTION ? warningEmailAddressMessage : warningMobileNumberMessage
        return (
            <View style={{
                flex: 1,
                marginTop: 100,
                alignItems: 'center',
            }}>

                {
                    this.state.isPinSend == true ?
                        <View>
                            <MaterialCommunityIcons
                                name="check-circle-outline"
                                color="white"
                                size={130}
                                style={{ alignSelf: 'center' }}
                            />

                            <Text style={styles.cardActivatedText}>{message}</Text>
                            <Text style={styles.cardVisitText}>{userDetails}</Text>
                        </View>
                        :
                        <View>
                            <Text style={styles.cardActivatedText}>{warningMessage}</Text>
                            <Text style={styles.cardVisitText}>{userDetails}</Text>
                        </View>
                }
            </View>
        )
    }

    showSettingUI() {

        const editMessage = this.state.pinSendOption == PIN_SEND_OPTIONS.EMAIL_OPTION ? emailAddressMessage : smsMessage
        const settingsMessage = this.state.pinSendOption == PIN_SEND_OPTIONS.EMAIL_OPTION ? settingEmailAddressMessage : settingPhoneNumberMessage

        return (
            <View style={{
                height: 150,
                width: SCREEN_WIDTH,
                bottom: 0,
                alignItems: 'center',
                paddingTop: 10,
            }}>

                <View style={{
                    opacity: this.state.isPinSend == false ? 1 : 0
                }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.settingButtonAction()}>
                        <Text style={{
                            color: "white",
                            fontSize: moderateScale(14, 0.09),
                            fontFamily: 'OpenSans-SemiBold'
                        }}>{editMessage}</Text>

                        <Text style={{
                            color: DARK_BLUE_COLOR,
                            fontSize: moderateScale(14, 0.09),
                            fontFamily: 'OpenSans-SemiBold'
                        }}>{settingsMessage}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{
                    top: 20,
                    height: 40,
                    width: SCREEN_WIDTH - 80,
                    marginHorizontal: 20,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title={this.state.isPinSend == false ? "Generate Pin" : "Done"}
                        onPress={() => this.submitButtonAction()}
                    />
                </View>
            </View>
        )
    }

    settingButtonAction() {
        if (this.state.pinSendOption == PIN_SEND_OPTIONS.EMAIL_OPTION) {
            this.props.navigation.navigate("EditEmailAddressScreen")
        }
        else {
            this.props.navigation.navigate("EditPhoneNumberScreen")
        }
    }

    submitButtonAction() {
        if (this.state.isPinSend == false) {
            this.remoteGenerateATMPinCall()
        }
        else {
            this.props.navigation.goBack()
        }
    }

    showSpinner() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideSpinner() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    cardActionBaseViewStyle: {
        alignSelf: 'center',
        height: 40,
        width: SCREEN_WIDTH,
        justifyContent: 'space-evenly',

        flexDirection: 'row',
        alignItems: 'center'
    },
    messageStyle: {
        color: 'white',
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-SemiBold'
    },
    pinSendView: {
        marginTop: 20,
        height: 80,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    pinSendButton: {
        width: 140,
        height: 35,
        borderRadius: 35 / 2,
        borderWidth: 0.5,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    pinSenText: {
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-SemiBold'
    },
    cardActivatedText: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        marginHorizontal: 20,
        fontSize: moderateScale(17, 0.09),
        fontFamily: "OpenSans-Regular"
    },
    cardVisitText: {
        top: 15,
        marginHorizontal: 40,
        textAlign: 'center',
        color: 'white',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
})