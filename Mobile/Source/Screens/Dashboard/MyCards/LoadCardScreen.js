
import React, { Component } from 'react';
import {
    View,
    Text,
    Keyboard,
    TextInput,
    StyleSheet
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import {
    Components,
    Helper
} from '../../../Helper/FilePath';
import { TOP_BAR_HEIGHT, SCREEN_WIDTH, APP_THEME, SOMETHING_WRONG_MESSAGE, NO_INTERNET_CONNECTION, IS_IOS } from '../../../Helper/Constants';
import { moderateScale } from '../../../Helper/Scaling';

export default class LoadCardScreen extends Component {

    constructor() {
        super()

        this.state = {
            walletInfo: {
                "walletBalance": "0.0",
                "walletCurrency": "SGD"
            },
            isTouchEnabled: true
        }
    }

    componentDidMount() {
        this.addObservers()
    }

    componentWillUnmount() {
        didFocusObserver.remove();
    }

    addObservers() {
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {
                this.fetchWalletInfo()
            }
        );
    }

    fetchWalletInfo() {
        const walletInfo = Helper.USER_MANAGER.getInstance().getWalletBalance()
        this.setState({
            walletInfo: walletInfo
        })
    }

    remoteLoadUnloadCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showLoader()
        this.refs.submitButton.startAnimation()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { card, isLoadCard } = this.props.navigation.state.params
        const amount = this.refs.textInput._lastNativeText

        if (isLoadCard) {
            apiHelper.loadCard(user.user_hash_id, card.card_hash_id, amount, "Load card")
                .then((responseJson) => {
                    this.hideLoader()
                    if (responseJson) {
                        this.handleLoadUnlodResponse(responseJson)
                    }
                    else {
                        this.showServerErrorMessage()
                    }
                })
                .catch((error) => {
                    this.hideLoader()
                    this.showServerErrorMessage()
                })
        }
        else {
            apiHelper.unloadCard(user.user_hash_id, card.card_hash_id, amount, "Unload card")
                .then((responseJson) => {
                    this.hideLoader()
                    if (responseJson) {
                        this.handleLoadUnlodResponse(responseJson)
                    }
                    else {
                        this.showServerErrorMessage()
                    }
                })
                .catch((error) => {
                    this.hideLoader()
                    this.showServerErrorMessage()
                })
        }
    }

    handleLoadUnlodResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {

            const { card, isLoadCard } = this.props.navigation.state.params
            const amount = this.refs.textInput._lastNativeText

            const walletInfo = this.state.walletInfo
            if (isLoadCard) {
                walletInfo.walletBalance = parseFloat(this.state.walletInfo.walletBalance) - parseFloat(amount)
            }
            else {
                walletInfo.walletBalance = parseFloat(this.state.walletInfo.walletBalance) + parseFloat(amount)
            }

            this.setState({
                walletInfo: walletInfo
            })

            Helper.USER_MANAGER.getInstance().setWalletBalance(walletInfo)
            const transactionId = responseJson.data.transactionId
            this.props.navigation.navigate("LoadCardSuccessScreen", {
                card: card,
                amount: amount,
                isLoadCard: isLoadCard,
                transactionId: transactionId
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    render() {
        const { card, isLoadCard } = this.props.navigation.state.params
        const cardName = card.name
        const cardNumber = "XXXX"
        const cardCurrency = card.currency
        const cardAmount = parseFloat(card.card_balance).toFixed(2)

        const message = isLoadCard ? "Enter the amount to load into your " + cardName + " card which will be deducted from your wallet"
            : "Enter the amount to load into your wallet which will be deducted from your " + cardName + " card"

        return (
            <Components.BACKGROUND_IMAGE
                style={styles.container}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}
                isShowMMLogo={true}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title={isLoadCard ? 'Load Card' : 'Unload Card'}
                    isBackButtonNeeded={true}
                    isRightCrossNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                    crossButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={{
                    flex: 1,
                    top: TOP_BAR_HEIGHT,
                }}>
                    <View style={{
                        height: 84,
                        // backgroundColor: 'rgb(248,248,248)',
                        flexDirection: 'row',
                        paddingVertical: 10,
                    }}>

                        <View style={{ flex: 0.5, justifyContent: 'space-evenly', alignItems: 'center', paddingRight: 20 }}>
                            <Text style={{
                                color: "white",
                                fontSize: moderateScale(13.0)
                            }}>
                                Wallet Balance
                            </Text>

                            <Text style={{
                                color: "white",
                                fontSize: moderateScale(18.0)
                            }}>
                                {this.state.walletInfo.walletCurrency} {this.state.walletInfo.walletBalance}
                            </Text>
                        </View>

                        <View style={{ flex: 0.5, justifyContent: 'space-evenly', alignItems: 'center' }}>
                            <Text style={{
                                color: "white",
                                fontSize: moderateScale(13.0)
                            }}>
                                Card Balance
                            </Text>

                            <Text style={{
                                color: "white",
                                fontSize: moderateScale(18.0)
                            }}>
                                {cardCurrency} {cardAmount}
                            </Text>
                        </View>
                    </View>

                    <View style={{
                        paddingVertical: 5,
                        justifyContent: 'space-around',
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            color: "white",
                            textAlign: 'center',
                            fontSize: moderateScale(14.0),
                            paddingVertical: 10,
                            marginHorizontal: 20
                        }}>
                            Your Loading {cardName} card - {cardNumber}
                        </Text>

                        <Text style={{
                            color: "white",
                            textAlign: 'center',
                            fontSize: moderateScale(11.5),
                            paddingVertical: 10,
                            marginHorizontal: 20
                        }}>
                            {message}
                        </Text>
                    </View>

                    <View style={{
                        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09),
                        flexDirection: 'column',
                        top: 20,
                        height: 60
                    }}>

                        <View style={styles.currencyContainerStyle}>
                            <Text style={styles.currencyTextStyle}>
                                SGD
                                </Text>
                            <View style={styles.verticalSeparatorStyle} />
                            <TextInput
                                placeholder="Enter Amount"
                                ref="textInput"
                                placeholderTextColor='white'
                                style={styles.textInputStyle} />
                        </View>
                        <View style={styles.underlineStyle} />
                    </View>

                    <View style={styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title={isLoadCard ? "Load Card" : "Unload Card"}
                            onPress={() => this.loadCardButtonAction()}
                        />
                    </View>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        );
    }

    showLoader() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideLoader() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }


    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    loadCardButtonAction() {

        const amount = this.refs.textInput._lastNativeText
        let validator = new Helper.VALIDATOR(amount)
        let errorMessage = validator.validateAmount()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        const { card, isLoadCard } = this.props.navigation.state.params
        if (isLoadCard) {
            const walletAmount = this.state.walletInfo.walletBalance
            if (parseFloat(walletAmount) < parseFloat(amount)) {
                this.dropdown.alertWithType('error', "", "Entered amount should not be greater than Wallet balance");
                return;
            }
        }
        else {

            const cardAmount = card.card_balance
            if (parseFloat(cardAmount) < parseFloat(amount)) {
                this.dropdown.alertWithType('error', "", "Entered amount should not be greater than Card balance");
                return;
            }
        }

        Keyboard.dismiss()

       // if (this.isLoadCardLimitReached() == false) {
            this.remoteLoadUnloadCall()
       // }        
    }

    isLoadCardLimitReached() {

        const { card, isLoadCard } = this.props.navigation.state.params

        const amount = parseFloat(this.refs.textInput._lastNativeText).toFixed(2)       
        
        if (isLoadCard == true) {
            const message = new Helper.KYC_LIMIT().checkForLoadLimit(amount)
            if (message.length > 0) {
                this.dropdown.alertWithType('error', "", message);
                return true
            }
        }
        else {
            const message = new Helper.KYC_LIMIT().checkForUnloadLimit(amount)
            if (message.length > 0) {
                this.dropdown.alertWithType('error', "", message);
                return true
            }
        }        

        return false
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    currencyContainerStyle: {
        top: 5,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center'
    },
    currencyTextStyle: {
        color: "white",
        fontSize: moderateScale(15, 0.09)
    },
    verticalSeparatorStyle: {
        left: 5,
        height: "50%",
        width: 1,
        backgroundColor: 'gray'
    },
    textInputStyle: {
        flex: 1,
        left: 10,
        color: 'white',
        paddingBottom: IS_IOS ? 0 : 5,
        fontSize: moderateScale(15, 0.09)
    },
    underlineStyle: {
        height: 1,
        bottom: -3,
        width: "100%",
        backgroundColor: '#C0C0C0'
    },
    submitButtonBase: {
        top: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
}) 