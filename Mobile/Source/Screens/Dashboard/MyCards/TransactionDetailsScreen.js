
import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    StyleSheet
} from 'react-native';

import {
    Components
} from '../../../Helper/FilePath';
import { moderateScale } from '../../../Helper/Scaling';
import { format } from 'date-fns'

export default class TransactionDetailsScreen extends Component {

    render() {
        const params = this.props.navigation.state.params;
        const transaction = params.transaction;
        
        const date = new Date(transaction.date)
        const dateString = format(date, 'YYYY-DD-MM HH:MM:SS')

        let loyaltyPoints = 0.0
        if (transaction.loyalty_points != null) {
            loyaltyPoints = parseFloat(transaction.loyalty_points).toFixed(2)
        }        

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Transactions Details'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={styles.loyaltyPointsContainerViewStyle}>
                    {/* <Text style={styles.loyaltyAmountStyle}>
                        {loyaltyPoints}
                    </Text> */}

                    <Text style={styles.loyaltyMessageStyle}>
                        {this.getMessage(transaction)}
                    </Text>
                </View>

                <View style={styles.detailViewStyle}>
                    <Text style={styles.detailsTextStyle}>
                        Details
                    </Text>

                    <View style={styles.subContainer}>
                        <View>
                            <Text style={styles.titleStyle}>
                                Transaction ID
                            </Text>

                            <Text style={styles.detailStyle}>
                                {transaction.id}
                            </Text>
                        </View>

                        <View>
                            <Text style={styles.titleStyle}>
                                Done at
                            </Text>

                            <Text style={styles.detailStyle}>
                                {dateString}
                            </Text>
                        </View>

                        <View>
                            <Text style={styles.titleStyle}>
                                Remark
                            </Text>

                            <Text style={styles.detailStyle}>
                                {transaction.status}
                            </Text>
                        </View>
                    </View>
                </View>
            </Components.BACKGROUND_IMAGE>
        );
    }

    getMessage(transaction) {

        const amount = parseFloat(transaction.amount).toFixed(2)
        const currency = transaction.currency

        let message = ""
        switch (transaction.type) {
            case 'unload': {
                message = "You have successfully unloaded $ " + amount + " " + currency + " in wallet from card"
            }
                break;

            case 'load': {
                message = "Your have successfully loaded $ " + amount + " " + currency + " in card from your wallet"
            }
                break;

            case 'get card': {
                message = 'Your card' + ' activated successfully'
            }
                break;

            case 'loyalty points': {
                message = "Loyalty points received on purchase SGD " + transaction.balance + "\n" + "at " + transaction.vendor
            }
                break;
        }

        return message
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    loyaltyAmountStyle: {
        color: 'black',
        fontSize: moderateScale(25, 0.09),
        fontFamily: 'Ubuntu-Medium'
    },
    loyaltyMessageStyle: {
        color: '#707070',
        textAlign: 'center',
        marginHorizontal:5,
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    detailsTextStyle: {
        color: 'white',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    detailViewStyle: {
        top: 110,
        height: 200,
        marginHorizontal: 15
    },
    loyaltyPointsContainerViewStyle: {
        top: 100,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F0F0F0',
        marginHorizontal: 15,
        marginBottom: 10
    },
    subContainer: {
        flex: 1,
        backgroundColor: '#F0F0F0',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingVertical: 10
    },
    titleStyle: {
        color: '#A8A8A8',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    detailStyle: {
        color: '#282828',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular'
    }
})