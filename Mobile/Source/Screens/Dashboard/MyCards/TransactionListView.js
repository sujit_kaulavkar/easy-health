
import React, { Component } from 'react';
import {
    View,
    FlatList,
    Text,
    StyleSheet,
    Animated,
    TouchableOpacity
} from 'react-native';
import { format } from 'date-fns'
import { Helper } from '../../../Helper/FilePath';
import { moderateScale } from '../../../Helper/Scaling';
import { TOP_BAR_HEIGHT, STATUS_BAR_HEIGHT_IOS, APP_THEME, SOMETHING_WRONG_MESSAGE, NO_INTERNET_CONNECTION } from '../../../Helper/Constants';
import DropdownAlert from 'react-native-dropdownalert';
import Events from 'react-native-simple-events';

//Offset value for which we need to load more the content
export const CONTENT_LOAD_OFFSET_POSITION = 0.3

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
export default class TransactionListView extends Component {

    constructor(props) {
        super(props)

        overScrollModeStatus = 'never'
        visibleItemIndex = 0;

        totalPages = 1;
        pageNumber = 1;
        isLoadingMore = false

        const scrollAnim = new Animated.Value(0);
        const offsetAnim = new Animated.Value(0);
        selectTransactionAction = this.selectTransactionAction.bind(this)
        onViewableItemsChanged = this.onViewableItemsChanged.bind(this)
        remoteCardTransactionsCall = this.remoteCardTransactionsCall.bind(this)

        this.state = {
            isShowSpinner: false,
            transactions: [],
            scrollAnim,
            offsetAnim,
            clampedScroll: Animated.diffClamp(
                Animated.add(
                    scrollAnim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 0.5],
                        extrapolateLeft: 'clamp',
                    }),
                    offsetAnim,
                ),
                0,
                TOP_BAR_HEIGHT - STATUS_BAR_HEIGHT_IOS,
            ),
        }
    }

    componentDidMount() {
        this.state.scrollAnim.addListener(({ value }) => {
            this.handleScroll()
        });

        this.addObservers()
        this.remoteCardTransactionsCall()
    }

    componentWillUnmount() {
        Events.rm("updateCardDetails")
        this.state.scrollAnim.removeAllListeners();
    }

    addObservers() {
        Events.on('updateCardDetails', 'updateCardDetails', this.updateCardDetails)
    }

    updateCardDetails = () => {
        totalPages = 1;
        pageNumber = 1;
        isLoadingMore = false
        this.setState({
            transactions: []
        })
        this.remoteCardTransactionsCall()
    }

    shouldComponentUpdate(nextProps, nextState) {
        const isCardChange = nextProps.selectedCard != this.props.selectedCard
        const isLoadingTransactions = nextState.transactions.length != this.state.transactions.length

        if (isCardChange) {

            setTimeout(() => {
                totalPages = 1;
                pageNumber = 1;
                isLoadingMore = false
                this.setState({
                    transactions: []
                })
                this.remoteCardTransactionsCall()
            }, 100);
        }

        if (isLoadingTransactions) {
            setTimeout(() => {
                this.handleLoadMore()
            }, 100);
        }

        return isCardChange || isLoadingTransactions
    }

    remoteCardTransactionsCall = () => {
        const cardId = this.props.selectedCard.card_hash_id
        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.cardTransactions(cardId, pageNumber, user.user_hash_id)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleCardTransactionResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleCardTransactionResponse(responseJson) {
        const status = responseJson.status
        if (status == 1) {
            const data = responseJson.data
            totalPages = responseJson.total_pages
            pageNumber = responseJson.page
            this.setState({
                transactions: this.state.transactions.concat(data)
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    handleScroll() {
        if (this.state.transactions && (this.state.transactions.length > 0)) {
            if (visibleItemIndex / this.state.transactions.length > CONTENT_LOAD_OFFSET_POSITION) {
                this.handleLoadMore()
            }
        }
    }

    clearPages() {
        totalPages = 1;
        pageNumber = 1;
        isLoadingMore = false;
    }

    handleLoadMore = () => {
        if ((totalPages != 1) && totalPages > pageNumber) {
            if (isLoadingMore == false) {
                isLoadingMore = true
                pageNumber = pageNumber + 1;
                this.remoteCardTransactionsCall()
            }
        }
    }

    onViewableItemsChanged({ viewableItems }) {

        let item = viewableItems[viewableItems.length - 1]
        if (item) {
            visibleItemIndex = item.index
        }
    }

    viewabilityConfig = {
        viewAreaCoveragePercentThreshold: 90 // how much an item need to be visible in precent
    }

    render() {

        const animOffset = Animated.event([{
            nativeEvent:
            {
                contentOffset:
                {
                    y: this.state.scrollAnim
                }
            }
        }],
            { useNativeDriver: true }
        )

        return (
            <View style={styles.container}>

                <AnimatedFlatList
                    onScroll={animOffset}
                    scrollEventThrottle={1}
                    removeClippedSubviews={true}
                    legacyImplementation={false}
                    overScrollMode={overScrollModeStatus}
                    key="transactionFlatList"
                    style={styles.flatListStyle}
                    data={this.state.transactions}
                    ListHeaderComponent={() => this.renderHeader()}
                    ListFooterComponent={() => this.props.showSuspendCardOption}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => this.renderCellItem(item, index)}
                    onViewableItemsChanged={(this.onViewableItemsChanged)}
                    viewabilityConfig={this.viewabilityConfig}/>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </View>
        );
    }

    renderHeader() {

        return (
            <View>
                {this.props.headerComponent}

                <Text style={{
                    color: 'white',
                    fontSize: moderateScale(17, 0.09),
                    left: 15,
                    marginTop: 20,
                    height: 40,
                    fontFamily: 'OpenSans-Regular'
                }}>
                    {this.state.transactions.length > 0 ? "Transactions" : ""}
                </Text>
            </View>
        )

        return null
    }

    renderCellItem(item, index) {

        const dateString = item.date
        return (
            <View style={styles.cellStyle}>
                <TouchableOpacity
                    activeOpacity={1}
                    style={styles.cellSubContainer}
                    onPress={() => this.selectTransactionAction(index)}>
                    <View style={styles.circleViewStyle} />

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={styles.purchaseContainerStyle}>
                            <Text style={styles.transactionStatusStyle}>
                                {item.type.capitalize()}
                            </Text>

                            <Text style={styles.venderNameStyle} numberOfLines={2} adjustsFontSizeToFit={true}>
                                {item.vendor_name}
                            </Text>

                            <Text style={styles.transactionDateStyle}>
                                {dateString}
                            </Text>
                        </View>

                        <View style={styles.amountContainerStyle}>
                            {this.showTransactionAmount(item)}
                            <Text style={[styles.venderNameStyle]}>
                                <Text style={styles.titleStyle}>Points Received</Text> {item.loyalty_points == null ? "0.00" : parseFloat(item.loyalty_points).toFixed(2)}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    selectTransactionAction(index) {
        const transaction = this.state.transactions[index]
        this.props.selectedIndex(transaction)
    }

    showTransactionAmount(item) {

        if (item.amount != null) {
            return (
                <Text style={[styles.transactionStatusStyle, { color: this.changeTrasactionColor(item) }]}>
                    <Text style={styles.titleStyle}>{item.currency}</Text> {this.formatTransactionAmount(item)}
                </Text>
            )
        }
        else {
            return null
        }
    }

    changeTrasactionColor(item) {
        const indicator = item.indicator;
        let color = "#404040"
        switch (indicator) {
            case "credit":
                color = "green"
                break;

            case "debit":
                color = "red"
                break;

            default:
                break;
        }

        return color
    }

    formatTransactionAmount(item) {
        const indicator = item.indicator;
        let amount = item.amount
        switch (indicator) {
            case "credit":
                amount = "+" + parseFloat(item.amount).toFixed(2)
                break;

            case "debit":
                amount = "-" + parseFloat(item.amount).toFixed(2)
                break;

            default:
                break;
        }
        return amount
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }


    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    flatListStyle: {
        flex: 1
    },
    cellStyle: {
        flex: 1,
        height: 110,
        flexDirection: 'row'
    },
    cellSubContainer: {
        flex: 1,
        height: 95,
        flexDirection: 'row',
        backgroundColor: '#F0F0F0',
        marginHorizontal: 15,
        borderRadius: 10
    },
    circleViewStyle: {
        marginLeft: 15,
        width: 50,
        height: 50,
        alignSelf: 'center',
        borderRadius: 25,
        backgroundColor: '#D3D3D3'
    },
    purchaseContainerStyle: {
        flex: 0.85,
        left: 20,
        marginVertical: 10,
        alignItems: 'flex-start',
        justifyContent: "space-around"
    },
    transactionStatusStyle: {
        color: '#404040',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    venderNameStyle: {
        color: 'black',
        textAlign: 'right',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    transactionDateStyle: {
        color: '#606060',
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    amountContainerStyle: {
        flex: 0.75,
        right: 15,
        alignItems: 'flex-end',
        justifyContent: "space-around"
    },
    titleStyle: {
        color: "#606060",
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    }
})

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
