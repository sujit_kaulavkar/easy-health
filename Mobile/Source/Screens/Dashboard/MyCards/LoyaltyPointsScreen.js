
import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    StyleSheet
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    moderateScale
} from '../../../Helper/Scaling';

import {
    TOP_BAR_HEIGHT,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_NAME,
    OK_TEXT
} from '../../../Helper/Constants';

import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';

export default class LoyaltyPointsScreen extends Component {

    constructor() {
        super()
        this.state = {
            isTouchEnabled: true
        }
    }

    remoteUpdateWalletBalanceCall() {

        const { card } = this.props.navigation.state.params
        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const value = this.refs.loyaltyPoints.getText()
        this.refs.submitButton.startAnimation()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.updateWalletWithLoyaltyPoints(
            user.user_hash_id,
            card.card_hash_id,
            value
        ).then((responseJson) => {
            this.refs.submitButton.stopAnimation()
            if (responseJson) {
                this.handleUpdateWalletBalanceResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.refs.submitButton.stopAnimation()
                this.showServerErrorMessage()
            })
    }

    handleUpdateWalletBalanceResponse(responseJson) {
        const status = responseJson.status;
        if (status == 200) {

            const value = this.refs.loyaltyPoints.getText()
            const walletInfo = Helper.USER_MANAGER.getInstance().getWalletBalance()
            walletInfo.walletBalance = parseFloat(walletInfo.walletBalance) + parseFloat(value)
            Helper.USER_MANAGER.getInstance().setWalletBalance(walletInfo)

            const { card, updateLoyaltyPoints } = this.props.navigation.state.params
            card.loyalty_points = parseFloat(card.loyalty_points) - parseFloat(value)

            if (updateLoyaltyPoints) {
                updateLoyaltyPoints()
            }

            Alert.alert(APP_NAME, "Wallet balance updated succesfully", [
                { text: OK_TEXT, onPress: () => this.props.navigation.goBack() }
            ], { cancelable: false })
        }
        else {
            const errorMessage = responseJson.errorMessage;
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    render() {
        const { card } = this.props.navigation.state.params
        const loyaltyPoints = parseFloat(card.loyalty_points).toFixed(2)

        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Loyalty Points'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={styles.balanceContainerStyle}>
                    <Text style={styles.walletBalanceStyle}>
                        Loyalty Points
                    </Text>

                    <Text style={styles.amountStyle}>
                        {loyaltyPoints}
                    </Text>

                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{
                            color: "white",
                            fontSize: moderateScale(13.5, 0.09),
                            fontFamily: 'OpenSans-Regular'
                        }}>
                            Add your Loyalty Points to Wallet Balance
                    </Text>
                    </View>
                </View>

                <View style={styles.amountInputContaierStyle}>
                    <Components.USER_INPUT
                        ref="loyaltyPoints"
                        returnKeyType={'done'}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                    >Enter Points</Components.USER_INPUT>
                </View>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title="Save"
                        onPress={() => this.doneButtonAction()} />
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        )
    }

    doneButtonAction() {
        const value = this.refs.loyaltyPoints.getText()
        let validator = new Helper.VALIDATOR(value)
        let errorMessage = validator.validateLoyaltyPoints()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        const { card } = this.props.navigation.state.params
        const loyaltyPoints = parseFloat(card.loyalty_points)

        if (parseFloat(value) > loyaltyPoints) {
            this.dropdown.alertWithType('error', "", "Entered amount is greater than loyalty points");
            return;
        }

        this.remoteUpdateWalletBalanceCall()
    }

    startLoader() {
        this.setState({
            isTouchEnabled: false
        })
    }

    stopLoader() {
        this.setState({
            isTouchEnabled: true
        })
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    walletBalanceStyle: {
        color: 'white',
        fontSize: moderateScale(22, 0.09),
        fontFamily: 'OpenSans-SemiBold'
    },
    balanceContainerStyle: {
        margin: 20,
        flexDirection: 'column',
        top: TOP_BAR_HEIGHT,
        height: 100,
        borderWidth: 1,
        borderRadius: 1,
        borderColor: 'rgba(255, 255, 255, 0.5)',
        borderStyle: 'dashed',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    amountStyle: {
        color: 'white',
        fontSize: moderateScale(23, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    amountInputContaierStyle: {
        flexDirection: 'column',
        top: 70,
        height: 80
    },
    currencyContainerStyle: {
        top: 5,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center'
    },
    textInputStyle: {
        flex: 1,
        left: 10,
        color: "white",
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    underlineStyle: {
        height: 1,
        bottom: -3,
        width: "100%",
        backgroundColor: '#C0C0C0'
    },
    submitButtonBase: {
        top: 100
    }
})
