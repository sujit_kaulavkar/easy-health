import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Modal from "react-native-modal";
import { moderateScale } from '../../../Helper/Scaling';
import { SCREEN_WIDTH, BLUE_COLOR, APP_THEME, IS_IOS } from '../../../Helper/Constants';
import {
    Helper
} from '../../../Helper/FilePath';

const generateCVCText = "Generate CVC"
const cvcMessageText = "Your cards CVC is automatically generated. This expires after 10 mins or if you navigate away. The code expires in and is valid for one time use"

export default class CVCView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isVisible: true,
            cvcNumber: "●●●",
            isShowSpinner: false
        }
    }

    render() {        
        const isDisableGenerateCVC = false

        return (
            <Modal 
                isVisible={this.state.isVisible}
                onBackdropPress={() => this.backDropPress()}
                style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />            

                <View style={styles.subContainerStyle}>
                    <View style={styles.CVCViewStyle}>

                        <Text style={styles.cvcNumberStyle}>{this.state.cvcNumber}</Text>

                        <TouchableOpacity
                            activeOpacity={1}
                            disabled={isDisableGenerateCVC}
                            style={[styles.generateCVCButtonStyle, { backgroundColor: isDisableGenerateCVC ? '#E8E8E8' : BLUE_COLOR }]} onPress={() => this.generateCVC()}>
                            <Text style={styles.genrateCVCTextStyle}>{generateCVCText}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.CVCMessageViewStyle}>
                        <Text style={styles.CVCMessageStyle}>
                            {cvcMessageText}
                        </Text>
                    </View>
                </View>

            </Modal>
        )
    }

    backDropPress() {        
        this.setState({
            isVisible: false
        })

        setTimeout(() => {
            if (this.props.removeCVCVIew) {
                this.props.removeCVCVIew()
            } 
        }, 300);       
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    generateCVC() {
        this.remoteGenerateCVCCall()
    }

    remoteGenerateCVCCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.props.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const card = this.props.selectedCard
        apiHelper.generateCVC(user.user_hash_id, card.card_hash_id)
            .then((responseJson) => {
                if (responseJson) {
                    this.handleGenerateCVCResponse(responseJson)
                }
                else {
                    this.props.showServerErrorMessage()
                }

                this.hideSpinner()
            })
            .catch((error) => {
                this.hideSpinner()
                this.props.showServerErrorMessage()
            })
    }

    handleGenerateCVCResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const cvc = responseJson.data.value
            this.setState({
                cvcNumber: cvc
            })
        }
        else {
            this.props.showServerErrorMessage()
        }
    }
}

const styles = StyleSheet.create({
    container: {                     
        marginTop: 20,
        flexDirection: 'column',        
        alignItems: 'center',
        justifyContent: 'center'
    },
    subContainerStyle: {
        top: 10,
        flexDirection: 'row',
        height:150,
        borderRadius:10,
        backgroundColor:'#D3D3D3'  
    },
    CVCViewStyle: {
        width: 160,
        left: 0,
        justifyContent: 'space-around',
    },
    CVCMessageViewStyle: {
        flex: 1,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
    },
    CVCMessageStyle: {
        color: BLUE_COLOR,
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    },
    generateCVCButtonStyle: {
        marginHorizontal: 18,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center'
    },
    genrateCVCTextStyle: {
        fontSize: moderateScale(17, 0.09),
        color: 'white',
        fontFamily: 'BarlowCondensed-Medium'
    },
    cvcNumberStyle: {
        height: 35,
        paddingHorizontal: IS_IOS ? 15 : 0,
        letterSpacing: 25,
        textAlign: 'center',
        marginHorizontal: 18,
        borderColor: BLUE_COLOR,
        borderWidth: 1,
        lineHeight: 33,
        color: BLUE_COLOR,
        fontSize: moderateScale(15, 0.09),
        backgroundColor: 'white'
    }
})