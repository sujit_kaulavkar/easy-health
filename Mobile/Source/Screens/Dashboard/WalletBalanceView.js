import React, { Component } from 'react';

import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import { moderateScale } from '../../Helper/Scaling';

export default class WalletBalanceView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            walletInfo: {
                "walletBalance": "0.0",
                "walletCurrency": "SGD"
            }         
        }
    }

    render() {

        const { walletInfo } = this.props
        return (
            <View style={styles.container}>
                <Text style={styles.titleStyle}>
                    Wallet Balance
                </Text>

                <Text style={styles.amountStyle}>
                    $ {walletInfo.walletBalance} {walletInfo.walletCurrency}
                </Text>
            </View>
        )
    }
}

WalletBalanceView.defaultProps = {
    walletBalace: 0.00
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleStyle:{
        color:'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
    },
    amountStyle:{
        color: 'white',
        fontSize: moderateScale(24, 0.09),
        fontFamily: 'Ubuntu-Medium',
    }
})