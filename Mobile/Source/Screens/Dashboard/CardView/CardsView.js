
import React, { PureComponent } from 'react';
import Carousel from 'react-native-carousel-control';
import PageControl from 'react-native-page-control';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

import { itemWidth, itemHeight } from './styles/SliderEntry.style';
import SliderEntry from './components/SliderEntry';
import { styles } from './styles/index.style';

import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import { moderateScale } from '../../../Helper/Scaling';
import { SCREEN_WIDTH, BLUE_COLOR } from '../../../Helper/Constants';

export default class CardsView extends PureComponent {

    constructor(props) {
        super(props)

        const card = props.selectedCard;
        let selectedIndex = 0;

        if (card) {
            selectedIndex = props.cards.indexOf(card)
        }

        this.state = {
            currentCardIndex: selectedIndex
        }

        handleCardScroll = this.handleCardScroll.bind(this)
        handleCardSelection = this.handleCardSelection.bind(this)
        showLoyaltyPointScreen = this.showLoyaltyPointScreen.bind(this)
    }

    componentWillUnmount() {
        const blankState = {};
        Object.keys(this.state).forEach(stateKey => {
            blankState[stateKey] = undefined;
        });
        this.setState(blankState);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.props.cards.length ? this.renderCarouselItem() : this.noCardAdded()}
            </View>
        )
    }

    renderCarouselItem() {
        const item = this.props.cards[this.state.currentCardIndex]
        const textInputComponents = this.props.cards.map((card) => this.renderItemWithParallax(card))
        return (
            <View>
                <View style={{ width: SCREEN_WIDTH, height: itemHeight }}>
                    <Carousel
                        ref="carousel"
                        pageWidth={itemWidth}
                        sneak={50}
                        key="carousel"
                        enableMomentum={true}
                        swipeThreshold={0.1}
                        currentPage={this.state.currentCardIndex}
                        style={{ height: itemHeight }}
                        onPageChange={(pageNumber) => this.handleCardScroll(pageNumber)}>
                        {textInputComponents}
                    </Carousel>
                </View>

                <PageControl
                    style={myStyles.pageControlStyle}
                    numberOfPages={this.props.cards.length}
                    currentPage={this.state.currentCardIndex}
                    hidesForSinglePage
                    pageIndicatorTintColor='rgba(62,81,153,1.0)'
                    currentPageIndicatorTintColor='white'
                    indicatorStyle={{ width: 5 }}
                    currentIndicatorStyle={{ width: 20 }}
                    indicatorSize={{ width: 2, height: 4 }} />

                {this.showCardDetails(item)}
            </View>
        );
    }

    noCardAdded() {
        return (
            <View style={{ height: 100, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: "white", fontFamily: 'OpenSans-Regular', fontSize: moderateScale(18, 0.09) }}> You don't have a card</Text>
            </View>
        )
    }

    showCardDetails(item) {

        const loyaltyPoints = parseFloat(item.loyalty_points)
        return (
            item ?
                <View style={{ paddingTop: 20 }}>
                    {
                        this.props.selectedCard
                            ?
                            <Components.CARD_NAME selectedCard={item} />
                            :
                            <Text style={myStyles.cardStyle}>{item.name}</Text>
                    }
                    <View>
                        <View style={{paddingTop: 10, height:60 }}>
                            {this.showCardBalance(item)}

                            <Text style={[myStyles.cardStyle]}>
                                <Text style={myStyles.balanceStyle}>Loyalty Points : </Text>
                                {loyaltyPoints}
                                <Text> Points</Text>
                            </Text>
                        </View>
                    </View>
                </View>
                : null
        )
    }

    showCardBalance(item) {

        const isActive = item.status == "locked" ? false : true
        if (isActive == false) {
            return null
        }

        const walletInfo = Helper.USER_MANAGER.getInstance().getWalletBalance()
        const isWalletEmpty = walletInfo.walletBalance >= 1 ? false : true
        const isCardEmpty = item.card_balance >= 1 ? false : true

        const balance = parseFloat(item.card_balance).toFixed(2)
        const currency = item.currency

        return (
            <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>

                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                    {this.props.loadCard ? <View style={{ flex: 1 }}></View>
                        : null}

                    <View style={{ flex: 1, alignSelf:'center'}}>
                        <View style={{ alignSelf: 'center', width: 250, top: 2 }}>
                            <Text style={myStyles.cardStyle}>
                                <Text style={myStyles.balanceStyle}>Card Balance : </Text>
                                $ {balance}
                                <Text> {currency}</Text>
                            </Text>
                        </View>
                    </View>

                    {this.props.loadCard ? <View style={{ flexDirection: 'row', marginLeft: 15, height: 30, flex: 1 }}>
                        <EvilIcons.Button
                            name="plus"
                            color={isWalletEmpty ? "rgba(255, 255, 255, 0.5)" : "white"}
                            size={35}
                            style={{ bottom: 10, right: -20 }}
                            disabled={isWalletEmpty}
                            backgroundColor={"transparent"}
                            onPress={() => this.props.loadCard()} />
                        <EvilIcons.Button
                            name="minus"
                            color={isCardEmpty ? "rgba(255, 255, 255, 0.5)" : "white"}
                            size={35}
                            style={{ bottom: 10, paddingLeft: -20 }}
                            disabled={isCardEmpty}
                            backgroundColor={"transparent"}
                            onPress={() => this.props.unloadCard()} />
                    </View> : null}
                </View>
            </View>
        )
    }

    loadCard() {
        console.log("loadCard");
    }

    unloadCard() {
        console.log("unloadCard");
    }

    showLoyaltyPointScreen() {
        const item = this.props.cards[this.state.currentCardIndex]
        if (this.props.showLoyaltyPointScreen) {
            this.props.showLoyaltyPointScreen(item)
        }
    }

    renderItemWithParallax(item) {
        return (
            <SliderEntry
                item={item}
                key={item.name}
                destinationCall={() => this.handleCardSelection()}
            />
        );
    }

    handleCardScroll(pageNumber) {

        if (this.state.currentCardIndex != pageNumber) {
            this.setState({
                currentCardIndex: pageNumber
            })
            this.props.handleCardScroll(this.props.cards[pageNumber])
        }
    }

    handleCardSelection() {
        if (typeof this.props.handleSelectedCard == 'function') {
            this.props.handleSelectedCard(this.props.cards[this.state.currentCardIndex])
        }
    }
}

const myStyles = StyleSheet.create({
    pageControlStyle: {
        left: 0,
        right: 0
    },
    cardStyle: {
        color: 'white',
        alignSelf: 'center',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'Ubuntu-Medium',
        top: -5
    },
    balanceStyle: {
        color: 'white',
        fontFamily: 'OpenSans-Regular',
        fontSize: moderateScale(16, 0.09)
    },
    button: {
        height: 50,
        width: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    editStyle: {
        width: 20,
        height: 20,
        margin: 10
    }
})
