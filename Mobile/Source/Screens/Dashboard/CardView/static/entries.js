export var cardsArray = [
    {
        "key": 1,        
        "cardName": "Card 1",
        "validTill": '05/19',
        "cardBalance": "10",
        "loyaltyPointBalance": "10",
        "illustration": require("../../../../Resources/Images/Cards/card1.png"),
        "transactions": [
            {
                "transactionId": "DD234623462834",
                "remark": "Transaction Successful",
                "transactionType": "Load",
                "venderName": "Merchant Asia Top",
                "transactionDate": "30 Sep 2018, 22:20:52",
                "transactionAmount": "30",
                "transactionPoints": "10"
            },
            {
                "transactionId": "DD999888777",
                "remark": "Transaction Successful",
                "transactionType": "Unload",
                "venderName": "Adidas",
                "transactionDate": "30 Aug 2018, 20:20:52",
                "transactionAmount": "40",
                "transactionPoints": "20"
            },
            {
                "transactionId": "DD234324234234234",
                "remark": "Transaction Successful",
                "transactionType": "Purchase",
                "venderName": "Nike",
                "transactionDate": "30 July 2018, 19:20:52",
                "transactionAmount": "60",
                "transactionPoints": "30"
            }
        ]
    },
    {
        "key": 2,
        "cardName": "Card 2",
        "validTill": '05/20',
        "cardBalance": "20",
        "loyaltyPointBalance": "20",
        "illustration": require("../../../../Resources/Images/Cards/card2.png"),
        "transactions": [
            {
                "transactionId": "DD112212112121",
                "remark": "Transaction Successful",
                "transactionType": "Top",
                "venderName": "Nike",
                "transactionDate": "30 Sep 2018, 22:20:52",
                "transactionAmount": "1000",
                "transactionPoints": "100"
            },
            {
                "transactionId": "DD222212312312321",
                "remark": "Transaction Successful",
                "transactionType": "Purchase",
                "venderName": "Big Bazar",
                "transactionDate": "30 Aug 2018, 20:20:52",
                "transactionAmount": "300",
                "transactionPoints": "70"
            },
            {
                "transactionId": "DD4434334334343",
                "remark": "Transaction Successful",
                "transactionType": "Unload",
                "venderName": "Merchant Asia Top",
                "transactionDate": "30 July 2018, 19:20:52",
                "transactionAmount": "600",
                "transactionPoints": "30"
            }
        ]
    },
    {
        "key": 3,
        "cardName": "Card 3",
        "validTill": '05/21',
        "cardBalance": "30",
        "loyaltyPointBalance": "30",
        "illustration": require("../../../../Resources/Images/Cards/card3.png"),
        "transactions": [
            {
                "transactionId": "DD00099999998988787876",
                "remark": "Transaction Successful",
                "transactionType": "Unload",
                "venderName": "Merchant Asia Top",
                "transactionDate": "30 Sep 2018, 22:20:52",
                "transactionAmount": "30",
                "transactionPoints": "10"
            },
            {
                "transactionId": "DD234623462834",
                "remark": "Transaction Successful",
                "transactionType": "Purchase",
                "venderName": "Adidas",
                "transactionDate": "30 Aug 2018, 20:20:52",
                "transactionAmount": "40",
                "transactionPoints": "20"
            },
            {
                "transactionId": "DD999888777",
                "remark": "Transaction Successful",
                "transactionType": "Topup",
                "venderName": "Nike",
                "transactionDate": "30 July 2018, 19:20:52",
                "transactionAmount": "60",
                "transactionPoints": "30"
            }
        ]
    },
    {
        "key": 4,
        "cardName": "Card 4",
        "validTill": '05/22',
        "cardBalance": "40",
        "loyaltyPointBalance": "40",
        "illustration": require("../../../../Resources/Images/Cards/card4.png"),
        "transactions": [
            {
                "transactionId": "DD112212112121",
                "remark": "Transaction Successful",
                "transactionType": "Purchase",
                "venderName": "Nike",
                "transactionDate": "30 Sep 2018, 22:20:52",
                "transactionAmount": "1000",
                "transactionPoints": "100"
            },
            {
                "transactionId": "DD222212312312321",
                "remark": "Transaction Successful",
                "transactionType": "Unload",
                "venderName": "Big Bazar",
                "transactionDate": "30 Aug 2018, 20:20:52",
                "transactionAmount": "300",
                "transactionPoints": "70"
            },
            {
                "transactionId": "DD4434334334343",
                "remark": "Transaction Successful",
                "transactionType": "Load",
                "venderName": "Merchant Asia Top",
                "transactionDate": "30 July 2018, 19:20:52",
                "transactionAmount": "600",
                "transactionPoints": "30"
            }
        ]
    },
    {
        "key": 5,
        "cardName": "Card 5",
        "validTill": '05/23',
        "cardBalance": "50",
        "loyaltyPointBalance": "50",
        "illustration": require("../../../../Resources/Images/Cards/card5.png"),
        "transactions": [
            {
                "transactionId": "DD999888777",
                "remark": "Transaction Successful",
                "transactionType": "Load",
                "venderName": "Merchant Asia Top",
                "transactionDate": "30 Sep 2018, 22:20:52",
                "transactionAmount": "30",
                "transactionPoints": "10"
            },
            {
                "transactionId": "DD234623462834",
                "remark": "Transaction Successful",
                "transactionType": "Unload",
                "venderName": "Adidas",
                "transactionDate": "30 Aug 2018, 20:20:52",
                "transactionAmount": "40",
                "transactionPoints": "20"
            },
            {
                "transactionId": "DD00099999998988787876",
                "remark": "Transaction Successful",
                "transactionType": "Topup",
                "venderName": "Nike",
                "transactionDate": "30 July 2018, 19:20:52",
                "transactionAmount": "60",
                "transactionPoints": "30"
            }
        ]
    }
];

export const TopSpendings = [
    {
        "venderName": "Merchant A",
        "amountSpend": "$ 6000.00"
    },
    {
        "venderName": "Merchant B",
        "amountSpend": "$ 5000.00"
    },
    {
        "venderName": "Merchant C",
        "amountSpend": "$ 4000.00"
    }
]