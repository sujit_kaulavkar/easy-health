import React, { PureComponent } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import styles from '../styles/SliderEntry.style';

import {
    Components
} from '../../../../Helper/FilePath';
import { format } from 'date-fns'
import { moderateScale } from '../../../../Helper/Scaling';
import { BLUE_COLOR } from '../../../../Helper/Constants';

export default class SliderEntry extends PureComponent {

    constructor(props) {
        super(props)
        destinationCall = this.destinationCall.bind(this)
    }

    showCardDetails() {
        const { item } = this.props;
        const dateString = format(item.expiry_date, 'YY-MM')
        const isActive = item.status == "locked" ? false : true
        const name = item.name
        const number = item.card_number
        
        return <View style={styles.imageContainer}>
            <View style={{flex:1}}>                
                {this.showCardImage(isActive, item)}              
                {this.showCardName(isActive, name)}
                {this.showCardNumber(number)}                
            </View>
 
            {this.showValidity(isActive, dateString)}
            
        </View>
    }

    showCardImage(isActive, item) {
        if (isActive == false) {
            return <View style={[styles.imageStyle, {backgroundColor: 'gray' }]}/>
        }

      return <Image
            defaultSource={require('./placeholder_credit_card.png')}
            source={{ uri: item.image}}
            resizeMode="cover"
            style={styles.imageStyle}
            />
    }

    showCardName(isActive, name) {

        let cardName = "Suspended Card"
        let color = "white"
        if (isActive == true) {
            cardName = name
            color = BLUE_COLOR
        }

        return <View style={{ alignItems: 'center', justifyContent: 'center', position: 'absolute', height: "100%", width: "100%" }}>
            <Text
                adjustsFontSizeToFit
                style={{ fontFamily: 'OpenSans-Regular', color: color, fontSize: moderateScale(16, 0.09), textAlign: 'center', marginHorizontal: 15 }}>{cardName}</Text>
        </View>
    }

    showCardNumber(cardNumber) {
        return <View style={{alignItems: 'center', justifyContent: 'center', position: 'absolute', height: "100%", width: "100%" }}>
            <Text
                adjustsFontSizeToFit
                style={{ top: 25,  fontFamily: 'OpenSans-Regular', color: "black", fontSize: moderateScale(16, 0.09), textAlign: 'center', marginHorizontal: 15 }}>{cardNumber}</Text>
        </View>
    }

    showValidity(isActive, dateString) {
        return isActive == true ? <View style={{ position: 'absolute', bottom: 15, left: 0, marginHorizontal: 20 }}>
                <Text style={{ fontSize: moderateScale(14, 0.09), fontFamily: 'OpenSans-Regular', color: BLUE_COLOR, left: 0 }}>Valid : {dateString}</Text>
            </View> : null        
    }

    render() {
        return (
            <TouchableOpacity
                activeOpacity={1}
                style={[styles.slideInnerContainer]}
                onPress={() => this.destinationCall()}>
                {this.showCardDetails()}
            </TouchableOpacity>
        );
    }

    destinationCall() {
        this.props.destinationCall(this.props.item)
    }
}