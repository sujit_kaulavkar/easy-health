import { StyleSheet, Dimensions } from 'react-native';
import { colors } from './index.style';
import DeviceDetector from '../../../../Helper/DeviceDetector';

import {
    IS_IOS
} from '../../../../Helper/Constants';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const maxWidth = (viewportHeight < viewportWidth) ? viewportHeight : viewportWidth
function wp(percentage) {

    const value = (percentage * maxWidth) / 100;
    return Math.round(value);
}

const slideWidth = wp(DeviceDetector.getInstance().isTablet == true ? 50 : 75);
const slideHeight = slideWidth * 0.666667;

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth;
export const itemHeight = itemWidth * 0.666667;

export const topHeaderHeight = (DeviceDetector.getInstance().isTablet == true ? maxWidth * 0.45 : maxWidth * 0.6);

const entryBorderRadius = 3;

export default StyleSheet.create({
    slideInnerContainer: {
        width: itemWidth,
        height: slideHeight,
        backgroundColor: 'transparent',
        alignItems:'center',
        justifyContent:'center',
        paddingBottom: 18
    },
    imageContainer: {
        flex: 1,
        top:0,
        backgroundColor: 'transparent',
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    },
    imageStyle: {
        width: itemWidth - 40,
        height: slideHeight - 30,
        top:10,
        borderRadius: IS_IOS ? entryBorderRadius : 0,
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        borderRadius: IS_IOS ? entryBorderRadius : entryBorderRadius,
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    }
});
