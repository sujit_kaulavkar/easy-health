import React, { PureComponent } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    BackHandler,
    TouchableOpacity
} from 'react-native';

import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';

import {
    DASHBOARD,
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT, APP_THEME, SOMETHING_WRONG_MESSAGE, NO_INTERNET_CONNECTION, BLUE_COLOR,
} from '../../Helper/Constants';

import { moderateScale } from '../../Helper/Scaling';

export default class DashboardScreen extends PureComponent {

    constructor(props) {
        super(props)

        this.state = {
            isShowSpinner: false,
            myCards: [],
            walletInfo: {
                "walletBalance": "0.0",
                "walletCurrency": "SGD"
            }
        }

        handleCardScroll = this.handleCardScroll.bind(this)
        handleSelectedCard = this.handleSelectedCard.bind(this)
        showLoyaltyPointScreen = this.showLoyaltyPointScreen.bind(this)
    }

    componentDidMount() {
        Helper.PUSH_HELPER.getInstance()
        this.addObservers()
        this.remoteWalletDetailsCall()
        this.remoteCheckForKYCStatus()
    }

    componentWillUnmount() {
        didBlurObserver.remove();
        didFocusObserver.remove();
    }

    addObservers() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {
                Events.trigger('unlockDrawer')
            }
        );

        // listener called when viewWillDisappear
        didBlurObserver = this.props.navigation.addListener(
            'didBlur',
            payload => {
                Events.trigger('lockDrawer')
            }
        );
    }

    remoteWalletDetailsCall() {

        // this.handleWalletDetailsResponse(WalletsJson)
        // return;

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.myWallet(user.user_hash_id).then((responseJson) => {
            if (responseJson) {
                this.handleWalletDetailsResponse(responseJson)
            }
            else {
                this.hideSpinner()
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleWalletDetailsResponse(responseJson) {

        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            const walletBalance = data.funds.available.amount
            const walletCurrency = data.funds.available.currency
            const walletInfo = {
                "walletBalance": walletBalance,
                "walletCurrency": walletCurrency
            }

            this.setState({
                walletInfo: walletInfo
            })

            const cards = data.cards
            if (cards && cards.length) {
                this.remoteMyCardsCall()
            }
            else {
                this.hideSpinner()
            }

            this.refs.top3Spending.remoteTop3SpendingsCall()
            Helper.USER_MANAGER.getInstance().setWalletBalance(walletInfo)
            Helper.USER_MANAGER.getInstance().setWalletDetails(responseJson.data.details)
            Helper.DATABASE_HANDLER.getInstance().saveWalletDetails(responseJson.data.details)
        }
        else {
            this.hideSpinner()
            this.showServerErrorMessage()
        }
    }

    remoteMyCardsCall() {

        // this.handleMyCardsResponse(CardsJson)
        // return;

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }
        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.myCards(user.user_hash_id).then((responseJson) => {
            this.hideSpinner()
            if (responseJson) {
                this.handleMyCardsResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleMyCardsResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            this.setState({
                myCards: data
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    remoteCheckForKYCStatus() {

        // const user = Helper.USER_MANAGER.getInstance().getUser()
        // if (user.kyc_status != "not_submitted" || user.kyc_status == "submitted" || user.kyc_status == "pending") {
        //     return;
        // }

        let apiHelper = Helper.API_HELPER.getInstance();
        apiHelper.kycStatus(user.user_hash_id).then((responseJson) => {
            if (responseJson) {
                const status = responseJson.data.status
                if (status == "approved") {
                    user.kyc_status = "approved"
                    Alert.alert(APP_NAME, "Your KYC is approved")
                }
                else if (status == "rejected") {
                    user.kyc_status = "rejected"
                    Alert.alert(APP_NAME, "Your KYC is rejected. You need to submit document again")
                }
            }
        })
            .catch(() => {
            })
    }

    render() {        
        return (
            <Components.BACKGROUND_IMAGE
                isShowMMLogo={true}
                style={{ flex: 1, flexDirection: 'column' }}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Dashboard'
                    isMenuButtonNeeded={true}
                    isBellNeeded={true}
                    bellButtonAction={() => {
                        this.showNotificationScreen()
                    }}
                    menuButtonAction={() => {
                        this.props.navigation.openDrawer()
                    }} />

                <ScrollView
                    style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1 }}>

                    <View style={{ height: 84 }}>
                        <DASHBOARD.WALLET_BALANCE_VIEW walletInfo={this.state.walletInfo} />
                    </View>

                    <View>
                        <DASHBOARD.CARDS_VIEW
                            classRef={this}
                            showLoyaltyPointScreen={(card) => this.showLoyaltyPointScreen(card)}
                            cards={this.state.myCards}
                            handleCardScroll={(card) => this.handleCardScroll(card)}
                            handleSelectedCard={(card) => { this.handleSelectedCard(card) }} />

                        {this.state.myCards.length > 0 ? this.showMMMessage() : null}
                    </View>

                    <View>
                        <TouchableOpacity
                            refs=""
                            activeOpacity={1}
                            style={styles.kycButton}
                            onPress={() => this.checkForKYC()}>
                            <Text style={styles.kycText}>
                                {this.kycStatus()}
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, paddingTop: 30, alignItems: 'flex-start' }}>
                        <DASHBOARD.TOP_SPENDING_VIEW ref="top3Spending"/>
                    </View>
                </ScrollView>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        )
    }

    /*
pending
submitted
approved
rejected
not_submitted
*/
    kycStatus() {
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const status = user.kyc_status
        switch (status) {
            case 'pending': {
                return "Your KYC is pending for approval"
            }
                break;

            case 'submitted': {
                return "Your KYC is submitted for approval"
            }
                break;

            case 'approved': {
                return "Your KYC is approved "
            }
                break;

            case 'rejected': {
                return "Your KYC is rejected. Submit again"
            }
                break;

            case 'not_submitted': {
                return "Your have not submitted KYC"
            }
                break;

            default: {
                return "Your have not submitted KYC"
            }
        }
    }
    
    checkForKYC() {

        const user = Helper.USER_MANAGER.getInstance().getUser()
        if (user.kyc_status == "pending" || user.kyc_status == "approved" || user.kyc_status == "submitted") {
//            return;
        }

        this.props.navigation.navigate("KYCPersonalDetailScreen", {
            isFromRegistration: false
        })
    }


    showMMMessage() {
        return (
            <Text style={{
                top: 5,
                textAlign: 'center',
                color: 'white',
                marginHorizontal: 20,
                fontFamily: 'OpenSans-Regular',
                fontSize: moderateScale(13, 0.09)
            }}>
                This card is issued by MatchMove Pay Pte Ltd pursuant to license by MasterCard Asia/Pacific Pte Ltd
                    </Text>
        )
    }

    showLoyaltyPointScreen(card) {
        this.props.navigation.navigate("LoyaltyPointsScreen", {
            card: card
        })
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    handleCardScroll(slideIndex) {
    }

    handleSelectedCard(card) {
        this.props.navigation.navigate("TransactionHistoryScreen", {
            selectedCard: card,
            cards: this.state.myCards
        })
    }

    showNotificationScreen() {
        this.props.navigation.navigate("NotificationScreen")
    }

    handleBackFromLogin = () => {
        return true;
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    kycButton: {
        flex: 1,
        height: 50,
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: BLUE_COLOR,
        borderRadius: 10,
        marginHorizontal: 30
    },
    kycText: {
        color: 'white',
        textAlign:'center',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    mmMessage: {
        marginTop: 5,
        textAlign: 'center',
        color: 'white',
        marginHorizontal: 20,
        fontFamily: 'OpenSans-Regular',
        fontSize: moderateScale(15, 0.09)
    }
})
