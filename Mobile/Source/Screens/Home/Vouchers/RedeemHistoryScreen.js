import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    moderateScale
} from '../../../Helper/Scaling';

import {
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    APP_NAME,
    BLUE_COLOR
} from '../../../Helper/Constants';

import RedeemSuccessView from './RedeemSuccessView';

export default class RedeemHistoryScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            redeemHistory: [{
                "merchant": "Kenko",
                "date": "3 Jan 2018"
            },
            {
                "merchant": "Kenko 1",
                "date": "4 Jan 2018"
            },
            {
                "merchant": "Kenko 2",
                "date": "5 Jan 2018"
            }],
            isShowSuceessUI: false
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <FlatList
                    scrollEventThrottle={1}
                    removeClippedSubviews={true}
                    legacyImplementation={false}
                    key="redeemHistoryFlatList"
                    style={styles.flatListStyle}
                    data={this.state.redeemHistory}
                    extraData={this.state}
                    keyExtractor={(item) => item.merchant}
                    renderItem={({ item }) => this.renderCellItem(item)} />

                {this.state.isShowSuceessUI ? <RedeemSuccessView removeSuccessUI={() => this.removeSuccessUI()} /> : null}

            </Components.BACKGROUND_IMAGE>
        );
    }

    renderCellItem(item) {
        return (
            <View style={{ 
                flex: 1, 
                height: 40, 
                flexDirection: 'row',
                justifyContent:'space-between',
                marginHorizontal: 20
                }}>
                <Text style={styles.text}>
                    {item.merchant}
                </Text>

                <Text style={styles.text}>
                    {item.date}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    flatListStyle: {
        flex: 1,
        marginHorizontal: 10,
        marginTop: 10
    },
    seeMoreView: {
        flex: 1,
        height: 50,
        alignSelf: 'center'
    },
    text: {
        fontSize: moderateScale(20, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: "white"
    }
})
