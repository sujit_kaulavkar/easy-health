
import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import MyVouchersScreen from './MyVouchersScreen';
import RedeemHistoryScreen from './RedeemHistoryScreen';
import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    APP_NAME,
    BLUE_COLOR
} from '../../../Helper/Constants';
import { moderateScale } from '../../../Helper/Scaling';

const FirstRoute = () => (
    <MyVouchersScreen />
);
const SecondRoute = () => (
    <RedeemHistoryScreen />
);

export default class VouchersMainScreen extends Component {
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'VOUCHERS' },
            { key: 'second', title: 'REDEMPTION HISTORY' },
        ],
    };

    render() {
        return (

            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='MY VOUCHERS'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                <TabView
                    style={{top: TOP_BAR_HEIGHT}}
                    navigationState={this.state}
                    renderTabBar={props =>
                        <TabBar
                            style={{backgroundColor:'white'}}
                            labelStyle={{
                                color:'gray',
                                fontSize: moderateScale(20, 0.09),
                                fontFamily: 'BarlowCondensed-Medium'
                            }}
                            {...props}
                            indicatorStyle={{ backgroundColor: BLUE_COLOR }}
                        />
                    }
                    renderScene={SceneMap({
                        first: FirstRoute,
                        second: SecondRoute,
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}/>

            </Components.BACKGROUND_IMAGE>
        );
    }
}

const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
});