
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import { itemWidth, itemHeight } from '../../Dashboard/CardView/styles/SliderEntry.style';
import Carousel from 'react-native-carousel-control';
import VoucherCell from './VoucherCell';

export default class VouchersCarousel extends Component {

    constructor(props) {
        super(props)

        this.currentPage = props.currentPage
        onPageChange = this.onPageChange.bind(this)
    }

    render() {

        const offerComponent = this.props.vouchers.map((voucher) => this.renderOffers(voucher))

        return (
            <View style={styles.container}>
                <Carousel
                    ref="carousel"
                    pageWidth={itemWidth}
                    key="carousel"
                    swipeThreshold={0.1}
                    currentPage={this.currentPage}
                    onPageChange={(pageNumber) => this.onPageChange(pageNumber)}
                    style={{ height: itemHeight}}>
                    {offerComponent}
                </Carousel>
            </View>
        )
    }

    renderOffers(item) {
        return (
            <View style={{
                top: 10,
                marginBottom: 10
            }}>
                <VoucherCell item={item} />
            </View>
        );
    }

    onPageChange(pageNumber) { 
        if (this.currentPage != pageNumber) {
            this.currentPage = pageNumber
            const voucher = this.props.vouchers[pageNumber]
            if (this.props.handleSelectedVoucher) {
                setTimeout(() => {
                    this.props.handleSelectedVoucher(voucher)
                }, 0);
            }
        }
    }

    selectedOptionAction() {
        if (this.props.selectedOptionAction) {
        }
    }
}

const styles = StyleSheet.create({
    container: {
        height: 100
    }
})