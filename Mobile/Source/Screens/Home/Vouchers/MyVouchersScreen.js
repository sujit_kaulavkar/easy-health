import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Animated,
    FlatList,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT,
    STATUS_BAR_HEIGHT_IOS,
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    APP_NAME,
    BLUE_COLOR
} from '../../../Helper/Constants';

import VoucherCell from './VoucherCell';
import DropdownAlert from 'react-native-dropdownalert';

import {
    vouchers
} from '../Static/vouchers';
import { CONTENT_LOAD_OFFSET_POSITION } from '../../Notification/NotificationScreen';

export default class MyVouchersScreen extends Component {

    constructor(props) {
        super(props)

        totalPages = 1;
        pageNumber = 1;
        isLoadingMore = false

        overScrollModeStatus = 'never'
        visibleItemIndex = 0;

        const scrollAnim = new Animated.Value(0);
        const offsetAnim = new Animated.Value(0);

        this.state = {
            scrollAnim,
            offsetAnim,
            isShowSpinner: false,
            vouchers: [],
            isShowSuceessUI: false,
            clampedScroll: Animated.diffClamp(
                Animated.add(
                    scrollAnim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 0.5],
                        extrapolateLeft: 'clamp',
                    }),
                    offsetAnim,
                ),
                0,
                TOP_BAR_HEIGHT - STATUS_BAR_HEIGHT_IOS,
            ),
        }

        reloadData = this.reloadData.bind(this)
        onViewableItemsChanged = this.onViewableItemsChanged.bind(this)
        selectedOptionAction = this.selectedOptionAction.bind(this)
        removeSuccessUI = this.removeSuccessUI.bind(this)
    }

    componentDidMount() {
        this.state.scrollAnim.addListener(({ value }) => {
            this.handleScroll()
        });

        this.remoteFetchVouchers()
    }

    componentWillUnmount() {
        this.state.scrollAnim.removeAllListeners();
    }

    handleScroll() {
        const { vouchers } = this.state
        if (vouchers && (vouchers.length > 0)) {
            if (visibleItemIndex / vouchers.length > CONTENT_LOAD_OFFSET_POSITION) {
                this.handleLoadMore()
            }
        }
    }

    handleLoadMore = () => {

        if ((totalPages != 1) && totalPages > pageNumber) {
            if (isLoadingMore == false) {
                isLoadingMore = true
                pageNumber = pageNumber + 1;
                this.remoteFetchVouchers()
            }
        }
    }

    onViewableItemsChanged({ viewableItems }) {

        let item = viewableItems[viewableItems.length - 1]
        if (item) {
            visibleItemIndex = item.index
        }
    }

    remoteFetchVouchers() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        if (pageNumber == 1) {
            this.showSpinner()
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.myVouchers(user.user_hash_id, pageNumber)
            .then((responseJson) => {
                this.isLoadingMore = false
                this.hideSpinner()
                if (responseJson) {
                    this.handleMyVouchers(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.isLoadingMore = false
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleMyVouchers(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            totalPages = data.totalPageNumber
            const vouchers = data.template
            this.setState({
                vouchers: vouchers
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='VOUCHERS'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                <FlatList
                    scrollEventThrottle={1}
                    removeClippedSubviews={true}
                    legacyImplementation={false}
                    key="vouchersFlatList"
                    style={styles.flatListStyle}
                    data={this.state.vouchers}
                    extraData={this.state}
                    keyExtractor={(item) => item.voucher_id}
                    renderItem={({ item }) => this.renderCellItem(item)}
                />

                {/* {this.state.isShowSuceessUI ? <RedeemSuccessView removeSuccessUI={() => this.removeSuccessUI()} /> : null} */}

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    renderCellItem(item) {
        return <VoucherCell item={item} selectedOptionAction={(voucher) => this.selectedOptionAction(voucher)}/>
    }

    selectedOptionAction(item) {

        const index = this.state.vouchers.indexOf(item)        
        this.props.navigation.navigate("VoucherDetailScreen", {
            voucherIndex: index,
            selectedVoucher: item,
            allVouchers: this.state.vouchers
        })
    }

    removeSuccessUI() {
        this.setState({
            isShowSuceessUI: false
        })
    }

    reloadData() {
        const { vouchers } = this.state
        this.setState({
            vouchers: vouchers
        })
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    flatListStyle: {
        flex: 1,
        marginHorizontal: 10,
        marginTop: TOP_BAR_HEIGHT + 10
    }
})
