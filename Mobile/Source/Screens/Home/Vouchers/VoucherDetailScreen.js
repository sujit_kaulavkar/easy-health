import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    moderateScale
} from '../../../Helper/Scaling';

import {
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    APP_NAME,
    BLUE_COLOR,
    CANCEL_TEXT,
    YES_TEXT
} from '../../../Helper/Constants';

import DropdownAlert from 'react-native-dropdownalert';
import RedeemSuccessView from './RedeemSuccessView';
import VouchersCarousel from './VouchersCarousel';

export default class VoucherDetailScreen extends Component {

    constructor(props) {
        super(props)

        const { allVouchers, selectedVoucher, voucherIndex } = props.navigation.state.params

        this.state = {
            currentVoucherIndex: voucherIndex,
            selectedVoucher: selectedVoucher,
            vouchers: allVouchers,
            isShowSpinner: false,
            isShowSuceessUI: false,
        }

        handleSelectedVoucher = this.handleSelectedVoucher.bind(this)
        handleSelectedAddress = this.handleSelectedAddress.bind(this)
    }

    remoteRedeemVoucher() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.redeemVoucher(user.user_hash_id, this.state.selectedVoucher.voucher_id)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleRedeemVoucher(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleRedeemVoucher(responseJson) {
        const status = responseJson.status
        if (status == 1) {
            this.state.selectedVoucher.is_redeem = true
            this.setState({})
            this.showRedeemVoucherSuccessPopup()
        }
        else {
            this.showServerErrorMessage()
        }
    }

    render() {

        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='VOUCHERS'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}/>

                <ScrollView
                    style={{ flex: 1, top: TOP_BAR_HEIGHT }}
                    contentContainerStyle={{ flexGrow: 1 }}>

                    <View style={styles.container}>
                        <VouchersCarousel 
                        vouchers={this.state.vouchers} 
                        currentPage={this.state.currentVoucherIndex}
                        handleSelectedVoucher={(voucher) => this.handleSelectedVoucher(voucher)}
                        />                      
                    </View>

                    {this.rememptionHistoryView()}
                    {this.voucherDescriptionView()}
                    {this.tncView()}
                    {this.reedemButton()}

                </ScrollView>

                {this.state.isShowSuceessUI ? 
                    <RedeemSuccessView 
                    ref="redeemSuccessView"
                        selectedVoucher={this.state.selectedVoucher}
                    removeSuccessUI={() => this.removeSuccessUI()} 
                    selectAddress={() => this.selectAddress()}/> : null}

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    handleSelectedVoucher(voucher) {  
        if (voucher != this.state.selectedVoucher) {
            this.setState({
                selectedVoucher: voucher
            })
        }        
    }

    renderOffers(item) {
        return (
            <View style={{
                top: 10,
                marginBottom: 10
            }}>
                <VoucherCell item={item} />
            </View>
        );
    }

    renderRememptionOutlets(outlet) {
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={{
                    color: 'GRAY',
                    fontSize: moderateScale(17, 0.09),
                    fontFamily: 'BarlowCondensed-Light'
                }}>- {outlet}</Text>
            </View>
        );
    }

    rememptionHistoryView() {

        const { selectedVoucher } = this.state
        const outlets = selectedVoucher.redemption_outlets.map((outlet) => this.renderRememptionOutlets(outlet))
        return (
            outlets.length > 0 ? <View style={{
                marginTop: 20,
                marginHorizontal: 20
            }}>
                <Text style={styles.redeemDetailTitle}>
                    REDEMPTION HISTORY
                        </Text>
                {outlets}
            </View> : null
        )
    }

    voucherDescriptionView() {
        const { selectedVoucher } = this.state
        return (
            selectedVoucher.description.length > 0 ?
                <View style={{
                    marginTop: 50,
                    marginHorizontal: 20
                }}>
                    <Text style={styles.redeemDetailTitle}>
                        DESCRIPTION
                        </Text>

                    <Text style={{
                        color: 'GRAY',
                        fontSize: moderateScale(17, 0.09),
                        fontFamily: 'BarlowCondensed-Light'
                    }}>
                        {selectedVoucher.description}
                    </Text>
                </View> : null
        )
    }

    tncView() {
        const { selectedVoucher } = this.state        
        return (
            selectedVoucher.terms_and_conditions != null ?
                <View style={{
                    marginTop: 50,
                    marginHorizontal: 20
                }}>
                    <Text style={styles.redeemDetailTitle}>
                        TERMS AND CONDITIONS
                        </Text>

                    <Text style={{
                        color: 'GRAY',
                        fontSize: moderateScale(17, 0.09),
                        fontFamily: 'BarlowCondensed-Light'
                    }}>
                        {selectedVoucher.terms_and_conditions}
                    </Text>
                </View> : null
        )
    }

    reedemButton() {

        const isRedeemedVoucher = this.state.selectedVoucher.is_redeem
        if (isRedeemedVoucher == true) {
            return null
        }

        return (            
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => this.redeemVoucher()}
                style={{
                    marginTop: 50,
                    width: 120,
                    height: 35,
                    alignSelf: 'center',
                    borderRadius: 10,
                    backgroundColor: 'rgba(255,255,255,0.7)',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Text style={{
                    color: BLUE_COLOR,
                    fontSize: moderateScale(16, 0.09),
                    fontFamily: 'BarlowCondensed-Regular'
                }}>
                    REDEEM
                </Text>
            </TouchableOpacity>
        )
    }

    redeemVoucher() {
        this.showRedeemVoucherSuccessPopup()
        return;
        Alert.alert(
            APP_NAME,
            "Do you want to redeem the voucher?",
            [
                {
                    text: CANCEL_TEXT, onPress: () => { }
                },
                {
                    text: YES_TEXT, onPress: () => { this.remoteRedeemVoucher() }
                }
            ],
            { cancelable: false }
        )
    }

    showRedeemVoucherSuccessPopup() {
        this.setState({
            isShowSuceessUI: true
        })
    }

    removeSuccessUI() {
        this.setState({
            isShowSuceessUI: false
        })
    }

    selectAddress() {
        this.props.navigation.navigate("MyAddressScreen", {
            isFromVoucher: true,
            handleSelectedAddress: (type, address) => this.handleSelectedAddress(type, address)
        })
    }

    handleSelectedAddress(type, address) {
        this.refs.redeemSuccessView.showSelectedAddress(type, address)
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        width: SCREEN_WIDTH,
        marginTop: 10,
        height: 100
    },
    redeemDetailTitle: {
        color: BLUE_COLOR,
        fontSize: moderateScale(19, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    }
})
