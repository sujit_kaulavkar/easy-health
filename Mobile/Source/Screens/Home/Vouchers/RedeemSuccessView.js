import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Alert,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Modal from "react-native-modal";
import DropdownAlert from 'react-native-dropdownalert';
import { moderateScale } from '../../../Helper/Scaling';
import { 
    BLUE_COLOR, 
    APP_THEME, 
    APP_NAME,
    SCREEN_WIDTH,
    SCREEN_HEIGHT} from '../../../Helper/Constants';
import {
    Helper
} from '../../../Helper/FilePath';

const THANK_YOU_TEXT = "THANK YOU !"
const REDEEM_TITLE_TEXT = "YOUR GIFT VOUCHER HAS BEEN REDEEMED"
const REDEEM_MESSAGE_TEXT = "WE HAVE A REWARDS PROGRAMME JUST FOR YOU! \n POINTS THAT WILL NEVER EXPIRE !"
const LETS_START_TEXT = "YOUR LOYALTY REWARDS CARD WILL BE MAILED TO YOU \n WITHIN 5-7 WORKING DAYS."
const ASK_ME_LATER_TEXT = "ASK ME LATER"

export default class RedeemSuccessView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isVisible: true,
            addressType: null,
            selectedAddress: null
        }
    }

    remoteRequestloyaltyCard() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }        

        this.showSpinner()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.requestLoyaltyCard(user.user_hash_id, this.state.addressType, this.props.selectedVoucher.voucher_id)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleRequestLoyaltyCard(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleRequestLoyaltyCard(responseJson) {
        const status = responseJson.status
        if (status == 1) {
            this.backDropPress()
        }
        else {
            this.showServerErrorMessage()
        }
    }

    render() {
        return (
            <View
 //               isVisible={this.state.isVisible}
                onBackdropPress={() => this.backDropPress()}
                style={styles.container}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <View style={{}}>
                    <Image
                        style={{height:120, width:120, top: 40, zIndex: 1}}
                        source={require("../../../Resources/Images/blue_checkmark.png")}
                    />
                </View>

                <View style={styles.subContainerStyle}>
                    <Text style={[{marginTop: 60}, styles.thankYouText]}>
                        {THANK_YOU_TEXT}
                    </Text>

                    <Text style={styles.thankYouText}>
                        {REDEEM_TITLE_TEXT}
                    </Text>

                    <Text style={styles.redeemMessageText}>
                        {REDEEM_MESSAGE_TEXT}
                    </Text>

                    <Text style={styles.letsStartText}>
                        {LETS_START_TEXT}
                    </Text>

                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.selectAddress()}
                        style={styles.addressButton}>
                        <Text numberOfLines={1} style={styles.okText}>
                            {this.state.selectedAddress == null ? 'SELECT ADDRESS' : this.state.selectedAddress}
                        </Text>
                    </TouchableOpacity>

                    <View style={styles.buttonBase}>
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => this.redeemVoucher()}
                            style={styles.OkButton}>
                            <Text style={styles.okText}>
                                OK
                           </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => this.askMeLater()}
                            style={styles.askMeLaterButton}>
                            <Text style={styles.askMeLaterText}>
                                {ASK_ME_LATER_TEXT}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />                    
            </View>
        )
    }

    showSelectedAddress(type, address) {

        const newAddress = address.address_line_1 + ' ' +
                            address.address_line_2 + ' ' +
                            address.city + ' ' +
                            address.state + ' ' +
                            address.country + ' ' +
                            address.zip_code + ' '

        this.setState({
            addressType: type,
            selectedAddress: newAddress
        })
    }

    selectAddress() {
        if (this.props.selectAddress) {
            this.props.selectAddress()
        }
    }

    redeemVoucher() {
        if (this.state.selectedAddress == null) {
            Alert.alert(APP_NAME, "Select address to dispatch loyalty card")
        }
        else {
            this.remoteRequestloyaltyCard()
        }        
    }

    askMeLater() {
        this.backDropPress()
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }


    backDropPress() {
        this.setState({
            isVisible: false
        })

        setTimeout(() => {
            if (this.props.removeSuccessUI) {
                this.props.removeSuccessUI()
            }
        }, 300);
    }

    onCloseDropdown() {}
}

const styles = StyleSheet.create({
    container: {
        zIndex:10,
        position:'absolute',
        marginTop: 20,
        backgroundColor:'rgba(0,0,0,0.3)',
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    subContainerStyle: {
        top: 10,
        height: 500,
        width: 350,
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: 'white'
    },
    thankYouText: {
        color: 'black',
        textAlign: 'center',
        fontSize: moderateScale(20, 0.09),
        fontFamily: 'BarlowCondensed-Medium'
    },
    redeemMessageText: {
        marginTop: 10,
        textAlign: 'center',
        color: 'gray',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Light',
        lineHeight: 25
    },
    letsStartText: {
        marginTop: 50,
        textAlign: 'center',
        color: 'gray',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Light'
    },
    buttonBase: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    addressButton:{
        width: "90%",
        height: 35,
        marginTop:20,
        marginBottom: 10,
        borderWidth: 2,
        borderColor: BLUE_COLOR,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    OkButton: {
        width: 100,
        height: 35,
        marginBottom: 10,
        borderWidth: 2,
        borderColor: BLUE_COLOR,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    okText: {
        color: 'gray',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    },
    askMeLaterButton: {
        width: 110,
        height: 35,
        borderWidth: 2,
        marginBottom: 10,
        borderColor: BLUE_COLOR,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    askMeLaterText: {
        color: 'gray',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'BarlowCondensed-Regular'
    }
})