import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
} from 'react-native';

import {
    Helper
} from '../../../Helper/FilePath';

import {
    SCREEN_WIDTH
} from '../../../Helper/Constants';

import {
    moderateScale
} from '../../../Helper/Scaling';

export default class VoucherCell extends PureComponent {

    render() {

        const { item } = this.props

        let apiHelper = Helper.API_HELPER.getInstance();
        const vendorImage = apiHelper.baseUrl() + item.voucher_image_url;

        return (
            <View>
                <View style={styles.cellContainer}>
                    <View style={styles.cellBaseContainer}>
                        <Image
                            resizeMode="stretch"
                            style={[styles.backgroundImage]}
                            source={this.voucherTypeImage(item)} />

                        <View style={styles.merchantDetails}>
                            <Image
                                style={styles.merchantLogo}
                                source={{ uri: vendorImage }} />

                            <View styl={styles.merchantBaseView}>
                                <Text style={styles.title}>
                                    {item.discount_offer}
                                </Text>
                                <Text style={styles.message}>
                                    {item.voucher_title}
                                </Text>
                                <Text style={styles.expiryDate}>
                                    EXPIRES ON {item.expiry_date}
                                </Text>
                            </View>
                            <View style={[styles.clickButton, { backgroundColor: item.is_redeem == true ? "rgba(0,0,0,0.5)" : 'transparent' }]} />
                        </View>

                        {
                            item.is_redeem == true ?
                                <Image
                                    resizeMode='contain'
                                    style={{
                                        position: 'absolute',
                                        right: 0,
                                        width: 220,
                                        height: "100%"
                                    }}
                                    source={require("../../../Resources/Images/Voucher/redeem.png")} /> : null
                        }

                        <TouchableOpacity
                            activeOpacity={1}
                            style={styles.clickButton} onPress={() => this.selectedOptionAction(item)} />
                    </View>
                </View>
            </View>
        )
    }

    voucherTypeImage(item) {
        switch (item.voucher_type) {
            case 'Discount': {
                return require("../../../Resources/Images/Voucher/discount_vouchers.png")
            }
                break;

            case 'Points': {
                return require("../../../Resources/Images/Voucher/points_vouchers.png")
            }
                break;

            default: {
                return require("../../../Resources/Images/Voucher/offer_vouchers.png")
            }
        }
    }

    selectedOptionAction(item) {
        if (this.props.selectedOptionAction) {
            this.props.selectedOptionAction(item)
        }
    }
}

const styles = StyleSheet.create({
    cellContainer: {
        width: "100%",
        height: SCREEN_WIDTH * 0.96 * 0.22 + 20,
        alignItems: 'center'
    },
    cellBaseContainer: {
        width: "100%",
        height: SCREEN_WIDTH * 0.96 * 0.22,
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    merchantDetails: {
        width: SCREEN_WIDTH,
        height: 100,
        flexDirection: 'row'
    },
    merchantLogo: {
        top: 8,
        borderRadius: 8,
        marginHorizontal: 10,
        width: 80,
        height: 80
    },
    merchantBaseView: {
        backgroundColor: 'yellow',
        height: 100,
        width: 200
    },
    title: {
        top: 5,
        marginHorizontal: 5,
        fontSize: moderateScale(20, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: 'white'
    },
    message: {
        marginHorizontal: 5,
        fontSize: moderateScale(20, 0.09),
        fontFamily: 'BarlowCondensed-SemiBold',
        color: 'white'
    },
    expiryDate: {
        marginTop: 10,
        marginHorizontal: 5,
        fontSize: moderateScale(13, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: 'white'
    },
    clickButton: {
        borderRadius: 8,
        flex: 1,
        width: '96%',
        height: '100%',
        zIndex: 0,
        position: 'absolute'
    }
})
