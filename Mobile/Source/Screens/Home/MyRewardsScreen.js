import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Animated,
    FlatList,
    TextInput,
    StyleSheet
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT,
    STATUS_BAR_HEIGHT_IOS,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    SCREEN_WIDTH
} from '../../Helper/Constants';

import DropdownSearchList from '../../Components/DropdownSearchList';
import DropdownAlert from 'react-native-dropdownalert';
import { moderateScale } from '../../Helper/Scaling';

export default class MyRewardsScreen extends Component {

    constructor(props) {
        super(props)

        totalPages = 1;
        pageNumber = 1;
        isLoadingMore = false

        overScrollModeStatus = 'never'
        visibleItemIndex = 0;

        const scrollAnim = new Animated.Value(0);
        const offsetAnim = new Animated.Value(0);

        this.state = {
            vendors:[],
            scrollAnim,
            offsetAnim,
            isShowSpinner: false,
            vouchers: [],
            isShowSuceessUI: false,
            clampedScroll: Animated.diffClamp(
                Animated.add(
                    scrollAnim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 0.5],
                        extrapolateLeft: 'clamp',
                    }),
                    offsetAnim,
                ),
                0,
                TOP_BAR_HEIGHT - STATUS_BAR_HEIGHT_IOS,
            ),
        }

        reloadData = this.reloadData.bind(this)
        remoteFetchVendor = this.remoteFetchVendor.bind(this)
    }

    componentDidMount() {
        this.remoteFetchVouchers()
    }

    remoteFetchVouchers() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        if (pageNumber == 1) {
            this.showSpinner()
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.myVouchers(user.user_hash_id, pageNumber)
            .then((responseJson) => {
                this.isLoadingMore = false
                this.hideSpinner()
                if (responseJson) {
                    this.handleMyVouchers(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.isLoadingMore = false
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleMyVouchers(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            totalPages = data.totalPageNumber
            const vouchers = data.template
            this.setState({
                vouchers: vouchers
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    remoteFetchVendor(text) {        
        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        apiHelper.vendors(text).then((responseJson) => {
            if (responseJson) {
                this.handleVendorResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.showServerErrorMessage()
            })
    }

    handleVendorResponse(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            let vendors = data.template
            this.setState({
                venders: vendors
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='REWARDS'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                {this.renderSearchBar()}

                <FlatList
                    scrollEventThrottle={1}
                    removeClippedSubviews={true}
                    legacyImplementation={false}
                    key="vouchersFlatList"
                    style={styles.flatListStyle}
                    data={this.state.vouchers}
                    extraData={this.state}
                    keyExtractor={(item) => item.voucher_id}
                    renderItem={({ item }) => this.renderCellItem(item)}
                />
                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    renderCellItem(item) {

        const logo = "https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg"
        return (
            <View style={styles.cellView}>
                <View style={styles.cellContainer}>
                    <View style={styles.cellBaseContainer}>

                        <View style={styles.merchantDetails}>
                            <Image
                                style={styles.merchantLogo}
                                source={{ uri: logo }} />

                            <View styl={styles.merchantBaseView}>
                                <Text style={styles.title}>
                                    Merchant Name
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    renderSearchBar() {
        return (
            <View style={{
                width: SCREEN_WIDTH,
                position: 'absolute',
                top: TOP_BAR_HEIGHT,
                zIndex: 10
            }}>
                <DropdownSearchList
                    onTextChange={text => this.remoteFetchVendor(text)}
                    onItemSelect={item => () => {
                        this.props.navigation.navigate("InputCardNumberScreen", {
                            vendor : this.state.vendors[0]
                        })}}
                    containerStyle={{ padding: 5 }}
                    textInputStyle={{
                        padding: 12,
                        borderWidth: 1,
                        borderColor: '#ccc',
                        borderRadius: 5,
                    }}
                    itemStyle={{
                        padding: 10,
                        marginTop: 0,
                        backgroundColor: 'white',
                        borderColor: 'gray',
                        borderWidth: 0.5,
                        borderRadius: 5,
                    }}
                    itemTextStyle={{ color: '#222' }}
                    itemsContainerStyle={{ backgroundColor: 'white', maxHeight: 200 }}
                    items={this.state.vendors}
                    defaultIndex={1}
                    placeholder="Search by merchant"
                    resetValue={false}
                    underlineColorAndroid="transparent" />
            </View>
        )
    }

    reloadData() {
        const { vouchers } = this.state
        this.setState({
            vouchers: vouchers
        })
    }

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    flatListStyle: {
        flex: 1,
        marginHorizontal: 10,
        marginTop: TOP_BAR_HEIGHT + TOP_BAR_HEIGHT + 10
    },
    textInputStyle: {
        fontFamily: "BarlowCondensed-Regular",
        fontSize: moderateScale(17, 0.09),
        color: 'white',
        flex: 1,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09),
        backgroundColor: 'transparent',
    },
    cellView: {
        width: SCREEN_WIDTH,
    },
    cellContainer: {
        width: SCREEN_WIDTH * 0.96,
        height: SCREEN_WIDTH * 0.96 * 0.22 + 20,
        alignItems: 'center'
    },
    cellBaseContainer: {
        width: SCREEN_WIDTH * 0.96,
        height: SCREEN_WIDTH * 0.96 * 0.22,
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    merchantDetails: {
        width: SCREEN_WIDTH,
        height: 100,
        flexDirection: 'row'
    },
    merchantLogo: {
        top: 8,
        marginHorizontal: 10,
        width: 80,
        height: 80
    },
    merchantBaseView: {
        backgroundColor: 'yellow',
        height: 100,
        width: 200
    },
    title: {
        top: 30,
        marginHorizontal: 5,
        fontSize: moderateScale(20, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: 'white'
    }
})
