
import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import { itemWidth, itemHeight } from '../Dashboard/CardView/styles/SliderEntry.style';
import Carousel from 'react-native-carousel-control';

import {
    homeItemsArray
} from './Static/entries';

export default class OffersView extends Component {

    constructor() {
        super()
        
        this.state = {
            currentCardIndex: 0,
            offers: ["Hello 1", "Hello 2", "Hello 3", "Hello 4"]
        }
    }

    componentDidMount() {
    }

    render() {

        const offerComponent = this.state.offers.map((offer) => this.renderItemWithParallax(offer))

        return (
            <View style={styles.container}>
                <Carousel
                    ref="carousel"
                    pageWidth={itemWidth}                    
                    key="carousel"
                    swipeThreshold={0.1}
                    currentPage={this.state.currentCardIndex}
                    style={{ height: itemHeight }}>     
                    {offerComponent}               
                </Carousel>
            </View>
        )
    }

    renderItemWithParallax(item) {
        return (
            <View style={{backgroundColor:'yellow', flex:1, alignItems:'center', justifyContent:'center'}}>
                <Text>
                    {item}
                </Text>

                <TouchableOpacity style={{ flex: 1, top: 0, left: 0, bottom: 0, right: 0, zIndex: 0, position: 'absolute' }} onPress={() => this.selectedOptionAction()} />
            </View>
        );
    }

    selectedOptionAction() {
        if (this.props.selectedOptionAction) {
            this.props.selectedOptionAction(homeItemsArray[1])
        }
    }
}

const styles = StyleSheet.create({
    container: {
        height: 80,
    }
})