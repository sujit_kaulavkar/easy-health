import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Alert,
    Keyboard,
    FlatList,
    StyleSheet,
    BackHandler,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    APP_NAME,
    CANCEL_TEXT,
    OK_TEXT
} from '../../Helper/Constants';

import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default class PointsToCashScreen extends Component {

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='POINTS TO CASH'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

            </Components.BACKGROUND_IMAGE>
        );
    }
}