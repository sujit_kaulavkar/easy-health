
import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import {
    Components
} from '../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT, SCREEN_WIDTH, BLUE_COLOR, SCREEN_HEIGHT
} from '../../Helper/Constants';

import {
    homeItemsArray
} from './Static/entries';

import  OffersView from './OffersView';
import { moderateScale } from '../../Helper/Scaling';

export default class HomeScreen extends Component {

    constructor() {
        super()
        this.state = {
            numberOfRewards: 0
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='HOME'
                    isMenuButtonNeeded={true}
                    menuButtonAction={() => {
                        this.props.navigation.openDrawer()
                    }} />

                <View style={{marginTop: TOP_BAR_HEIGHT}}>
                    <OffersView selectedOptionAction={(item) => this.selectedOptionAction(item)}/>
                </View>

                <View style={{flex:1}}>
                    <FlatList
                        scrollEventThrottle={1}
                        removeClippedSubviews={true}
                        legacyImplementation={false}
                        key="optionsFlatlist"
                        style={styles.flatListStyle}
                        data={homeItemsArray}
                        keyExtractor={(item) => item.index}
                        renderItem={({ item, index }) => this.renderCellItem(item, index)}
                    />
                </View>

            </Components.BACKGROUND_IMAGE>
        );
    }

    renderCellItem(item) {

        const imageWidth = SCREEN_WIDTH
        const imageHeight = (SCREEN_HEIGHT - TOP_BAR_HEIGHT - 80 - 35)/3

        return (
            <View style={{ 
                width: imageWidth, 
                height: imageHeight,
                justifyContent:'center',
                justifyContent:'center'
                }}>
                    <Image
                        resizeMode="stretch"
                        style={{
                            width: imageWidth, 
                            height: imageHeight - 10, 
                            position: 'absolute'
                        }}
                        source={item.icon}/>

                        <Text style={{                                                        
                            color:'white',
                            textAlign:'center',
                            fontSize: moderateScale(30, 0.09),
                            fontFamily: "BarlowCondensed-Regular"}}>
                         {item.title}
                        </Text>

                <TouchableOpacity style={styles.cellButton} onPress={() => this.selectedOptionAction(item)} />
            </View>
        )
    }

    selectedOptionAction(item) {        
        switch (item.index) {
            case 1: {
                this.props.navigation.navigate("MyRewardsScreen")
            }
            break;

            case 2: {
                this.props.navigation.navigate("MyVouchersScreen")
            }
                break;

            case 3: {
                this.props.navigation.navigate("PointsToCashScreen")
            }
                break;

                default:
                break;
        }
    }
}

const styles = StyleSheet.create({
    flatListStyle: {
        flex: 1,        
        marginHorizontal: 10,
        marginTop: 10
    },
    cellButton: {
        flex: 1,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: 0,
        position: 'absolute'
    }
})
