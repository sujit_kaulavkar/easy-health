export const homeItemsArray = [
    {
        "index": 1,
        "icon": require("../../../Resources/Images/Home/my_rewards.png"),
        "title": "REWARDS"
    },
    {
        "index": 2,
        "icon": require("../../../Resources/Images/Home/my_vouchers.png"),
        "title": "VOUCHERS"
    },
    {
        "index": 3,
        "icon": require("../../../Resources/Images/Home/points_to_cash.png"),        
        "title": "POINTS TO CASH"
    }
]