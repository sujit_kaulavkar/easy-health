export const vouchers = [
    {
        "type": 1,
        "typeImage": require("../../../Resources/Images/Voucher/points_vouchers.png"),
        "icon": require("../../../Resources/Images/edit.png"),
        "voucherTitle": "MOBILE SIGN UP VOUCHER",
        "voucherMessage": "GET 1000 REWARD POINTS",
        "expiry": "3 MARCH 2019"
    },
    {
        "type": 2,
        "typeImage": require("../../../Resources/Images/Voucher/discount_vouchers.png"),
        "icon": require("../../../Resources/Images/edit.png"),
        "voucherTitle": "MOBILE SIGN UP VOUCHER",
        "voucherMessage": "GET 1000 REWARD POINTS",
        "expiry": "3 MARCH 2019"
    },
    {
        "type": 3,
        "typeImage": require("../../../Resources/Images/Voucher/offer_vouchers.png"),
        "icon": require("../../../Resources/Images/edit.png"),
        "voucherTitle": "MOBILE SIGN UP VOUCHER",
        "voucherMessage": "GET 1000 REWARD POINTS",
        "expiry": "3 MARCH 2019"
    }
]