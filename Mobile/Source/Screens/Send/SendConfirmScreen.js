
import React, { Component } from 'react';
import { StatusBar, Text, View, ScrollView, Alert } from 'react-native'
import Styles from "./style";

import {
    Components,
    TOP_UP,
    Helper
} from '../../Helper/FilePath';

import DropdownAlert from 'react-native-dropdownalert';
import {
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME,
    APP_NAME,
    CANCEL_TEXT,
    YES_TEXT
} from '../../Helper/Constants';

const transferFeeAmount = 0.0

export default class SendConfirmScreen extends Component {

    constructor() {
        super()
        this.state = {
            isTouchEnabled: true
        }
        transferButtonAction = this.transferButtonAction.bind(this)
    }

    remoteInitiatePayRequest() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.refs.submitButton.startAnimation()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { topupAmount, mobileNumber } = this.props.navigation.state.params;

        apiHelper.initiateCreditTransfer(user.user_hash_id, mobileNumber, topupAmount)
            .then((responseJson) => {
                this.hideSpinner()
                if (responseJson) {
                    this.handleInitiateTransferSuccess(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideSpinner()
                this.showServerErrorMessage()
            })
    }

    handleInitiateTransferSuccess(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            this.showSuccessScreen(responseJson.transactionId, "")
        }
        else if (status == 403) { // assumpti""on is that transaction is in pending state 

            const id = responseJson.data.transfer_id
            this.showSuccessScreen("", id)
        }
        else {
            const errorMessage = responseJson.errorMessage;
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    showSuccessScreen(transactionId, transfer_id = "") {
        const { topupAmount } = this.props.navigation.state.params;
        this.props.navigation.navigate("SendSuccessScreen", {
            topupAmount: topupAmount,
            transferFee: transferFeeAmount,
            transfer_id: transfer_id,
            transactionId: transactionId
        })
    }

    render() {

        const { topupAmount, mobileNumber, message } = this.props.navigation.state.params;
        const amount = parseFloat(topupAmount);
        const transferFee = transferFeeAmount;

        return (
            <Components.BACKGROUND_IMAGE
                isShowMMLogo={true}
                style={Styles.container}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Send'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <ScrollView
                    style={Styles.scrollViewStyle}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1 }}>

                    <View style={{
                        paddingTop: 30,
                    }}>

                        <TOP_UP.TOTAL_AMOUNT_VIEW
                            separatorLineColor="#D3D3D3"
                            topupAmount={amount}
                            transferFee={transferFee} />
                    </View>

                    <Text style={Styles.recepientStyle}>Recipent Details</Text>

                    <View style={Styles.recipentContainer}>
                        <Text style={Styles.mobileTextStyle}>Mobile Number</Text>
                        <Text style={Styles.mobileValueStyle}>{mobileNumber}</Text>

                        {message ? <View>
                            <Text style={Styles.messageStyle}>Message</Text>
                            <Text style={Styles.mobileValueStyle}>{message}</Text>
                        </View>
                            : <View />}

                    </View>

                    <View style={Styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title="Transfer"
                            onPress={() => this.transferButtonAction()}
                        />
                    </View>

                </ScrollView>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    showSpinner() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideSpinner() {
        this.refs.submitButton.stopAnimation()
        if (this.state.isTouchEnabled == false) {
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    transferButtonAction() {
        this.showWarningMessage()
    }

    showWarningMessage() {
        const { topupAmount, mobileNumber } = this.props.navigation.state.params;
        Alert.alert(
            APP_NAME,
            "You will pay SGD " + topupAmount + " to " + mobileNumber + ". Are you sure you want to continue?",
            [
                {
                    text: CANCEL_TEXT, onPress: () => { }
                },
                {
                    text: YES_TEXT, onPress: () => {
                        this.remoteInitiatePayRequest()
                    }
                }
            ],
            { cancelable: false }
        )
    }

    onCloseDropdown() { }
}