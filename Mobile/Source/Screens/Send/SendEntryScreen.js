
import React, { Component } from 'react';
import {
  View,
  Keyboard,
  StyleSheet,
  ScrollView,
  Text,
  TextInput,
  BackHandler
} from 'react-native';

import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';

import {
  Components,
  Helper,
  DASHBOARD
} from '../../Helper/FilePath';
import { 
  TOP_BAR_HEIGHT, 
  SCREEN_WIDTH, 
  APP_THEME, 
  WRONG_PHONE_NUMBER_MESSAGE,
  WALLET_EMPTY_MESSAGE,
  LESS_WALLET_BALANCE_MESSAGE,
  SOMETHING_WRONG_MESSAGE,
  IS_IOS
} from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default class SendEntryScreen extends Component {

  constructor() {
    super()

    this.state = {
      walletInfo: {
        "walletBalance": "0.0",
        "walletCurrency": "SGD"
      }
    }
    proceedButtonAction = this.proceedButtonAction.bind(this)
  }

  componentDidMount() {
    this.addObservers()
    const walletInfo = Helper.USER_MANAGER.getInstance().getWalletBalance()
    this.setState({
      walletInfo: walletInfo
    })
  }

  componentWillUnmount() {
    didBlurObserver.remove();
    didFocusObserver.remove();
  }

  addObservers() {

    BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
    // listener called when viewWillAppear
    didFocusObserver = this.props.navigation.addListener(
      'didFocus',
      payload => {
        Events.trigger('unlockDrawer')
      }
    );

    // listener called when viewWillDisappear
    didBlurObserver = this.props.navigation.addListener(
      'didBlur',
      payload => {
        Events.trigger('lockDrawer')
      }
    );
  }

  render() {
    return (
      <Components.BACKGROUND_IMAGE style={styles.container}>
        
        <Components.TOP_BAR
          topbarTranslate={0}
          ref="topbarRef"
          title='Send'
          isMenuButtonNeeded={true}
          menuButtonAction={() => {
            Keyboard.dismiss()
            this.props.navigation.openDrawer()
          }}/>

        <ScrollView
          style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
          keyboardShouldPersistTaps="always"
          contentContainerStyle={{ flexGrow: 1 }}>

          <View>
            <View style={{ height: 100 }}>
              <DASHBOARD.WALLET_BALANCE_VIEW walletInfo={this.state.walletInfo} />
            </View>

            <View style={styles.mobileNumberViewStyle}>
              <Text style={styles.mobileNumberTextStyle}>Mobile Number</Text>
              <Components.MOBILE_NUMBER ref="mobilenumber" />
            </View>

            <View style={{
              height: 60,
              top: 30,
              marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
            }}>
              <Text style={{ color: "white" }}>Enter Amount</Text>
              <View style={styles.currencyContainerStyle}>
                <Text style={styles.currencyTextStyle}>
                  SGD
              </Text>
                <View style={styles.verticalSeparatorStyle} />
                <TextInput ref="textInput" style={styles.textInputStyle}/>
              </View>
              <View style={styles.underlineStyle} />
            </View>

            <View style={{
              height: 60,
              top: 60,
              marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
            }}>
              <Text style={{ color: "white" }}>Enter Message</Text>
              <TextInput 
              placeholder="optional" 
              ref="messageTextInput" 
              placeholderTextColor="rgba(255,255,255,0.5)"
              style={styles.textInputStyle} />
              <View style={styles.underlineStyle} />
            </View>

            <View style={styles.submitButtonBase}>
              <Components.BUTTON_SUBMIT
                ref="submitButton"
                activeOpacity={0.7}
                style={styles.btnSubmit}
                title="Proceed"
                onPress={() => this.proceedButtonAction()}
              />
            </View>
          </View>
        </ScrollView>

        <DropdownAlert
          inactiveStatusBarBackgroundColor={APP_THEME}
          ref={ref => this.dropdown = ref}
          onClose={data => this.onCloseDropdown(data)} />

      </Components.BACKGROUND_IMAGE>
    );
  }

  proceedButtonAction() {

    const walletbalance = parseFloat(this.state.walletInfo.walletBalance)
    const amount = this.refs.textInput._lastNativeText == undefined ? "0.0" : this.refs.textInput._lastNativeText
    const mbNumber = this.refs.mobilenumber.phoneNumber()

    if (walletbalance <= 0) {
      this.dropdown.alertWithType('error', "", WALLET_EMPTY_MESSAGE);
      return;
    }
    else if (walletbalance < parseFloat(amount)) {
      this.dropdown.alertWithType('error', "", LESS_WALLET_BALANCE_MESSAGE);
      return;
    }

    const isPhoneNumberValid = this.refs.mobilenumber.isValidPhoneNumber()    
    if (isPhoneNumberValid == false) {
      this.dropdown.alertWithType('error', "", WRONG_PHONE_NUMBER_MESSAGE);
      return;
    }

    let validator = new Helper.VALIDATOR(mbNumber)
    let errorMessage = validator.validateMobileNumber()
    if (errorMessage.length != 0) {
      this.dropdown.alertWithType('error', "", errorMessage);
      return;
    }
    
    validator = new Helper.VALIDATOR(amount)
    errorMessage = validator.validateAmount()
    if (errorMessage.length != 0) {
      this.dropdown.alertWithType('error', "", errorMessage);
      return;
    }

    let message = this.refs.messageTextInput._lastNativeText
    if (message == null || message == undefined) {
      message = ""
    }

    message = message.trim()
    Keyboard.dismiss()
    this.props.navigation.navigate("SendConfirmScreen", {
      topupAmount: amount,
      mobileNumber: mbNumber,
      message: message
    })
  }

  showServerErrorMessage() {
    setTimeout(() => {
      this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
    }, 500);
  }

  showNoInternetAlert() {
    this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
  }

  handleBackFromLogin = () => {
    return true;
  }

  onCloseDropdown() {}
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mobileNumberViewStyle: {
    height: 70,
    top: 20
  },
  mobileNumberTextStyle: {
    color: "white",
    marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
  },
  balanceContainerStyle: {
    flexDirection: 'column',
    top: TOP_BAR_HEIGHT,
    height: 100,
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  amountStyle: {
    color: '#202020',
    fontSize: moderateScale(23, 0.09),
    fontFamily: 'Ubuntu-Regular'
  },
  walletBalanceStyle: {
    color: '#505050',
    fontSize: moderateScale(17, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  currencyContainerStyle: {
    top: 5,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },
  currencyTextStyle: {
    color:"white",
    fontSize: moderateScale(15, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  verticalSeparatorStyle: {
    left: 5,
    height: "50%",
    width: 1,
    backgroundColor: 'gray'
  },
  textInputStyle: {
    flex: 1,
    left: 10,
    top: IS_IOS ? 0 : 0,
    color:"white",
    fontSize: moderateScale(15, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  underlineStyle: {
    height: 1,
    bottom: -3,
    width: "100%",
    backgroundColor: '#C0C0C0'
  },
  submitButtonBase: {
    top: 100
  }
})
