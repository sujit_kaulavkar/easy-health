
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import Events from 'react-native-simple-events';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { format } from 'date-fns'
import { Components, TOP_UP, Helper } from "../../Helper/FilePath";
import { moderateScale } from '../../Helper/Scaling';
import {
    TOP_BAR_HEIGHT,
    DrawerItems,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION
} from '../../Helper/Constants';
import DropdownAlert from 'react-native-dropdownalert';

export default class SendSuccessScreen extends Component {

    constructor(props) {
        super(props)

        const { transactionId } = props.navigation.state.params;

        this.state = {
            transferStatus: transactionId.length == 0 ? "Transfer Pending" : "Transfer Complete",
            title: transactionId.length == 0 ? "Pending" : "Success"
        }
    }

    componentDidMount() {
        const { transactionId } = this.props.navigation.state.params;
        if (transactionId.length == 0) {
            this.remoteConfirmTransfer()
        }
    }

    remoteConfirmTransfer() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { transfer_id } = this.props.navigation.state.params;

        apiHelper.confirmCreditTransfer(user.user_hash_id, transfer_id)
            .then((responseJson) => {
                if (responseJson) {
                    this.handleConfirmTransferSuccess(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.showServerErrorMessage()
            })
    }

    handleConfirmTransferSuccess(responseJson) {
        const status = responseJson.status
        if (status == 200) {
            this.setState({
                transferStatus: "Transfer Complete",
                title: "Success"
            })
        }
        else if (status == 403) {
            this.remoteConfirmTransfer()
        }
        else {
            const errorMessage = responseJson.errorMessage;
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    render() {
        const params = this.props.navigation.state.params;
        const amount = parseFloat(params.topupAmount);
        const transferFee = parseFloat(params.transferFee);
        const transactionId = params.transactionId;

        const date = new Date()
        const dateString = format(date, 'DD MMM YYYY HH:MM')

        const transferStatus = transactionId.length == 0 ? "Transfer Pending" : "Transfer Complete"
        const title = transactionId.length == 0 ? "Pending" : "Success"

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title={title}
                    isBackButtonNeeded={true}
                    isRightCrossNeeded={true}
                    backButtonAction={() => {
                        this.goToDashboard()
                    }}
                    crossButtonAction={() => {
                        this.goToDashboard()
                    }}
                />

                <ScrollView
                    style={{ flex: 1, top: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1, paddingBottom: TOP_BAR_HEIGHT }}>

                    <View style={styles.subContainer}>

                        <MaterialCommunityIcons
                            name="check-circle-outline"
                            color="white"
                            size={130}
                            style={{ alignSelf: 'center' }}
                        />

                        <Text style={styles.cardActivatedText}>{transferStatus}</Text>
                    </View>

                    <View style={styles.amountBaseStyle}>
                        <Text style={{
                            alignSelf: 'center',
                            top: 30,
                            flex: 1,
                            color: "white",
                            fontSize: moderateScale(17, 0.09),
                            fontFamily: 'OpenSans-Regular'
                        }}>
                            Transaction Receipt
                        </Text>

                        <View style={{
                            marginHorizontal: 40,
                            top: -60,
                            paddingTop: 20,
                            flex: 1,
                            paddingHorizontal: -10,
                        }}>

                            <TOP_UP.TOTAL_AMOUNT_VIEW
                                separatorLineColor="#D3D3D3"
                                topupAmount={amount}
                                transferFee={transferFee} />
                        </View>
                    </View>

                    <View style={styles.paymentReferenceViewStyle}>

                        {transactionId.length == 0 ? null :
                            <View style={styles.referenceViewStyle}>
                                <Text style={styles.referenceTitleStyle}>Transaction ID</Text>
                                <Text style={styles.referenceDetailStyle}>{transactionId}</Text>
                            </View>
                        }

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>TimeStamp</Text>
                            <Text style={styles.referenceDetailStyle}>{dateString}</Text>
                        </View>

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>Status</Text>
                            <Text style={styles.referenceDetailStyle}>{transferStatus}</Text>
                        </View>

                    </View>
                </ScrollView>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    goToDashboard() {
        Events.trigger('ChangeDrawerMenu', { id: DrawerItems.DASHBOARD })
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardActivatedText: {
        color: 'white',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'Ubuntu-Medium'
    },
    amountBaseStyle: {
        height: 270,
        top: 5,
        justifyContent: 'center'
    },
    paymentReferenceViewStyle: {
        top: -30,
        paddingVertical: 15,
        marginHorizontal: 40,
        paddingHorizontal: 20,
        backgroundColor: '#F0F0F0',
        justifyContent: 'space-between'
    },
    referenceViewStyle: {
        flex: 1,
        marginVertical: 5,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    referenceTitleStyle: {
        color: '#989898',
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    referenceDetailStyle: {
        color: '#808080',
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    }
})