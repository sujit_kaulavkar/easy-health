
import { StyleSheet } from 'react-native';
import { TOP_BAR_HEIGHT, SCREEN_WIDTH } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default StyleSheet.create({
    container: {
        flex: 1
    },
    scrollViewStyle: {
        flex: 1, marginTop: TOP_BAR_HEIGHT
    },
    recepientStyle: {
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: 'white',
        marginStart: 30,
        marginTop: 30
    },

    recipentContainer: {
        backgroundColor: '#EDEDED',
        borderWidth: 1,
        borderRadius: 1,
        borderColor: 'gray',
        borderStyle: 'dashed',
        marginHorizontal: 20,
        marginVertical: 10,
        padding: 20
    },
    mobileTextStyle: {
        color: '#606060',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
    },
    mobileValueStyle: {
        color: '#787878',
        fontSize: moderateScale(14.5, 0.09),
        fontFamily: 'OpenSans-Regular',
    },
    messageStyle: {
        color: '#606060',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
        marginTop: 20
    },
    submitButtonBase: {
        marginTop: 30,
        marginBottom: 20,
        height: 50,
    }
});