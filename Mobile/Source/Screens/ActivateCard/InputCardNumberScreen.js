import React, { Component } from 'react';
import {
    View,
    Text,
    Keyboard,
    StyleSheet
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';
import {
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    APP_THEME,
    NO_INTERNET_CONNECTION,
    ENTER_CARD_NUMBER_MESSAGE,
    OTP_OPTIONS,
    DARK_BLUE_COLOR,
    BLUE_COLOR
} from '../../Helper/Constants';

const CARD_TYPE_CODE = "asiatopmcpcard"

export default class InputCardNumberScreen extends Component {

    constructor() {
        super()
        this.state = {
            isShowSpinner: false
        }
        showOTPScreen = this.showOTPScreen.bind(this)
    }

    remoteActivateCardCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const params = this.props.navigation.state.params;
        const cardNumber = this.refs.cardInput.cardNumber()
        const vendorId = params.vendor.vendorId

        apiHelper.activateCard(user.user_hash_id, CARD_TYPE_CODE, cardNumber, vendorId)
            .then((responseJson) => {
                this.refs.submitButton.stopAnimation()
                if (responseJson) {
                    this.handleActivateCardResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.refs.submitButton.stopAnimation()
                this.showServerErrorMessage()
            })

    }

    handleActivateCardResponse(response) {
        const status = response["status"];
        if (status == 1) {
            this.showOTPScreen()
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE
                style={styles.container}
                isShowMMLogo={true}
                isTouchEnabled={this.state.isShowSpinner == false ? "auto" : "none"}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    isPortraitMode={true}
                    ref="topbarRef"
                    title='MY REWARDS'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                <View style={{ flex: 1, top: 100, marginHorizontal: 20 }}>

                    <Text style={{
                        color: BLUE_COLOR,
                        marginHorizontal: 40,
                        fontFamily: 'BarlowCondensed-Regular',
                        fontSize: moderateScale(16, 0.09)
                    }}>
                        ENTER 16-DIGIT CARD NUMBER
                    </Text>

                    <Components.CARD_NUMBER_INPUT
                        ref="cardInput"
                        validCardNumber={(isValidNumber) => this.validCardNumber(isValidNumber)}
                    />

                    <Text style={{
                        top: 50,
                        color:'white',
                        marginHorizontal: 40,
                        fontFamily: 'BarlowCondensed-Regular',
                        fontSize: moderateScale(15, 0.09)
                    }}>  
                        This card is issued by MatchMove Pay Pte Ltd pursuant to license by MasterCard Asia/Pacific Pte Ltd
                    </Text>

                    <View style={styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title="SUBMIT"
                            onPress={() => this.proceedButtonAction()}
                        />
                    </View>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        )
    }

    validCardNumber(isValidNumber) {
        if (isValidNumber) {
            this.refs.submitButton.enableSubmitButton()
        }
        else {
            this.refs.submitButton.disableSubmitButton()
        }
    }

    _onChange = (formData) => {};

    showSpinner() {
        if (this.state.isShowSpinner == false) {
            this.setState({
                isShowSpinner: true
            })
        }
    }

    hideSpinner() {
        if (this.state.isShowSpinner == true) {
            this.setState({
                isShowSpinner: false
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    proceedButtonAction() {

        const cardNumber = this.refs.cardInput.cardNumber()
        if (cardNumber.trim().length <= 0) {
            this.dropdown.alertWithType("error", "", ENTER_CARD_NUMBER_MESSAGE)
            return;
        }

        this.refs.submitButton.startAnimation()
        Keyboard.dismiss()
        this.remoteActivateCardCall()
    }

    showOTPScreen() {
        const cardNumber = this.refs.cardInput.cardNumber()
        const params = this.props.navigation.state.params;
        const vendor = params["vendor"]

        const user = Helper.USER_MANAGER.getInstance().getUser()
        this.props.navigation.navigate("OTPVerificationScreen", {
            otpNavigationScreen: OTP_OPTIONS.IS_FROM_ACTIVATE_CARD,
            mobileNumber: user.mobileNumber,
            cardNumber: cardNumber,
            vendorId: vendor.vendorId,
            cardTypeCode: CARD_TYPE_CODE
        })
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitButtonBase: {
        alignSelf: 'center',
        top: 100,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    input: {
        fontSize: 16,
        color: "black",
        fontFamily: 'OpenSans-Regular'
    }
})