import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Keyboard,
    FlatList,
    StyleSheet,
    BackHandler,
    TouchableOpacity,
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';

import {
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_THEME
} from '../../Helper/Constants';

import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default class VenderScreen extends Component {

    constructor() {
        super()

        this.state = {
            loading: false,
            vendors: [],
            selectedVendor: undefined
        }

        onSearchTextChange = this.onSearchTextChange.bind(this)
        onSearchEnd = this.onSearchEnd.bind(this)
        handleVendorResponse = this.handleVendorResponse.bind(this)
        didSelectItemFromCell = this.didSelectItemFromCell.bind(this)
    }

    componentDidMount() {
        this.refs.submitButton.disableSubmitButton()
        this.addObservers()
    }

    componentWillUnmount() {
        didBlurObserver.remove();
        didFocusObserver.remove();
    }

    addObservers() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {
                Events.trigger('unlockDrawer')
            }
        );

        // listener called when viewWillDisappear
        didBlurObserver = this.props.navigation.addListener(
            'didBlur',
            payload => {
                Events.trigger('lockDrawer')
            }
        );
    }

    remoteFetchVendor(text) {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        apiHelper.vendors(text).then((responseJson) => {
            if (responseJson) {
                this.handleVendorResponse(responseJson)
            }
            else {
                this.showServerErrorMessage()
            }
        })
            .catch((error) => {
                this.showServerErrorMessage()
            })        
    }

    handleVendorResponse(responseJson) {        
        const status = responseJson.status
        if (status == 200) {
            const data = responseJson.data
            let vendors = data.template
            this.setState({
                venders: vendors
            })
        }
        else {
            this.showServerErrorMessage()
        }
    }

    onSearchTextChange(text) {
        this.remoteFetchVendor(text)
    }

    onSearchEnd(text) {
        // this.remoteFetchVendor(text)
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='MY REWARDS'
                    isMenuButtonNeeded={true}
                    menuButtonAction={() => {
                        Keyboard.dismiss()
                        this.props.navigation.openDrawer()
                    }} />

                <View style={{ top: 90 }}>
                    <Components.USER_INPUT
                        ref="vendorInput"
                        returnKeyType={'done'}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        onTextChange={(text) => {
                            this.onSearchTextChange(text)
                        }}
                        onEditingEnd={(event) => {
                            this.onSearchEnd(event.nativeEvent.text)
                        }}
                    >SELECT MERCHANT</Components.USER_INPUT>
                </View>

                <View style={styles.flatListContainerStyle}>
                    <FlatList
                        style={styles.flatListStyle}
                        data={this.state.venders}
                        extraData={this.state}
                        key="vendorFlatList"
                        keyboardShouldPersistTaps="always"
                        keyExtractor={(item) => item.vendorId.toString()}
                        renderItem={({ item, index }) => this.renderCellItem(item, index)}
                    />
                </View>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title="NEXT"
                        onPress={() => this.proceedButtonAction()}
                    />
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </Components.BACKGROUND_IMAGE>
        )
    }

    renderCellItem(item) {
        const imageWidth = 80;
        const imageHeight = 80;

        const selectedVendor = this.state.selectedVendor
        let vendorId = -1
        if (selectedVendor != undefined) {
            vendorId = selectedVendor.vendorId
        }

        return (
            <View style={[styles.cellContainerStyle, { top: 6, width: SCREEN_WIDTH - 70, height: imageHeight + 20 }]}>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View>
                        <Image
                            source={{ uri: item.vendorImageUrl }}
                            style={{ left: 5, backgroundColor: 'gray', width: imageWidth - 10, height: imageHeight - 10 }}
                            onLoadStart={() => this.setState({ loading: true })}
                            onLoadEnd={() => {
                                this.setState({ loading: false })
                            }} />
                        {this.state.loading && <Components.LOADING_VIEW />}
                    </View>

                    <View style={{ flex: 1 }}>
                        <Text numberOfLines={2} style={styles.postTitleStyle}>
                            {item.vendorTitle}
                        </Text>
                    </View>

                    <View style={{ alignSelf: 'flex-end', justifyContent: 'center', flexDirection: 'row', height: 50, width: 50 }}>
                        <MaterialIcons
                            name={vendorId == item.vendorId ? "radio-button-checked" : "radio-button-unchecked"}
                            color="white"
                            size={25}
                        />
                    </View>
                </View>

                <View style={{ position: 'absolute', bottom: 1, height: 1, width: SCREEN_WIDTH, backgroundColor: '#C0C0C0' }} />
                <TouchableOpacity style={{ flex: 1, zIndex: 0, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} onPress={() => this.didSelectItemFromCell(item)} />
            </View>
        )
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    proceedButtonAction() {
        this.props.navigation.navigate("InputCardNumberScreen", {
            "vendor": this.state.selectedVendor
        })
    }

    didSelectItemFromCell = (item) => {
        this.setState({
            selectedVendor: item
        })

        this.refs.submitButton.enableSubmitButton()
        // this.props.navigation.navigate("InputCardNumberScreen", {
        //     vendor: item
        // })
    }

    handleBackFromLogin = () => {
        return true;
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    flatListContainerStyle: {
        flex: 1,
        marginTop: 150,
        marginBottom: 100
    },
    flatListStyle: {
        flex: 1,
        marginHorizontal: 35
    },
    cellContainerStyle: {
        left: 0,
        right: 0,
        justifyContent: 'center'
    },
    postTitleStyle: {
        left: 15,
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'BarlowCondensed-Regular',
        color: 'white',
        textAlignVertical: 'top',
    }
})