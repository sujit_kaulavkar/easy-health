
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import {
    Components
} from '../../Helper/FilePath';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Events from 'react-native-simple-events';
import { moderateScale } from '../../Helper/Scaling';
import { SCREEN_WIDTH, DrawerItems, TOP_BAR_HEIGHT, BLUE_COLOR } from '../../Helper/Constants';

export default class ActivateCardSuccessScreen extends Component {

    constructor() {
        super()
        goToDashboard = this.goToDashboard.bind(this)
    }

    render() {
        return (

            <Components.BACKGROUND_IMAGE style={styles.mainContainer}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Success'
                    isRightCrossNeeded={true}
                    crossButtonAction={() => {
                        this.goToDashboard()
                    }} />

                <View style={styles.subContainer}>

                    <MaterialCommunityIcons
                        name="check-circle-outline"
                        color= "white"
                        size={130}
                        style={{ alignSelf: 'center' }}
                    />

                    <Text style={styles.cardActivatedText}>Card Activated</Text>
                    <Text style={styles.cardVisitText}>Your new card is successfully activated {'\n'} Kindly visit home to start using it</Text>
                </View>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        title="Go to Dashboard"
                        onPress={() => this.goToDashboard()}
                    />
                </View>
            </Components.BACKGROUND_IMAGE>
        );
    }

    goToDashboard() {    
        Events.trigger('ChangeDrawerMenu', { id: DrawerItems.DASHBOARD })
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1       
    },
    subContainer: {        
        top:TOP_BAR_HEIGHT,
        flex:0.80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardActivatedText: {
        fontSize: 20,
        color: 'white',
        fontSize: moderateScale(17,0.09),
        fontFamily: "Ubuntu-Medium"
    },
    cardVisitText: {
        top:5,
        marginHorizontal: 40,
        textAlign: 'center',
        color:'white',
        fontSize: moderateScale(13.5, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: 40,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
});
