
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    StatusBar,
    ScrollView
} from 'react-native';
import Events from 'react-native-simple-events';
import {
    Components
} from '../../Helper/FilePath';

import {
    moderateScale
} from '../../Helper/Scaling';
import { SCREEN_WIDTH, TOP_BAR_HEIGHT } from '../../Helper/Constants';
import Octicons from 'react-native-vector-icons/Octicons';

export default class ActivateCardScreen extends Component {

    componentDidMount() {
        this.addObservers()
    }

    componentWillUnmount() {
        didBlurObserver.remove();
        didFocusObserver.remove();
    }

    addObservers() {
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {
                Events.trigger('unlockDrawer')
            }
        );

        // listener called when viewWillDisappear
        didBlurObserver = this.props.navigation.addListener(
            'didBlur',
            payload => {
                Events.trigger('lockDrawer')
            }
        );
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.mainContainer}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Activate Card'
                    isMenuButtonNeeded={true}
                    menuButtonAction={() => {
                        this.props.navigation.openDrawer()
                    }} />

                <ScrollView
                    style={{ flex: 1, top:TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1, paddingBottom: 10 }}>

                    <View style={styles.cardBaseView}>
                        <Octicons
                            name="credit-card"
                            color="gray"
                            size={150}
                            style={{ alignSelf: 'center' }} />
                    </View>

                    <Text style={styles.activateCardStyle}>Activate your card in three simple steps</Text>

                    <View style={styles.messageContainer}>
                        <Text style={styles.stepText}>Select Vendor</Text>
                        <Image
                            style={styles.arrowStyle}
                            source={require('./download-arrow.png')} />
                        <Text style={styles.stepText}>Enter Your Card Number</Text>
                        <Image
                            style={styles.arrowStyle}
                            source={require('./download-arrow.png')} />
                        <Text style={styles.stepText}>Enter OTP to Complete Activation</Text>
                    </View>

                    <View style={styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title="Proceed"
                            onPress={() => this.proceedButtonAction()}
                        />
                    </View>
                </ScrollView>
            </Components.BACKGROUND_IMAGE>
        );
    }

    proceedButtonAction() {
        this.props.navigation.navigate("VenderScreen")
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1       
    },
    messageContainer: {
        top: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    stepText: {
        fontSize: 20,
        fontWeight: '400',
        color: '#585858',
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    arrowStyle: {
        width: 21,
        height: 26,
        margin: 10
    },
    activateCardStyle: {
        alignSelf: 'center',
        top: 10,
        fontSize: 22,
        marginTop: 30,
        color: '#404040',
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    submitButtonBase: {
        position: 'absolute',
        bottom: TOP_BAR_HEIGHT,
        paddingBottom: 30,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
    },
    cardBaseView: {
        top: 20,
        height: 140,
        width: 220,
        alignSelf: 'center',
        borderRadius: 10
    }
});
