
import React, { Component } from 'react';
import {
  View,
  Text,
  Keyboard,
  TextInput,
  StyleSheet,
  BackHandler
} from 'react-native';
import Events from 'react-native-simple-events';
import { Components, Helper } from "../../Helper/FilePath";
import DropdownAlert from 'react-native-dropdownalert';
import {
  moderateScale
} from '../../Helper/Scaling';
import { SCREEN_WIDTH, APP_THEME } from '../../Helper/Constants';

export default class RequestAmountScreen extends Component {

  constructor() {
    super()
      generateCodeButtonAction = this.generateCodeButtonAction.bind(this)
  }
  
  componentDidMount() {
    this.addObservers()
  }

  componentWillUnmount() {
    didBlurObserver.remove();
    didFocusObserver.remove();
  }

  addObservers() {

    BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
    // listener called when viewWillAppear
    didFocusObserver = this.props.navigation.addListener(
      'didFocus',
      payload => {
        Events.trigger('unlockDrawer')
      }
    );

    // listener called when viewWillDisappear
    didBlurObserver = this.props.navigation.addListener(
      'didBlur',
      payload => {
        Events.trigger('lockDrawer')
      }
    );
  }

  render() {
    return (
      <Components.BACKGROUND_IMAGE style={styles.container}>
        <Components.TOP_BAR
          topbarTranslate={0}
          ref="topbarRef"
          title='Request'
          isMenuButtonNeeded={true}
          menuButtonAction={() => {
            Keyboard.dismiss()
            this.props.navigation.openDrawer()
          }}
        />

        <View style={styles.amountInputContaierStyle}>
          <Text style={{color:"white", fontFamily: 'OpenSans-Regular'}}>
            Enter Amount
          </Text>

          <View style={{ height: 60 }}>
            <View style={styles.currencyContainerStyle}>
              <Text style={styles.currencyTextStyle}>
                SGD
              </Text>
              <View style={styles.verticalSeparatorStyle} />
              <TextInput ref="textInput" style={styles.textInputStyle} />
            </View>
            <View style={styles.underlineStyle} />
          </View>
        </View>

        <View style={styles.submitButtonBase}>
          <Components.BUTTON_SUBMIT
            ref="submitButton"
            activeOpacity={0.7}
            title="Generate Code"
            onPress={() => this.generateCodeButtonAction()}
          />
        </View>

        <DropdownAlert
          inactiveStatusBarBackgroundColor={APP_THEME}
          ref={ref => this.dropdown = ref}
          onClose={data => this.onCloseDropdown(data)} />
      </Components.BACKGROUND_IMAGE>
    );
  }

    generateCodeButtonAction() {
    const value = this.refs.textInput._lastNativeText
    let validator = new Helper.VALIDATOR(value)
    let errorMessage = validator.validateAmount()
    if (errorMessage.length != 0) {
      this.dropdown.alertWithType('error', "", errorMessage);
      return;
    }
    
      Keyboard.dismiss()
      
      this.props.navigation.navigate("RequestQRScreen", {
      topupAmount: value
    })
  }

  handleBackFromLogin = () => {
    return true;
  }

  onCloseDropdown() { }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  submitButtonBase: {
    top:150,
    marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09)
  },
  amountInputContaierStyle: {
    marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09),
    flexDirection: 'column',
    top: 120,
    height: 80
  },
  currencyContainerStyle: {
    top: 5,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },
  currencyTextStyle: {
    color:"white",
    fontSize: moderateScale(15, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  verticalSeparatorStyle: {
    left: 5,
    height: "50%",
    width: 1,
    backgroundColor: 'gray'
  },
  textInputStyle: {
    flex: 1,
    left: 10,
    color:"white",
    fontSize: moderateScale(15, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  underlineStyle: {
    height: 1,
    bottom: -3,
    width: "100%",
    backgroundColor: '#C0C0C0'
  }
})