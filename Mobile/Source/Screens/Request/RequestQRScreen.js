
import React, { Component } from 'react';
import { Text, View, ScrollView, Alert } from 'react-native'
import Styles from "./request_style";
import QRCode from 'react-native-qrcode';
import LottieView from 'lottie-react-native';

import {
  Components,
  Helper
} from '../../Helper/FilePath';
import {
  TOP_BAR_HEIGHT,
  APP_NAME,
  CANCEL_TEXT,
  YES_TEXT
} from '../../Helper/Constants';
export default class RequestQRScreen extends Component {

  constructor(props) {
    super(props);

    const amount = this.props.navigation.state.params.topupAmount
    const user = Helper.USER_MANAGER.getInstance().getUser()
    this.state = {
      data: {
        currency: 'SGD',
        requestAmount: parseFloat(amount).toFixed(2),
        passcode: Math.floor(100000 + Math.random() * 900000),
        mobileNumber: user.mobileNumber
      }
    }

    timerStop = this.timerStop.bind(this)
  }

  componentDidMount() {

    Helper.FIREBASE_HELPER.getInstance().addObserver(this.state.data.passcode).then((result) => {
      this.handlePaySuccess(result)
    })
    Helper.FIREBASE_HELPER.getInstance().sendPayInfo(this.state.data.mobileNumber, this.state.data.passcode, this.state.data.requestAmount)
  }

  componentWillUnmount() {
    Helper.FIREBASE_HELPER.getInstance().removeObserver(this.state.data.passcode)
  }

  handlePaySuccess(result) {    
    if (result != undefined) {
      const status = result.payStatus;
      const amount = result.requestAmount;
      const mbNo = result.email;
      if (status == "Settle" && amount == this.state.data.requestAmount && mbNo == this.state.data.email) {
        this.props.navigation.navigate("PaySuccessScreen", {
          payResult: result
        })
      }
    }
  }

  getAmountView = () => {
    return (
      <View style={Styles.amountConytainerStyle}>
        <Text style={Styles.amountTextStyle}>{this.state.data.currency + ' ' + this.state.data.requestAmount}</Text>
      </View>
    )
  }

  getQRCodeImageview = () => {

    return (
      <View style={Styles.QrCodeContainerStyle}>
        <QRCode
          ref={'qr'}
          value={JSON.stringify(this.state.data)}
          size={150}
          bgColor='black'
          fgColor='white' />
        <Text style={Styles.QRCodeTextStyle}>Please show this QR code to receive your fund</Text>
      </View>
    )
  }

  getTimerComponent = () => {
    // Timer component
    return (
      <View style={{ flexDirection: 'row' }}>
        <Text style={{ color: 'white' }}>Expires in </Text>
        <Components.TIMER time={240} ref="timer" timerStop={() => this.timerStop()} />
      </View>
    )
  }

  render() {
    return (
      <Components.BACKGROUND_IMAGE 
        isShowMMLogo={true}
        style={Styles.mainContainerStyle}>      
        <Components.TOP_BAR
          topbarTranslate={0}
          ref="topbarRef"
          title='Request'
          isBackButtonNeeded={true}
          backButtonAction={() => {
            this.props.navigation.goBack()
          }}
        />

        <ScrollView
          style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
          keyboardShouldPersistTaps="always"
          contentContainerStyle={{ flexGrow: 1 }}>

          {this.getAmountView()}
          {this.getQRCodeImageview()}
          <View style={Styles.subContainerStyle}>
            <Text style={Styles.ORTextStyle}>OR</Text>
            <Text style={Styles.usePasscodeTextStyle}>Use passcode</Text>
            <Text style={Styles.passcodeTextStyle}>{this.state.data.passcode}</Text>
            {this.getTimerComponent()}
          </View>
        </ScrollView>

        {/* spinner Component */}
        <View style={{ height: 100 }}>
          <View style={{
            width: 100, height: 100,
            bottom: 20,
            alignSelf: 'center', alignItems: 'center', justifyContent: 'center'
          }}>
            <LottieView
              ref={lottie => this.lottie = lottie}
              style={{
                width: 50,
                height: 50,
              }}
              autoPlay={true}
              loop={true}
              source={require("../../Resources/Lottie/request_loader.json")}
            />
          </View>

          <Text style={Styles.spinnerTextStyle} >Waiting for payment...</Text>
        </View>
      </Components.BACKGROUND_IMAGE>
    );
  }

  timerStop() {
    Helper.FIREBASE_HELPER.getInstance().deletePayStatus(this.state.data.passcode)
    this.showCodeExpiryMessage()
  }

  showCodeExpiryMessage() {
    Alert.alert(
      APP_NAME,
      "Passcode is expired now. Do you want to create again?",
      [
        {
          text: CANCEL_TEXT, onPress: () => { this.props.navigation.goBack() }
        },
        {
          text: YES_TEXT, onPress: () => {
            this.createPasscode()
          }
        }
      ],
      { cancelable: false }
    )
  }

  createPasscode() {
    this.refs.timer.startTimer()
    const amount = this.props.navigation.state.params.topupAmount
    const user = Helper.USER_MANAGER.getInstance().getUser()
    this.setState({
      data: {
        currency: 'SGD',
        requestAmount: amount,
        passcode: Math.floor(100000 + Math.random() * 900000),
        mobileNumber: user.mobileNumber
      }
    })

    Helper.FIREBASE_HELPER.getInstance().sendPayInfo(this.state.data.mobileNumber, this.state.data.passcode, this.state.data.requestAmount)
  }
}
