import { StyleSheet} from 'react-native';


export default StyleSheet.create({
    mainContainerStyle: {
        flex:1
    },

    amountConytainerStyle  :{        
        margin:30,
        alignContent:'center',
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'center',
        width:250, 
        borderRadius: 4, 
        height: 80 
    },
    amountTextStyle:{
        color:'white',
        fontSize: 35,
        fontWeight:'bold' 
    },
    QrCodeContainerStyle : {
        alignItems: 'center', 
        justifyContent: 'center',
        marginTop:10
    },
    QRCodeTextStyle :{
        color : 'white',
        fontSize: 15,
        fontWeight:'300', 
        marginTop: 10 
    },
    subContainerStyle : {
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    ORTextStyle :{
         fontSize: 30,
         fontWeight:'500',
          color: 'white',
          marginTop:20
    },
    usePasscodeTextStyle :{
        color : 'white',
        fontSize: 15,
        marginBottom:10,
        fontWeight:'300'
    },
    passcodeTextStyle : {
        fontSize: 35,
        fontWeight:'900',
        color:'white'
    },
    spinnerTextStyle : {
        color : 'white',
        fontWeight:'300',
        bottom:0,
        position:'absolute',
        alignSelf:'center',
        fontSize:15,
        margin:20
    },
});