
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  Keyboard,
  BackHandler
} from 'react-native';

import {
  Components,
  Helper
} from '../../Helper/FilePath';

import {
  moderateScale
} from '../../Helper/Scaling';

import { format } from 'date-fns'
import Events from 'react-native-simple-events';
import DropdownAlert from 'react-native-dropdownalert';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {
  SCREEN_WIDTH,
  APP_THEME,
  SOMETHING_WRONG_MESSAGE,
  NO_INTERNET_CONNECTION,
  ENTER_PASSCODE_MESSAGE,
  WRONG_PASSCODE_MESSAGE
} from '../../Helper/Constants';

export default class PayScreen extends Component {

  constructor() {
    super()

    transactionDetails = ""

    this.state = {
      isShowSpinner: false
    }
    payButtonAction = this.payButtonAction.bind(this)
  }

  componentDidMount() {
    this.addObservers()
  }

  componentWillUnmount() {
    didBlurObserver.remove();
    didFocusObserver.remove();
  }

  addObservers() {

    BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
    // listener called when viewWillAppear
    didFocusObserver = this.props.navigation.addListener(
      'didFocus',
      payload => {
        Events.trigger('unlockDrawer')
      }
    );

    // listener called when viewWillDisappear
    didBlurObserver = this.props.navigation.addListener(
      'didBlur',
      payload => {
        Events.trigger('lockDrawer')
      }
    );
  }

  onScanSuccess(payload) {
    const data = JSON.parse(payload.data);

    if (data) {
      const result = data
      if (result.passcode != undefined) {
        transactionDetails = payload
        this.updateFirebaseDatabase(result)
      }
      else {
        this.dropdown.alertWithType("error", "", WRONG_PASSCODE_MESSAGE)
      }
    }
    else {
      this.dropdown.alertWithType("error", "", WRONG_PASSCODE_MESSAGE)
    }
  }

  remoteInitiatePayRequest(payload) {
    let apiHelper = Helper.API_HELPER.getInstance();
    if (apiHelper.isInternetConnected() == false) {
      this.showNoInternetAlert()
      return;
    }

    this.showSpinner()
    const user = Helper.USER_MANAGER.getInstance().getUser()
    const mobile = payload.mobileNumber
    const amount = payload.requestAmount

    apiHelper.initiateCreditTransfer(user.user_hash_id, mobile, amount)
      .then((responseJson) => {
        if (responseJson) {
          this.handleInitiateTransferSuccess(responseJson, payload)
        }
        else {
          this.hideSpinner()
          this.showServerErrorMessage()
        }
      })
      .catch((error) => {
        this.hideSpinner()
        this.showServerErrorMessage()
      })
  }

  handleInitiateTransferSuccess(responseJson, payload) {
    const status = responseJson.status
    if (status == 200) {
      this.hideSpinner()
      const transactionId = responseJson.data.id
      const date = new Date()
      const dateString = format(date, 'YYYY-DD-MM HH:MM:SS')

      this.showSuccessScreen(transactionId, transactionDetails.requestAmount, dateString)
    }
    else if (status == 400) {
      this.hideSpinner()
      this.dropdown.alertWithType("error", "", "Fund transfer to same user is not allowed")
    }
    else if (status == 403) { // ssumption is that transaction is in pending state 
      this.remoteConfirmTransfer(responseJson)
    }
    else {
      this.hideSpinner()
      this.showServerErrorMessage()
    }
  }

  remoteConfirmTransfer(request) {

    let apiHelper = Helper.API_HELPER.getInstance();
    if (apiHelper.isInternetConnected() == false) {
      this.showNoInternetAlert()
      return;
    }

    const user = Helper.USER_MANAGER.getInstance().getUser()
    apiHelper.confirmCreditTransfer(user.user_hash_id, request.data.transfer_id)
      .then((responseJson) => {
        if (responseJson) {
          this.handleConfirmTransferSuccess(responseJson, request)
        }
        else {
          this.hideSpinner()
          this.showServerErrorMessage()
        }
      })
      .catch((error) => {
        this.hideSpinner()
        this.showServerErrorMessage()
      })
  }

  handleConfirmTransferSuccess(responseJson, request) {
    const status = responseJson.status
    if (status == 200) {

      this.hideSpinner()
      const transactionId = responseJson.id
      const date = new Date()
      const dateString = format(date, 'YYYY-DD-MM HH:MM:SS')
      this.showSuccessScreen(transactionId, transactionDetails.requestAmount, dateString)
    }
    else if (status == 403) {
      this.remoteConfirmTransfer(request)
    }
    else {
      this.hideSpinner()
      const errorMessage = responseJson.errorMessage;
      if (errorMessage) {
        this.dropdown.alertWithType('error', "", errorMessage);
      }
      else {
        this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
      }
    }
  }


  render() {
    return (
      <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>

        <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />
        <Components.TOP_BAR
          topbarTranslate={0}
          ref="topbarRef"
          title='Pay'
          isMenuButtonNeeded={true}
          menuButtonAction={() => {
            Keyboard.dismiss()
            this.props.navigation.openDrawer()
          }}
        />

        <View style={styles.amountInputContaierStyle}>
          <Text style={{ color: "white", fontFamily: 'OpenSans-Regular' }}>
            Enter Passcode
          </Text>

          <View style={{ height: 100, justifyContent: 'space-between' }}>
            <View style={styles.currencyContainerStyle}>
              <TextInput
                placeholder="******"
                placeholderTextColor="white"
                ref="textInput"
                secureTextEntry={true}
                style={styles.textInputStyle} />
              <View style={styles.underlineStyle} />
            </View>

            <View style={styles.submitButtonBase}>
              <Components.BUTTON_SUBMIT
                ref="submitButton"
                activeOpacity={0.7}
                title="Pay"
                onPress={() => this.payButtonAction()}
              />
            </View>
          </View>
        </View>

        <View style={{ marginTop: 80, flex: 1 }}>
          <QRCodeScanner
            onRead={this.onScanSuccess.bind(this)}
            showMarker={true}
          />
        </View>

        <DropdownAlert
          inactiveStatusBarBackgroundColor={APP_THEME}
          ref={ref => this.dropdown = ref}
          onClose={data => this.onCloseDropdown(data)} />
      </Components.BACKGROUND_IMAGE>
    );
  }

  showServerErrorMessage() {
    setTimeout(() => {
      this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
    }, 500);
  }

  showNoInternetAlert() {
    this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
  }

  showSpinner() {
    if (this.state.isShowSpinner == false) {
      this.setState({
        isShowSpinner: true
      })
    }
  }

  hideSpinner() {
    if (this.state.isShowSpinner == true) {
      this.setState({
        isShowSpinner: false
      })
    }
  }

  payButtonAction() {
    Keyboard.dismiss()
    const value = this.refs.textInput._lastNativeText

    if (value == undefined || value.trim().length == 0) {
      this.dropdown.alertWithType("error", "", ENTER_PASSCODE_MESSAGE)
    }

    Helper.FIREBASE_HELPER.getInstance().isPasscodeExist(value).then((result) => {
      if (result != null) {
        transactionDetails = result
        this.updateFirebaseDatabase(result)
      }
      else {
        this.dropdown.alertWithType("error", "", WRONG_PASSCODE_MESSAGE)
      }
    })
  }

  updateFirebaseDatabase(payload) {
    this.remoteInitiatePayRequest(payload)
  }

  showSuccessScreen(transactionId, requestAmount, timeStamp) {
    Helper.FIREBASE_HELPER.getInstance().updatePayStatus(transactionDetails.passcode, transactionId, timeStamp)
    this.props.navigation.navigate("PaySuccessScreen", {
      payResult: {
        transactionId: transactionId,
        requestAmount: requestAmount,
        timeStamp: timeStamp
      }
    })
  }

  handleBackFromLogin = () => {
    return true;
  }

  onCloseDropdown() { }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  amountInputContaierStyle: {
    marginHorizontal: moderateScale(SCREEN_WIDTH * 0.1, 0.09),
    flexDirection: 'column',
    top: 100,
    height: 120
  },
  currencyContainerStyle: {
    top: 5,
    height: 40,
  },
  textInputStyle: {
    flex: 1,
    color: "white",
    fontSize: moderateScale(15, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  underlineStyle: {
    height: 1,
    bottom: -3,
    width: "100%",
    backgroundColor: '#C0C0C0'
  },
  submitButtonBase: {
    height: 40
  }
});