
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import Events from 'react-native-simple-events';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Components } from "../../Helper/FilePath";
import { moderateScale } from '../../Helper/Scaling';
import { TOP_BAR_HEIGHT, DrawerItems } from '../../Helper/Constants';

export default class PaySuccessScreen extends Component {

    render() {
        const payResult = this.props.navigation.state.params.payResult;
        const transactionId = payResult.transactionId;

        const dateString = payResult.timeStamp
        const paymentAmount = payResult.requestAmount

        return (
            <Components.BACKGROUND_IMAGE style={styles.container}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Success'
                    isBackButtonNeeded={true}
                    isRightCrossNeeded={true}
                    backButtonAction={() => {
                        this.goToDashboard()
                    }}
                    crossButtonAction={() => {
                        this.goToDashboard()
                    }}
                />

                <ScrollView
                    style={{ flex: 1, top: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1, paddingBottom: TOP_BAR_HEIGHT }}>

                    <View style={styles.subContainer}>

                        <MaterialCommunityIcons
                            name="check-circle-outline"
                            color= "white"
                            size={130}
                            style={{ alignSelf: 'center' }}
                        />

                        <Text style={styles.cardActivatedText}>Payment Successful</Text>

                        <Text style={styles.transactionReceiptTextStyle}>
                            Transaction Receipt
                        </Text>
                    </View>

                    <View style={styles.paymentSuccessViewStyle}>
                        <Text style={styles.textStyle}>
                            Payment Amount
                        </Text>

                        <Text style={styles.textStyle}>
                            {paymentAmount} SGD
                        </Text>
                    </View>

                    <View style={styles.paymentReferenceViewStyle}>

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>Transaction ID</Text>
                            <Text style={styles.referenceDetailStyle}>{transactionId}</Text>
                        </View>

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>TimeStamp</Text>
                            <Text style={styles.referenceDetailStyle}>{dateString}</Text>
                        </View>

                        <View style={styles.referenceViewStyle}>
                            <Text style={styles.referenceTitleStyle}>Status</Text>
                            <Text style={styles.referenceDetailStyle}>Success</Text>
                        </View>

                    </View>
                </ScrollView>
            </Components.BACKGROUND_IMAGE>
        );
    }

    goToDashboard() {
        Events.trigger('ChangeDrawerMenu', { id: DrawerItems.DASHBOARD })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardActivatedText: {
        color: 'white',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'Ubuntu-Medium'
    },
    amountBaseStyle: {
        height: 70,
        top: 70,
        marginHorizontal: moderateScale(25, 0.09),
        justifyContent: 'center',
        alignItems: "center"
    },
    paymentReferenceViewStyle: {
        top: 100,
        height: 160,
        marginHorizontal: 40,
        paddingHorizontal: 20,
        backgroundColor: '#F0F0F0',
        justifyContent: 'space-evenly'
    },
    referenceViewStyle: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    referenceTitleStyle: {
        color: '#484848',
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    referenceDetailStyle: {
        color: '#808080',
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    transactionReceiptTextStyle: {
        alignSelf: 'center',
        top: 70,
        flex: 1,
        color: 'white',
        fontSize: moderateScale(17, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    paymentSuccessViewStyle: {
        height: 55,
        top:50,
        flexDirection: 'row',
        marginHorizontal: moderateScale(40, 0.09),
        justifyContent: 'space-between',
        alignItems:'center',
        paddingHorizontal: 20,
        backgroundColor: '#F0F0F0',
    },
    textStyle: {
        fontSize: moderateScale(14, 0.09),
        color: '#484848',
        fontFamily: 'OpenSans-Regular'
    }
})