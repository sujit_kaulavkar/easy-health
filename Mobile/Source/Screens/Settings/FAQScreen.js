
import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import {
    Components
} from '../../Helper/FilePath';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { BLUE_COLOR, SCREEN_WIDTH, DARK_BLUE_COLOR, TOP_BAR_HEIGHT } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default class FAQScreen extends Component {

    constructor() {
        super()

        this.state = {
            faqs: ["FAQ1", "FAQ2", "FAQ3"]
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='FAQ'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={{flex: 1}}>
                    <FlatList
                        style={styles.flatListStyle}
                        data={this.state.faqs}
                        extraData={this.state}
                        key="FAQFlatList"
                        keyExtractor={(item) => item}
                        renderItem={({ item }) => this.renderCellItem(item)}
                    />
                </View>

            </Components.BACKGROUND_IMAGE>
        );
    }

    renderCellItem(item, index) {

        return (
            <View style={styles.cellStyle}>
                <View style={styles.cellSubcontainer}>

                    <View style={{
                        width: 7,
                        height: 60,
                        backgroundColor: BLUE_COLOR
                    }}/>

                    <Text style={styles.itemNameStyle}>{item}</Text>

                    <MaterialIcons
                        style={styles.rightArrowStyle}
                        name='keyboard-arrow-right'
                        color={DARK_BLUE_COLOR}
                        size={25}
                    />
                </View>

                <TouchableOpacity style={styles.rowSelectionStyle} onPress={() => this.didSelectItemFromCell(index)} />
            </View>
        )
    }

    didSelectItemFromCell(item) {
        this.props.navigation.navigate("FAQDetailScreen")
    }
}

const styles = StyleSheet.create({
    flatListStyle: {
        flex: 1,
        marginTop: TOP_BAR_HEIGHT + 20
    },
    cellStyle: {
        flexDirection: "row",
        height: 70,
    },
    cellSubcontainer: {
        height: 60,
        width: SCREEN_WIDTH,
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
        flexDirection: 'row'
    },
    itemNameStyle: {
        flex: 1,
        marginLeft: 15,
        top: -2,
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    rightArrowStyle: {
        marginRight: 10
    },
    rowSelectionStyle: {
        flex: 1,
        zIndex: 0,
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0
    }
})