
import React, { Component } from 'react';
import {
    View,
    StatusBar
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';
import AddressCell from './AddressCell';
import { TOP_BAR_HEIGHT } from '../../../Helper/Constants';

export const ADDRESS_TYPE = {
    RESIDENTIAL_ADDRESS: 0,
    BILLING_ADDRESS: 1
}

export default class MyAddressScreen extends Component {

    constructor() {
        super()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const residentialsAddress = user.address.residential
        const bilingAddress = user.address.billing
        
        this.state = {
            residentialsAddress: residentialsAddress,
            bilingAddress: bilingAddress
        }
    }

    componentDidMount() {
        this.addObservers()
    }

    componentWillUnmount() {
        didFocusObserver.remove();    
    }

    addObservers() {
        // listener called when viewWillAppear
        didFocusObserver = this.props.navigation.addListener(
            'didFocus',
            payload => {                
                this.showUserAddress()
            }
        );
    }

    showUserAddress() {
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const residentialsAddress = user.address.residential
        const bilingAddress = user.address.billing

        this.setState({
            residentialsAddress: residentialsAddress,
            bilingAddress: bilingAddress
        })
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='My Address'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={{ top: TOP_BAR_HEIGHT }}>
                    <AddressCell
                        address={this.state.residentialsAddress}
                        type={ADDRESS_TYPE.RESIDENTIAL_ADDRESS}
                        showDetailsScreen={(type) => this.showDetailsScreen(type)}
                        addNewAddress={(type) => this.addNewAddress(type)}
                    />

                    <AddressCell
                        address={this.state.bilingAddress}
                        type={ADDRESS_TYPE.BILLING_ADDRESS}
                        showDetailsScreen={(type) => this.showDetailsScreen(type)}
                        addNewAddress={(type) => this.addNewAddress(type)}
                    />
                </View>

            </Components.BACKGROUND_IMAGE>
        );
    }

    showDetailsScreen(type) {

        const { residentialsAddress, bilingAddress } = this.state
        const address = (type == ADDRESS_TYPE.RESIDENTIAL_ADDRESS ? residentialsAddress : bilingAddress)
        const { isFromVoucher, handleSelectedAddress} = this.props.navigation.state.params
        if (isFromVoucher == true) {
            if (handleSelectedAddress) {
                handleSelectedAddress(type, address)
            }        
            
            this.props.navigation.goBack()
            return;
        }
        
        this.props.navigation.navigate("AddAddressScreen", {
            address: address,
            type: type            
        })
    }

    addNewAddress(type) {
        this.props.navigation.navigate("AddAddressScreen", {
            type: type
        })
    }
}