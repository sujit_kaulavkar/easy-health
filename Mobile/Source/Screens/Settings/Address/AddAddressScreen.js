
import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    Keyboard,
    ScrollView,
    StatusBar,
    StyleSheet,
} from 'react-native';

import {
    Components,
    Helper,
} from '../../../Helper/FilePath';
import DropdownAlert from 'react-native-dropdownalert';
import { moderateScale } from '../../../Helper/Scaling';
import {
    SCREEN_WIDTH,
    TOP_BAR_HEIGHT,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    ADD_ADDRESS_SUCCESSFUL_MESSAGE,
    UPDATE_ADDRESS_SUCCESSFUL_MESSAGE,
    APP_NAME,
    YES_TEXT
} from '../../../Helper/Constants';

import { ADDRESS_TYPE } from './MyAddressScreen';

export default class AddAddressScreen extends Component {

    constructor() {
        super()

        this.state = {
            isTouchEnabled: true,
            isShowCountries: false
        }

        showCountrySelectionUI = this.showCountrySelectionUI.bind(this)
    }

    remoteChangeAddressCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showLoader()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const { type } = this.props.navigation.state.params
        const addressType = (type == ADDRESS_TYPE.RESIDENTIAL_ADDRESS) ? "residential" : "billing"
        const address1 = this.refs.addressLineOne.getText()
        const address2 = this.refs.addressLineTwo.getText()
        const city = this.refs.city.getText()
        const state = this.refs.state.getText()
        const countryCode = this.refs.country.getCountryCode()
        const country = this.refs.country.getCountry()
        const zipCode = this.refs.zipCode.getText()

        apiHelper.addUpdateAddress(user.user_hash_id, addressType, address1, address2, city, state, country, countryCode, zipCode)
            .then((responseJson) => {
                this.hideLoader()
                if (responseJson) {

                    this.updateAddress()
                    this.handleChangeAddressResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleChangeAddressResponse(response) {
        const status = response["status"];
        if (status == 1) {
            const { address } = this.props.navigation.state.params
            if (address != null) {
                this.showSuccessAlert(UPDATE_ADDRESS_SUCCESSFUL_MESSAGE)
            }
            else {
                this.showSuccessAlert(ADD_ADDRESS_SUCCESSFUL_MESSAGE)
            }
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    updateAddress() {
        const { type } = this.props.navigation.state.params
        var address = {}
        address["address_line_1"] = this.refs.addressLineOne.getText()
        address["address_line_2"] = this.refs.addressLineTwo.getText()
        address["city"] = this.refs.city.getText()
        address["state"] = this.refs.state.getText()
        address["country"] = this.refs.country.getCountry()
        address["country_code"] = this.refs.country.getCountryCode()
        address["zip_code"] = this.refs.zipCode.getText()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        if (type == ADDRESS_TYPE.RESIDENTIAL_ADDRESS) {
            user.address.residential = address
        }
        else {
            user.address.billing = address
        }

        Helper.USER_MANAGER.getInstance().setUser(user)
        Helper.DATABASE_HANDLER.getInstance().saveUserDetails(user)
    }

    showSuccessAlert(message) {
        Alert.alert(
            APP_NAME,
            message,
            [{
                text: YES_TEXT, onPress: () => this.props.navigation.goBack()
            }],
            { cancelable: false }
        )
    }

    render() {
        const { type, address } = this.props.navigation.state.params
        return (
            <Components.BACKGROUND_IMAGE
                isShowMMLogo={false}
                style={{ flex: 1 }}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='My Address'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <Text style={styles.messageStyle}>
                    {type == ADDRESS_TYPE.RESIDENTIAL_ADDRESS ? "Residential Address" : "Billing Address"}
                </Text>

                <ScrollView>
                    <View style={styles.subContainer}>

                        <Components.USER_INPUT
                            ref="addressLineOne"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            defaultText={address == undefined ? "" : address.address_line_1}
                            customStyles={{ containerStyle: { marginHorizontal: 10 } }}
                        >Address line 1</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="addressLineTwo"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            defaultText={address == undefined ? "" : address.address_line_2}
                            customStyles={{ containerStyle: { marginHorizontal: 10 } }}
                        >Address line 2</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="city"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            defaultText={address == undefined ? "" : address.city}
                            customStyles={{ containerStyle: { marginHorizontal: 10 } }}
                        >City</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="state"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            defaultText={address == undefined ? "" : address.state}
                            customStyles={{ containerStyle: { marginHorizontal: 10 } }}
                        >State</Components.USER_INPUT>

                        <Components.COUNTRY_SELECTION ref="country" />

                        <Components.USER_INPUT
                            ref="zipCode"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            defaultText={address == undefined ? "" : address.zip_code}
                            customStyles={{ containerStyle: { marginHorizontal: 10 } }}
                        >ZIP code</Components.USER_INPUT>

                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            style={styles.btnSubmit}
                            title="Save"
                            customStyles={{ containerStyle: { top: 10 } }}
                            onPress={() => this.saveButtonAction()}
                        />
                    </View>
                </ScrollView>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    showCountrySelectionUI() {
        this.setState({
            isShowCountries: true
        })
    }

    saveButtonAction() {
        this.validateAddress()
    }

    validateAddress() {

        const address1 = this.refs.addressLineOne.getText()
        const address2 = this.refs.addressLineTwo.getText()
        const city = this.refs.city.getText()
        const state = this.refs.state.getText()
        const countryCode = this.refs.country.getCountryCode()
        const zipCode = this.refs.zipCode.getText()

        let validator = new Helper.VALIDATOR(address1)
        let errorMessage = validator.validateAddressLine1()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(address2)
        errorMessage = validator.validateAddressLine2()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(city)
        errorMessage = validator.validateCity()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(state)
        errorMessage = validator.validateState()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(countryCode)
        errorMessage = validator.validateCountry()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(zipCode)
        errorMessage = validator.validateZIPCode()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        Keyboard.dismiss()
        this.remoteChangeAddressCall()
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("error", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    showLoader() {
        if (this.state.isTouchEnabled == true) {
            this.refs.submitButton.startAnimation()
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideLoader() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    subContainer: {
        marginTop: 20,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.05, 0.09),
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: 'white',
        height: 480
    },
    messageStyle: {
        color: 'white',
        marginTop: TOP_BAR_HEIGHT + 20,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.05, 0.09),
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-SemiBold'
    },
    btnSubmit: {
        paddingTop: 20
    }
})