

import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {
    Helper,
    Components
} from '../../../Helper/FilePath';
import { SCREEN_WIDTH } from '../../../Helper/Constants';
import { moderateScale } from '../../../Helper/Scaling';
import { ADDRESS_TYPE } from './MyAddressScreen';

export default class AddressCell extends Component {
    render() {

        const { address } = this.props
        return (
            <View>
                {address ? this.showAddressDetails() : this.showAddAddressUI()}
            </View>
        );
    }

    showAddress() {

        const { address } = this.props
        return address.address_line_1 + ' ' +
            address.address_line_2 + ' ' +
            address.city + ' ' +
            address.state + ' ' +
            address.country + ' ' +
            address.zip_code + ' '
    }

    showAddressDetails() {

        const { type } = this.props
        return (
            <View style={styles.addressViewStyle}>
                <Text style={styles.titleStyle}>
                    {type == ADDRESS_TYPE.RESIDENTIAL_ADDRESS ? "Residential Address" : "Billing Address"}
                </Text>

                <TouchableOpacity
                    activeOpacity={1}
                    style={styles.addressDetailsContainer}
                    onPress={() => this.showDetailsScreen(type)}>

                    <View style={styles.addressDetailsStyle}>
                        <Text style={styles.addressTextStyle}>
                            {this.showAddress()}
                        </Text>
                    </View>

                    <MaterialIcons
                        style={styles.editIconStyle}
                        name='edit'
                        color="white"
                        size={20}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    showAddAddressUI() {
        const { type } = this.props
        return (
            <View style={styles.addAddressStyle}>
                <TouchableOpacity
                    style={styles.addAddressBtnStyle}
                    activeOpacity={1}
                    onPress={() => this.addNewAddress(type)}>

                    <Text style={styles.addTextStyle}>
                        {type == ADDRESS_TYPE.RESIDENTIAL_ADDRESS ? "Add residential address" : "Add billing address"}
                    </Text>

                    <SimpleLineIcons
                        style={styles.plusIconStyle}
                        name='plus'
                        color="white"
                        size={20}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    showDetailsScreen(type) {
        this.props.showDetailsScreen(type)
    }

    addNewAddress(type) {        
        this.props.addNewAddress(type)
    }
}

const styles = StyleSheet.create({

    addressViewStyle: {
        marginTop: 20,
        //height: 100,
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.05, 0.09)
    },
    addressDetailsContainer: {
        borderWidth: 1,
        top: 10,
        flexDirection: 'row',
        borderStyle: 'dashed',
        borderColor: 'white'
        //height: 70,
    },
    addressDetailsStyle: {
        flex: 1,
        marginRight: 10
    },
    addTextStyle: {
        color: 'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
        left: 20,
        top: 11,
        alignSelf: 'flex-start'
    },
    addAddressStyle: {
        marginHorizontal: moderateScale(SCREEN_WIDTH * 0.05, 0.09),
        marginTop: 30,
        //height: 50,        
        //backgroundColor: 'red'
    },
    addAddressBtnStyle: {
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: 'white'
    },
    editIconStyle: {
        right: 10,
        alignSelf: 'center'
    },
    plusIconStyle: {
        right: 20,
        bottom: 9,
        alignSelf: 'flex-end'
    },
    titleStyle: {
        color: "white",
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    addressTextStyle: {
        color: "white",
        margin: 5,
        fontSize: moderateScale(14, 0.09),
        fontFamily: 'OpenSans-Regular'
    }
})
