import React, { Component } from 'react';
import {
    View,
    Text,
} from 'react-native';

import { Components } from "../../../Helper/FilePath";
import { moderateScale } from '../../../Helper/Scaling';
import { Helper } from "../../../Helper/FilePath";

const FEMALE = "female"
const MALE = "male"

export default class GenderView extends Component {

    constructor(props) {
        super(props)

        const user = Helper.USER_MANAGER.getInstance().getUser()
        let gender = MALE
        if (user != undefined && user.gender != undefined) {
            gender = user.gender
        }

        this.state = {
            isFemale: gender == FEMALE ? true : false
        }

        radioButtonAction = this.radioButtonAction.bind(this)
    }

    render() {
        return (
            <View style={{ height: 60 }}>
                <Text style={{
                    color: "rgba(30, 34, 170, 0.7)",
                    fontSize: moderateScale(18, 0.09),
                    fontFamily: 'BarlowCondensed-Medium',
                }}>Gender</Text>
                <View style={{top: 7, justifyContent: 'space-between', flexDirection: 'row', width: "60%" }}>
                    <Components.RADIO_BUTTON
                        title={MALE.capitalize()}
                        tag={1}
                        isSelected={!this.state.isFemale}
                        radioButtonAction={(gender) => this.radioButtonAction(gender)} />

                    <Components.RADIO_BUTTON
                        title={FEMALE.capitalize()}
                        tag={2}
                        isSelected={this.state.isFemale}
                        radioButtonAction={(gender) => this.radioButtonAction(gender)} />

                </View>
            </View>
        )
    }

    radioButtonAction(gender) {        
        this.setState({
            isFemale: gender.toLowerCase() == FEMALE ? true : false
        })
    }

    gender() {
        return this.state.isFemale ? FEMALE : MALE
    }
}
