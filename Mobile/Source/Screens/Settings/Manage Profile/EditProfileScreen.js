import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    Alert,
    Keyboard
} from 'react-native';

import {
    Components,
    Helper
} from '../../../Helper/FilePath';

import {
    SCREEN_WIDTH,
    TOP_BAR_HEIGHT,
    NO_INTERNET_CONNECTION,
    SOMETHING_WRONG_MESSAGE,
    APP_NAME,
    CHANGE_PROFILE_SUCCESSFUL,
    OK_TEXT,
    APP_THEME
} from '../../../Helper/Constants';

import ProfilePicView from './ProfilePicView';
import DOBView from './DOBView';
import UserForm from './UserForm';
import GenderView from './GenderView';

import DropdownAlert from 'react-native-dropdownalert';
import Events from 'react-native-simple-events';

export default class EditProfileScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isMale: true,
            imageObject: null,
            isTouchEnabled: true
        }
    }

    remoteEditProfileCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showLoader()
        this.refs.submitButton.startAnimation()

        const imageSting = this.refs.profilePic.getImageString()
        const title = this.refs.userForm.title()
        const firstName = this.refs.userForm.firstName()
        const lastName = this.refs.userForm.lastName()
        const gender = this.refs.gender.gender()
        const dob = this.refs.dob.dob()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        const userObject = {}
        userObject["title"] = title
        userObject["first_name"] = firstName
        userObject["last_name"] = lastName
        userObject["gender"] = gender
        userObject["birthday"] = dob

        const params = JSON.stringify(userObject)
        apiHelper.updateUser(user.user_hash_id, params, imageSting)
            .then((responseJson) => {
                this.hideLoader()
                if (responseJson) {
                    this.handleEditProfileResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleEditProfileResponse(response) {
        const status = response.status;
        if (status == 200) {

            const user = Helper.USER_MANAGER.getInstance().getUser()
            user.firstName = this.refs.userForm.firstName()
            user.lastName = this.refs.userForm.lastName()
            user.title = this.refs.userForm.title()
            user.gender = this.refs.gender.gender()
            user.birthday = this.refs.dob.dob()

            const data = response.data.data
            if (data) {
                const imageUrl = data.profile_pic_url
                if (imageUrl && imageUrl != null) {
                    user.profile_pic_url = imageUrl
                }
            }

            Helper.USER_MANAGER.getInstance().setUser(user)
            Helper.DATABASE_HANDLER.getInstance().saveUserDetails(user)

            Events.trigger("updateUserData")
            Alert.alert(APP_NAME, CHANGE_PROFILE_SUCCESSFUL, [
                { text: OK_TEXT, onPress: () => this.props.navigation.popToTop() }
            ], { cancelable: false })
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>

                <ScrollView
                    style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ flexGrow: 1 }}>

                    <View>
                        <ProfilePicView ref="profilePic" />
                    </View>

                    <View style={{ height: 180 }}>
                        <UserForm ref="userForm" />
                    </View>

                    <View style={{ marginTop: 70, marginHorizontal: SCREEN_WIDTH * 0.09 }}>
                        <GenderView ref="gender" />
                    </View>

                    <View style={{ marginTop: 20 }}>
                        <DOBView ref="dob" />
                    </View>

                    <View style={styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            style={styles.btnSubmit}
                            title="Edit Now"
                            onPress={() => this.ediProfileButtonAction()}
                        />
                    </View>
                </ScrollView>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Manage Profile'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        )
    }

    ediProfileButtonAction() {
        const firstName = this.refs.userForm.firstName()
        const lastName = this.refs.userForm.lastName()

        let validator = new Helper.VALIDATOR(firstName)
        let errorMessage = validator.validateFirstName()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(lastName)
        errorMessage = validator.validateLastName()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        Keyboard.dismiss()
        this.remoteEditProfileCall()
    }

    showLoader() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideLoader() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    profilePicViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 130
    },
    profilePicStyle: {
        height: 80,
        width: 80,
        borderRadius: 80,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'rgba(255,255,255,0.5)',
    },
    submitButtonBase: {
        flex: 0.5,
        marginTop: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    separatorView: {
        height: 1.5,
        marginBottom: 10,
        backgroundColor: '#C8C8C8'
    }
})