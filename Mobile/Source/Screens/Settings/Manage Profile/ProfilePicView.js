

import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Components, Helper } from '../../../Helper/FilePath';
import { DARK_BLUE_COLOR } from '../../../Helper/Constants';
import { moderateScale } from '../../../Helper/Scaling';

export default class ProfilePicView extends Component {

    constructor(props) {
        super(props)

        imageData = undefined
        const user = Helper.USER_MANAGER.getInstance().getUser()

        let url = null
        if (user != undefined && user.profile_pic_url != undefined) {
            url = Helper.API_HELPER.getInstance().baseUrl() +  user.profile_pic_url
        }

        this.state = {
            imageUrl: url
        }
    }

    render() {
        const { isEditable } = this.props
        return (
            <View>
                <View style={styles.profilePicViewStyle}>
                    <TouchableOpacity
                        disabled={!isEditable}
                        activeOpacity={1}
                        onPress={() => this.profilePicButtonAction()}
                        style={styles.profilePicStyle}>
                        {this.state.imageUrl ?
                            <Image
                                defaultSource={require('../../Drawer/placeholder_image.png')}
                                style={{ height: "100%", width: "100%", flex: 1, borderRadius: 40 }}
                                source={{ uri: this.state.imageUrl }}
                            />
                            :
                            <Image
                                source={require("../../../Resources/Images/user.png")}
                            />
                        }
                    </TouchableOpacity>
                    {isEditable ?
                        <View style={{
                            right: -25,
                            top: -16,
                            backgroundColor: DARK_BLUE_COLOR,
                            width: 25,
                            height: 25,
                            borderRadius: 12.5,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <FontAwesome
                                name="edit"
                                size={14}
                                color="white" />
                        </View> : null}
                </View>

                <Text style={{
                    marginTop:10,
                    color: DARK_BLUE_COLOR,
                    alignSelf:'center',
                    fontSize: moderateScale(17, 0.09),
                    fontFamily: 'BarlowCondensed-Regular'}}>
                    KYC Status : {user.kyc_status}
                </Text>
            </View>
        );
    }

    profilePicButtonAction() {
        const imagePicker = new Components.IMAGE_PICKER()
        imagePicker.showImagePicker().then((imageData) => {
            this.setState({
                imageUrl: imageData.uri
            })

            this.imageData = imageData.data
        })
            .catch(() => {

            })
    }

    getImageString() {
        return this.imageData
    }
}

ProfilePicView.defaultProps = {
    isEditable: true
}

const styles = StyleSheet.create({
    profilePicViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
        top: 10
    },
    profilePicStyle: {
        height: 80,
        width: 80,
        borderRadius: 80,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'rgba(255,255,255,0.5)',
    }
})
