import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
} from 'react-native';

import DatePicker from 'react-native-datepicker'

import {
    SCREEN_WIDTH
} from '../../../Helper/Constants';
import { moderateScale } from '../../../Helper/Scaling';
import { Helper } from '../../../Helper/FilePath';
import { format } from 'date-fns';

export default class DOBView extends Component {

    constructor(props) {
        super(props)

        const user = Helper.USER_MANAGER.getInstance().getUser()
        this.state = {
            date: user.birthday == undefined ? null : user.birthday
        }
    }

    render() {        
        return (
            <View>
                <Text style={styles.title}>Date of Birth</Text>
                <View style={{ height: 40, justifyContent: 'center', flexDirection: 'row' }}>
                
                    <DatePicker
                        style={{ width: SCREEN_WIDTH - SCREEN_WIDTH * 0.09, borderColor: 'transparent' }}
                        date={this.state.date}
                        mode="date"
                        placeholder="select date"
                        format="YYYY-MM-DD"
                        maxDate={format(Date(), 'YYYY-MM-DD')}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                right: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                left: 15,
                                borderColor: 'transparent',
                                alignItems: 'flex-start',
                            },
                            placeholderText: {
                                top: 5,
                                color: "rgba(255,255,255,0.7)",                                
                            },
                            dateText: {
                                top: 5,
                                color: "white",                                
                            },
                            dateIcon: {
                                right:20
                            }
                        }}
                        onDateChange={(date) => {this.setState({date:date})}}
                    />                    
                </View>

                <View style={styles.separatorView} />
            </View>
        );
    }

    dob() {
        return this.state.date
    }
}

const styles = StyleSheet.create({
    title: {
        color: "white",
        top:5,
        left: moderateScale(SCREEN_WIDTH * 0.09, 0.09),
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    separatorView: {
        height: 1.5, 
        alignSelf:'center',       
        width: SCREEN_WIDTH - SCREEN_WIDTH * 0.17,
        backgroundColor: '#C8C8C8'
    }
})