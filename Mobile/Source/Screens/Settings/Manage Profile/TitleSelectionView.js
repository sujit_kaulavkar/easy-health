
import React from 'react';
import { StyleSheet, View } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

import { SCREEN_WIDTH } from '../../../Helper/Constants';
import { Helper } from "../../../Helper/FilePath";
import { moderateScale } from '../../../Helper/Scaling';

export default class TitleSelectionView extends React.Component {
    constructor(props) {
        super(props);

        const user = Helper.USER_MANAGER.getInstance().getUser()

        let title = "Mr"
        if (user != undefined && user.title != undefined ) {
            title = user.title
        }

        this.state = {
            favTitle: title,
            items: [
                {
                    label: 'Mr',
                    value: 'Mr',
                },
                {
                    label: 'Mrs',
                    value: 'Mrs',
                },
                {
                    label: 'Miss',
                    value: 'Miss',
                },
                {
                    label: 'Dr',
                    value: 'Dr',
                },
                {
                    label: 'Madam',
                    value: 'Madam',
                }
            ]
        };
    }

    render() {

        return (
            <View style={styles.container}>

                 <RNPickerSelect
                    placeholder={{
                        label: 'Select title',                        
                        value: null,
                    }}
                    placeholderTextColor='rgba(255, 255, 255, 0.7)'
                    hideIcon={true}
                    items={this.state.items}
                    onValueChange={(value) => {
                        this.setState({
                            favTitle: value,
                        });
                    }}
                    style={{ ...pickerSelectStyles }}
                    value={this.state.favTitle}
                />

                <View style={{top:35, right: SCREEN_WIDTH * 0.12, position:'absolute'}}>
                    <SimpleLineIcons
                        name="arrow-down"
                        size={17}
                        color='white'
                    />
                </View>

                <View style={styles.separatorView} />
            </View>
        );
    }

    getTitle() {
        return this.state.favTitle
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center'
    },
    separatorView: {
        height: 1.5,
        alignSelf: 'center',
        marginBottom: 10,
        width: SCREEN_WIDTH - SCREEN_WIDTH * 0.17,        
        backgroundColor: 'rgba(255,255,255,0.7)'
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {        
        paddingTop: 15,
        marginHorizontal: SCREEN_WIDTH * 0.07,
        paddingBottom: 12,
        color: 'rgba(30, 34, 170, 1.0)',
        // fontSize: moderateScale(17, 0.09),
        // fontFamily: 'BarlowCondensed-Medium'     
    },
    inputAndroid: {
        top: 5,
        marginHorizontal: SCREEN_WIDTH * 0.07,
        paddingBottom: 0,
        color: 'rgba(30, 34, 170, 1.0)',
        // fontSize: moderateScale(17, 0.09),
        // fontFamily: 'BarlowCondensed-Medium'        
    },
    underline: {
        borderTopColor: 'transparent',
    }    
});
