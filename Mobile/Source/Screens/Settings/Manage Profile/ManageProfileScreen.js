import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    StatusBar,
} from 'react-native';

import {
    Helper,
    Components
} from '../../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT,
} from '../../../Helper/Constants';

import ProfilePicView from './ProfilePicView';

export default class ManageProfileScreen extends Component {

    constructor(props) {
        super(props)
        ediProfileButtonAction = this.ediProfileButtonAction.bind(this)
    }

    render() {

        const user = Helper.USER_MANAGER.getInstance().getUser()
        console.log("user", user);        
        return (
            <Components.BACKGROUND_IMAGE style={styles.container}
                isTouchEnabled="auto">
                
                <ScrollView
                    style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{flexGrow: 1 }}>

                    <View>
                        <ProfilePicView ref="profilePic" isEditable={false}/>
                    </View>

                    <View style={{ height: 400 }} pointerEvents="none">

                        <Components.USER_INPUT
                            ref="title"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={true}
                            defaultText={user.title}
                        >Title</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="firstname"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={true}
                            defaultText={user.firstName}
                        >First Name</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="lastname"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            defaultText={user.lastName}
                        >Last Name</Components.USER_INPUT >

                        <Components.USER_INPUT
                            ref="gender"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={true}
                            defaultText={user.gender}
                        >Gender</Components.USER_INPUT>

                        <Components.USER_INPUT
                            ref="dob"
                            returnKeyType={'done'}
                            autoCapitalize={'none'}
                            autoCorrect={true}
                            defaultText={user.birthday}
                        >Date Of Birth</Components.USER_INPUT>
                    </View>

                    <View style={styles.submitButtonBase}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            style={styles.btnSubmit}
                            title="Edit Profile"
                            onPress={() => this.ediProfileButtonAction()}
                        />
                    </View>
                </ScrollView>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Manage Profile'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />
            </Components.BACKGROUND_IMAGE>
        )
    }

    ediProfileButtonAction() {
        this.props.navigation.navigate("EditProfileScreen")
    }
} 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    profilePicViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 130
    },
    profilePicStyle: {
        height: 80,
        width: 80,
        borderRadius: 80,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'rgba(255,255,255,0.5)',
    },
    submitButtonBase: {
        flex: 0.5,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    separatorView: {
        height: 1.5,
        marginBottom: 10,
        backgroundColor: '#C8C8C8'
    }
})