import React, { Component } from 'react';
import {
    View,    
} from 'react-native';

import { Helper, Components } from '../../../Helper/FilePath';

import TitleSelectionView from './TitleSelectionView';

export default class UserForm extends Component {

    constructor(props) {
        super(props)

        const user = Helper.USER_MANAGER.getInstance().getUser()        
        userFirstName = ""
        userLastName = ""                
        if (user != undefined) {
            userFirstName = user.firstName
            userLastName = user.lastName
        }
    }

    render() {
        return (
            <View style={{ height: 250}}>

                <TitleSelectionView ref="title"/>

                <Components.USER_INPUT
                    ref="firstname"
                    placeholder="First Name"
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={true}
                    defaultText={userFirstName}
                >First Name</Components.USER_INPUT>

                <Components.USER_INPUT
                    ref="lastname"
                    placeholder="Last Name"
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    defaultText={userLastName}
            >Last Name</Components.USER_INPUT >
                </View>
        )
    }

    title() {
        return this.refs.title.getTitle()
    }

    firstName() {
        return this.refs.firstname.getText()
    }

    lastName() {
        return this.refs.lastname.getText()
    }
}