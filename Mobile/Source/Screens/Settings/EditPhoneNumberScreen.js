import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    Keyboard
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    APP_THEME,
    APP_NAME,
    OK_TEXT,
    OTP_OPTIONS,
    OTP_SEND_MESSAGE,
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
} from '../../Helper/Constants.js';

import { moderateScale } from "../../Helper/Scaling";

export default class EditPhoneNumberScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isTouchEnabled: true,
        }
    }

    submitButtonAction() {

        const isPhoneNumberValid = this.refs.mobilenumber.isValidPhoneNumber()
        const mbNumber = this.refs.mobilenumber.phoneNumber()
        const countryCode = this.refs.mobilenumber.countryCode()

        if (isPhoneNumberValid == false) {
            this.dropdown.alertWithType('error', "", "Phone number is not valid");
            return;
        }

        let validator = new Helper.VALIDATOR(mbNumber)
        let errorMessage = validator.validateMobileNumber()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        Keyboard.dismiss()
        this.remoteEditMobileNumberCall(mbNumber, countryCode);
    }

    removeCountryCode(mobileNumber, countryCode) {
        return mobileNumber.replace("+" + countryCode, "")
    }

    remoteEditMobileNumberCall(mobileNumber, countryCode) {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        const onlyMbNo = this.removeCountryCode(mobileNumber, countryCode)
        this.showLoader()
        this.refs.submitButton.startAnimation()
        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.editMobileNumber(user.user_hash_id, countryCode, mobileNumber)
            .then((responseJson) => {
                this.hideLoader()
                if (responseJson) {
                    this.handleEditMobileNumberResponse(mobileNumber, responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleEditMobileNumberResponse(mobileNumber, response) {
        const status = response["status"];
        if (status == true) {
            this.showMobileNumberVerificationScreen(mobileNumber)
        }
        else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    showMobileNumberVerificationScreen = (mobilenumber) => {

        setTimeout(() => {
            Alert.alert(APP_NAME, OTP_SEND_MESSAGE, [
                {
                    text: OK_TEXT, onPress: () => this.props.navigation.navigate('OTPVerificationScreen', {
                        otpNavigationScreen: OTP_OPTIONS.IS_FROM_EDIT_PHONE_NUMBER,
                        mobileNumber: mobilenumber
                    })
                }
            ], { cancelable: false })
        }, 500);
    }

    render() {

        return (
            <Components.BACKGROUND_IMAGE
                style={styles.container}
                isShowMMLogo={false}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    isPortraitMode={true}
                    ref="topbarRef"
                    title='Change Phone Number'
                    isBackButtonNeeded={true}
                    backButtonAction={() => this.props.navigation.goBack()}
                />

                <View style={styles.subContainer}>
                    <Components.MOBILE_NUMBER ref="mobilenumber" />
                    <Text style={styles.messageStyle}>Verification OTP will be send to this entered Mobile Number</Text>

                    <View style={styles.submitButtonBaseStyle}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title="Submit"
                            onPress={() => this.submitButtonAction()}
                        />
                    </View>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    showLoader() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideLoader() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    subContainer: {
        flex: 1,
        top: TOP_BAR_HEIGHT + 10,
        flexDirection: 'column',
        height: 70
    },
    messageStyle: {
        top: 50,
        fontSize: moderateScale(11, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: 'white',
        marginHorizontal: SCREEN_WIDTH * 0.09,
    },
    submitButtonBaseStyle: {
        top: 120,
        alignSelf: 'center'
    }
});