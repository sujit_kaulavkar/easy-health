import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import Octicons from 'react-native-vector-icons/Octicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { moderateScale } from '../../Helper/Scaling';

import { FEEDBACK_OPTIONS } from './HelpFeedbackScreen';

export default class SupportUI extends Component {

    render() {
        return (
            <View style={styles.container}>

                <TouchableOpacity
                    style={{ marginTop: 20 }}
                    activeOpacity={1}
                    onPress={() => this.props.feedbackSelectionOption(FEEDBACK_OPTIONS.EMAIL)}>
                    <View style={styles.subContainer}>
                        <Octicons
                            style={styles.iconStyle}
                            name="mail"
                            color="white"
                            size={50}
                        />
                    </View>

                    <Text style={styles.titleStyle}>
                        Email
                        </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{ marginTop: 30 }}
                    activeOpacity={1}
                    onPress={() => this.props.feedbackSelectionOption(FEEDBACK_OPTIONS.TELEPHONE)}>
                    <View style={styles.subContainer}>
                        <SimpleLineIcons
                            style={styles.iconStyle}
                            name="phone"
                            color="white"
                            size={50}
                        />
                    </View>
                    <Text style={styles.titleStyle}>
                        Telephone
                        </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{ marginTop: 30 }}
                    activeOpacity={1}
                    onPress={() => this.props.feedbackSelectionOption(FEEDBACK_OPTIONS.FAQ)}>
                    <View style={styles.subContainer}>
                        <MaterialCommunityIcons
                            style={styles.iconStyle}
                            name="help"
                            color="white"
                            size={50}
                        />
                    </View>
                    <Text style={styles.titleStyle}>
                        FAQ
                        </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    subContainer: {
        height: 90,
        width: 90,
        borderRadius: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        borderWidth: 1
    },
    iconStyle: {

    },
    titleStyle: {
        marginTop: 5,
        color: 'white',
        textAlign: 'center',
        fontSize: moderateScale(13, 0.09),
        fontFamily: 'OpenSans-Regular'
    }
})