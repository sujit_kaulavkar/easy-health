export const settingOPtions = [
    {
        "icon": "account-edit",     //MaterialCommunityIcons
        "name": "MANAGE PROFILE",
    },
    {
        "icon": "map-outline",              
        "name": "MY ADDRESS",
    },
    {
        "icon": "square-edit-outline",      
        "name": "EDIT EMAIL ADDRESS",
    },
    {
        "icon": "phone-settings", 
        "name": "EDIT PHONE NUMBER",
    },
    {
        "icon": "lock-outline",         
        "name": "CHANGE PASSWORD",
    },
    {
        "icon": "help-circle-outline",
        "name": "SUPPORT",
    }
]

export const SETTINGS_TAGS = {
    MANAGE_PROFILE: 0,
    MY_ADDRESS: 1,
    EDIT_EMAIL_ADDRESS: 2,
    EDIT_PHONE_NUMBER: 3,
    CHANGE_PASSWORD: 4,
    HELP_FEEDBACK: 5
}