

import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Linking
} from 'react-native';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {
    Components
} from '../../Helper/FilePath';
import {
    TOP_BAR_HEIGHT,    
} from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';
import SupportUI from './SupportUI';

export const FEEDBACK_OPTIONS = {
    EMAIL: 0,
    TELEPHONE: 1,
    FAQ: 2
}

export default class HelpFeedbackScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starCount: 3.5
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>

                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Support'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }} />

                <ScrollView
                    style={{ flex: 1, marginTop: TOP_BAR_HEIGHT }}
                    keyboardShouldPersistTaps="always"
                    contentContainerStyle={{ paddingTop: 20, flexGrow: 1 }}>

                    <FontAwesome5
                        name="hands-helping"
                        color="white"
                        size={70}
                        style={{ alignSelf: 'center' }}
                    />

                    <View style={{ marginTop: 20 }}>

                        <Text style={{
                            alignSelf:'center',
                            color: 'white',
                            marginTop: 15,
                            fontSize: moderateScale(20, 0.09),
                            fontFamily: 'OpenSans-SemiBold'
                        }}>
                            Help and Feedback
                        </Text>

                        <Text style={{
                            textAlign:'center',
                            color: 'white',
                            marginTop: 15,
                            fontSize: moderateScale(13, 0.09),
                            fontFamily: 'OpenSans-Regular'
                        }}>
                            Require assistance or would like to leave a feedback? {'\n'}
                            Feel free to get in touch with us
                        </Text>
                    </View>

                    <View style={{
                        marginTop:30,
                        marginHorizontal: 20,                        
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>

                        <SupportUI feedbackSelectionOption={(option) => this.feedbackSelectionOption(option)}/>
                    </View>

                </ScrollView>

            </Components.BACKGROUND_IMAGE>
        );
    }

    feedbackSelectionOption(option) {
        switch (option) {
            case FEEDBACK_OPTIONS.EMAIL :{
                Linking.openURL("mailto:asiatop.support@asiatop.sg")
            }
            break;
            
            case FEEDBACK_OPTIONS.TELEPHONE: {
                Linking.openURL("tel:${+65 8733 7774}")
            }
            break;

            case FEEDBACK_OPTIONS.FAQ: {
                //this.props.navigation.navigate("FAQScreen")
            }
        }
    }
}