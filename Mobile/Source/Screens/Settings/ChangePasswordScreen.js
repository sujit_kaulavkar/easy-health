

import React, { Component } from 'react';
import {
    View,
    Alert,
    Keyboard,
    StatusBar,
    StyleSheet
} from 'react-native';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    TOP_BAR_HEIGHT,
    APP_THEME,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
    APP_NAME,
    OK_TEXT,
    CHANGE_PASSWORD_SUCCESSFUL
} from '../../Helper/Constants';

import DropdownAlert from 'react-native-dropdownalert';

export default class ChangePasswordScreen extends Component {

    constructor() {
        super()

        this.state = {
            isTouchEnabled: true
        }
    }

    remoteChangePasswordCall() {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.refs.submitButton.startAnimation()
        const oldpassword = this.refs.oldPassword.getText()
        const newPassword = this.refs.newPassword.getText()
        const user = Helper.USER_MANAGER.getInstance().getUser()

        apiHelper.changePassword(user.mobileNumber, oldpassword, newPassword)
            .then((responseJson) => {
                this.stopLoading()
                if (responseJson) {
                    this.handleChangepasswordResponse(responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                console.log("error", error);
                this.stopLoading()
                this.showServerErrorMessage()
            })
    }

    handleChangepasswordResponse(responseJson) {
        const status = responseJson.status
        if (status == 1) {
            Alert.alert(APP_NAME, CHANGE_PASSWORD_SUCCESSFUL, [
                { text: OK_TEXT, onPress: () => this.props.navigation.goBack() }
            ], { cancelable: false })
        }
        else {
            const errorMessage = responseJson.errorMessage
            if (errorMessage != undefined) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.props.showServerErrorMessage()
            }
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>


                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='Change Password'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={styles.userInput}>
                    <Components.USER_INPUT
                        ref="oldPassword"
                        secureTextEntry={true}
                        returnKeyType={'done'}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        rightImage={true}
                    >Enter Old Password</Components.USER_INPUT>

                    <Components.USER_INPUT
                        ref="newPassword"
                        secureTextEntry={true}
                        returnKeyType={'done'}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        rightImage={true}
                    >Enter New Password</Components.USER_INPUT>

                    <Components.USER_INPUT
                        ref="confirmPassword"
                        secureTextEntry={true}
                        returnKeyType={'done'}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        rightImage={true}
                    >Enter Confirm Password</Components.USER_INPUT>
                </View>

                <View style={styles.submitButtonBase}>
                    <Components.BUTTON_SUBMIT
                        ref="submitButton"
                        activeOpacity={0.7}
                        style={styles.btnSubmit}
                        title="Change"
                        onPress={() => this.submitButtonAction()}
                    />
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    submitButtonAction() {
        const oldPassword = this.refs.oldPassword.getText()
        const confirmPassword = this.refs.confirmPassword.getText()
        const newPassword = this.refs.newPassword.getText()

        let validator = new Helper.VALIDATOR(oldPassword)
        let errorMessage = validator.validatePassword()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(newPassword)
        errorMessage = validator.validateNewPassword()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        validator = new Helper.VALIDATOR(confirmPassword)
        errorMessage = validator.validateConfirmPassword(newPassword)
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        this.startLoading()
        Keyboard.dismiss()
        this.remoteChangePasswordCall()
    }

    startLoading() {

        this.setState({
            isTouchEnabled: false
        })
        this.refs.submitButton.startAnimation()
    }

    stopLoading() {

        this.setState({
            isTouchEnabled: true
        })

        this.refs.submitButton.stopAnimation()
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}


const styles = StyleSheet.create({
    userInput: {
        top: TOP_BAR_HEIGHT + 20,
        height: 190
    },
    submitButtonBase: {
        top: 150
    }
})