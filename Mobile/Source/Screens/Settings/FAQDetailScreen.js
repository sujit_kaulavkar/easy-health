
import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import {
    Components
} from '../../Helper/FilePath';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { BLUE_COLOR, SCREEN_WIDTH, DARK_BLUE_COLOR, TOP_BAR_HEIGHT } from '../../Helper/Constants';
import { moderateScale } from '../../Helper/Scaling';

export default class FAQDetailScreen extends Component {

    constructor() {
        super()

        this.state = {
            selectedIndex: -1,
            faqs: [{
                "Who is AsiaTop Loyalty?": "We offer an app for shoppers to reach rewards more quickly, by combining loyalty points earned from various brands. You can convert points into spending money, transfer them to another AsiaTop user and even withdraw them as cash."
            }, {
                "Are you regulated by any consumer or financial body?": "We have an ACRA registration just like any other business in Singapore and we are regulated by the Monetary Authority of Singapore (MAS)."
            }]
        }
    }

    render() {
        return (
            <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    ref="topbarRef"
                    title='FAQ'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={{ flex: 1 }}>
                    <FlatList
                        style={styles.flatListStyle}
                        data={this.state.faqs}
                        extraData={this.state}
                        key="FAQFlatList"
                        keyExtractor={(item) => item}
                        renderItem={({ item, index }) => this.renderCellItem(item, index)}
                    />
                </View>

            </Components.BACKGROUND_IMAGE>
        );
    }

    renderCellItem(item, index) {
        return (

            <View style={styles.cellStyle}>

                <View style={{ marginVertical: 5 }}>

                    <View style={styles.cellSubcontainer}>

                        <Text style={styles.itemNameStyle}>{Object.keys(item)[0]}</Text>
                        <SimpleLineIcons
                            style={styles.rightArrowStyle}
                            name={this.state.selectedIndex == index ? 'arrow-down' : 'arrow-up'}
                            color={DARK_BLUE_COLOR}
                            size={20}
                        />
                    </View>

                    {this.state.selectedIndex == index ?
                        <View style={styles.cellSubcontainer}>
                            <Text style={styles.itemNameStyle}>{Object.values(item)[0]}</Text>
                        </View> : null}
                </View>

                <TouchableOpacity style={styles.rowSelectionStyle} onPress={() => this.didSelectItemFromCell(index)} />
            </View>
        )
    }

    didSelectItemFromCell(index) {
        
        if (this.state.selectedIndex == index) {
            this.setState({
                selectedIndex: -1
            })
        }
        else {
            this.setState({
                selectedIndex: index
            })
        }
    }
}

const styles = StyleSheet.create({
    flatListStyle: {
        flex: 1,
        marginTop: TOP_BAR_HEIGHT + 20
    },
    cellStyle: {
        backgroundColor: '#DCDCDC',
        marginVertical: 5,
        marginHorizontal: 5
    },
    cellSubcontainer: {
        minHeight: 50,
        marginVertical: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    itemNameStyle: {
        flex: 1,
        marginLeft: 15,
        top: -2,
        fontSize: moderateScale(15, 0.09),
        fontFamily: 'OpenSans-Regular'
    },
    rightArrowStyle: {
        marginRight: 15
    },
    rowSelectionStyle: {
        flex: 1,
        zIndex: 0,
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0
    }
})