
import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  TouchableOpacity,
  StyleSheet,
  BackHandler
} from 'react-native';

import Events from 'react-native-simple-events';
import { Components } from "../../Helper/FilePath";
import { settingOPtions, SETTINGS_TAGS } from '../Settings/Static/SettingOptions';
import { TOP_BAR_HEIGHT, SCREEN_WIDTH, DARK_BLUE_COLOR, BLUE_COLOR } from '../../Helper/Constants';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { moderateScale } from '../../Helper/Scaling';

export default class SettingsScreen extends Component {

  constructor() {
    super()
    openModule = this.openModule.bind(this)
  }

  componentDidMount() {
    this.addObservers()
  }

  componentWillUnmount() {
    didBlurObserver.remove();
    didFocusObserver.remove();
  }

  addObservers() {

    BackHandler.addEventListener('hardwareBackPress', this.handleBackFromLogin);
    // listener called when viewWillAppear
    didFocusObserver = this.props.navigation.addListener(
      'didFocus',
      payload => {
        Events.trigger('unlockDrawer')
      }
    );

    // listener called when viewWillDisappear
    didBlurObserver = this.props.navigation.addListener(
      'didBlur',
      payload => {
        Events.trigger('lockDrawer')
      }
    );
  }

  render() {
    return (
      <Components.BACKGROUND_IMAGE style={{ flex: 1 }}>
        

        <Components.TOP_BAR
          topbarTranslate={0}
          ref="topbarRef"
          title='SETTINGS'
          isBellNeeded={true}
          bellButtonAction={() => {
            this.showNotificationScreen()
          }}
          isMenuButtonNeeded={true}
          menuButtonAction={() => {
            this.props.navigation.openDrawer()
          }}
        />

        {this.showChooseOptionLabel()}
        {this.showSettingOptions()}
      </Components.BACKGROUND_IMAGE>
    );
  }

  showChooseOptionLabel() {
    return (
      <Text style={styles.chooseOptionStyle}>
      CHOOSE AN OPTION
      </Text>
    )
  }

  showSettingOptions() {
    return (
      <View style={{ flex: 1}}>
        <FlatList
          style={styles.flatListStyle}
          data={settingOPtions}
          keyExtractor={(item, index) => item.name}
          renderItem={({ item, index }) => this.renderCellItem(item, index)}
        />
      </View>
    )
  }

  renderCellItem(item, index) {

    return (
      <View style={styles.cellStyle}>
        <View style={styles.cellSubcontainer}>

          <View style={{
            width: 7,
            height: 60,
            backgroundColor: BLUE_COLOR
          }} />
          <MaterialCommunityIcons
            style={styles.iconStyle}
            name={item["icon"]}
            color={DARK_BLUE_COLOR}
            size={30}
          />
          <Text style={styles.itemNameStyle}>{item.name}</Text>

          <MaterialIcons
            style={styles.rightArrowStyle}
            name='keyboard-arrow-right'
            color={DARK_BLUE_COLOR}
            size={25}
          />
        </View>

      </View>
    )
  }

  didSelectItemFromCell(index) {
    switch (index) {
      case SETTINGS_TAGS.MANAGE_PROFILE:
        this.openModule("ManageProfileScreen")
        break;

      case SETTINGS_TAGS.MY_ADDRESS:
        this.openModule("MyAddressScreen")
        break;

      case SETTINGS_TAGS.EDIT_EMAIL_ADDRESS:
        this.openModule("EditEmailAddressScreen")
        break;

      case SETTINGS_TAGS.EDIT_PHONE_NUMBER:
        this.openModule("EditPhoneNumberScreen")
        break;

      case SETTINGS_TAGS.CHANGE_PASSWORD:
        this.openModule("ChangePasswordScreen")
        break;

      case SETTINGS_TAGS.HELP_FEEDBACK:
        this.openModule("HelpFeedbackScreen")
        break;

      default:
        break;
    }    
  }

  openModule = (route) => {
    if (route == null) return;
    this.props.navigation.navigate(route);
  }

  showNotificationScreen() {
    this.props.navigation.navigate("NotificationScreen")
  }

  handleBackFromLogin = () => {
    return true;
  }
}

const styles = StyleSheet.create({
  flatListStyle: {
    flex: 1,
    paddingTop: 10
  },
  cellStyle: {
    flexDirection: "row",
    height: 70,
  },
  cellSubcontainer: {
    height: 60,
    width: SCREEN_WIDTH,
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
    flexDirection: 'row'
  },
  iconStyle: {
    marginLeft: 15
  },
  itemNameStyle: {
    flex: 1,
    marginLeft: 15,
    top: -2,
    fontSize: moderateScale(15, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  rightArrowStyle: {
    marginRight: 10
  },
  chooseOptionStyle: {
    left:15,    
    height:30,
    color: DARK_BLUE_COLOR,
    marginTop: TOP_BAR_HEIGHT + 15,    
    fontSize: moderateScale(15, 0.09),
    fontFamily: 'OpenSans-Regular'
  },
  rowSelectionStyle: { 
    flex: 1, 
    zIndex: 0, 
    position: 'absolute', 
    top: 0, 
    bottom: 0, 
    right: 0, 
    left: 0 
  }
})