import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    Keyboard
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';

import {
    Components,
    Helper
} from '../../Helper/FilePath';

import {
    APP_THEME,
    APP_NAME,
    OK_TEXT,
    OTP_OPTIONS,
    OTP_SEND_MESSAGE,
    TOP_BAR_HEIGHT,
    SCREEN_WIDTH,
    SOMETHING_WRONG_MESSAGE,
    NO_INTERNET_CONNECTION,
} from '../../Helper/Constants.js';

import { moderateScale } from "../../Helper/Scaling";

export default class EditEmailAddressScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isTouchEnabled: true,
        }
    }

    submitButtonAction() {
        const emailAddress = this.refs.emailAddress.getText()
        let validator = new Helper.VALIDATOR(emailAddress)
        let errorMessage = validator.validateEmailAddress()
        if (errorMessage.length != 0) {
            this.dropdown.alertWithType('error', "", errorMessage);
            return;
        }

        Keyboard.dismiss()
        this.remoteEditEmailAddressCall(emailAddress);
    }

    remoteEditEmailAddressCall(emailAddress) {

        let apiHelper = Helper.API_HELPER.getInstance();
        if (apiHelper.isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.showLoader()
        this.refs.submitButton.startAnimation()

        const user = Helper.USER_MANAGER.getInstance().getUser()
        apiHelper.editEmailAddress(user.user_hash_id, emailAddress)
            .then((responseJson) => {
                this.hideLoader()
                if (responseJson) {
                    this.handleEditEmailAddressResponse(emailAddress, responseJson)
                }
                else {
                    this.showServerErrorMessage()
                }
            })
            .catch((error) => {
                this.hideLoader()
                this.showServerErrorMessage()
            })
    }

    handleEditEmailAddressResponse(emailAddress, response) {
        const status = response["status"];
        if (status == 1) {
            this.showMobileNumberVerificationScreen(emailAddress)
        }
        else if (status == 400) {
            this.dropdown.alertWithType('error', "", "Email address already exist");
        } else {
            const errorMessage = response["errorMessage"];
            if (errorMessage) {
                this.dropdown.alertWithType('error', "", errorMessage);
            }
            else {
                this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
            }
        }
    }

    showMobileNumberVerificationScreen = (emailAddress) => {

        const user = Helper.USER_MANAGER.getInstance().getUser()

        setTimeout(() => {
            Alert.alert(APP_NAME, OTP_SEND_MESSAGE, [
                {
                    text: OK_TEXT, onPress: () => this.props.navigation.navigate('OTPVerificationScreen', {
                        otpNavigationScreen: OTP_OPTIONS.IS_FROM_EDIT_EMAIL,
                        emailAddress: emailAddress,
                        mobileNumber: user.mobileNumber
                    })
                }
            ], { cancelable: false })
        }, 500);
    }

    render() {

        return (
            <Components.BACKGROUND_IMAGE
                style={styles.container}
                isShowMMLogo={false}
                isTouchEnabled={this.state.isTouchEnabled ? "auto" : "none"}>
                <Components.TOP_BAR
                    topbarTranslate={0}
                    isPortraitMode={true}
                    ref="topbarRef"
                    title='Edit Email Address'
                    isBackButtonNeeded={true}
                    backButtonAction={() => this.props.navigation.goBack()}
                />

                <View style={styles.subContainer}>

                    <Components.USER_INPUT
                        ref="emailAddress"
                        autoCapitalize={'none'}
                        returnKeyType={'done'}
                        autoCorrect={true}
                    >Enter new Email Address</Components.USER_INPUT>

                    <Text style={styles.messageStyle}>Verification OTP will be send to this entered Email ID</Text>

                    <View style={styles.submitButtonBaseStyle}>
                        <Components.BUTTON_SUBMIT
                            ref="submitButton"
                            activeOpacity={0.7}
                            title="Submit"
                            onPress={() => this.submitButtonAction()}
                        />
                    </View>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />

            </Components.BACKGROUND_IMAGE>
        );
    }

    showLoader() {
        if (this.state.isTouchEnabled == true) {
            this.setState({
                isTouchEnabled: false
            })
        }
    }

    hideLoader() {
        if (this.state.isTouchEnabled == false) {
            this.refs.submitButton.stopAnimation()
            this.setState({
                isTouchEnabled: true
            })
        }
    }

    showServerErrorMessage() {
        setTimeout(() => {
            this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
        }, 500);
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    subContainer: {
        flex: 1,
        top: TOP_BAR_HEIGHT + 10,
        flexDirection: 'column',
        height: 80
    },
    messageStyle: {
        top: 70,
        fontSize: moderateScale(11, 0.09),
        fontFamily: 'OpenSans-Regular',
        color: 'white',
        marginHorizontal: SCREEN_WIDTH * 0.09,
    },
    submitButtonBaseStyle: {
        top: 120,
        alignSelf: 'center'
    }
});