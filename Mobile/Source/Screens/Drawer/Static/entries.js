export const DrawerItemsArray = [
    {
        "icon": "view-dashboard-outline",
        "title": "APPOINTMENTS"
    },
    {
        "icon": "plus-circle-outline",
        "title": "BOOK APPOINTMENT"
    },
    {
        "icon": "settings",
        "title": "SETTINGS"
    },
    {
        "icon": "logout",
        "title": "LOGOUT"
    }    
]