
import React, { Component } from 'react';
import {
    Animated,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    View,
    Platform,
    TouchableOpacity,
    Alert
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';
import { DrawerActions, NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Events from 'react-native-simple-events';

const HEADER_MAX_HEIGHT = 200;

import {
    Helper
} from '../../Helper/FilePath';

import {
    DrawerItems,
    CANCEL_TEXT,
    YES_TEXT,
    LOGOUT_MESSAGE,
    APP_NAME,
    APP_THEME,
    IS_IOS,
    NO_INTERNET_CONNECTION,
    SOMETHING_WRONG_MESSAGE,
} from '../../Helper/Constants';

import {
    DrawerItemsArray
} from './Static/entries';
import { moderateScale } from '../../Helper/Scaling';

class Drawer extends Component {

    constructor(props) {
        super(props)
        const user = Helper.USER_MANAGER.getInstance().getUser()

        this.state = {
            selectedIndex: 0,
            isShowSpinner: false,
            title:  user.name,
            profilePicUrl: "",//user.profile_pic_url,
            scrollY: new Animated.Value(
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0
            )
        };

        openModule = this.openModule.bind(this)
        changeDrawerMenu = this.changeDrawerMenu.bind(this)
        rowButtonAction = this.rowButtonAction.bind(this)
        updateUserData = this.updateUserData.bind(this)
        deleteUserData = this.deleteUserData.bind(this)
        logoutUserFromApp = this.logoutUserFromApp.bind(this)
        showLogoutErrorMessage = this.showLogoutErrorMessage.bind(this)
    }

    componentDidMount() {
        Events.on('ChangeDrawerMenu', 'ChangeDrawerMenu', this.changeDrawerMenu)
        Events.on('updateUserData', 'updateUserData', this.updateUserData)
        Events.on('logoutUserFromApp', 'logoutUserFromApp', this.logoutUserFromApp)
    }

    componentWillUnmount() {
        Events.rm('ChangeDrawerMenu', 'ChangeDrawerMenu')
        Events.rm('updateUserData', 'updateUserData')
        Events.rm('logoutUserFromApp', 'logoutUserFromApp')
    }

    rowItems() {
        return DrawerItemsArray
    }

    render() {        
        
        return (
            <View style={styles.fill}>
                <Helper.SPINNER visible={this.state.isShowSpinner} animation="fade" color={APP_THEME} />

                <ScrollView
                    style={[{ marginTop: IS_IOS ? HEADER_MAX_HEIGHT : 0 }]}
                    scrollEventThrottle={1}>
                    {this._renderScrollViewContent()}
                </ScrollView>

                <View pointerEvents="none" style={styles.header}>
                    {this.state.profilePicUrl ?
                        <Image
                            defaultSource={require('./placeholder_image.png')}
                            style={[styles.backgroundImage, { borderRadius: HEADER_MAX_HEIGHT / 5 }]}
                            source={{ uri: Helper.API_HELPER.getInstance().baseUrl() + this.state.profilePicUrl }}
                        />
                        :
                        <Image
                            style={styles.backgroundImage}
                            source={require('./placeholder_image.png')}
                        />
                    }
                </View>
                <View style={styles.bar}>
                    <Text style={styles.title} numberOfLines={2}>{this.state.title}</Text>
                </View>

                <DropdownAlert
                    inactiveStatusBarBackgroundColor={APP_THEME}
                    ref={ref => this.dropdown = ref}
                    onClose={data => this.onCloseDropdown(data)} />
            </View>
        );
    }

    _renderScrollViewContent() {
        const data = this.rowItems();
        return (
            <View style={styles.scrollViewContent}>
                {data.map((item, i) => (
                    <View key={i} style={styles.row}>
                        <View style={styles.separatorStyle} />
                        <TouchableOpacity
                            key={i}
                            activeOpacity={0.9}
                            style={styles.rowItemStyle}
                            onPress={() => this.rowButtonAction(i)}>
                            <MaterialCommunityIcons
                                style={styles.iconStyle}
                                name={item.icon}
                                color={this.state.selectedIndex == i ? "#6bb1ce" : "white"}
                                size={25}
                            />
                            <Text style={[styles.titleStyle, { color: this.state.selectedIndex == i ? "#6bb1ce" : "white" }]}>{item.title}</Text>
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
        )
    }

    changeDrawerMenu = (request) => {
        this.switchToModule(request["id"])
    }

    updateUserData = () => {

        const user = Helper.USER_MANAGER.getInstance().getUser()
        this.setState({
            profilePicUrl: user.profile_pic_url,
            title: user.firstName + ' ' + user.lastName
        })
    }

    rowButtonAction(i) {

        if (i == undefined || i == null) return;

        this.props.navigation.dispatch(DrawerActions.closeDrawer())
        if ((this.state.selectedIndex == i) && (this.state.selectedIndex != DrawerItems.LOGOUT)) {
            return;
        }
        
        this.switchToModule(i)
    }

    switchToModule(i) {

        this.setState({
            selectedIndex: i
        })

        switch (i) {
            case DrawerItems.DASHBOARD:
                this.openModule("appoinmentStack")
                break;

            case DrawerItems.APPOINMENT:
                this.openModule("AppoimentStackNavigation")
                break;

            case DrawerItems.SETTINGS:
                this.openModule("SettingsStackNavigation")
                break;

            case DrawerItems.LOGOUT:
                this.logout()
                break;

            default:
                break;
        }
    }

    openModule = (route) => {

        if (route == null) return;
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
    }

    logout() {
        Alert.alert(
            APP_NAME,
            LOGOUT_MESSAGE,
            [
                {
                    text: CANCEL_TEXT, onPress: () => { }
                },
                {
                    text: YES_TEXT, onPress: () => { setTimeout(() => {
                        this.remoteLogoutCall()
                    }, 500); }
                }
            ],
            { cancelable: false }
        )
    }

    remoteLogoutCall() {

        if (Helper.API_HELPER.getInstance().isInternetConnected() == false) {
            this.showNoInternetAlert()
            return;
        }

        this.setState({
            isShowSpinner: true
        })        
        
        Helper.API_HELPER.getInstance().logout().then((responseJson) => {
            if (responseJson) {
                this.handleLogoutResponse(responseJson)
            }
        })
            .catch((error) => {
                
                this.setState({
                    isShowSpinner: false
                })
                setTimeout(() => {
                    this.dropdown.alertWithType("info", "", SOMETHING_WRONG_MESSAGE)
                }, 500);
            })
    }

    handleLogoutResponse(response) {
        const status = response.success;
        if (status == true) {
            this.deleteUserData()
        }
        else {
            const errorCode = response.errorCode
            if (errorCode == 110) {
                this.showLogoutErrorMessage(response)
            }
        }
    }

    logoutUserFromApp = (response) => {
        const responseJson = response["responseJson"]
        if (responseJson != undefined || responseJson != null) {
            this.showLogoutErrorMessage(responseJson)
        }
    }

    showLogoutErrorMessage = (response) => {
        Alert.alert(
            APP_NAME,
            response["errorMessage"],
            [
                {
                    text: YES_TEXT, onPress: () => this.deleteUserData()
                }
            ],
            { cancelable: false }
        )
    }

    deleteUserData() {
        console.log("deleteUserData");
        
        this.setState({
            isShowSpinner: false,
            selectedIndex: 0,
            title: "",
            profilePicUrl: ""
        })

        Helper.DATABASE_HANDLER.getInstance().deleteUserDetails()
        Helper.DATABASE_HANDLER.getInstance().deleteWalletDetails()
        Helper.DATABASE_HANDLER.getInstance().deleteCardDetails()
        Helper.USER_MANAGER.getInstance().clearData()
        this.props.navigation.replace("LoginScreen")
    }

    showNoInternetAlert() {
        this.dropdown.alertWithType("error", "", NO_INTERNET_CONNECTION)
    }

    onCloseDropdown() { }
}

Drawer.propTypes = {
    navigation: PropTypes.object
};

export default Drawer;

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: 'rgba(100,100,100,0.9)'
    },
    content: {
        flex: 1,
    },
    header: {
        backgroundColor: 'rgba(100,100,100,0.9)',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: '#A0A0A0',
        borderBottomWidth: 1,
    },
    backgroundImage: {
        position: 'absolute',
        width: HEADER_MAX_HEIGHT / 2.5,
        height: HEADER_MAX_HEIGHT / 2.5,
        // borderRadius: HEADER_MAX_HEIGHT/4,
        resizeMode: 'cover',
    },
    bar: {
        marginTop: Platform.OS === 'ios' ? 50 : 38,
        height: 50,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 90,
    },
    title: {
        color: 'white',
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Bold',
        alignSelf: 'center',
        textAlign: 'center',
        marginHorizontal: 5,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0
    },
    row: {
        height: 60
    },
    rowItemStyle: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    separatorStyle: {
        height: 1,
        bottom: 1,
        backgroundColor: 'rgba(255,255,255,0.3)'
    },
    iconStyle: {
        left: 30
    },
    titleStyle: {
        left: 60,
        fontSize: moderateScale(16, 0.09),
        fontFamily: 'OpenSans-Regular',
    }
});
