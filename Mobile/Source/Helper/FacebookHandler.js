
import { AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

export default class FacebookHandler {

  static instance = null;
  static createInstance() {

    var object = new FacebookHandler();
    return object;
  }

  static getInstance() {

    if (!FacebookHandler.instance) {

      FacebookHandler.instance = FacebookHandler.createInstance();
    }

    return FacebookHandler.instance;
  }
  
  loginToFacebook() {
    this.logout()

    return new Promise((resolve, reject) => {
        LoginManager.logInWithReadPermissions(['public_profile', 'email']).then((result) => {
          if (result.isCancelled) {
            reject(undefined)
          }
          else {
            AccessToken.getCurrentAccessToken().then((data) => {
              if (!data) {
                reject(undefined)
              }
              else {

                // Create a graph request asking for user information with a callback to handle the response.

                let infoRequest = new GraphRequest('/me', {
                  httpMethod: 'GET',
                  version: 'v2.5',
                  parameters: {
                    'fields': {
                      'string': 'first_name, last_name, email, id, picture{ url }'
                    }
                  }
                }, (err, res) => {
                  if (err) {
                    reject(undefined)
                  }
                  else {
                    resolve(res)
                  }
                });

                // Start the graph request.
                new GraphRequestManager().addRequest(infoRequest).start();
              }
            })             
          }
        }).catch((error) =>{
          console.log("error is", error);          
        })        
    })
  }

  _responseInfoCallback(error, result) {
    if (error) {

    } else {
    }
  }

  logout() {
    LoginManager.logOut()
  }
}