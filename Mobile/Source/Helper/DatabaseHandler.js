import SecureStorage, { ACCESS_CONTROL, ACCESSIBLE, AUTHENTICATION_TYPE } from 'react-native-secure-storage'

import {
    AsyncStorage
} from 'react-native';

const USER_DETAILS = 'userDetails'
const WALLET_DETAIL = 'walletDetails'
const CARD_DETAILS = 'cardDetails'

export default class DatabaseHandler {

    static instance = null;
    static createInstance() {
        var object = new DatabaseHandler();
        return object;
    }

    static getInstance() {
        if (!DatabaseHandler.instance) {

            DatabaseHandler.instance = DatabaseHandler.createInstance();
        }

        return DatabaseHandler.instance;
    }

    /***********************************/

    saveToDB = (key, data) => {

        const config = {
            accessControl: ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
            accessible: ACCESSIBLE.WHEN_UNLOCKED_THIS_DEVICE_ONLY,
            authenticationPrompt: 'auth with yourself',
            service: 'example',
            authenticateType: AUTHENTICATION_TYPE.BIOMETRICS,
        }
        
        this.clearDataFromDB(key)
        try {
            AsyncStorage.setItem(key, JSON.stringify(data))            
        } catch (error) {
            console.log(error)
        }
    }

    getDataFromDB = (key) => {

        const config = {
            accessControl: ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
            accessible: ACCESSIBLE.WHEN_UNLOCKED_THIS_DEVICE_ONLY,
            authenticationPrompt: 'auth with yourself',
            service: 'example',
            authenticateType: AUTHENTICATION_TYPE.BIOMETRICS,
        }

        return new Promise(function (resolve, reject) {
            try {
                AsyncStorage.getItem(key).then((value) => {
                    if (value == undefined) {
                        resolve()
                    }
                    else {
                        resolve(JSON.parse(value))
                    }                       
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }

    clearDataFromDB = (key) => {
        try {
            AsyncStorage.removeItem(key).then((value) => {
                if (value != null) {

                }
            });
            // AsyncStorage.removeItem(key).then((value) => {
            //     if (value != null) {
                    
            //     }
            // });
        } catch (error) {
            console.log(error)
        }
    }

    /***********************************/

    //User details
    saveUserDetails = (userInfo) => {
        this.saveToDB(USER_DETAILS, userInfo)
    }

    getUserDetails = () => {
        return new Promise((resolve, reject) => {
            try {
                this.getDataFromDB(USER_DETAILS).then((data) => {
                    resolve(data)
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }

    deleteUserDetails = () => {
        this.clearDataFromDB(USER_DETAILS)
    }

    /***********************************/

    //Wallet details
    saveWalletDetails = (walletInfo) => {
        this.saveToDB(WALLET_DETAIL, walletInfo)
    }

    getWalletDetails = () => {
        return new Promise((resolve, reject) => {
            try {
                this.getDataFromDB(WALLET_DETAIL).then((data) => {
                    resolve(data)
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }

    deleteWalletDetails = () => {
        this.clearDataFromDB(WALLET_DETAIL)
    }

    /***********************************/

    //Card details
    saveCardDetails = (cardInfo) => {
        this.saveToDB(CARD_DETAILS, cardInfo)
    }

    getCardDetails = () => {
        return new Promise((resolve, reject) => {
            try {
                this.getDataFromDB(CARD_DETAILS).then((data) => {
                    resolve(data)
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }

    deleteCardDetails = () => {
        this.clearDataFromDB(CARD_DETAILS)
    }
}