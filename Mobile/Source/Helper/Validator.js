
export var Validator = function (text) {

    this.text = text == undefined ? "" : text;

    this.validateFirstName = function() {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter full name"
        }        
        return ""
    }

    this.validateMiddleName = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter Middle name"
        }
        return ""
    }

    this.validateAddress = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter address"
        }
        return ""
    }

    this.validateAge = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter Age"
        }

        var reg = /^\d+$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Enter valid age"
        }
        return ""
    }

    this.validateEmailAddress = function() {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter Email Address"
        }

        var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Email Address is not valid"
        }

        return ""
    }

    this.validateMobileNumber = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter mobile number"
        }

        var reg = /^[+]\d+$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Mobile number should contain only numbers. Only country code '+' sign is allowed."
        }

        return ""
    }

    this.validatePassword = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter Password"
        }

        isValid = this.validateLength(8)
        if (isValid == false) {
            return "Password should be greater than 8 characters"
        }

        var reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+){8,}$/; 
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Password should be alphanumeric"
        }

        return ""
    }

    this.validateNewPassword = function (password) {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter New Password"
        }

        isValid = this.validateLength(8)
        if (isValid == false) {
            return "New Password should be greater than 8 characters"
        }

        var reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+){8,}$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "New Password should be alphanumeric"
        }

        return ""
    }   
    
    this.validateConfirmPassword = function (password) {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter Confirm Password"
        }

        isValid = this.validateLength(8)
        if (isValid == false) {
            return "Confirm Password should be greater than 8 characters"
        }

        var reg = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+){8,}$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Confirm Password should be alphanumeric"
        }

        if (this.text != password) {
            return "Confirm Password is not matching with New Password"
        }

        return ""
    }    

    this.validateAmount = function () {        

        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter amount"
        }

        var reg = /^([1-9]\d*)(\.\d+)?$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Entered amount is not valid"
        }

        return ""
    }

    this.validateCardNumber = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter card number"
        }

        var reg = /^\d{16}$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Card number should contain only numbers"
        }

        return ""
    }

    this.validateAddressLine1 = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter address line 1"
        }
        return ""
    }

    this.validateAddressLine2 = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter address line 2"
        }
        return ""
    }
    
    this.validateCity = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Select city"
        }
        return ""
    }

    this.validateState = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Select state"
        }
        return ""
    }

    this.validateCountry = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Select country"
        }
        return ""
    }
    
    this.validateZIPCode = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter ZIP code"
        }
        return ""
    }

    this.validateLoyaltyPoints = function () {

        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter loyalty Points"
        }

        var reg = /^([1-9]\d*)(\.\d+)?$/;
        isValid = this.validateForRegex(reg)
        if (isValid == false) {
            return "Entered loyalty points are not valid"
        }
        return ""
    }
    
    this.validateIDNumber = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter document ID number"
        }
        return ""
    }

    this.validateDate = function () {
        let isValid = this.isEmpty()
        if (isValid == false) {
            return "Enter date and time"
        }
        return ""
    }

    return ""
}

Validator.prototype.removeSpaces = function () {
    return this.text.trim()
}

Validator.prototype.isEmpty = function () {
    text = this.removeSpaces(this.text)
    return text.length > 0 ? true : false
}

Validator.prototype.validateLength = function (numberOfChar) {
    text = this.removeSpaces(this.text)
    return text.length >= numberOfChar ? true : false
}

Validator.prototype.validateForRegex = function (reg) {
    return reg.test(this.text)
}
