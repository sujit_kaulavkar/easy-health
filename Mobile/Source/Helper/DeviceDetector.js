import React, {
    PixelRatio,
    Platform,
    Dimensions
} from 'react-native';

const windowSize = Dimensions.get('window');

export default class DeviceDetector {

    static instance = null;
    static createInstance() {

        var object = new DeviceDetector();
        return object;
    }

    static getInstance() {

        if (!DeviceDetector.instance) {

            DeviceDetector.instance = DeviceDetector.createInstance();
        }

        return DeviceDetector.instance;
    }

    constructor() {
        this.pixelDensity = PixelRatio.get();
        this.width = windowSize.width;
        this.height = windowSize.height;
        this.adjustedWidth = this.width * this.pixelDensity;
        this.adjustedHeight = this.height * this.pixelDensity;

        this.isPhoneOrTablet();
        this.isIosOrAndroid();
        this.detectIphoneX();
    }

    isTablet() {
        return this.isTablet
    }

    isPhone() {
        return this.isPhone
    }

    isIphoneX() {
        return this.isIphoneX
    }

    isIos() {
        return this.isIos
    }

    isAndroid() {
        return this.isAndroid
    }

    isPhoneOrTablet() {
        if (this.pixelDensity < 2 && (this.adjustedWidth >= 1000 || this.adjustedHeight >= 1000)) {
            this.isTablet = true;
            this.isPhone = false;
        } else if (this.pixelDensity === 2 && (this.adjustedWidth >= 1920 || this.adjustedHeight >= 1920)) {
            this.isTablet = true;
            this.isPhone = false;
        } else {
            this.isTablet = false;
            this.isPhone = true;
        }
    }

    isIosOrAndroid() {
        if (Platform.OS === 'ios') {
            this.isIos = true;
            this.isAndroid = false;
        } else {
            this.isIos = false;
            this.isAndroid = true;
        }
    }

    detectIphoneX() {
        if (Platform.OS === 'ios' &&
            !Platform.isTVOS &&
            !Platform.isTVOS &&
            (windowSize.height === 812 || windowSize.width === 812)) {
            this.isIphoneX = true;
        } else {
            this.isIphoneX = false;
        }

    }
}
