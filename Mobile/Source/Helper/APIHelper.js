
import { NetInfo } from "react-native";

import {
  IOS,
  ANDROID,
  IS_IOS
} from './Constants.js';
import UserManager from './UserManager';
import fetch from 'react-native-cancelable-fetch';
import Events from 'react-native-simple-events';

const ACCEPT_HEADER = 'application/json';
const CONTENT_TYPE_HEADER = 'application/x-www-form-urlencoded;'
const AUTH_TOKEN = "H43wI1zvkww3MCEagPhTdcM3yR80SMyh"
const API_TIMEOUT = 30 * 1000 // 30 sec

const POST_REQUEST = "POST"
const GET_REQUEST = "GET"
const PUT_REQUEST = "PUT"
const DELETE_REQUEST = "DELETE"

//const BASE_URL = "https://prod-186.adpost.com"      // PRODUCTION
const BASE_URL = "http://192.168.1.89:81/api/"       // DEVELOPMENT
//const BASE_URL = "http://local.asiatop.com"         // LOCAL 

const URL = BASE_URL

const LOGIN_URL = URL + 'user/signin';
const REGISTRATION_URL = URL + 'users';
const MM_REGISTRATION_URL = URL + 'user/mmadd';
const SOCIAL_LOGIN_URL = URL + 'user/socialLogin';
const FORGOT_PASSWORD_URL = URL + 'user/forgetPassword';
const VERIFY_OTP_URL = URL + 'user/verifyOTP';
const RESET_PASSWORD_URL = URL + 'user/resetPassword';
const UPDATE_DEVICE_TOKEN_URL = URL + 'user/updateDeviceToken';
const RESEND_OTP_URL = URL + 'user/resendOTP';
const LOGOUT_URL = URL + 'user/logout';
const DOCUMENT_ID_URL = URL + 'enumerations/id_types';
const KYC_PERSONAL_DETAILS_URL = URL + "user/updateUserDetailsKyc";
const KYC_UPLOAD_DOC_URL = URL + 'user/updateDocumentsKyc';
const KYC_STATUS_URL = URL + 'user/kyc-status';
const MY_WALLETS_URL = URL + 'wallet';
const MY_CARDS_URL = URL + 'card/wp-details';
const TOP_SPENDINGS_URL = URL + "transaction/topSpending";
const CARD_TRANSACTION_URL = URL + 'transaction/wp-transaction';
const VENDOR_URL = URL + 'vendor';
const ACTIVATE_CARD_URL = URL + "card/activate";
const ACTIVATE_VIRTUAL_CARD = URL + "card/create-virtual-card"
const LOAD_URL = URL + "transaction/load";
const UNLOAD_URL = URL + "transaction/unload";
const INITIATE_CREDIT_TRANSFER_URL = URL + "transaction/transfer/initiate";
const CONFIRM_CREDIT_TRANSFER_URL = URL + "transaction/transfer/confirm";
const SUSPEND_CARD = URL + "card/suspend";
const GENERATE_CVC_CARD = URL + "card/cvc";
const GENERATE_ATM_PIN = URL + "card/atmpin"
const EDIT_CARD_NAME_URL = URL + "card/name"
const CHANGE_PASSWORD_URL = URL + "user/changePassword"
const UPDATE_USER_URL = URL + "user/update"
const NOTIFICATION_URL = URL + "user/getActivity"
const NOTIFICATION_READ_URL = URL + "user/updateActivity"
const ADD_EDIT_ADDRESS_URL = URL + "userDetails/address"
const EDIT_MOBILE_NUMBER_URL = URL + "user/updateMobile"
const EDIT_EMAIL_ADDRESS_URL = URL + "user/updateEmail"
const FUND_PROVIDER_URL = URL + "topup/fundProvider";
const INITIATE_MASTERCARD_PAYMENT_URL = URL + "topup/checkoutDirect/";
const CHECK_PAYMENT_STATUS_URL = URL + "topup/getTransactionStatus";
const INITIATE_SINGPOST_PAYMENT_URL = URL + "topup/checkout/tokens/";
const UPDATE_LOYALTY_POINTS_URL = URL + "wallet/funds"
const MY_VOUCHERS_URL = URL + 'voucher/get_vouchers';
const MY_REWARDS_URL = URL + 'voucher/get_rewards';
const REDEEM_VOUCHER_URL = URL + 'voucher/redeem_voucher';
const REQUEST_LOYALTY_CARD_URL = URL + 'voucher/dispatch_loyalty_card';

const MY_APPOINMENTS = URL + "appointments"
const FACILITES = URL + "facilities"
const HOSPITALS = URL + "facilities"
const PLACE_ORDER = URL + "appointments"

export default class APIHelper {

  static instance = null;
  isInternetAvailable = true;

  static createInstance() {

    var object = new APIHelper();
    return object;
  }

  static getInstance() {

    if (!APIHelper.instance) {

      APIHelper.instance = APIHelper.createInstance();
    }

    return APIHelper.instance;
  }

  constructor() {
    createRequest = this.createRequest.bind(this)
  }

  addInternetListener() {
    NetInfo.addEventListener('connectionChange', this.internetStatusChange);
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      this.isInternetAvailable = connectionInfo.type == "none" ? false : true
    });
  }

  internetStatusChange = (connectionInfo) => {
    this.isInternetAvailable = connectionInfo.type == "none" ? false : true
  }

  isInternetConnected() {
    return this.isInternetAvailable;
  }

  baseUrl() {
    return BASE_URL;
  }

  stopAPICall = () => {
    fetch.abort(this);
  }

  remoteAPICall(api, body, requestMethod) {    
    
    return new Promise((resolve, reject) => {
      try {
        let responseObject = this.createRequest(body, api, requestMethod)
        if (responseObject) {
          responseObject.then((response) => response.json())
            .then((responseJson) => {
              console.log("master responseJson", responseJson);

              const errorCode = responseJson.errorCode
              if (errorCode == 110) {
                Events.trigger("logoutUserFromApp", { responseJson })
              }
              else {
                resolve(responseJson)
              }
            })
            .catch((error) => {
              reject(error)
            })
        }
      }
      catch (error) {
        reject(error)
      }
    })
  }

  createRequest = (request, api, requestMethod, customHeaders = undefined) => {
    let formBody = null
    if (requestMethod != GET_REQUEST) {
      formBody = Object.keys(request).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(request[key])).join('&')
    }

    let headers = {
      'Accept': ACCEPT_HEADER,
      'Content-Type': CONTENT_TYPE_HEADER,
      "Authorization": AUTH_TOKEN
    }

    const accessToken = UserManager.getInstance().getAccessToken()    
    
    if (accessToken.length > 0) {
      headers["Authorization"] = "Bearer " + accessToken
    }

    if (customHeaders != undefined) {
      Object.assign(headers, customHeaders)
    }

    console.log("requestMethod", requestMethod);
    console.log("api", api);
    console.log("request", request);
    console.log("headers", headers);

    return fetch(api, {
      timeout: API_TIMEOUT,
      method: requestMethod,
      headers: headers,
      body: formBody
    }, this)
  }

  login = (email, password) => {

    this.stopAPICall()
    var newRequest = {}
    newRequest["email"] = email
    newRequest["password"] = password

    return new Promise((resolve, reject) => {
      this.remoteAPICall(LOGIN_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  registration = (firstname, emailAddress, mobileNumber, age, gender, address, password) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["name"] = firstname
    newRequest["email"] = emailAddress
    newRequest["contact_no"] = mobileNumber
    newRequest["age"] = age
    newRequest["gender"] = gender
    newRequest["address"] = address
    newRequest["password"] = password

    return new Promise((resolve, reject) => {
      this.remoteAPICall(REGISTRATION_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  mm_registration = (firstName, lastName, preferredname, mobileNumber, email, password) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["first_name"] = firstName
    newRequest["last_name"] = lastName
    newRequest["preferred_name"] = preferredname
    newRequest["mobile"] = mobileNumber
    newRequest["email"] = email
    newRequest["password"] = password
    newRequest["platform"] = IS_IOS ? IOS : ANDROID
    newRequest["device_token"] = UserManager.getInstance().getDeviceToken()

    return new Promise((resolve, reject) => {
      this.remoteAPICall(MM_REGISTRATION_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  socialLogin = (firstName, lastName, socialId, socialType) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["first_name"] = firstName
    newRequest["last_name"] = lastName
    newRequest["social_id"] = socialId
    newRequest["social_type"] = socialType
    newRequest["platform"] = IS_IOS ? IOS : ANDROID
    newRequest["device_token"] = UserManager.getInstance().getDeviceToken()

    return new Promise((resolve, reject) => {
      this.remoteAPICall(SOCIAL_LOGIN_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  forgotPassword = (mobileNumber) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["email"] = mobileNumber

    return new Promise((resolve, reject) => {
      this.remoteAPICall(FORGOT_PASSWORD_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  verifyOTP = (otpCode,email) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["email"] = email
    newRequest["otp"] = otpCode

    return new Promise((resolve, reject) => {
      this.remoteAPICall(VERIFY_OTP_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  resetPassword = (email, password) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["email"] = email
    newRequest["password"] = password

    return new Promise((resolve, reject) => {
      this.remoteAPICall(RESET_PASSWORD_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  updateDeviceToken = (accessToken, deviceToken) => {
    var newRequest = {}
    newRequest["access_token"] = accessToken
    newRequest["platform"] = IS_IOS ? IOS : ANDROID
    newRequest["device_token"] = deviceToken

    return new Promise((resolve, reject) => {
      this.remoteAPICall(UPDATE_DEVICE_TOKEN_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  resendOTP = (mobileNumber, action) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["mobile_number"] = mobileNumber
    newRequest["action"] = action
    newRequest["platform"] = IS_IOS ? IOS : ANDROID

    return new Promise((resolve, reject) => {
      this.remoteAPICall(RESEND_OTP_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  updateKYCUserDetails = (userHashId, userDetails, image, idType, idNumber, countryOfIssue) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["details"] = userDetails
    newRequest["image"] = image
    newRequest["id_type"] = idType
    newRequest["id_number"] = idNumber
    newRequest["country_of_issue"] = countryOfIssue

    return new Promise((resolve, reject) => {
      this.remoteAPICall(KYC_PERSONAL_DETAILS_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  uploadKYCDocument = (userHashId, frontImage, backImage) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["front_image"] = frontImage
    newRequest["back_image"] = backImage

    return new Promise((resolve, reject) => {
      this.remoteAPICall(KYC_UPLOAD_DOC_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  kycStatus = (userHashId) => {
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId

    return new Promise((resolve, reject) => {
      this.remoteAPICall(KYC_STATUS_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  logout = () => {    
    this.stopAPICall()    
    return new Promise((resolve, reject) => {
      this.remoteAPICall(LOGOUT_URL, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  documentIds = () => {
    this.stopAPICall()
    return new Promise((resolve, reject) => {
      this.remoteAPICall(DOCUMENT_ID_URL, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  vendors = (searchQuery) => {
    this.stopAPICall()
    const url = VENDOR_URL + "/" + searchQuery

    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  // Cards 
  myWallet = (userHashId) => {
    //this.stopAPICall()
    const url = MY_WALLETS_URL + "/" + userHashId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  myCards = (userHashId) => {
    this.stopAPICall()
    const url = MY_CARDS_URL + "/" + userHashId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  top3Spendings = (userHashId) => {
    const url = TOP_SPENDINGS_URL + "?user_hash_id=" + userHashId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  cardTransactions = (cardId, pageNumber, userHashId) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["card_hash_id"] = cardId
    newRequest["page"] = pageNumber
    newRequest["user_hash_id"] = userHashId
    const url = CARD_TRANSACTION_URL + "?user_hash_id=" + userHashId + "&card_hash_id=" + cardId + "&page=" + pageNumber

    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  activateCard = (userHashId, cardTypeCode, cardNumber, vendorId) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["card_type_code"] = cardTypeCode
    newRequest["card_number"] = cardNumber
    newRequest['vendor_id'] = vendorId

    return new Promise((resolve, reject) => {
      this.remoteAPICall(ACTIVATE_CARD_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  activateVirtualCard = (userHashId, vendorId) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest['vendor_id'] = vendorId

    return new Promise((resolve, reject) => {
      this.remoteAPICall(ACTIVATE_VIRTUAL_CARD, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  loadCard = (userHashId, card_hash_id, amount, message = "") => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["card_hash_id"] = card_hash_id
    newRequest['amount'] = amount
    newRequest['message'] = message

    return new Promise((resolve, reject) => {
      this.remoteAPICall(LOAD_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  unloadCard = (userHashId, card_hash_id, amount, message = "") => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["card_hash_id"] = card_hash_id
    newRequest['amount'] = amount
    newRequest['message'] = message
    return new Promise((resolve, reject) => {
      this.remoteAPICall(UNLOAD_URL, newRequest, DELETE_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  initiateCreditTransfer = (userHashId, mobileNumber, amount) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["mobile_number"] = mobileNumber
    newRequest['amount'] = amount

    return new Promise((resolve, reject) => {
      this.remoteAPICall(INITIATE_CREDIT_TRANSFER_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  confirmCreditTransfer = (userHashId, transferId) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest['id'] = transferId

    return new Promise((resolve, reject) => {
      this.remoteAPICall(CONFIRM_CREDIT_TRANSFER_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  suspendCard = (userHashId, cardHashId) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["card_hash_id"] = cardHashId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(SUSPEND_CARD, newRequest, DELETE_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  generateCVC = (userHashId, cardHashId) => {
    this.stopAPICall()
    const url = GENERATE_CVC_CARD + "?card_hash_id=" + cardHashId + "&user_hash_id=" + userHashId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  generateATMPin = (userHashId, cardHashId, paymentMode) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["card_hash_id"] = cardHashId
    newRequest["mode"] = paymentMode
    return new Promise((resolve, reject) => {
      this.remoteAPICall(GENERATE_ATM_PIN, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  editCardName = (userHashId, cardHashId, name) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["card_hash_id"] = cardHashId
    newRequest["name"] = name
    return new Promise((resolve, reject) => {
      this.remoteAPICall(EDIT_CARD_NAME_URL, newRequest, PUT_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  changePassword = (mobileNumber, old_password, password) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["mobile_number"] = mobileNumber
    newRequest["old_password"] = old_password
    newRequest["password"] = password
    return new Promise((resolve, reject) => {
      this.remoteAPICall(CHANGE_PASSWORD_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  updateUser = (userHashId, userDetails, imageBase64String) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["details"] = userDetails
    if (imageBase64String != undefined) {
      newRequest["image"] = "data:image/png;base64," + imageBase64String
    }

    return new Promise((resolve, reject) => {
      this.remoteAPICall(UPDATE_USER_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  notification = (userHashId, pageNumber) => {
    this.stopAPICall()
    const url = NOTIFICATION_URL + "?user_hash_id=" + userHashId + "&page=" + pageNumber

    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  notificationRead = (userHashId, notificationId = "") => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId

    if (notificationId != "") {
      newRequest["activity_id"] = notificationId
    }

    return new Promise((resolve, reject) => {
      this.remoteAPICall(NOTIFICATION_READ_URL, newRequest, PUT_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  addUpdateAddress = (userHashId, addressType, line1, line2, city, state, country, countryCode, zipCode) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["address_type"] = addressType
    newRequest["address_1"] = line1
    newRequest["address_2"] = line2
    newRequest["city"] = city
    newRequest["state"] = state
    newRequest["country"] = country
    newRequest["country_code"] = countryCode
    newRequest["zipcode"] = zipCode

    return new Promise((resolve, reject) => {
      this.remoteAPICall(ADD_EDIT_ADDRESS_URL, newRequest, PUT_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  editMobileNumber = (userHashId, countryCode, mobileNumber) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["mobile_country_code"] = countryCode
    newRequest["mobile_number"] = mobileNumber
    return new Promise((resolve, reject) => {
      this.remoteAPICall(EDIT_MOBILE_NUMBER_URL, newRequest, PUT_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  editEmailAddress = (userHashId, emailAddress) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["email"] = emailAddress

    return new Promise((resolve, reject) => {
      this.remoteAPICall(EDIT_EMAIL_ADDRESS_URL, newRequest, PUT_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  fundProviders = () => {
    this.stopAPICall()
    return new Promise((resolve, reject) => {
      this.remoteAPICall(FUND_PROVIDER_URL, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  initiateMastercardPayment = (providerHashId, amount, kyc_status, user_hash_id, email, cardNumber, expiryMonth, expiryYear, cvvCode) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["amount"] = amount
    newRequest["kyc_status"] = kyc_status
    newRequest["user_hash_id"] = user_hash_id
    newRequest["email"] = email
    newRequest["card_number"] = cardNumber
    newRequest["expiry_month"] = expiryMonth
    newRequest["expiry_year"] = expiryYear
    newRequest["cvv_code"] = cvvCode

    const url = INITIATE_MASTERCARD_PAYMENT_URL + providerHashId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  checkPaymentStatus = (userHashId, paymentRefId) => {
    this.stopAPICall()
    const url = CHECK_PAYMENT_STATUS_URL + "/" + userHashId + "/" + paymentRefId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  initiateSingPostPayment = (providerHashId, amount, channelName, kyc_status, user_hash_id, email) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["provider_channel"] = channelName
    newRequest["amount"] = amount
    newRequest["kyc_status"] = kyc_status
    newRequest["user_hash_id"] = user_hash_id
    newRequest["email"] = email

    const url = INITIATE_SINGPOST_PAYMENT_URL + providerHashId + "?provider_channel=" + channelName + "&amount=" + amount + "&user_kyc_status=" + kyc_status + "&user_hash_id=" + user_hash_id
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  updateWalletWithLoyaltyPoints = (userHashId, cardHashId, points) => {
    this.stopAPICall()
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["card_hash_id"] = cardHashId
    newRequest["amount"] = points
    return new Promise((resolve, reject) => {
      this.remoteAPICall(UPDATE_LOYALTY_POINTS_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  myVouchers = (userHashId, pageNumber) => {
    this.stopAPICall()
    const url = MY_VOUCHERS_URL + "?user_hash_id=" + userHashId + "&page=" + pageNumber

    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  myRewards = (userHashId) => {
    this.stopAPICall()

    const url = MY_REWARDS_URL + "?user_hash_id=" + userHashId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(url, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  redeemVoucher = (userHashId, voucherId) => {
    this.stopAPICall()
    
    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["voucher_id"] = voucherId

    return new Promise((resolve, reject) => {
      this.remoteAPICall(REDEEM_VOUCHER_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  requestLoyaltyCard = (userHashId, addressType, voucherId) => {
    this.stopAPICall()

    var newRequest = {}
    newRequest["user_hash_id"] = userHashId
    newRequest["address_type"] = addressType
    newRequest["voucher_id"] = voucherId

    return new Promise((resolve, reject) => {
      this.remoteAPICall(REQUEST_LOYALTY_CARD_URL, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

/*******************************************************************************************/

  myAppoinments = () => {
    this.stopAPICall()

    return new Promise((resolve, reject) => {
      this.remoteAPICall(MY_APPOINMENTS, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  facilites = () => {
    this.stopAPICall()

    return new Promise((resolve, reject) => {
      this.remoteAPICall(FACILITES, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  hospitals = (facilityId) => {
    this.stopAPICall()

    const api = HOSPITALS + '/' + facilityId
    return new Promise((resolve, reject) => {
      this.remoteAPICall(api, null, GET_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  placeOrder = (name, gender, age, date, hospitalId, facilityId, contact_Number, address, paymentMode) => {
    this.stopAPICall()

    var newRequest = {}
    newRequest["name"] = name
    newRequest["gender"] = gender
    newRequest["age"] = age
    newRequest["appointment_date"] = date
    newRequest["location"] = address
    newRequest["contact"] = contact_Number
    newRequest["hospital_id"] = hospitalId
    newRequest["facility_id"] = facilityId
    newRequest["payment_mode"] = paymentMode
    newRequest["note"] = ""
    newRequest["status"] = "in-process"

    return new Promise((resolve, reject) => {
      this.remoteAPICall(PLACE_ORDER, newRequest, POST_REQUEST).then((result) => {
        resolve(result)
      }).catch((error) => {
        reject(error)
      })
    })
  }
}