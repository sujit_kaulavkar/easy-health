
export default class ServerHandler {

    static instance = null;
    static createInstance() {
        var object = new ServerHandler();
        return object;
    }

    static getInstance() {
        if (!ServerHandler.instance) {
            ServerHandler.instance = ServerHandler.createInstance();
        }
        return ServerHandler.instance;
    }

    remoteAPICall(api, body, requestMethod) {
        return new Promise((resolve, reject) => {            
            let responseObject = Helper.API_HELPER.getInstance().createRequest(api, body, requestMethod)
            if (responseObject) {
                responseObject.then((response) => response.json())
                    .then((responseJson) => {
                        resolve(responseJson)
                    })
                    .catch((error) => {
                        reject(error)
                    })
            }
        })
    }
}
