
import { GoogleSignin } from 'react-native-google-signin';

export default class GoogleHandler {

    static instance = null;
    static createInstance() {

        var object = new GoogleHandler();
        return object;
    }

    static getInstance() {

        if (!GoogleHandler.instance) {

            GoogleHandler.instance = GoogleHandler.createInstance();
        }

        return GoogleHandler.instance;
    }

    constructor() {
    }

    loginToGoogle() {

        return new Promise((resolve, reject) => {
            GoogleSignin.configure({
                iosClientId: '324271046976-a8v7sn55ib15lgrgvs5uai5vgp20alij.apps.googleusercontent.com',
                offlineAccess: false
            })
            GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true }).then(() => {
                GoogleSignin.signIn().then((user) => {
                    resolve(user)
                })
                .catch((error) => {
                    reject(error)
                })
            }).catch((error) => {
                console.log("error", error);                
                reject(error)
            })
        })
    }

    logout() {
        GoogleSignin.signOut()
    }
}
