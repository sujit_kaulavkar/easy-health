
export default class UserManager {

    static instance = null;


    static createInstance() {

        var object = new UserManager();
        return object;
    }

    static getInstance() {

        if (!UserManager.instance) {
            UserManager.instance = UserManager.createInstance();
        }
        return UserManager.instance;
    }

    constructor() {

        user = undefined
        walletInfo = {
            "walletBalance": "0.0",
            "walletCurrency": "SGD"
        }
        
        userWalletLimit = null
        deviceToken = ""
    }

    setUser(userDetails) {
        user = userDetails
    }

    getUser() {
        return user
    }

    setDeviceToken(token) {
        deviceToken = token
    }

    getDeviceToken() {
        return deviceToken
    }

    setWalletBalance(wallet) {
        walletInfo = wallet        
    }

    getWalletBalance() {
        return walletInfo
    }

    setWalletLimit(walletLimit) {
        userWalletLimit = walletLimit
    }

    getWalletLimit() {
        return userWalletLimit
    }

    getAccessToken() {                           
        if (user != undefined) {
            return user.accessToken
        }

        return ""
    }

    clearData() {
        this.setUser(undefined)
        this.setDeviceToken(undefined) 
        this.setWalletBalance(undefined)
    }
}