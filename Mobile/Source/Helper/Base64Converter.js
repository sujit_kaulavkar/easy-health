//import RNFetchBlob from 'react-native-fetch-blob';

export default class Base64Converter {
    
    static instance = null;
    static createInstance() {

        var object = new Base64Converter();
        return object;
    }

    static getInstance() {

        if (!Base64Converter.instance) {

            Base64Converter.instance = Base64Converter.createInstance();
        }

        return Base64Converter.instance;
    }

    constructor() {

    }
    
    // convertToBase64 = (imageUrl) => {

    //     let imagePath = null;
    //     let image_url_length = imageUrl;

    //     return new Promise(function (resolve, reject) {
    //         try {

    //             RNFetchBlob
    //                 .config({
    //                     fileCache: true
    //                 })
    //                 .fetch('GET', imageUrl)
    //                 // the image is now dowloaded to device's storage
    //                 .then((resp) => {
    //                     // the image path you can use it directly with Image component
    //                     imagePath = resp.path();
    //                     return resp.readFile('base64')
    //                 })
    //                 .then((base64Data) => {                        
    //                     resolve(base64Data)                                                
    //                 })
    //         }
    //         catch (error) {
    //             reject(error)
    //         }
    //     })        
    // }
}