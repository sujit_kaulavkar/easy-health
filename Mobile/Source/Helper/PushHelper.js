import { Notification, NotificationOpen } from 'react-native-firebase';
import {
    Platform,
} from 'react-native';
// import firebase from 'react-native-firebase';
import APIHelper from './APIHelper';
import UserManager from './UserManager';

export default class PushHelper {

    static instance = null;
    static createInstance() {
        var object = new PushHelper();
        object.clearBadgeCount()
        return object;
    }

    static getInstance() {
        if (!PushHelper.instance) {
            PushHelper.instance = PushHelper.createInstance();
        }
        return PushHelper.instance;
    }

    constructor() {
        // this.bindMethods()
        // this.initialize()
    }

    // bindMethods() {
    //     this.handle_notification_clicked = this.handle_notification_clicked.bind(this);
    // }

    // initialize() {
    //     firebase.messaging().hasPermission().then(enabled => {
    //         if (enabled) {
    //             //user has permissions
    //         } else {
    //             //user doesn't have permission
    //             firebase.messaging().requestPermission().then(() => {
    //                 //User has authorised
    //             }).catch(error => {
    //                 //User has rejected permissions
    //             });
    //         }
    //     });

    //     firebase.messaging().getToken().then((token) => {
    //         //console.log("DEVICE_TOKEN :: ", token)
    //         if (token != null) {
    //             UserManager.getInstance().setDeviceToken(token)
    //             let user = UserManager.getInstance().getUser()
    //             if (user && user["accessToken"]) {
    //                 APIHelper.getInstance().updateDeviceToken(user["accessToken"], token)
    //             }
    //         }
    //     });

    //     firebase.messaging().onTokenRefresh((token) => {
    //         // console.log("DEVICE_TOKEN refreshed :: ", token)
    //         if (token != null) {
    //             UserManager.getInstance().setDeviceToken(token)
    //             if (user && user["accessToken"]) {
    //                 APIHelper.getInstance().updateDeviceToken(user["accessToken"], token)
    //             }
    //         }
    //     });

    //     const channel = new firebase.notifications.Android.Channel(
    //         '34567633554454',
    //         'AsiaTopChannel',
    //         firebase.notifications.Android.Importance.Max
    //     ).setDescription('AsiaTop test channel');
    //     firebase.notifications().android.createChannel(channel);

    //     this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
    //     });

    //     this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
    //         this.handle_notification_in_foreground(notification);
    //     });

    //     this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
    //         const notification: Notification = notificationOpen.notification;
    //         this.handle_notification_clicked(notification)
    //     });

    //     firebase.notifications().getInitialNotification().then((notificationOpen: NotificationOpen) => {
    //         if (notificationOpen) {
    //             const action = notificationOpen.action;
    //             const notification: Notification = notificationOpen.notification;
    //             this.handle_notification_clicked(notification)
    //         }
    //     });
    // }

    // handle_notification_clicked(notification) {
    //     firebase.notifications().cancelAllNotifications();
    // }

    // handle_notification_in_foreground(notification) {
    //     this.fire_local_notification(notification)
    // }

    // fire_local_notification(notification) {

    //     if (Platform.OS === 'android') {
    //         const localNotification = new firebase.notifications.Notification({
    //             sound: 'default',
    //             show_in_foreground: true,
    //         })
    //             .setNotificationId(notification.notificationId)
    //             .setTitle(notification.title)
    //             .setBody(notification.body)
    //             .android.setChannelId('34567633554454')
    //             .android.setSmallIcon('ic_launcher')
    //             .android.setAutoCancel(true)
    //             .setData({ "title": "AsiaTop" })
    //             .android.setPriority(firebase.notifications.Android.Priority.High);

    //         firebase.notifications()
    //             .displayNotification(localNotification)
    //             .catch(err => console.error("err", err));
    //     }
    //     else if (Platform.OS === 'ios') {
    //         const localNotification = new firebase.notifications.Notification()
    //             .setNotificationId(notification.notificationId)
    //             .setTitle(notification.title)
    //             .setSubtitle(notification.subtitle)
    //             .setBody(notification.body)
    //             .setData({ "title": "AsiaTop" })
    //             .ios.setBadge(notification.ios.badge);

    //         firebase.notifications()
    //             .displayNotification(localNotification)
    //             .catch(err => console.error(err));
    //     }
    // }

    // For iOS only
    setBadgeCount = (count) => {
        //firebase.notifications().setBadge(count)
    }

    // For iOS only
    clearBadgeCount = () => {
       // firebase.notifications().setBadge(0)
    }
}