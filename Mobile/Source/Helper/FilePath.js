
/******************************************************************************/

import MobileNumber from '../Components/MobileNumber/MobileNumber';
import DropDown from '../Components/DropDown/DropDown';
import ImagePicker from '../Components/ImagePicker/ImagePicker';
import Router from '../Components/Router/Router';
import Timer from '../Components/Timer/Timer';
import TopBar from '../Components/TopBar/TopBar';
import ButtonSubmit from '../Components/UserInput/ButtonSubmit';
import UserInput from '../Components/UserInput/UserInput';
import UserInput1 from '../Components/UserInput/UserInput1';
import CardNumberInput from '../Components/UserInput/CardNumberInput';
import RadioButton from '../Components/RadioButton';
import LoadingView from '../Components/LoadingIndicator/LoadingView';
import BackgroundImage from '../Components/Background/BackgroundImage';
import CardName from '../Components/Card Name/CardName';
import CountrySelection from '../Components/CountrySelection/CountrySelection';

export const Components = {
    MOBILE_NUMBER: MobileNumber,
    DROP_DOWN: DropDown,
    IMAGE_PICKER: ImagePicker,
    ROUTER: Router,
    TIMER: Timer,
    TOP_BAR: TopBar,
    BUTTON_SUBMIT: ButtonSubmit,
    USER_INPUT: UserInput,
    USER_INPUT1: UserInput1,
    RADIO_BUTTON: RadioButton,
    LOADING_VIEW: LoadingView,
    CARD_NUMBER_INPUT: CardNumberInput,
    BACKGROUND_IMAGE: BackgroundImage,
    CARD_NAME: CardName,
    COUNTRY_SELECTION: CountrySelection
}

/******************************************************************************/

import APIHelper from '../Helper/APIHelper';
import AppRating from '../Helper/AppRating';
import Base64Converter from '../Helper/Base64Converter';
import Constants from '../Helper/Constants';
import DatabaseHandler from '../Helper/DatabaseHandler';
import DeviceDetector from '../Helper/DeviceDetector';
import FacebookHandler from '../Helper/FacebookHandler';
import GoogleHandler from '../Helper/GoogleHandler';
import PushHelper from '../Helper/PushHelper';
import ServerWrapper from '../Helper/ServerWrapper';
import Spinner from '../Helper/Spinner';
import UserManager from '../Helper/UserManager';
import FirebaseHelper from '../Helper/FirebaseHelper';
import ServerHandler from '../Helper/ServerWrapper';
import { Validator } from '../Helper/Validator';
import KYCLimit from '../Helper/KYCLimit';

export const Helper = {
    API_HELPER: APIHelper,
    APP_RATING: AppRating,
    BASE_64_CONVERTER: Base64Converter,
    CONSTANTS: Constants,
    DATABASE_HANDLER: DatabaseHandler,
    DEVICE_DETECTOR: DeviceDetector,
    FACEBOOK_HANDLER: FacebookHandler,
    GOOGLE_HANDLER: GoogleHandler,
    PUSH_HELPER: PushHelper,
    SERVER_WRAPPER: ServerWrapper,
    SPINNER: Spinner,
    USER_MANAGER: UserManager,
    VALIDATOR: Validator,
    FIREBASE_HELPER: FirebaseHelper,
    SERVER_WRAPPER: ServerHandler,
    KYC_LIMIT: KYCLimit
}

/******************************************************************************/

import KYCResidentilAddressScreen from '../Screens/Login/KYC/KYCAddressScreen';
import KYCPersonalDetailScreen from '../Screens/Login/KYC/KYCPersonalDetailScreen';
import KYCUploadIDProofScreen from '../Screens/Login/KYC/KYCUploadIDProofScreen';
import KYCUploadView from '../Screens/Login/KYC/KYCUploadView';
import DocumentSelectionScreen from '../Screens/Login/KYC/DocumentSelectionScreen';

export const KYC = {
    RESIDENTIAL_ADDRESS_SCREEN: KYCResidentilAddressScreen,
    PERSONAL_DETAIL_SCREEN: KYCPersonalDetailScreen,
    UPLOAD_ID_PROOF_SCREEN: KYCUploadIDProofScreen,
    UPLOAD_VIEW: KYCUploadView,
    DOCUMENT_SELECTION: DocumentSelectionScreen
}

/******************************************************************************/

import HomeScreen from '../Screens/Home/HomeScreen';
import VouchersMainScreen from '../Screens/Home/Vouchers/VouchersMainScreen';
import MyVouchersScreen from '../Screens/Home/Vouchers/MyVouchersScreen';
import MyRewardsScreen from '../Screens/Home/MyRewardsScreen';
import PointsToCashScreen from '../Screens/Home/PointsToCashScreen';
import VoucherDetailScreen from '../Screens/Home/Vouchers/VoucherDetailScreen';

export const HOME = {
    HOME_SCREEN: HomeScreen,
    VOUCHERS_MAIN_SCREEN: VouchersMainScreen,
    MY_VOUCHER_SCREEN: MyVouchersScreen,
    MY_REWARDS_SCREEN: MyRewardsScreen,
    POINTS_TO_CASH_SCREEN: PointsToCashScreen,
    VOUCHER_DETAIL_SCREEN: VoucherDetailScreen
}

/******************************************************************************/

import ForgotPasswordScreen from '../Screens/Login/ForgotPasswordScreen';
import MySplashScreen from '../Screens/Login/MySplashScreen';
import LoginScreen from '../Screens/Login/LoginScreen';
import OTPVerificationScreen from '../Screens/Login/OTPVerificationScreen';
import RegistrationScreen from '../Screens/Login/RegistrationScreen';
import ResetPasswordScreen from '../Screens/Login/ResetPasswordScreen';
import TNCView from '../Screens/Login/TNCView';
import TNCScreen from '../Screens/Login/TNCScreen';
import KYCScreen from '../Screens/Login/KYC/KYCScreen';
import TutorialScreen from '../Screens/Login/TutorialScreen';
import AppoinmentTypeScreen from '../Screens/Appoiment/AppoinmentTypeScreen';
import MyAppoints from '../Screens/Appoiment/MyAppoints';
import CheckListScreen from '../Screens/Appoiment/CheckListScreen';
import HospitalList from '../Screens/Appoiment/HospitalList';
import MYPaymentScreen from '../Screens/Appoiment/MYPaymentScreen';
import SummaryAppoinment from '../Screens/Appoiment/SummaryAppoinment';
import AppoinmentDateScreen from '../Screens/Appoiment/AppoinmentDateScreen';
import ConfirmAppoinment from '../Screens/Appoiment/ConfirmAppoinment';

export const LOGIN = {
    SPLASH_SCREEN: MySplashScreen,
    FORGOT_PASSWORD_SCREEN: ForgotPasswordScreen,
    LOGIN_SCREEN: LoginScreen,
    OTP_VERIFICATION_SCREEN: OTPVerificationScreen,
    REGISTRATION_SCREEN: RegistrationScreen,
    RESET_PASSWORD_SCREEN: ResetPasswordScreen,
    TNC_VIEW: TNCView,
    TNC_SCREEN: TNCScreen,
    KYC_SCREEN: KYCScreen,
    TUTORIAL_SCREEN: TutorialScreen,
    APPOINMENT_SCREEN: AppoinmentTypeScreen,
    MY_APPOINMENTS: MyAppoints,
    CEHCK_LIST: CheckListScreen,
    HOSPITAL_LIST: HospitalList,
    CONFIRM_APPOINMENT: ConfirmAppoinment,
    MY_PAYMENT_SCREEN: MYPaymentScreen,
    SUMMARY: SummaryAppoinment,
    APPOINMENT_DATE: AppoinmentDateScreen,
    CONFIRM_APPOINTMENT: ConfirmAppoinment
}

/******************************************************************************/

import DashboardScreen from '../Screens/Dashboard/DashboardScreen';
import WalletBalanceView from '../Screens/Dashboard/WalletBalanceView';
import CardsView from '../Screens/Dashboard/CardView/CardsView';
import TopSpendingsView from '../Screens/Dashboard/TopSpendingsView';
import TransactionHistoryScreen from '../Screens/Dashboard/MyCards/TransactionHistoryScreen';
import TransactionDetailsScreen from '../Screens/Dashboard/MyCards/TransactionDetailsScreen';
import TransactionListView from '../Screens/Dashboard/MyCards/TransactionListView';
import ATMPinScreen from '../Screens/Dashboard/MyCards/ATMPinScreen';
import LoadCardScreen from '../Screens/Dashboard/MyCards/LoadCardScreen';
import LoadCardSuccessScreen from '../Screens/Dashboard/MyCards/LoadCardSuccessScreen';
import CVCView from '../Screens/Dashboard/MyCards/CVCView';
import SuspendCardView from '../Screens/Dashboard/MyCards/SuspendCardView';
import LoyaltyPointsScreen from '../Screens/Dashboard/MyCards/LoyaltyPointsScreen';
import NotificationScreen from '../Screens/Notification/NotificationScreen';
import NotificationDetailScreen from '../Screens/Notification/NotificationDetailScreen';

export const DASHBOARD = {
    LANDING_SCREEN: DashboardScreen,
    WALLET_BALANCE_VIEW: WalletBalanceView,
    CARDS_VIEW: CardsView,
    TOP_SPENDING_VIEW: TopSpendingsView,
    TRANSACTION_HISTORY_SCREEN: TransactionHistoryScreen,
    TRANSACTION_DETAILS_SCREEN: TransactionDetailsScreen,
    TRANSACTION_LIST_VIEW: TransactionListView,
    LOAD_CARDS_SCREEN: LoadCardScreen,
    LOAD_CARD_SUCCESS_SCREEN: LoadCardSuccessScreen,
    CVC_VIEW: CVCView,
    SUSPEND_CARD_VIEW: SuspendCardView,
    ATM_PIN_SCREEN: ATMPinScreen,
    NOTIFICATION_SCREEN: NotificationScreen,
    NOTIFICATION_DETAILS_SCREEN: NotificationDetailScreen,
    LOYALTY_POINTS_SCREEN: LoyaltyPointsScreen
}

/******************************************************************************/

import Drawer from '../Screens/Drawer/Drawer'

export const DRAWER = {
    DRAWER: Drawer
}

/******************************************************************************/

import TopUpScreen from '../Screens/TopUp/TopUpScreen'
import ChoosePaymentScreen from '../Screens/TopUp/ChoosePaymentScreen';
import PaymmentSuccessScreen from '../Screens/TopUp/PaymmentSuccessScreen';
import TotalAmountView from '../Screens/TopUp/TotalAmountView';
import PaymentModeView from '../Screens/TopUp/PaymentModeView';
import PaymentScreen from '../Screens/TopUp/PaymentScreen';
import CardInputScreen from '../Screens/TopUp/CardInputScreen';
import SingPostTopupStepsScreen from '../Screens/TopUp/SingPostTopupStepsScreen';

export const TOP_UP = {
    TOPUP: TopUpScreen,
    CHOOSE_PAYMENT: ChoosePaymentScreen,
    PAYMENT_SUCCESS: PaymmentSuccessScreen,
    TOTAL_AMOUNT_VIEW: TotalAmountView,
    PAYMENT_MODE_VIEW: PaymentModeView,
    PAYMENT_SCREEN: PaymentScreen,
    CARD_INPUT_SCREEN: CardInputScreen,
    SING_POST_TOPUP_STEPS_SCREEN: SingPostTopupStepsScreen
}

/******************************************************************************/

import ActivateCardScreen from '../Screens/ActivateCard/ActivateCardScreen'
import ActivateCardSuccessScreen from '../Screens/ActivateCard/ActivateCardSuccessScreen'
import InputCardNumberScreen from '../Screens/ActivateCard/InputCardNumberScreen';
import VenderScreen from '../Screens/ActivateCard/VenderScreen';

export const ACTIVATE_CARD = {
    ACTIVATE_CARD: ActivateCardScreen,
    ACTIVATE_CARD_SUCCESS: ActivateCardSuccessScreen,
    INPUT_CARD_NUMBER_SCREEN: InputCardNumberScreen,
    VENDER_SCREEN: VenderScreen
}

/******************************************************************************/

import ClaimCancelScreen from '../Screens/Claim_Cancel/ClaimCancelScreen'

export const CLAIM_CANCEL = {
    CLAIM_CANCEL: ClaimCancelScreen
}

/******************************************************************************/

import SendEntryScreen from '../Screens/Send/SendEntryScreen'
import SendConfirmScreen from '../Screens/Send/SendConfirmScreen'
import SendSuccessScreen from '../Screens/Send/SendSuccessScreen'

export const SEND = {
    SEND_ENTRY: SendEntryScreen,
    SEND_CONFIRM: SendConfirmScreen,
    SEND_SUCCESS: SendSuccessScreen
}

/******************************************************************************/
import PayScreen from '../Screens/Pay/PayScreen'
import PaySuccessScreen from '../Screens/Pay/PaySuccessScreen';

export const PAY = {
    PAY: PayScreen,
    PAY_SUCCESS: PaySuccessScreen
}

/******************************************************************************/

import RequestQRScreen from '../Screens/Request/RequestQRScreen'
import RequestAmountScreen from '../Screens/Request/RequestAmountScreen'

export const REQUEST = {
    REQUEST: RequestAmountScreen,
    REQUEST_QR: RequestQRScreen,
    PAY_SUCCESS: PaySuccessScreen
}

/******************************************************************************/

import SettingsScreen from '../Screens/Settings/SettingsScreen'
import ManageProfileScreen from '../Screens/Settings/Manage Profile/ManageProfileScreen'
import MyAddressScreen from '../Screens/Settings/Address/MyAddressScreen'
import AddAddressScreen from '../Screens/Settings/Address/AddAddressScreen'
import EditEmailAddressScreen from '../Screens/Settings/EditEmailAddressScreen'
import EditPhoneNumberScreen from '../Screens/Settings/EditPhoneNumberScreen'
import ChangePasswordScreen from '../Screens/Settings/ChangePasswordScreen'
import HelpFeedbackScreen from '../Screens/Settings/HelpFeedbackScreen'
import ProfilePicView from '../Screens/Settings/Manage Profile/ProfilePicView'
import EditProfileScreen from '../Screens/Settings/Manage Profile/EditProfileScreen';
import FAQScreen from '../Screens/Settings/FAQScreen';
import FAQDetailScreen from '../Screens/Settings/FAQDetailScreen';

export const SETTINGS = {
    SETTINGS: SettingsScreen,
    NOTIFICATION_SCREEN: NotificationScreen,
    MANAGE_PROFILE_SCREEN: ManageProfileScreen,
    MY_ADDRESS_SCREEN: MyAddressScreen,
    EDIT_EMAIL_ADDRESS_SCREEN: EditEmailAddressScreen,
    EDIT_PHONE_NUMBER_SCREEN: EditPhoneNumberScreen,
    CHANGE_PASSWORD_SCREEN: ChangePasswordScreen,
    HELP_FEEDBACK_SCREEN: HelpFeedbackScreen,
    PROFILE_PIC_VIEW: ProfilePicView,
    ADD_ADDRESS_SCREEN: AddAddressScreen,
    EDIT_PROFILE_SCREEN: EditProfileScreen,
    FAQ_SCREEN: FAQScreen,
    FAQ_DETAIL_SCREEN: FAQDetailScreen
}