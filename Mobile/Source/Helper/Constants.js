
import { Dimensions, Platform } from 'react-native';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

export const IS_IOS = Platform.OS === 'ios'

// This needs to update whenever build needs to publish on play store.		
export const appVersionCode = 10	

export const isIphoneX = () => {
    let d = Dimensions.get('window');
    const { height, width } = d;

    return (
        // This has to be iOS duh
        IS_IOS &&

        // Accounting for the height in either orientation
        (height === 812 || width === 812)
    );
}

export const STATUS_BAR_HEIGHT_IOS = Platform.select({ ios: 20, android: 24 });
export const TOP_BAR_HEIGHT = Platform.select({ ios: isIphoneX() == true ? 84: 64, android: 60 });

//Platform
export const IOS = "iOS"
export const ANDROID = "Android"

export const APP_THEME = "rgba(255,255,255,1)"

// Internet connection event
export const INTERNET_CONNECTION_CHANEGD = "change"

// App theme color
export const DARK_BLUE_COLOR = "rgba(30,34,170,1)"
export const BLUE_COLOR = "rgba(64,126,201,1)"
export const LIGHT_BLUE_COLOR = "rgba(70,152,203,1)"
export const PINK_COLOR = "rgba(218,73,119,1)"

// Alert messages
export const NO_INTERNET_CONNECTION = "Please connect to the Internet"
export const NO_RESULT_FOUND_MESSAGE = "No results found"
export const SOMETHING_WRONG_MESSAGE = "Unable to connect to server. Please try again"
export const LOGOUT_MESSAGE = "Please confirm that you wish to log out"
export const NEW_UPDATE_MESSAGE = "A new version of this app has been released"
export const UPDATE_NOW_BTN = "Update Now"
export const NO_FAVORITE_POST_MESSAGE = "You have no favorite posts"
export const EXTERNAL_STORAGE_PERMISSION_ISSUE = "Adpost Home Ideas needs access to your EXTERNAL storage, so you can take awesome pictures"
export const POST_SHARE_LIMIT_MESSAGE = "You can share max 5 posts at a time"
export const OTP_SEND_MESSAGE = "We have sent you an Email with the verification code"
export const REGISTRATION_SUCCESSFUL_MESSAGE = "Your Registration is successful"
export const PASSWORD_RESET_SUCCESSFUL_MESSAGE = "Your password reset successfully"
export const ENTER_CARD_NUMBER_MESSAGE = "Enter card number"
export const ENTER_PASSCODE_MESSAGE = "Enter passcode"
export const WRONG_PASSCODE_MESSAGE = "Entered passcode is not correct"
export const SUSPEND_CARD_MESSAGE = "Are you sure want to suspend your card? You have to contact customer support to reactive it in future"
export const CHANGE_PASSWORD_SUCCESSFUL = "Your password changed successfully"
export const CHANGE_PROFILE_SUCCESSFUL = "Your profile updated successfully"
export const CHANGE_EMAIL_SUCCESSFUL = "Your email address changed successfully"
export const CHANGE_MOBILE_NUMBER_SUCCESSFUL = "Your mobile number changed successfully"
export const WRONG_PHONE_NUMBER_MESSAGE = "Phone number is not valid"
export const WALLET_EMPTY_MESSAGE = "Your wallet is empty"
export const LESS_WALLET_BALANCE_MESSAGE = "Your wallet balance is less than entered amount"
export const SELECT_PAYMENT_MODE_MESSAGE = "Select payment mode"
export const CARD_ALREADY_USED_MESSAGE = "The card number is already used"
export const WRONG_CARD_ENTRY_MESSAG = "The card number is wrong"
export const ADD_ADDRESS_SUCCESSFUL_MESSAGE = "Address added successfully"
export const UPDATE_ADDRESS_SUCCESSFUL_MESSAGE = "Address updated successfully"
export const EXIT_MESSAGE = 'Do you want to quit application?'

// Template name
export const HOME_TEMPLATE = 'home'

export const TEMPLATE = "home_app"

export const HOME_CATEGORY = "homes_cat"

// APP name
export const APP_NAME = "Easy Health"

// Buttons
export const OK_TEXT = "Ok"
export const NO_TEXT = "No"
export const YES_TEXT = "Yes"
export const CANCEL_TEXT = "Cancel"

// First Launch
export const IS_HOME_FIRST_LAUNCH = 'isHomeFirstLaunch';
export const IS_EXPLORER_FIRST_LAUNCH = 'isExplorerFirstLaunch';

//Offset value for which we need to load more the content
export const CONTENT_LOAD_OFFSET_POSITION = 0.3

//Drawer width  w.r.t. screen width when app is in landscape mode
export const TABLET_DRAWER_WIDTH_OFFSET = 0.3
export const PHONE_DRAWER_WIDTH_OFFSET = 0.4

// Minimum character to get the search result
export const MIN_SEARCH_CHAR_LENGTH = 3

// Minimum LHS clicks to show the rate IO
export const MIN_LHS_CLICKS = 7

// Max post share count
export const MAX_POST_SHARE_COUNT = 5

// KYC
export const POST_KYC = "post_kyc"
export const PRE_KYC = "pre_kyc"

export const socialType = {
    noOptionSelected: 0
}

export const DrawerItems = {
    DASHBOARD: 0,
    APPOINMENT: 1,
    SETTINGS: 2, 
    LOGOUT: 3
}

export const OTP_OPTIONS = {
    IS_FROM_REGISTRATION: 0,
    IS_FROM_FORGOT_PASSWORD: 1,
    IS_FROM_ACTIVATE_CARD: 2,
    IS_FROM_EDIT_EMAIL: 3,
    IS_FROM_EDIT_PHONE_NUMBER: 4
}

export const PAYMENT_STATUS = {
    SUCCESSFUL: 1,
    PENDING: 2,
    FAILURE: 3
}