import firebase from 'react-native-firebase';

import {
} from 'react-native';

const PENDING_STATUS = "Pending"
const SETTLE_STATUS = "Settle"

export default class FirebaseHelper {

    static instance = null;
    static createInstance() {
        var object = new FirebaseHelper();
        return object;
    }

    static getInstance() {
        if (!FirebaseHelper.instance) {
            FirebaseHelper.instance = FirebaseHelper.createInstance();
        }
        return FirebaseHelper.instance;
    }

    constructor() {

        var config = {
            databaseURL: "https://asiatop-loyalty.firebaseio.com",
            projectId: "asiatop-loyalty",
        };
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
    }

    addObserver(code) {

        return new Promise((resolve, reject) => {
            // This is calledback from Firebase. Called whenever there is change the object.
            firebase.database().ref('payHistory/' + code + "/").on('value', function (snapshot) {
                const payObject = snapshot.val()
                if (payObject != undefined && payObject != null) {
                    if (payObject.payStatus == SETTLE_STATUS) {
                        resolve(payObject)
                    }
                }                
            });
        })
    }

    removeObserver(code) {
        firebase.database().ref('payHistory/' + code + "/").off()
    }

    // Called whenever new amount request is raised from 'Request' module. 
    // This will create one entry into Firebase DB.
    sendPayInfo(mobileNumber, code, amount) {
        firebase.database().ref('payHistory/' + code + "/").set({
            mobileNumber: mobileNumber,
            passcode: code,
            requestAmount: amount,
            payStatus: PENDING_STATUS,
        }).then((data) => {
            //success callback
            //console.log('data ', data)
        }).catch((error) => {
            //error callback
            //console.log('error ', error)
        })
    }

    // Called whenever pay request is raised from 'Pay' module.    
    // This will update entry for which request was made. 
    updatePayStatus(code, transactionId, timeStamp) {        
        firebase.database().ref('payHistory/' + code + "/").update({
            payStatus: SETTLE_STATUS,
            transactionId: transactionId,
            timeStamp: timeStamp
        }).then((data) => {
            resolve(data)
        }).catch((error) => {
            reject(error)
        })     
    }

    isPasscodeExist(code) {

        return new Promise((resolve) => {
            firebase.database().ref('payHistory/' + code + "/").once('value', (snapshot) => {
                const value = snapshot._value;
                //console.log("snapshot", value);                
                if (value == null) {
                    resolve(null)
                }
                else {
                    resolve(value)
                }
            });
        })
    }

    // Delete the entry. 
    deletePayStatus(code) {
        firebase.database().ref('payHistory/' + code + "/").remove();
    }
}