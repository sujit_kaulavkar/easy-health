

import {
    Helper
} from './FilePath';

export default class KYCLimit {

    checkForTopUpLimit(topupAmount) {
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const isPostKYC = user.kyc_status == "approved"

        //const wallet = Helper.USER_MANAGER.getInstance().getWalletLimit()
        const wallet = {
            "topup_limits": {
                "pre_kyc": {
                    "allowed": 1,
                    "lifetime_count_limit": 1,
                    "lifetime_transactional_limit": 500,
                    "daily_count_limit": null,
                    "daily_transactional_limit": 500,
                    "weekly_count_limit": null,
                    "weekly_transactional_limit": null,
                    "monthly_count_limit": null,
                    "monthly_transactional_limit": 500,
                    "unit_transaction_limit": 500
                },
                "post_kyc": {
                    "allowed": 1,
                    "lifetime_count_limit": 50000,
                    "lifetime_transactional_limit": 480000,
                    "daily_count_limit": null,
                    "daily_transactional_limit": 5000,
                    "weekly_count_limit": null,
                    "weekly_transactional_limit": null,
                    "monthly_count_limit": null,
                    "monthly_transactional_limit": 8000,
                    "unit_transaction_limit": 5000
                },
                "current": {
                    "lifetime_count": 1,
                    "lifetime_transactional": 23,
                    "daily_count": 0,
                    "daily_transactional": 0,
                    "weekly_count": 0,
                    "weekly_transactional": 0,
                    "monthly_count": 1,
                    "monthly_transactional": 23
                }
            }           
        }
        
        const topupLimits = wallet.topup_limits
        const currentLimit = topupLimits.current
        const kycLimit = isPostKYC == true ? topupLimits.post_kyc : topupLimits.pre_kyc
        
        // lifetime count 
        if (kycLimit.lifetime_count_limit != null && currentLimit.lifetime_count >= kycLimit.lifetime_count_limit) {
            return "You have reached your lifetime topup limit"
        }
        // lifetime transactions
        else if (kycLimit.lifetime_transactional_limit != null && currentLimit.lifetime_transactional >= kycLimit.lifetime_transactional_limit) {
            return "You have reached your lifetime topup transaction limit"
        }
        // daily count 
        if (kycLimit.daily_count_limit != null && currentLimit.daily_count >= kycLimit.daily_count_limit) {
            return "You have reached your daily topup limit. Try tomorrow"
        }
        // daily transactions
        else if (kycLimit.daily_transactional_limit != null && currentLimit.daily_transactional >= kycLimit.daily_transactional_limit) {
            return "You have reached your daily topup transaction limit. Try tomorrow"
        }
        // weekly count
        else if (kycLimit.weekly_count_limit != null && currentLimit.weekly_count >= kycLimit.weekly_count_limit) {
            return "You have reached your weekly topup limit. Try next week"
        }
        // weekly transactions
        else if (kycLimit.weekly_transactional_limit != null && currentLimit.weekly_transactional >= kycLimit.weekly_transactional_limit) {
            return "You have reached your weekly topup transaction limit. Try next week"
        }
        // monthly count
        else if (kycLimit.monthly_count_limit != null && currentLimit.monthly_count >= kycLimit.monthly_count_limit) {
            return "You have reached your monthly topup limit. Try next month"
        }
        // monthly transactions
        else if (kycLimit.monthly_transactional_limit != null && currentLimit.monthly_transactional >= kycLimit.monthly_transactional_limit) {
            return "You have reached your monthly topup transaction limit. Try next month"
        }     

        const lifetimeAMountDiff = kycLimit.lifetime_transactional_limit - currentLimit.lifetime_transactional
        if (lifetimeAMountDiff < topupAmount) {
            return "Topup amount is greater than your lifetime transaction limit. Please enter less topup amount"
        }

        if (kycLimit.daily_transactional_limit != null) {
            const dailyAmountDiff = kycLimit.daily_transactional_limit - currentLimit.daily_transactional
            if (dailyAmountDiff < topupAmount) {
                return "Topup amount is greater than your daily transaction limit. Please enter less topup amount"
            }
        }        

        if (kycLimit.weekly_transactional_limit != null) {
            const weeklyAmountDiff = kycLimit.weekly_transactional_limit - currentLimit.weekly_transactional
            if (weeklyAmountDiff < topupAmount) {
                return "Topup amount is greater than your weekly transaction limit. Please enter less topup amount"
            }
        }        

        if (kycLimit.monthly_transactional_limit != null) {
            const monthlyAmountDiff = kycLimit.monthly_transactional_limit - currentLimit.monthly_transactional
            if (monthlyAmountDiff < topupAmount) {
                return "Topup amount is greater than your monthly transaction limit. Please enter less topup amount"
            }
        }     

        return ""
    }

    checkForLoadLimit(amount) {
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const isPostKYC = user.kyc_status == "approved"

        //const wallet = Helper.USER_MANAGER.getInstance().getWalletLimit()
        const wallet = {                       
            "load_limits": {
                "pre_kyc": {
                    "allowed": 1,
                    "lifetime_count_limit": 1,
                    "lifetime_transactional_limit": 500,
                    "daily_count_limit": null,
                    "daily_transactional_limit": 500,
                    "weekly_count_limit": null,
                    "weekly_transactional_limit": null,
                    "monthly_count_limit": null,
                    "monthly_transactional_limit": 500,
                    "unit_transaction_limit": 500
                },
                "post_kyc": {
                    "allowed": 1,
                    "lifetime_count_limit": 50000,
                    "lifetime_transactional_limit": 480000,
                    "daily_count_limit": null,
                    "daily_transactional_limit": 2000,
                    "weekly_count_limit": null,
                    "weekly_transactional_limit": null,
                    "monthly_count_limit": null,
                    "monthly_transactional_limit": 8000,
                    "unit_transaction_limit": 2000
                },
                "current": {
                    "lifetime_count": 0,
                    "lifetime_transactional": 0,
                    "daily_count": 0,
                    "daily_transactional": 0,
                    "weekly_count": 0,
                    "weekly_transactional": 0,
                    "monthly_count": 0,
                    "monthly_transactional": 0
                }
            }
        }
        const loadLimits = wallet.load_limits
        const currentLimit = loadLimits.current
        const kycLimit = isPostKYC == true ? loadLimits.post_kyc : loadLimits.pre_kyc

        // lifetime count 
        if (kycLimit.lifetime_count_limit != null && currentLimit.lifetime_count >= kycLimit.lifetime_count_limit) {
            return "You have reached your lifetime loading limit"
        }
        // lifetime transactions
        else if (kycLimit.lifetime_transactional_limit != null && currentLimit.lifetime_transactional >= kycLimit.lifetime_transactional_limit) {
            return "You have reached your lifetime loading transaction limit"
        }
        // daily count 
        if (kycLimit.daily_count_limit != null && currentLimit.daily_count >= kycLimit.daily_count_limit) {
            return "You have reached your daily loading limit. Try tomorrow"
        }
        // daily transactions
        else if (kycLimit.daily_transactional_limit != null && currentLimit.daily_transactional >= kycLimit.daily_transactional_limit) {
            return "You have reached your daily loading transaction limit. Try tomorrow"
        }
        // weekly count
        else if (kycLimit.weekly_count_limit != null && currentLimit.weekly_count >= kycLimit.weekly_count_limit) {
            return "You have reached your weekly loading limit. Try next week"
        }
        // weekly transactions
        else if (kycLimit.weekly_transactional_limit != null && currentLimit.weekly_transactional >= kycLimit.weekly_transactional_limit) {
            return "You have reached your weekly loading transaction limit. Try next week"
        }
        // monthly count
        else if (kycLimit.monthly_count_limit != null && currentLimit.monthly_count >= kycLimit.monthly_count_limit) {
            return "You have reached your monthly loading limit. Try next month"
        }
        // monthly transactions
        else if (kycLimit.monthly_transactional_limit != null && currentLimit.monthly_transactional >= kycLimit.monthly_transactional_limit) {
            return "You have reached your monthly loading transaction limit. Try next month"
        }

        const lifetimeAMountDiff = kycLimit.lifetime_transactional_limit - currentLimit.lifetime_transactional
        if (lifetimeAMountDiff < amount) {
            return "Card load amount is greater than your lifetime transaction limit. Please enter less load amount"
        }

        if (kycLimit.daily_transactional_limit != null) {
            const dailyAmountDiff = kycLimit.daily_transactional_limit - currentLimit.daily_transactional
            if (dailyAmountDiff < amount) {
                return "Card load amount is greater than your daily transaction limit. Please enter less load amount"
            }
        }

        if (kycLimit.weekly_transactional_limit != null) {
            const weeklyAmountDiff = kycLimit.weekly_transactional_limit - currentLimit.weekly_transactional
            if (weeklyAmountDiff < amount) {
                return "Card load amount is greater than your weekly transaction limit. Please enter less load amount"
            }
        }

        if (kycLimit.monthly_transactional_limit != null) {
            const monthlyAmountDiff = kycLimit.monthly_transactional_limit - currentLimit.monthly_transactional
            if (monthlyAmountDiff < amount) {
                return "Card load amount is greater than your monthly transaction limit. Please enter less load amount"
            }
        }

        return ""
    }

    checkForUnloadLimit(amount) {
        const user = Helper.USER_MANAGER.getInstance().getUser()
        const isPostKYC = user.kyc_status == "approved"

        //const wallet = Helper.USER_MANAGER.getInstance().getWalletLimit()
        const wallet = {
            "unload_limits": {
                "pre_kyc": {
                    "allowed": 1,
                    "lifetime_count_limit": 1,
                    "lifetime_transactional_limit": 500,
                    "daily_count_limit": null,
                    "daily_transactional_limit": 500,
                    "weekly_count_limit": null,
                    "weekly_transactional_limit": null,
                    "monthly_count_limit": null,
                    "monthly_transactional_limit": 500,
                    "unit_transaction_limit": 500
                },
                "post_kyc": {
                    "allowed": 1,
                    "lifetime_count_limit": 50000,
                    "lifetime_transactional_limit": 480000,
                    "daily_count_limit": null,
                    "daily_transactional_limit": 2000,
                    "weekly_count_limit": null,
                    "weekly_transactional_limit": null,
                    "monthly_count_limit": null,
                    "monthly_transactional_limit": 8000,
                    "unit_transaction_limit": 2000
                },
                "current": {
                    "lifetime_count": 0,
                    "lifetime_transactional": 0,
                    "daily_count": 0,
                    "daily_transactional": 0,
                    "weekly_count": 0,
                    "weekly_transactional": 0,
                    "monthly_count": 0,
                    "monthly_transactional": 0
                }
            }
        }
        const loadLimits = wallet.load_limits
        const currentLimit = loadLimits.current
        const kycLimit = isPostKYC == true ? loadLimits.post_kyc : loadLimits.pre_kyc

        // lifetime count 
        if (kycLimit.lifetime_count_limit != null && currentLimit.lifetime_count >= kycLimit.lifetime_count_limit) {
            return "You have reached your lifetime unloading limit"
        }
        // lifetime transactions
        else if (kycLimit.lifetime_transactional_limit != null && currentLimit.lifetime_transactional >= kycLimit.lifetime_transactional_limit) {
            return "You have reached your lifetime unloading transaction limit"
        }
        // daily count 
        if (kycLimit.daily_count_limit != null && currentLimit.daily_count >= kycLimit.daily_count_limit) {
            return "You have reached your daily unloading limit. Try tomorrow"
        }
        // daily transactions
        else if (kycLimit.daily_transactional_limit != null && currentLimit.daily_transactional >= kycLimit.daily_transactional_limit) {
            return "You have reached your daily unloading transaction limit. Try tomorrow"
        }
        // weekly count
        else if (kycLimit.weekly_count_limit != null && currentLimit.weekly_count >= kycLimit.weekly_count_limit) {
            return "You have reached your weekly unloading limit. Try next week"
        }
        // weekly transactions
        else if (kycLimit.weekly_transactional_limit != null && currentLimit.weekly_transactional >= kycLimit.weekly_transactional_limit) {
            return "You have reached your weekly unloading transaction limit. Try next week"
        }
        // monthly count
        else if (kycLimit.monthly_count_limit != null && currentLimit.monthly_count >= kycLimit.monthly_count_limit) {
            return "You have reached your monthly unloading limit. Try next month"
        }
        // monthly transactions
        else if (kycLimit.monthly_transactional_limit != null && currentLimit.monthly_transactional >= kycLimit.monthly_transactional_limit) {
            return "You have reached your monthly unloading transaction limit. Try next month"
        }

        const lifetimeAMountDiff = kycLimit.lifetime_transactional_limit - currentLimit.lifetime_transactional
        if (lifetimeAMountDiff < amount) {
            return "Card unload amount is greater than your lifetime transaction limit. Please enter less load amount"
        }

        if (kycLimit.daily_transactional_limit != null) {
            const dailyAmountDiff = kycLimit.daily_transactional_limit - currentLimit.daily_transactional
            if (dailyAmountDiff < amount) {
                return "Card unload amount is greater than your daily transaction limit. Please enter less load amount"
            }
        }

        if (kycLimit.weekly_transactional_limit != null) {
            const weeklyAmountDiff = kycLimit.weekly_transactional_limit - currentLimit.weekly_transactional
            if (weeklyAmountDiff < amount) {
                return "Card unload amount is greater than your weekly transaction limit. Please enter less load amount"
            }
        }

        if (kycLimit.monthly_transactional_limit != null) {
            const monthlyAmountDiff = kycLimit.monthly_transactional_limit - currentLimit.monthly_transactional
            if (monthlyAmountDiff < amount) {
                return "Card unload amount is greater than your monthly transaction limit. Please enter less load amount"
            }
        }

        return ""
    }
}