import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';
import RootStacknavigation from './Source/Components/Router/Router';
import Events from 'react-native-simple-events';
import codePush from "react-native-code-push";

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.ON_NEXT_RESTART
}

class App extends Component {

  componentDidMount() {
    SplashScreen.hide();
  }

  constructor() {
    super()
    this.state = {
      drawerLockMode: 'unlocked'
    }

    Events.on('lockDrawer', 'lockDrawer', this.lockDrawer)
    Events.on('unlockDrawer', 'unlockDrawer', this.unlockDrawer)
  }

  lockDrawer = () => {
    this.setState({
      drawerLockMode: 'locked-closed'
    })
  }

  unlockDrawer = () => {
    this.setState({
      drawerLockMode: 'unlocked'
    })
  }

  render() {
    return (
      <RootStacknavigation screenProps={{ drawerLockMode: this.state.drawerLockMode }} />
    );
  }
}

export default codePush(codePushOptions)(App);