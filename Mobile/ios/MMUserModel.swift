//
//  MMUserModel.swift
//  AsianTop
//
//  Created by Sujit on 12/26/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit

class MMUserModel: NSObject {
  var accessToken     : String = ""
  var userHashId      : String = ""
  var mobile      : String = ""
  var email       : String = ""
  var verificationId  : String = ""
  var verificationToken  : String = ""
  var provider  : String = ""
  var clientName      : String = "AsiaTop"
  var productCode     : String = "sgasiatp"
  var mobileCountryCode : String = "65"

  func getUserModelForKYC() -> MMUserModel {
    let config = MMUserModel()
    config.verificationId = verificationId
    config.productCode = productCode
    config.clientName = clientName
    config.userHashId = userHashId
    config.mobile = mobile
    config.mobileCountryCode = mobileCountryCode
    config.email = email
    config.accessToken = accessToken
    return config
  }
}
