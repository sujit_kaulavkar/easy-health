//
//  RNTKYC.m
//  AsianTop
//
//  Created by Sujit on 12/25/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "RNTKYC.h"
#import "AsianTop-Swift.h"

@implementation RNTKYC

RCT_EXPORT_MODULE();
//- (UIView *)view {
//  UIView *mainView = [[UIView alloc] init];
//  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
//  [view setBackgroundColor:[UIColor yellowColor]];
//  [mainView addSubview:view];
//  return mainView;
//}

- (UIView *)view {
  NSLog(@"view loaded");
  UIView *mainView = [[UIView alloc] init];
  KYCMainViewController *kycMainVC = [[KYCMainViewController alloc] init];
  UIView *navView = (UIView *)[kycMainVC showStartKYCVC];
  [mainView addSubview:navView];
  return mainView;
}

@end
