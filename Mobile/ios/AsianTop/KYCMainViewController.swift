//
//  KYCMainViewController.swift
//  AsianTop
//
//  Created by Sujit on 12/25/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import UIKit
import MMeKYCVIXSDK

class KYCMainViewController: UIViewController, MMeKYCDelegate {
 
  func getMMSDKInitConfig() -> MMeKycConfig {
    let config = MMeKycConfig()
    return config
  }

  func getUserModelForKYC() -> MMUserModel {
    let config = MMUserModel()
    config.verificationId = ""
    config.productCode = "sgasiatop"
    config.clientName = "Asia Top"
    config.userHashId = "userHashId"
    config.mobile = "userMobile"
    config.mobileCountryCode = "userCountryCode"
    config.email = "userEmail"
    config.accessToken = ""
    return config
  }

  @objc func showStartKYCVC() -> UIView? {
    let vc = MMStartKYCViewController(
      userModel:MMUserModel(),
      config: self.getMMSDKInitConfig())

    let resultCode = vc.preStartCheckForError()
    if resultCode == .prestartCheckPassed {
      vc.delegate = self
      let navigationController = UINavigationController.init(rootViewController: vc)
      return navigationController.view
    }
  
    return nil
  }

//  MARK: - MMeKYCDelegate

  func didCompleteKycProcessWithPayload(payload: [AnyHashable : Any]?, resultCode: MMeKYCStatusCode?, error: MMeKYCErrorCode?) {
    if resultCode == .kycApproved {
      print("KYC success")
    }
    else {
      print("KYC not success")
    }
    self.navigationController?.popViewController(animated: true)
  }
}
