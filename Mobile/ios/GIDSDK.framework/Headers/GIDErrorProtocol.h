//
//  GIDErrorProtocol.h
//  greenID Generic
//
//  Created by Stefan Bouwer on 11/7/16.
//  Copyright © 2016 VIX Verify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIDSDKConstantsAndEnums.h"

/**
 Describes error messages that can be passed from the GID SDK to a client app.  The GIDErrorProtocol object will contain an error code (defined by GIDErrorCode), as well as an optional underlying NSError object, and optional strings containing the failure reason, a suggested recovery action, and short description.
 
 The optional description and failure reasons may change without notice.  They are not intended for passing to an end user, they are more for logging and development purposes.
 
 When handling a GIDErrorProtocol, you are encouraged to check the gidErrorCode, and make sure your code handles all error types, and then acts accordingly for you end user.
 */
@protocol GIDErrorProtocol <NSObject>

/// An optional NSError object, which has caused this GID SDK error to occur
- (NSError *)underlyingError;

/// The GID SDK error code for this error object.  See GIDErrorCode for a list of possible error codes
- (GIDErrorCode)gidErrorCode;
/// An optional longer string describing the error condition
- (NSString *)gidErrorLocalizedFailureReason;
/// An optional suggestion for how to fix the error
- (NSString *)gidErrorRecoverySuggestionString;
/// An optional short string describing the error condition
- (NSString *)gidErrorShortenedDescription;

@end
