//
//  GIDMainViewController.h
//  greenID Generic
//
//  Created by Jawad Ahmed on 10/1/15.
//  Copyright © 2016 VIX Verify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "GIDErrorProtocol.h"
#import <iFaceRec/iFaceRec-umbrella.h>

@class GIDMainViewController;

typedef void (^GIDWebviewLoadCompletionHandler)(void);


/**
 The delegate protocol for receiving messages from the GID SDK.  This includes messages about the SDK finishing ('handoff')
 */
@protocol GIDDelegate <NSObject>

/**
 This delegate method is called when the GIDSDK is finished - either a verification has been completed successfully, or something went wrong.
 
 The delegate handler should check the payload, and the resultCode, to make sure they handle any errors that may have occurred during the verification

 @param mainViewController The GIDMainViewController object that has just finished
 @param payload A dictionary that will contain the verification status, and possibly a verification token
 @param resultCode The result state of the verification attempt - see GIDResultCode
 @param error If there was an error in the verification attempt, this will contain more details
 */
- (void)mainViewController:(nonnull GIDMainViewController *)mainViewController didCompleteProcessWithPayload:(nullable NSDictionary *)payload resultCode:(GIDResultCode)resultCode error:(nullable id <GIDErrorProtocol>)error;
@optional
- (void)mainViewController:(nonnull GIDMainViewController *)mainViewController didCompleteProcessWithPayload:(nullable NSDictionary *)payload resultCode:(GIDResultCode)resultCode DEPRECATED_MSG_ATTRIBUTE("Please use the mainViewController:didCompleteProcessWithPayload:resultCode:error: method instead");
@end


/**
 The main view controller for the GID SDK.  This view controller should be allocated and initialised by your app, and put onto a navigation controller stack, in order to start the SDK workflow.  
 */
@interface GIDMainViewController : UIViewController <WKNavigationDelegate, WKScriptMessageHandler, UIAlertViewDelegate, UINavigationControllerDelegate>


/**
 An object that conforms to the GIDDelegate interface.
 */
@property (nonatomic, weak, nullable) id <GIDDelegate> delegate;


/**
 Creates and sets up a GIDSDK view controller.  You should pass in a dictionary containing the startup parameters, as documented in the 'iOS Integration' wiki page.
 
 @param configDict Dictionary with GIDSDK startup parameters
 @return GIDMainViewController object that you can push to a navigation controller stack
 */
- (nullable instancetype)initWithConfig:(nullable NSDictionary *)configDict;

- (nullable instancetype)init DEPRECATED_MSG_ATTRIBUTE("Please use initWithConfig: instead.");


/**
 Set the logging level for log messages coming from the GID SDK.  Log messages will be send to a GIDLoggerDelegate object if it is set using GIDLogManager

 @param level The log level to use.  Defaults to 'Info'.
 */
- (void) setLoggingLevel:(GIDLogLevel)level;


/**
 Checks for startup errors, before pushing the GIDMainViewController to the navigation stack.  Use of this method is optional, but recommended for best user experience.

 @param error A GIDErrorProtocol object, which on return will contain error details if there was an error
 @return A GIDResultCode describing if the startup checks have passed, or if an error occurred.
 */
- (GIDResultCode) prestartCheckForError:(id <GIDErrorProtocol> *)error;
@end
