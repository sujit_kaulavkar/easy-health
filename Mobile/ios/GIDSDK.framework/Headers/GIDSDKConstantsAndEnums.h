//
//  GIDSDKConstantsAndEnums.h
//  greenID Generic
//
//  Created by Stefan Bouwer on 11/4/16.
//  Copyright © 2016 VIX Verify. All rights reserved.
//

// Selfie scan startup keys

#define kGIDSDKSelfieScanEyeThreshold @"selfieEyeThreshold" // Default is 20
#define kGIDSKDSelfieScanNumberOfBlinks @"selfieNumberOfBlinks" // Default is 2
#define kGIDSDKSelfieScanResetTimeout @"selfieResetTimeout" // Default is 10
#define kGIDSDKDocumentCaptureResetTimeout @"documentCaptureResetTimeout" // Default is 15
#define kGIDSDKDocumentCaptureLicenceResetTimeout @"documentCaptureLicenceResetTimeout" // Default is 15
#define kGIDSDKDocumentCapturePassportResetTimeout @"documentCapturePassportResetTimeout" // Default is 15
#define kGIDSDKDocumentCaptureGreenIDResetTimeout @"documentCaptureGreenIDResetTimeout" // Default is 15
#define kGIDSDKMobileConfigurationUrl @"https://config.vixverify.com/certs/"
#define kGIDSDKMobileConfigCertificatesArray @"mobileConfigCertificatesArray"
#define kGIDSDKMobileConfigCertificatesPrimaryDomain @"mobileconfig.vixverify.com"

// User Inactivity startup keys
#define kGIDSDKMaxIdleTimeSeconds @"inactivityTimeoutSeconds" // Default is 300


/**
 Describes error types that may occur in the GID SDK

 */
typedef NS_ENUM(NSInteger, GIDErrorCode) {
    /// An internal error occurred, usually on the backend server
    GIDErrorCodeInternal = -1001,
    /// The user's phone could not connect to the network when needed, or a backend server had a network problem
    GIDErrorCodeNetwork = -1002,
    /// There was an error when trying to upload the document images to the server, to have them OCR'd
    GIDErrorCodeOCR = -1003,
    /// Reserved for future use
    GIDErrorCodeAID = -1004,
    /// There was an error using the document capture tool
    GIDErrorCodeSnap = -1005,
    /// There was a server side error, and this caused the GID SDK to halt
    GIDErrorCodeWebApp = -1006,
    /// The user did not use the app for a long time, the SDK has cancelled as a result
    GIDErrorCodeUserInactivity = -1007,
    /// The resource bundle has not been included in your app
    GIDErrorCodeResourceBundle = -1008,
    /// There was an error when initialising the GID SDK
    GIDErrorCodeInitFailed = -1009,
    /// There was an error when trying to verify the SSL connection
    GIDErrorCodeNetworkSecurityError = -10010,
};


/**
 The result code for a verification attempt.  This should be checked when a delegate receives a handoff from GIDSDK.
 
 If the verification was successful, the code will be GIDResultCodeSuccess.
 
 However, a delegate app should be prepared to handle any of the other result codes, and act accordingly.
 
*/
typedef NS_ENUM(NSInteger, GIDResultCode) {
    ///There was a general error.
    GIDResultCodeError = -1,
    ///There was no network on the user's device, or a server network connection was down
    GIDResultCodeNoNetwork = -2,
    ///Reserved for future use
    GIDResultCodePlaceholder = -3,
    ///There was an issue with SSL certificates
    GIDResultCodeNetworkSecurityError = -4,
    /// The verification was successful
    GIDResultCodeSuccess = 0,
    ///The user pressed the 'cancel' button at some point in the verification
    GIDResultCodeCancelled = 1,
    ///The user pressed the 'back' button, which took them back to the start of the verification
    GIDResultCodeBack = 2,
    ///The user did not use the device for a long time, and the verification was cancelled as a result.  Default of 5 minutes
    GIDResultCodeUserInactivity = 3,
    ///The resource bundle was missing - please check that you have built the app correctly
    GIDResultCodeResourceBundle = 4
};

/**
 The log level that the SDK will use to send logging information to a delegate object that implements the GIDLoggerDelegate interface.

 The log levels are ordered by severity.  Asking for logs at a particular level will include log messages from more sever messages as well.
 
 Logging messages will be sent to the [sdkDidLogLevel: levelCode: analyticsCode: source: message:] delegate method.

 */
typedef NS_ENUM(NSInteger, GIDLogLevel) {
    /// No logging will be sent
    GIDLogLevelNone = 0,
    /// Only the most severe errors will be sent
    GIDLogLevelError = 1,
    /// Errors and warnings will be sent
    GIDLogLevelWarn = 2,
    /// Errors, warnings and Information log messages will be sent
    GIDLogLevelInfo = 4,
    /// Logging about starting and finishing workflow tasks will be included
    GIDLogLevelTasks = 8,
    /// Logging about network requests will be included
    GIDLogLevelRequests = 16,
    /// All debugging messages will be included
    GIDLogLevelDebug = 32,
    /// Logging about UI interactions from the user will be included
    GIDLogLevelUI = 64,
    /// Verbose logging will be included.  Verbose logging is stripped from release builds by default, so this log level will not be visible to clients (but this log level will include all other log levels)
    GIDLogLevelVerbose = 128
};

/**
 Codes used to describle analytics messages that the GID SDK can send to a a delegate object that implements the GIDLoggerDelegate interface.
 
 Analytic messages will be sent to the [sdkDidLogLevel: levelCode: analyticsCode: source: message:] delegate method.
 
 */
typedef NS_ENUM(NSInteger, GIDAnalyticsCode) {
    /// Code for messages that don't fit into any other type
    GIDAnalyticsCodeUnknown = 0,
    /// Code for the loading screen shows in the SDK
    GIDAnalyticsCodeLoadingScreen = 1,
    /// Code for when the GID SDK is showing the document capture tool
    GIDAnalyticsCodeDocumentCapture = 2,
    /// Code for when the GID SDK is showing a document review screen
    GIDAnalyticsCodeDocumentReview = 3,
    /// Code for when the GID SDK is showing the selfie capture tool
    GIDAnalyticsCodeSelfieCapture = 4,
    /// Code for when the GID SDK is showing the additional document capture tool
    GIDAnalyticsCodeAdditionalDocumentCapture = 5,
    /// Code for when the loading screen is removed in the SDK
    GIDAnalyticsCodeLoadingScreenRemove = 6,
    /// Code for when the app moves into the foreground
    GIDAnalyticsCodeForeground = 7,
    /// Code for when the app moves into the background
    GIDAnalyticsCodeBackground = 8,
    /// Code for when the app is terminated
    GIDAnalyticsCodeFinish = 9,
    /// Code for when the user touches the screen or a button
    GIDAnalyticsCodeUITouch = 10,
};
