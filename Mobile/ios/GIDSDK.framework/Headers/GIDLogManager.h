//
//  GIDLogManager.h
//  greenID Generic
//
//  Created by Stefan Bouwer on 4/24/17.
//  Copyright © 2017 VIX Verify. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 A protocol that the client application can use to receive log messages from the SDK.
 
 To use this, you should implement the required delegate methods, then set the delegate using:
 
 GIDLogManager *logManager = [GIDLogManager sharedData];
 logManager.logger = delegateObject;
 
 The level of logging verbosity or severity can be set using the [setLoggingLevel:] on the GIDMainViewController.
 
 */
@protocol GIDLoggerDelegate <NSObject>

@optional
- (void)sdkDidLog:(NSString *)loggedString;

/**
 Delegate method called when the GID SDK wants to pass logging messages to the host application.
 
 This method will be called every time the SDK logs.  As this can be quite often, your implementation of this delegate method should not take too long, or do computationally expensive or blocking network operations, otherwise it may affect user experience.

 @param levelString The level of the logging message, as a string
 @param level The level of the logging message, as a GIDLogLevel enum
 @param analyticsCode The analytics code for this message, if it is analytics data
 @param source The source - which will be GIDSDK usually.  This string may change in future
 @param message The log message, as a string
 */
- (void)sdkDidLogLevel:(NSString *)levelString levelCode:(GIDLogLevel)level analyticsCode:(GIDAnalyticsCode)analyticsCode source:(NSString *)source message:(NSString *)message;
@end



/**
 A manager object that configures and controls logging messages coming from the SDK, and optionally can send log messages to the client app, if it implements a GIDLoggerDelegate protocol.
 */
@interface GIDLogManager : NSObject

/**
 The shared GID log manager object
 
 Use this shared object to configure the log manager.

 @return The shared log manager instance
 */
+ (instancetype)sharedData;


/**
 The GIDLoggerDelegate object.  Set this property to tell the GID SDK where to send log messages
 */
@property (nonatomic, weak) id <GIDLoggerDelegate> logger;


/**
 The log level for logging messages coming from the GID SDK.  Can be set using the GIDMainViewController [setLoggingLevel:] method. 
 */
@property (nonatomic) int logLevel;
@end



